package Csv;

import Logger.LoggerInterface;
import au.com.bytecode.opencsv.CSVReader;
import au.com.bytecode.opencsv.CSVWriter;
import com.relevantcodes.extentreports.LogStatus;
//import org.apache.poi.ss.usermodel.*;

import java.io.*;
import java.util.ArrayList;
//import java.util.Arrays;
import java.util.List;

public class CsvHandler {
//    private FileOutputStream fileExcelOut = null;
//    private Workbook wb = null;
//    private Sheet sheet;
//    private String excelPath = "";
    private CSVReader reader;
    private CSVWriter writer;
    private char separator;
    private String csvPath;
    private LoggerInterface logger;

    public CsvHandler(){
        this.reader =  null;
        this.writer = null;
    }

    public CsvHandler(LoggerInterface logger){
        this();
        this.logger = logger;
        this.separator = ',';
    }

    public CsvHandler(LoggerInterface logger, char separator){
        this();
        this.logger = logger;
        this.separator = separator;
    }

    public String getCsvPath() {
        return csvPath;
    }

    public String getAbsoluteCsvPath() {
        File file = new File(csvPath);
        return file.getAbsolutePath();
    }

    public void setCsvPath(String csvPath) {
        this.csvPath = csvPath;
    }

    public CSVWriter getWriter() {
        return writer;
    }

    public void setWriter(CSVWriter writer) {
        this.writer = writer;
    }

    public CSVReader getReader() {
        return reader;
    }

    public void setReader(CSVReader reader) {
        this.reader = reader;
    }

    public List<String[]> readAll(){
        List<String[]> list = new ArrayList<String[]>();
        try {
            reader = new CSVReader(new FileReader(csvPath));
            list = reader.readAll();
        } catch (Exception e){
            logger.log(LogStatus.ERROR, "An error occurred during CSV reading");
        } finally {
            try {
                reader.close();
            } catch (IOException e) {
                logger.log(LogStatus.ERROR, "An error occurred during CSV closing file");
            }

        }
        return list;
    }

    public void writeNextRow(ArrayList<String> list){
        String[] array = new String[list.size()];
        array = list.toArray(array);
        writeNextRow(array);
    }

    public void writeNextRow(String[] array) {
        try{
            //No quote, end line encoding is "\r\n" or "0x0D 0x0A"
            writer = new CSVWriter(new FileWriter(csvPath,true),separator, CSVWriter.NO_QUOTE_CHARACTER, "\r\n");
            writer.writeNext(array);
        } catch (Exception e){
            logger.log(LogStatus.ERROR, "An error occurred during CSV writing");
        } finally {
            try {
                writer.close();
            } catch (IOException e) {
                logger.log(LogStatus.ERROR, "An error occurred during CSV closing file");
            }
        }
    }

//    public void createExcelResultFile(String path, String title){
//
//        try {
//            String testCaseDate = new SimpleDateFormat("dd.MM.yyyy-HH.mm.").format(new Date());
//            excelPath = path + "-" + testCaseDate + ".xls";
//            fileExcelOut = new FileOutputStream(excelPath);
//            wb = new HSSFWorkbook();
//            sheet = wb.createSheet(title + "-" + testCaseDate);
//            fileExcelOut.close();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    }
//
//    public void createExcelSheetColumn(ArrayList<String> list) throws Exception{
//        if(wb == null){
//            throw new Exception("Excel File is not created");
//        }
//        try {
//            fileExcelOut = new FileOutputStream(excelPath);
//            Row row = sheet.createRow(0);
//            int i = 0;
//            Cell cell;
//            for (String l : list) {
//                cell = row.createCell(i);
//                i++;
//                cell.setCellValue(l);
//            }
//            wb.write(fileExcelOut);
//            fileExcelOut.close();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    }
//
//    public void writeExcelRow(ArrayList<String> list) throws Exception{
//        if(wb == null){
//            throw new Exception("Excel File is not created");
//        }
//
//
//        try {
//            fileExcelOut = new FileOutputStream(excelPath);
//            int rows = sheet.getPhysicalNumberOfRows();
//            int i = 0;
//            Cell cell;
//            Row row = sheet.createRow((short) rows);
//
//            CellStyle redStyle = wb.createCellStyle();
//            redStyle.setFillForegroundColor(IndexedColors.RED.getIndex());
//            CellStyle greenStyle = wb.createCellStyle();
//            greenStyle.setFillForegroundColor(IndexedColors.GREEN.getIndex());
//            for (String l : list) {
//                cell = row.createCell(i);
//                if(l.equals("PASS")){
//                    cell.setCellStyle(greenStyle);
//                } else if (l.equals("FAIL")){
//                    cell.setCellStyle(redStyle);
//                }
//                i++;
//                cell.setCellValue(l);
//            }
//            wb.write(fileExcelOut);
//            fileExcelOut.close();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    }
}
