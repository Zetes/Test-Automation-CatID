package DataBaseFramework;

import Logger.LoggerInterface;
import com.relevantcodes.extentreports.LogStatus;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;

public class SqlMiddleWare {
    private static LoggerInterface logger;
    private static String user;
    private static String password;
    private static String urlServer;
    private static Connection con;
    private static String strError;
    private static boolean sqlCommit;
    private static String driver;

    public static void setSqlCommit(boolean sqlCommit) {
        SqlMiddleWare.sqlCommit = sqlCommit;
    }

    public static void setUser(String user) {
        SqlMiddleWare.user = user;
    }

    public static void setPassword(String password) {
        SqlMiddleWare.password = password;
    }

    public static void setUrlServer(String urlServer) {
        SqlMiddleWare.urlServer = urlServer;
    }

    public SqlMiddleWare(LoggerInterface loggerParam, String userParam, String passwordParam, String urlServerParam, String driverConnector){
        logger = loggerParam;
        user = userParam;
        password = passwordParam;
        urlServer = urlServerParam;
        sqlCommit = false;
        driver = driverConnector;
    }

    public void setLogger(LoggerInterface logger) {
        this.logger = logger;
    }

    @SuppressWarnings("static-access")
    public ArrayList<HashMap> executeSqlQuery(String sqlStr) throws SQLException {
        ArrayList<HashMap> listResult = new ArrayList<HashMap>();
        HashMap<String,String> resultMap;
        try{
            logger.log(LogStatus.INFO,"SQL establishing connection...");

            double startTime = System.currentTimeMillis();
            boolean isBlob=false;
            // launch driver
            Class.forName(driver);

            //Set connection details
            final String SqlUser = user;
            final String SqlPassword = password;
            final String SqlServer = urlServer;
            //we try to connect on using a new thread to avoid blocking of the GUI
            ExecutorService service = Executors.newFixedThreadPool(4);
            strError = "";
            service.submit(new Runnable(){
                public void run(){
                    try{
                        con = DriverManager.getConnection(SqlServer,SqlUser,SqlPassword);
                    }
                    catch(Exception e){
                        strError = e.getMessage();
                        logger.log(LogStatus.ERROR, strError);
                    }
                }
            });
            //Try to connect only if the previous step is successful
            if (strError.equals("")) {
                //during connection process, we check if it is connected until max timeout then process statement case connection is OK
                int t = 0;
                final Object lock = new Object();
                synchronized (lock) {
                    while (con == null && t < 60) {
                        logger.log(LogStatus.INFO, "SQL Connecting");
                        lock.wait(500);
                        t = t + 1;
                    }
                    logger.log(LogStatus.INFO, "SQL Connected");
                }

                //check if connection exist or not
                if (con != null) {
                    logger.log(LogStatus.INFO, "SQL connection done to " + SqlServer + " as " + SqlUser);
                    logger.log(LogStatus.INFO, "SQL connection done to " + urlServer + " as " + user);
                    //set by default autoCommit to false
                    con.setAutoCommit(false);

                    // build and execute statement
                    Statement stmt = con.createStatement();
                    logger.log(LogStatus.INFO, "Statement : " + sqlStr);
                    logger.log(LogStatus.INFO, "Starting SQL statement ... ");

                    ResultSet rs = stmt.executeQuery(sqlStr);
                    logger.log(LogStatus.INFO, "SQL statement done");
                    //check statement is an update meaning a commit must be executed
                    if (sqlStr.substring(0, 6).toLowerCase().equals("update")) {
                        if (sqlCommit) {
                            con.commit();
                            logger.log(LogStatus.INFO, "UPDATE commit done.");
                        } else {
                            con.rollback();
                            logger.log(LogStatus.INFO, "UPDATE rollback.");
                        }
                    } else if (sqlStr.substring(0, 6).toLowerCase().equals("insert")) {
                        if (sqlCommit) {
                            con.commit();
                            logger.log(LogStatus.INFO, "INSERT commit done.");
                        } else {
                            con.rollback();
                            logger.log(LogStatus.INFO, "INSERT rollback");
                        }
                    } else if (sqlStr.substring(0, 6).toLowerCase().equals("delete")) {
                        if (sqlCommit) {
                            con.commit();
                            logger.log(LogStatus.INFO, "DELETE commit done");
                        } else {
                            con.rollback();
                            logger.log(LogStatus.INFO, "DELETE rollback");
                        }
                    } else if (sqlStr.substring(0, 6).toLowerCase().equals("select")) {
                        logger.log(LogStatus.INFO, "Preparing SELECT results");
                        ResultSetMetaData metadata = rs.getMetaData();
                        int columnCount = metadata.getColumnCount();
                        while (rs.next()) {
                            resultMap = new HashMap<String, String>();
                            for (int i = 1; i <= columnCount; i++) {
                                logger.log(LogStatus.INFO, metadata.getColumnName(i) + ": " + rs.getString(i));
                                resultMap.put(metadata.getColumnName(i), rs.getString(i));
                            }
                            listResult.add(resultMap);
                            logger.log(LogStatus.INFO, "");
                        }
                    }
                    con.close();
                    double endTime = System.currentTimeMillis();
                    //always add statement to the history available through SQL combobox
                    logger.log(LogStatus.INFO, "SQL execution time is  : " + (endTime - startTime) + " ms");
                } else {
                    logger.log(LogStatus.ERROR, strError + " - please verify data connection.");
                    con.close();
                    con = null;
                    strError = "";
                }
                //Connection must be set to NULL in order to allow multiple query (redo method)
                con = null;

            }

        }
        catch(Exception e){
            logger.log(LogStatus.ERROR,"ERROR executeSqlQuery : " + e.getMessage());
        }finally{
            if(con != null){
                con.close();
                con = null;
            }

        }
        return listResult;
    }

//    @SuppressWarnings("static-access")
//    public static void executeSqlQuery() throws SQLException{
//        try{
//            appendTextArea("SQL establishing connection...");
//            double startTime = System.currentTimeMillis();
//            boolean isBlob=false;
//            boolean isClob=false;
//            // launch driver
//            Class.forName("oracle.jdbc.driver.OracleDriver");
//
//            //Set connection details
//            final String SqlUser = txtUSER.getText();
//            final String SqlPassword = txtPASS.getText();
//            final String SqlServer = txtURI.getText();
//
//            //we try to connect on using a new thread to avoid blocking of the GUI
//            ExecutorService service = Executors.newFixedThreadPool(4);
//            service.submit(new Runnable(){
//                public void run(){
//                    try{
//                        con=DriverManager.getConnection(SqlServer,SqlUser,SqlPassword);
//                    }
//                    catch(Exception e){
//                        strError = e.getMessage();
//                        e.printStackTrace();
//                    }
//                }
//            });
//
//            //during connection process, we check if it is connected until max timeout then process statement case connection is OK
//            int t=0;
//            final Object lock = new Object();
//            synchronized (lock) {
//                while (con == null && t<60) {
//                    Document doc = textLog.getDocument();
//                    doc.insertString(doc.getLength(), "...", null);
//                    textLog.update(textLog.getGraphics());
//                    textLog.repaint();
//                    lock.wait(500);
//                    t=t+1;
//                }
//
//                Document doc = textLog.getDocument();
//                doc.insertString(doc.getLength(),"\n", null);
//            }
//
//            //check if connection exist or not
//            if(con!=null){
//                appendTextArea("SQL connection done...\n");
//
//                //set by default autoCommit to false
//                con.setAutoCommit(false);
//
//                // build and execute statement
//                stmt=con.createStatement();
//                appendTextArea("Statement : " + sqlStr + "\n");
//                appendTextArea("Starting SQL statement ... \n");
//
//                //case of BLOB
//                if(sqlStr.startsWith("BLOB")){
//                    sqlStr=sqlStr.substring(4);
//                    isBlob=true;
//                    isClob=false;
//                }
//
//                //case of Clob
//                if(sqlStr.startsWith("CLOB")){
//                    sqlStr=sqlStr.substring(4);
//                    isClob=true;
//                    isBlob=false;
//                }
//
//                rs=stmt.executeQuery(sqlStr);
//
//                //check statement is an update meaning a commit must be executed
//                if (sqlStr.substring(0,6).toString().toLowerCase().equals("update")){
//                    if (sqlCommit==true) {
//                        con.commit();
//                        appendTextArea("UPDATE commit done.\n");
//                    }
//                    else{
//                        con.rollback();
//                        appendTextArea("UPDATE rollback.\n");
//                    }
//                }
//                else{
//                    if (sqlStr.substring(0,6).toString().toLowerCase().equals("insert")){
//                        if (sqlCommit==true) {
//                            con.commit();
//                            appendTextArea("INSERT commit done.\n");
//                        }
//                        else{
//                            con.rollback();
//                            appendTextArea("INSERT rollback\n");
//                        }
//                    }
//                    else{
//                        if (sqlStr.substring(0,6).toString().toLowerCase().equals("delete")){
//                            if (sqlCommit==true) {
//                                con.commit();
//                                appendTextArea("DELETE commit done\n");
//                            }
//                            else{
//                                con.rollback();
//                                appendTextArea("DELETE rollback\n");
//                            }
//                        }
//                        else{
//                            if (sqlStr.substring(0,6).toString().toLowerCase().equals("select")){
//                                appendTextArea("Building outputs to table ... \n");
//                                rsmd = rs.getMetaData();
//
//                                //try for reading BLOB image data
//                                if(isBlob==true || isClob==true){
//                                    if (!rs.isBeforeFirst()){
//                                        appendTextArea("BLOB-CLOB data is empty\n");
//                                    }
//                                    else
//                                    if(rs.next()){
//                                        //BLOBselect "card"."fieldData" from CARDDB."card" where "card"."cardId"=1984212
//                                        //InputStream is = rs.getBinaryStream(1);
//                                        System.out.println("passé ici");
//                                        if(isBlob==true){
//                                            Blob blob = rs.getBlob(1);
//                                            long blobLen = blob.length();
//                                            int pos = 1;   // position is 1-based
//                                            int len = 10;
//                                            byte[] bytes = blob.getBytes(pos,(int)blobLen );
//                                            String str = new String(bytes,"UTF-8");
//                                            System.out.println(str);
//                                        }
//                                        if(isClob==true){
//                                            Clob clob = rs.getClob(1);
//                                            char clobVal[] = new char[(int) clob.length()];
//                                            Reader r = clob.getCharacterStream();
//                                            r.read(clobVal);
//                                            StringWriter str = new StringWriter();
//                                            str.write(clobVal);
//                                            System.out.println("result : " + str);
//                                        }
//                                        //displayJp2k(IOUtils.toByteArray(is));
//                                    }
//                                }
//                                else{
//                                    //Display results in query result frame
//                                    table = new JTable(resultSetToTableModel(rs));
//                                    table.setFont(new Font("Calibri", Font.PLAIN, 12));
//                                    resizeColumnWidth(table);
//                                    table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
//                                    frmResult.setVisible(true);
//                                    frmResult.scrollPaneResult.setViewportView(table);
//                                    frmResult.setTitle("SQL : " + sqlStr);
//                                    frmResult.lblQuery.setText("SQL : " + sqlStr);
//                                    frmResult.lblNbrRows.setText("Number of Rows retrieved : " + sqlNbrRows);
//                                    frmResult.requestFocusInWindow();
//
//                                }
//                            }
//                        }
//                    }
//                }
//                con.close();
//                double endTime = System.currentTimeMillis();
//                //check if SQL must be saved or not
//                if(sqlToLog==true){
//                    saveSqlToLog();
//                }
//                //always add statement to the history available through SQL combobox
//                cmbSql.addItem(textData.getText());
//                appendTextArea("SQL execution time is  : "+ (endTime-startTime) + " ms\n");
//                frmResult.lbExecTime.setText("Execution time : "+ (endTime-startTime)/1000 + " seconds");
//            }
//            else{
//                appendTextArea(strError + " - please verify data connection.\n");
//                con.close();
//                con=null;
//                strError ="";
//            }
//            //Connection must be set to NULL in order to allow multiple query (redo method)
//            con=null;
//        }
//        catch(Exception e){
//            appendTextArea("Error executeSqlQuery : " + e.getMessage() + "\n");
//            e.printStackTrace();
//        }finally{
//            if(con!=null){
//                con.close();
//                con=null;
//            }
//        }
//    }

}






