package DataBaseFramework;

import Logger.LoggerInterface;

public class SqlMySqlMiddleware extends SqlMiddleWare{

    public SqlMySqlMiddleware(LoggerInterface loggerParam, String userParam, String passwordParam, String urlServerParam){
        super(loggerParam, userParam, passwordParam, urlServerParam, "com.mysql.jdbc.Driver");
    }
}
