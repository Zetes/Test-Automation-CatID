package Driver.Appium;

import Driver.Driver;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import org.apache.commons.lang3.SystemUtils;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.Assert;

import javax.swing.*;
import java.net.URL;

/**
 * Created by rstevens on 10/11/2016.
 */
public class AndroidPhoneDriver extends Driver{
    protected static String basepath = System.getProperty("user.dir");

    public AndroidPhoneDriver(){
        DesiredCapabilities capabilities = new DesiredCapabilities();

        capabilities.setCapability("deviceName","");
        capabilities.setCapability("app", basepath + "\\com.amazon.windowshop_5.51.6810.apk");
        capabilities.setCapability("noReset", "false");
        capabilities.setCapability("newCommandTimeout","60");

        try{
            driver = new AndroidDriver(new URL("http://localhost:4735/wd/hub"), capabilities);
        }catch (Exception e)
        {
            e.printStackTrace();
        }



    }
}
