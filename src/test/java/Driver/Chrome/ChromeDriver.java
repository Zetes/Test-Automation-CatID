package Driver.Chrome;

import Driver.Driver;
import org.apache.commons.lang3.SystemUtils;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.Assert;

import java.net.URL;


/**
 * Created by rstevens on 26/09/2016.
 */
public class ChromeDriver extends Driver {
    public ChromeDriver(DesiredCapabilities cap)throws Exception{
        this(cap, null);
    }

    public ChromeDriver(DesiredCapabilities cap, String ip)throws Exception{
        if (SystemUtils.IS_OS_WINDOWS) {
            System.setProperty("webdriver.chrome.driver", this.basePath + "/src/test/java/Driver/Chrome/chromedriver-win.exe");
        }
        else if (SystemUtils.IS_OS_LINUX) {
            System.setProperty("webdriver.chrome.driver", this.basePath + "/src/test/java/Driver/Chrome/chromedriver-linux");
        }
        else if (SystemUtils.IS_OS_MAC) {
            System.setProperty("webdriver.chrome.driver", this.basePath + "/src/test/java/Driver/Chrome/chromedriver-macos");
        }
        else {
            Assert.assertTrue(false, "This platform is not supported for the moment.");
        }
        if(ip != null){
            driver = new RemoteWebDriver( new URL("http://" + ip + ":4444/wd/hub"), cap);
        } else {
            driver = new org.openqa.selenium.chrome.ChromeDriver(cap);
        }

        driver.manage().window().maximize();
    }
    public ChromeDriver(){
        if (SystemUtils.IS_OS_WINDOWS) {
            System.setProperty("webdriver.chrome.driver", this.basePath + "/src/test/java/Driver/Chrome/chromedriver-win.exe");
        }
        else if (SystemUtils.IS_OS_LINUX) {
            System.setProperty("webdriver.chrome.driver", this.basePath + "/src/test/java/Driver/Chrome/chromedriver-linux");
        }
        else if (SystemUtils.IS_OS_MAC) {
            System.setProperty("webdriver.chrome.driver", this.basePath + "/src/test/java/Driver/Chrome/chromedriver-macos");
        }
        else {
            Assert.assertTrue(false, "This platform is not supported for the moment.");
        }
        driver = new org.openqa.selenium.chrome.ChromeDriver();
        driver.manage().window().maximize();
    }
}
