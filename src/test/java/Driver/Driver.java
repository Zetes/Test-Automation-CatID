package Driver;

import org.openqa.selenium.WebDriver;

/**
 * Created by rstevens on 26/09/2016.
 */
public abstract class Driver implements DriverInterface {
    protected static String basePath = System.getProperty("user.dir");
    protected WebDriver driver;
    public WebDriver getDriver(){ return driver;}

    public void CloseDriver(){
        driver.close();
        driver.quit();
    }


}
