package Driver;

import org.openqa.selenium.WebDriver;

/**
 * Created by rstevens on 26/09/2016.
 */
public interface DriverInterface {
    WebDriver getDriver();
    void CloseDriver();
}
