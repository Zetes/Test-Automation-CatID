package Driver.Edge;

import Driver.Driver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.URL;


public class EdgeDriver extends Driver {
    public EdgeDriver(DesiredCapabilities cap)throws Exception{
        this(cap, null);
    }

    public EdgeDriver(DesiredCapabilities cap, String ip)throws Exception{
        System.setProperty("webdriver.edge.driver", this.basePath + "/src/test/java/Driver/Edge/MicrosoftWebDriver.exe");
        if(ip != null){
            driver = new RemoteWebDriver( new URL("http://" + ip + ":4444/wd/hub"), cap);
        } else {
            driver = new org.openqa.selenium.edge.EdgeDriver(cap);
        }

        driver.manage().window().maximize();
    }

    public EdgeDriver(){
        System.setProperty("webdriver.edge.driver", this.basePath + "/src/test/java/Driver/Edge/MicrosoftWebDriver.exe");
        driver = new org.openqa.selenium.edge.EdgeDriver();
        driver.manage().window().maximize();
    }
}
