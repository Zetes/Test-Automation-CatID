package Driver.Firefox;

import Driver.Driver;
import org.apache.commons.lang3.SystemUtils;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.Assert;

import java.net.URL;


/**
 * Created by rstevens on 26/09/2016.
 */
public class FirefoxDriver extends Driver {

    public FirefoxDriver(DesiredCapabilities cap) throws Exception{
        this(cap, null);
    }

    public FirefoxDriver(DesiredCapabilities cap, String ip) throws Exception{
        if (SystemUtils.IS_OS_WINDOWS) {
            System.setProperty("webdriver.gecko.driver", this.basePath + "/src/test/java/Driver/Firefox/geckodriver-win64.exe");
        }
        else if (SystemUtils.IS_OS_LINUX) {
            System.setProperty("webdriver.gecko.driver", this.basePath + "/src/test/java/Driver/Firefox/geckodriver-linux");
        }
        else if (SystemUtils.IS_OS_MAC) {
            System.setProperty("webdriver.gecko.driver", this.basePath + "/src/test/java/Driver/Firefox/geckodriver-macos");
        }
        else {
            Assert.assertTrue(false, "This platform is not supported for the moment.");
        }



        if(ip != null){
            driver = new RemoteWebDriver( new URL("http://" + ip + ":4444/wd/hub"), cap);
        } else {
            driver = new org.openqa.selenium.firefox.FirefoxDriver(cap);
        }
        driver.manage().window().maximize();
    }

    public FirefoxDriver(){
        if (SystemUtils.IS_OS_WINDOWS) {
            System.setProperty("webdriver.gecko.driver", this.basePath + "/src/test/java/Driver/Firefox/geckodriver-win64.exe");
        }
        else if (SystemUtils.IS_OS_LINUX) {
            System.setProperty("webdriver.gecko.driver", this.basePath + "/src/test/java/Driver/Firefox/geckodriver-linux");
        }
        else if (SystemUtils.IS_OS_MAC) {
            System.setProperty("webdriver.gecko.driver", this.basePath + "/src/test/java/Driver/Firefox/geckodriver-macos");
        }
        else {
            Assert.assertTrue(false, "This platform is not supported for the moment.");
        }
        driver = new org.openqa.selenium.firefox.FirefoxDriver();
        driver.manage().window().maximize();

    }
}
