package Driver.InternetExplorer;

import Driver.Driver;
import org.apache.commons.lang3.SystemUtils;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.URL;

public class InternetExplorerDriver extends Driver {
    public InternetExplorerDriver() throws Exception{
        this(null);

    }

    public InternetExplorerDriver(String ip) throws Exception{
        if (SystemUtils.IS_OS_WINDOWS) {
            System.setProperty("webdriver.ie.driver", this.basePath + "/src/test/java/Driver/InternetExplorer/IEDriverServer.exe");
        }

        if(ip != null){
            driver = new RemoteWebDriver( new URL("http://" + ip + ":4444/wd/hub"), DesiredCapabilities.internetExplorer());
        } else {
            driver = new org.openqa.selenium.ie.InternetExplorerDriver();
        }
        driver.manage().window().maximize();
    }
}
