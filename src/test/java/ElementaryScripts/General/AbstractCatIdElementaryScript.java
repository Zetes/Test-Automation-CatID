package ElementaryScripts.General;

import Screens.*;
import com.relevantcodes.extentreports.LogStatus;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.io.*;
import java.text.DateFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.*;


public abstract class AbstractCatIdElementaryScript extends AbstractElementaryScript {


    public String registrator = "";
    public String firstResponsible = "";
    public String newResponsible = "";
    public String tcFeatureListNumber = "";
    public String catIdentifier = "";

    public String getTcFeatureListNumber(){
        return tcFeatureListNumber;
    }

    public String getCatIdentifier(){
        return catIdentifier;
    }

    public void setCatIdentifier(String identifier){
        catIdentifier = identifier;
    }

    public void setTcFeatureListNumber(String role){
        tcFeatureListNumber = role;
    }

    public String getFirstRegistrator(){
        return registrator;
    }

    public void setFirstRegistrator(String role){
        registrator = role;
    }

    public String getFirstResponsible(){
        return firstResponsible;
    }

    public void setFirstResponsible(String role){
        firstResponsible = role;
    }

    public String getNewResponsible(){
        return newResponsible;
    }

    public void setNewResponsible(String role){
        newResponsible = role;
    }

    public void eazyId(WebEazyIdPage eazyIdPage){
        eazyId(eazyIdPage, false);
    }

    public void eazyId(WebEazyIdPage eazyIdPage, boolean checkValidPin){
        //eazYId sign
        WaitForElement(eazyIdPage.consentCheckbox,30);
        logger.log(LogStatus.INFO, "Check the consent");
        click(eazyIdPage.consentCheckbox);

        logger.log(LogStatus.INFO, "Click on OK");
        click(eazyIdPage.agreeButton);

        if(checkValidPin){
            logger.log(LogStatus.INFO, "Fill in invalid pin code");
            setText(eazyIdPage.signCode,"5678");

            logger.log(LogStatus.INFO, "Click on OK to sign");
            click(eazyIdPage.signOK);
            try{
                WaitForElement(eazyIdPage.errorMessageMessage,5);
                if(eazyIdPage.errorMessageMessage.getAttribute("textContent").equals(" Wrong PIN code entered [2 attempts remaining]")){
                    logger.log(LogStatus.INFO, "Invalid pin detected with correct retry indication");
                } else{
                    logger.log(LogStatus.ERROR, "Invalid pin detected without correct retry indication");
                }
            } catch (Exception e){
                logger.log(LogStatus.ERROR, "Invalid pin code not detected");
            }
            logger.log(LogStatus.INFO, "Fill in pin code");
            setText(eazyIdPage.signCode,"1234");

            logger.log(LogStatus.INFO, "Click on OK to sign");
            click(eazyIdPage.signOK);

        } else{
            logger.log(LogStatus.INFO, "Fill in pin code");
            setText(eazyIdPage.signCode,"1234");

            logger.log(LogStatus.INFO, "Click on OK to sign");
            click(eazyIdPage.signOK);
        }

    }

    public void eazyIdWithoutPin(WebEazyIdPage eazyIdPage){
        //eazYId sign
        WaitForElement(eazyIdPage.consentCheckbox,2);
        logger.log(LogStatus.INFO, "Check the consent");
        click(eazyIdPage.consentCheckbox);

        logger.log(LogStatus.INFO, "Click on OK");
        click(eazyIdPage.agreeButton);
    }

    public void eazySign(WebEazyIdPage eazyIdPage){

        WaitForElement(eazyIdPage.signaturesButton,60);
        logger.log(LogStatus.INFO, "Check the signature");
        click(eazyIdPage.signaturesButton);
        WaitForElement(eazyIdPage.signPencilVetButton,60);
        click(eazyIdPage.signPencilVetButton);
        logger.log(LogStatus.INFO, "Validate the signature");
        click(eazyIdPage.validateSignVetButton);
        setText(eazyIdPage.pinVetField,"1234");
        click(eazyIdPage.pinOkVetField);
        WaitForElement(eazyIdPage.pinDoneVetField,60);
        logger.log(LogStatus.INFO, "Finalize eazy sign");
        click(eazyIdPage.pinDoneVetField);
        WaitForElement(eazyIdPage.exitButton,60);
        click(eazyIdPage.exitButton);

    }

    public boolean isUiCalendarDateAvailable(WebElement weCalendarBody, String day){
        boolean isAvailable = false;

        List<WebElement> listTr = weCalendarBody.findElements(By.tagName("tr"));

        for(WebElement weTr : listTr){
            List<WebElement> listTd = weTr.findElements(By.tagName("td"));
            for(WebElement weTd : listTd){
//                System.out.println(weTd.getAttribute("textContent") + " " + day);
//                System.out.println(weTd.getAttribute("class"));
                if((weTd.getAttribute("class").equals("link")  ||  weTd.getAttribute("class").equals("link active today") ||  weTd.getAttribute("class").equals("link active today focus"))
                        && weTd.getAttribute("textContent").equals(day)){
                    isAvailable = true;
                }
            }
        }
        return isAvailable;
    }

    public boolean isUiCalendarDateClickableAndFieldFilled(WebElement weCalendar, WebElement weField, String day, String date ){
        boolean isOk = false;

        List<WebElement> listTd = weCalendar.findElements(By.tagName("td"));

        for(WebElement weTd : listTd){
//            System.out.println(weTd.getAttribute("textContent") + " " + day);
//            System.out.println(weTd.getAttribute("class"));

            if((weTd.getAttribute("class").equals("link") || weTd.getAttribute("class").equals("link active today") || weTd.getAttribute("class").equals("link active today focus") || weTd.getAttribute("class").equals("link focus"))
                    && weTd.getAttribute("textContent").equals(day)){
//                System.out.println(weTd.getAttribute("textContent"));
                click(weTd);
                date = date.replace(".","/");
//                System.out.println(date);
//                System.out.println(weField.getAttribute("value"));
                if(weField.getAttribute("value").equals(date)){
                    isOk = true;
                }
            }
        }

        return isOk;
    }

    public boolean canNavidateToPastAndFutureOnUiCalendar(WebElement weCalendar){
        boolean canNavigateToPast = false;
        boolean canNavigateToFuture = false;
        String currentMonthTimeStamp = new SimpleDateFormat("MM").format(new Date());
        String currentMonthName = getMonthForInt(Integer.parseInt(currentMonthTimeStamp));
        String previousMonthTimeStamp = new SimpleDateFormat("MM").format(new Date());
        String previousMonthName = getMonthForInt(Integer.parseInt(previousMonthTimeStamp));

        WebElement previousMonthButton = assignCalendarPreviousHeadUiButton(weCalendar);
        WebElement nextMonthButton = assignCalendarNextHeadUiButton(weCalendar);
        WebElement currentMonthLabel = assignCalendarCurrentHeadUiButton(weCalendar);

        if(previousMonthButton == null || nextMonthButton == null || currentMonthLabel == null){
            logger.log(LogStatus.ERROR, "Unable to find calendar UI head button");
        } else {
            logger.log(LogStatus.INFO, "Calendar UI head button are found");
            //Go to the past
            click(previousMonthButton);
            currentMonthLabel = assignCalendarCurrentHeadUiButton(weCalendar);
            if(previousMonthName.equals(currentMonthLabel.getAttribute("contentText"))){
                canNavigateToPast = true;
            }
        }

        previousMonthButton = assignCalendarPreviousHeadUiButton(weCalendar);
        nextMonthButton = assignCalendarNextHeadUiButton(weCalendar);
        currentMonthLabel = assignCalendarCurrentHeadUiButton(weCalendar);
        if(previousMonthButton == null || nextMonthButton == null || currentMonthLabel == null){
            logger.log(LogStatus.ERROR, "Unable to find calendar UI head button");
        } else {
            logger.log(LogStatus.INFO, "Calendar UI head button are found");
            //Go to the future
            click(nextMonthButton);
            currentMonthLabel = assignCalendarCurrentHeadUiButton(weCalendar);
            if(currentMonthName.equals(currentMonthLabel.getAttribute("contentText"))){
                canNavigateToFuture = true;
            }
        }


        return canNavigateToFuture && canNavigateToPast;
    }

    private WebElement assignCalendarUiButton(WebElement weCalendar, String type){
        List<WebElement> listWe = weCalendar.findElements(By.tagName("span"));
        WebElement button = null;
        for(WebElement we : listWe){ //There are 3 elements only
            if(type.equals("current") && we.getAttribute("class").equals("link")){
                button = we;
            }
            if(type.equals("previous") && we.getAttribute("class").contains("prev link")){
                button = we;
            }
            if(type.equals("next") && we.getAttribute("class").contains("next link")){
                button = we;
            }
        }
        return button;
    }

    private WebElement assignCalendarPreviousHeadUiButton(WebElement weCalendar){
        return assignCalendarUiButton(weCalendar,"previous");
    }

    private WebElement assignCalendarCurrentHeadUiButton(WebElement weCalendar){
        return assignCalendarUiButton(weCalendar,"current");
    }

    private WebElement assignCalendarNextHeadUiButton(WebElement weCalendar){
        return assignCalendarUiButton(weCalendar,"next");
    }

    public void checkDateUiCalendar( WebElement dateField, WebElement dateCalendarBody, WebElement dateCalendarHead, String titleField){

        Date tomorrowDate = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(tomorrowDate);
        calendar.add(Calendar.DATE,1);
        tomorrowDate = calendar.getTime();
        String tomorrowDateTimeStamp = new SimpleDateFormat("dd.MM.yyyy").format(tomorrowDate);
        String currentDateTimeStamp = new SimpleDateFormat("dd.MM.yyyy").format(new Date());
        //Set the date to tomorrow to handler transition between 2 months
        setText(dateField,tomorrowDateTimeStamp);
        //User can edit date
        //User can delete encoded value
        if(isEditable(dateField)){
            logger.log(LogStatus.INFO, titleField +" can be edit");
            setText(dateField,tomorrowDateTimeStamp); //In order to reset properly the date written
        } else {
            logger.log(LogStatus.ERROR, "ERROR: " + titleField + " cannot be edit");
        }

        //by clicking on the birthdate field
        //a calendar popup is displayed
        click(dateField);
//        System.out.println(dateField.getAttribute("value"));
        try {
            WaitForElement(dateCalendarBody,5);
            logger.log(LogStatus.INFO, titleField + " calendar is visible");
        } catch (Exception e){
            logger.log(LogStatus.ERROR, "ERROR: " + titleField + " calendar is not visible");
        }
        //user can navigate trough calendar in the past
        //user can navigate trough calendar in the future
        canNavidateToPastAndFutureOnUiCalendar(dateCalendarHead);

        //User can select up to current date
        String currentDayTimeStamp = new SimpleDateFormat("dd").format(new Date());
        if(currentDayTimeStamp.startsWith("0")){
            currentDayTimeStamp = currentDayTimeStamp.replace("0","");
        }
        String tomorrowDayTimeStamp = new SimpleDateFormat("dd").format(tomorrowDate);
        if(tomorrowDayTimeStamp.startsWith("0")){
            tomorrowDayTimeStamp = tomorrowDayTimeStamp.replace("0","");
        }
        if(isUiCalendarDateAvailable(dateCalendarBody, currentDayTimeStamp)
                && !isUiCalendarDateAvailable(dateCalendarBody, tomorrowDayTimeStamp)){
            logger.log(LogStatus.INFO, titleField + " can be selected up to current date");
        } else {
            logger.log(LogStatus.ERROR, "ERROR: " + titleField + " cannot be selected up to current date");
        }

        //by clicking in the calendar, the date is filled
        if(isUiCalendarDateClickableAndFieldFilled(dateCalendarBody, dateField, currentDayTimeStamp, currentDateTimeStamp)){
            logger.log(LogStatus.INFO, "By clicking in the calendar, the " + titleField + " is filled");
        } else {
            logger.log(LogStatus.ERROR, "ERROR: By clicking in the calendar, the " + titleField + " is not filled");
        }
    }

    public void clearCatInformationField(WebRegisterPage registerPage){
        scrollOnElement(registerPage.birthDate);
        setText(registerPage.birthDate,"");
        setText(registerPage.identificationDate,"");
        setText(registerPage.sterilizationDate,"");
        setText(registerPage.colorCatTextField,"");
        setText(registerPage.nameCatTextField,"");
        setText(registerPage.breedCatTextField,"");
    }

    String getMonthForInt(int num) {
        String month = "wrong";
        DateFormatSymbols dfs = new DateFormatSymbols();
        String[] months = dfs.getMonths();
        if (num >= 0 && num <= 11 ) {
            month = months[num];
        }
        return month;
    }

    public String getLastIdentifier() throws Exception{
        FileInputStream in = new FileInputStream(PATH_TO_IDENTIFIERS);
        BufferedReader br = new BufferedReader(new InputStreamReader(in));

        String line = null, tmp;
        while ((tmp = br.readLine()) != null){
            line = tmp;
        }
        br.close();
        in.close();
        return line;
    }

    public String getNextIdentifier() throws Exception{
        int lastIdentifierInteger = Integer.parseInt(this.getLastIdentifier());
        int firstNewIdentifier = lastIdentifierInteger + 1;
        String newFirstIdentifierString = "123456789" + String.valueOf(firstNewIdentifier);
        addNewIdentifier(String.valueOf(firstNewIdentifier));
        return newFirstIdentifierString;
    }

    public void addNewIdentifier(String id) throws Exception{
        Writer output = null;
        try {
            if (countFileLines(PATH_TO_IDENTIFIERS) > 100){
                output = new BufferedWriter(new FileWriter(PATH_TO_IDENTIFIERS, false));
            } else {
                output = new BufferedWriter(new FileWriter(PATH_TO_IDENTIFIERS, true));
            }
        } catch (Exception e){
            logger.log(LogStatus.ERROR, "Error while counting number of lines");
        }
        if( output != null){
            output.append("\n");
            output.append(id);
            output.close();
        } else {
            logger.log(LogStatus.ERROR,"IndentifierList file cannot be handle");
            throw new Exception("IndentifierList file cannot be handle");
        }

    }

    protected int countFileLines(String path)  throws Exception{

        int numberOfLines = 0;

        FileInputStream fis = new FileInputStream(path);
        LineNumberReader l = new LineNumberReader(
                new BufferedReader(new InputStreamReader(fis)));
        while (l.readLine()!= null){
            numberOfLines = l.getLineNumber();
        }

        return numberOfLines;
    }

    public void forceFillNewCatIdentification(WebElement identifierField){
        scrollOnElement(identifierField);
        //Read the last identifier from text file
        int identifierInteger = 0;
        try {
            identifierInteger = Integer.parseInt(this.getLastIdentifier());
            //Try to write the last identifier, if it is red increment it
            String identifierString = "123456789" + String.valueOf(identifierInteger+1);
            setText(identifierField,identifierString);
            //Try to limit the non-needed incrementation, otherwise too many ID is used for free
            while(isRed(getRgbString(identifierField))){
                //Compute the new first identifier
                identifierInteger++;
                identifierString = "123456789" + String.valueOf(identifierInteger);
                setText(identifierField,identifierString);
                addNewIdentifier(String.valueOf(identifierInteger));
            }

        } catch (Exception e) {
            logger.log(LogStatus.ERROR, e.getLocalizedMessage());
        }
    }

    public void fillTwoCatIdentifiers(WebRegisterPage registerPage) throws Exception{
        //Read the last identifier from text file
        int lastIdentifierInteger = 0;

        lastIdentifierInteger = Integer.parseInt(this.getLastIdentifier());

        //Compute the new first identifier
        int firstNewIdentifier = lastIdentifierInteger + 1;
        String newFirstIdentifierString = "123456789" + String.valueOf(firstNewIdentifier);
        //Compute the new second identifier
        int secondNewIdentifier = firstNewIdentifier + 1;
        String newSecondIdentifierString = "123456789" + String.valueOf(secondNewIdentifier);

        //Extend the Add Identifier
        if(registrator.contains("Veterinary")){
            scrollOnElement(registerPage.addIdentifierVet);
            click(registerPage.addIdentifierVet);
        } else {
            scrollOnElement(registerPage.addIdentifierOther);
            click(registerPage.addIdentifierOther);
        }

        //Fill the 2 identifiers
        setText(registerPage.firstIdentifier,newFirstIdentifierString);
        addNewIdentifier(String.valueOf(firstNewIdentifier));
        setText(registerPage.secondIdentifier,newSecondIdentifierString);
        addNewIdentifier(String.valueOf(secondNewIdentifier));
    }

    public boolean isRed(String [] rgbValues){
        //Example 255, 246, 246, 1
        sleep(500); //To make sure the color is updated
        return !rgbValues[0].equals(rgbValues[1]) && rgbValues[1].equals(rgbValues[2]);
    }

    public boolean isBlue(String [] rgbValues){
        //Example 246, 246, 255, 1
        sleep(500); //To make sure the color is updated
        return !rgbValues[2].equals(rgbValues[1]) && rgbValues[1].equals(rgbValues[0]);
    }

    public boolean isGreen(String [] rgbValues){
        //Example 244, 250, 244, 1
        sleep(500); //To make sure the color is updated
        return !rgbValues[1].equals(rgbValues[0]) && rgbValues[0].equals(rgbValues[2]);
    }

    public boolean isGray(String [] rgbValues){
        //Example 244, 244, 244, 1
        sleep(500); //To make sure the color is updated
//        System.out.println(rgbValues[0] + " " + rgbValues[1] + " " + rgbValues[2]);
        return rgbValues[0].equals(rgbValues[1]) && rgbValues[0].equals(rgbValues[2]);
    }

    public boolean isWhite(String [] rgbValues){
        //Example 244, 244, 244, 1
        sleep(500); //To make sure the color is updated
        return isGray(rgbValues) && rgbValues[0].equals("255");
    }

    public boolean isSpecificColor(String [] rgbValues, String red, String green, String blue){
        return  rgbValues[0].equals(red) && rgbValues[1].equals(green) && rgbValues[2].equals(blue);
    }

    public boolean isNoColor(String [] rgbValues){
        return isBlack(rgbValues);
    }

    public boolean isBlack(String [] rgbValues){
        //Example 244, 244, 244, 1
        sleep(500); //To make sure the color is updated
//        System.out.println(rgbValues[0] + " " + rgbValues[1] + " " + rgbValues[2]);
        return isGray(rgbValues) && rgbValues[0].equals("0");
    }

    public String[] getRgbString (WebElement we){
        String rgbValuRawe = we.getCssValue("background-color");
        String values = rgbValuRawe.substring(rgbValuRawe.indexOf("(") + 1, rgbValuRawe.indexOf(")"));
        String trimValues = values.replaceAll(" ", "");
        return  trimValues.split(",");
    }

    public void fillCatIdentifier(WebElement identifierField){
        scrollOnElement(identifierField);
        //Read the last identifier from text file
        int identifierInteger = 0;
        try {
            identifierInteger = Integer.parseInt(this.getLastIdentifier()) + 1;
            //Try to write the last identifier, if it is red increment it
            String lastIdentifierString = "123456789" + String.valueOf(identifierInteger);
            setText(identifierField,lastIdentifierString);
            int currentItteration = 0;
            //Try to limit the non-needed incrementation, otherwise too many ID is used for free
            sleep(1000);
            while(isRed(getRgbString(identifierField))){
                //Compute the new first identifier
                currentItteration++;
                if(currentItteration > 100){
                    break;
                }
                identifierInteger++;
                String newFirstIdentifierString = "123456789" + String.valueOf(identifierInteger);
                setText(identifierField,newFirstIdentifierString);
                addNewIdentifier(String.valueOf(identifierInteger));
                sleep(1000);
            }

        } catch (Exception e) {
            logger.log(LogStatus.ERROR, e.getLocalizedMessage());
        }
    }

    public static String genVscVatNbr(){
        // this function will build random VAT number format : BE + N10
        int yyyy = 1970 + (int)(Math.random() * ((2000 - 1970) + 1)); //4
        int mm = 10 + (int)(Math.random() * ((12 - 10) + 1));   //2
        int dd = 10 + (int)(Math.random() * ((28 - 10) + 1));  //2

        System.out.println("Year=" + yyyy* 10000);
        System.out.println("Month=" + mm* 100);
        System.out.println("Day=" + dd* 100);
        Long base = (long) (((yyyy * 10000) + (mm * 100) + (dd)));
//        Long base = (long) ((yyyy * 10000) + (mm * 100) + (dd * 100));
        long seq = 97 - (base % 97);
        System.out.println("base % 97=" + base % 97);
        System.out.println("Seq=" + seq);
        String vatNbr;
//        if(seq < (long) 10){
//            vatNbr = "BE" + (base * 100) + "0" + seq;
//        }else{
//            vatNbr = "BE" + (base * 100) + seq;
//        }
        vatNbr = "BE" + (base * 10) + seq;
        return vatNbr;
    }

    public File getLatestFilefromDir(String dirPath){
        File dir = new File(dirPath);
        File[] files = dir.listFiles();
        File lastModifiedFile;
        if (files == null || files.length == 0) {
            lastModifiedFile = null;
        } else {
            lastModifiedFile = files[0];
            for (int i = 1; i < files.length; i++) {
                if (lastModifiedFile.lastModified() < files[i].lastModified()) {
                    lastModifiedFile = files[i];
                }
            }
        }
        return lastModifiedFile;
    }

    protected String getCellString(int rowIndex, int columnIndex, WebElement identifierTableResults){
        String result = "";
        List<WebElement> listTrWebElement = identifierTableResults.findElements(By.tagName("tr"));
        if(rowIndex < listTrWebElement.size()){
            WebElement row = listTrWebElement.get(rowIndex);
            List<WebElement> listTdWebElement = row.findElements(By.tagName("td"));
            if(columnIndex <= listTdWebElement.size()){
                result = listTdWebElement.get(columnIndex).getAttribute("textContent");
                if(result == null){
                    result = "";
                }
            } else {
                logger.log(LogStatus.ERROR,"ERROR: The column index require is greater than the maximum number of columns");
            }
        } else {
            logger.log(LogStatus.ERROR,"ERROR: The row index require is greater than the maximum number of rows");
        }
        return result;
    }

    protected WebElement getCell(int rowIndex, int columnIndex, WebElement identifierTableResults){
        WebElement we = null;
        List<WebElement> listTrWebElement = identifierTableResults.findElements(By.tagName("tr"));
        if(rowIndex < listTrWebElement.size()){
            WebElement row = listTrWebElement.get(rowIndex);
            List<WebElement> listTdWebElement = row.findElements(By.tagName("td"));
            if(columnIndex <= listTdWebElement.size()){
                we = listTdWebElement.get(columnIndex);
            } else {
                logger.log(LogStatus.ERROR,"ERROR: The column index require is greater than the maximum number of columns");
            }
        } else {
            logger.log(LogStatus.ERROR,"ERROR: The row index require is greater than the maximum number of rows");
        }
        return we;
    }

    protected WebElement getRow(int rowIndex, WebElement identifierTableResults){
        WebElement we = null;
        List<WebElement> listTrWebElement = identifierTableResults.findElements(By.tagName("tr"));
        if(rowIndex < listTrWebElement.size()){
            we = listTrWebElement.get(rowIndex);
        } else {
            logger.log(LogStatus.ERROR,"ERROR: The row index require is greater than the maximum number of rows");
        }
        return we;
    }

    protected void registerSpecificVetCat(WebRegisterPage registerPage, WebStartPage startPage, String firstOwner, String identifier) throws Exception {
        click(startPage.registerTabButton);
        WaitForElement(registerPage.transfertOfResponsibility,60);
        //Fill First Responsible Info
        setText(registerPage.refNumberTextFieldFirstOwner, firstOwner);
        click(registerPage.selectFisrtResponsibleToolTip);

        setText(registerPage.firstIdentifier, identifier);
        String passportNumberDate = new SimpleDateFormat("ddMMyyyyHHmmss").format(new Date());
        String passportText = tcFeatureListNumber  + passportNumberDate;
        setText(registerPage.passportNumber, passportText);

        //Fill cat identification
        String timeStamp = new SimpleDateFormat("dd.MM.yyyy").format(new Date());
        setText(registerPage.birthDate,timeStamp);
        setText(registerPage.identificationDate,timeStamp);
        setText(registerPage.sterilizationDate, timeStamp);
        setText(registerPage.colorCatTextField, "black");
        setText(registerPage.nameCatTextField,"Minuit");
        setText(registerPage.breedCatTextField, "Ab");
        click(registerPage.selectBreedTooltip);

        //Submit the form
        click(registerPage.submitButtonVet);
        click(registerPage.signVetLaterOrAccrPendingButton);
    }

    protected HashMap<String,String> registerNewVetCatToPro(WebRegisterPage registerPage, WebStartPage startPage, String firstOwner) throws Exception {
        return registerNewVetCatToPro(registerPage, startPage, firstOwner, true);
    }

    protected String computeRandomRRN(){
        Random random = new Random();
        int base = random.nextInt(999999999);
        while(base < 900000000){
            base = random.nextInt(999999999);
        }
        int checksum = 97 - (base%97);
        String checkString;
        if(checksum < 10){
            checkString = "0" + Integer.toString(checksum);
        } else {
            checkString = Integer.toString(checksum);
        }
        String RRN = Integer.toString(base) +checkString;
        return  RRN;
    }

    protected HashMap<String,String> registerNewVetCatToPro(WebRegisterPage registerPage, WebStartPage startPage, String firstOwner, boolean isLater) throws Exception {
        return registerNewVetCatToPro(registerPage, startPage, firstOwner, isLater, null, false);
    }

    protected HashMap<String,String> registerNewVetCatToPro(WebRegisterPage registerPage, WebStartPage startPage, String firstOwner, boolean isLater, Date inputDate, boolean isParticular) throws Exception {
        HashMap<String,String> registrationFieldMap = new HashMap<String, String>();
        click(startPage.registerTabButton);
        WaitForElement(registerPage.transfertOfResponsibility,60);
        //Fill First Responsible Info
        if(isParticular){
            clickHiddenElement(registerPage.checkBoxPrivatePersonFirstOwner);
            setText(registerPage.searchFirstOwnerPrivatePersonField, firstOwner);
            click(registerPage.searchFirstOwnerPrivatePersonButton);

            if(isEditable(registerPage.firstNameTextFieldFirstOwner)){
                setText(registerPage.firstNameTextFieldFirstOwner,"John");
                setText(registerPage.lastNameTextFieldFirstOwner,"SPECIMEN");
                setText(registerPage.postalCodeAndMunicipalityTextFieldFirstOwner,"1930 Zaventem");
                setText(registerPage.streetTextFieldFirstOwner,"Excelsiorlaan");
                setText(registerPage.streetNumberTextFieldFirstOwner,"10");
                setText(registerPage.emailTextFieldFirstOwner,"john@zetes.com");
                setText(registerPage.phoneTextFieldFirstOwner,"7755991133");
                setText(registerPage.mobileTextFieldFirstOwner,"7755991133");
                setText(registerPage.faxTextFieldFirstOwner,"7755991133");
                registrationFieldMap.put("firstOwnerFirstName",registerPage.firstNameTextFieldFirstOwner.getAttribute("value"));
                registrationFieldMap.put("firstOwnerLastName",registerPage.lastNameTextFieldFirstOwner.getAttribute("value"));
                registrationFieldMap.put("firstOwnerZip",registerPage.postalCodeAndMunicipalityTextFieldFirstOwner.getAttribute("value"));
                registrationFieldMap.put("firstOwnerStreet",registerPage.streetTextFieldFirstOwner.getAttribute("value"));
                registrationFieldMap.put("firstOwnerStreetNumber",registerPage.streetNumberTextFieldFirstOwner.getAttribute("value"));
                registrationFieldMap.put("firstOwnerEmail",registerPage.emailTextFieldFirstOwner.getAttribute("value"));
                registrationFieldMap.put("firstOwnerPhone",registerPage.phoneTextFieldFirstOwner.getAttribute("value"));
                registrationFieldMap.put("firstOwnerMobile",registerPage.mobileTextFieldFirstOwner.getAttribute("value"));
                registrationFieldMap.put("firstOwnerFax",registerPage.faxTextFieldFirstOwner.getAttribute("value"));
            }
        } else {
            setText(registerPage.refNumberTextFieldFirstOwner, firstOwner);
            click(registerPage.selectFisrtResponsibleToolTip);
        }

        fillCatIdentifier(registerPage.firstIdentifier);
        String passportNumberDate = new SimpleDateFormat("ddMMyyyyHHmmss").format(new Date());
        String passportText = tcFeatureListNumber  + passportNumberDate;
        logger.log(LogStatus.INFO,"Passport number: " + passportText);
        setText(registerPage.passportNumber, passportText);

        //Fill cat identification
        String timeStamp;
        if(inputDate == null){
            timeStamp = new SimpleDateFormat("dd.MM.yyyy").format(new Date());
        } else {
            timeStamp = new SimpleDateFormat("dd.MM.yyyy").format(inputDate);
        }

        setText(registerPage.birthDate,timeStamp);
        setText(registerPage.identificationDate,timeStamp);
        setText(registerPage.sterilizationDate, timeStamp);
        setText(registerPage.colorCatTextField, "black");
        setText(registerPage.nameCatTextField,"Minuit");
        setText(registerPage.breedCatTextField, "Ab");
        click(registerPage.selectBreedTooltip);

        //Save in a map the registration info
        registrationFieldMap.put("RRN",registerPage.nationalNumberTextFieldFirstOwner.getAttribute("value"));
        registrationFieldMap.put("catIdentifier",getLastIdentifier());
        registrationFieldMap.put("catGender",registerPage.gender.getAttribute("value"));
        registrationFieldMap.put("catBirthDate",registerPage.birthDate.getAttribute("value"));
        registrationFieldMap.put("catIdentificationDate",registerPage.identificationDate.getAttribute("value"));
        registrationFieldMap.put("catSterilizationDate",registerPage.sterilizationDate.getAttribute("value"));
        registrationFieldMap.put("colorCatTextField",registerPage.colorCatTextField.getAttribute("value"));
        registrationFieldMap.put("nameCatTextField",registerPage.nameCatTextField.getAttribute("value"));
        registrationFieldMap.put("breedCatTextField",registerPage.breedCatTextField.getAttribute("value"));

        //Submit the form
        click(registerPage.submitButtonVet);
        if(isLater){
            click(registerPage.signVetLaterOrAccrPendingButton);
        } else {
            click(registerPage.signVetNowOrAccrSendVetButton);
            WebEazyIdPage eazyIdPage = new WebEazyIdPage(driver);
            eazyId(eazyIdPage);
        }
        return registrationFieldMap;
    }

    protected void registerNewVetCatToParticular(WebRegisterPage registerPage, WebStartPage startPage, String firstOwner) throws Exception {
        registerNewVetCatToParticular(registerPage, startPage, firstOwner, true);
    }

    protected HashMap<String,String> registerNewVetCatToParticular(WebRegisterPage registerPage, WebStartPage startPage, String firstOwner, boolean isLater) throws Exception {
        HashMap<String,String> registrationFieldMap = new HashMap<String, String>();
        click(startPage.registerTabButton);
        WaitForElement(registerPage.transfertOfResponsibility,60);
        //Fill First Responsible Info
        clickHiddenElement(registerPage.checkBoxPrivatePersonFirstOwner);
        setText(registerPage.searchFirstOwnerPrivatePersonField, firstOwner);
        clickHiddenElement(registerPage.searchFirstOwnerPrivatePersonButton);

        //Fill first name and last name if the owner is new
        if (isEditable(registerPage.firstNameTextFieldFirstOwner)) {
            setText(registerPage.firstNameTextFieldFirstOwner,"John");
            setText(registerPage.lastNameTextFieldFirstOwner,"SPECIMEN");
            setText(registerPage.nationalNumberTextFieldFirstOwner,firstOwner);
            setText(registerPage.postalCodeAndMunicipalityTextFieldFirstOwner,"1000 Bruxelles");
            setText(registerPage.streetTextFieldFirstOwner,"Chaussee D'ixelles");
            setText(registerPage.streetNumberTextFieldFirstOwner,"64");
            setText(registerPage.emailTextFieldFirstOwner,"john.specimen@zetes.com");
            registrationFieldMap.put("firstName",registerPage.firstNameTextFieldFirstOwner.getAttribute("value"));
            registrationFieldMap.put("lastName",registerPage.lastNameTextFieldFirstOwner.getAttribute("value"));
            registrationFieldMap.put("address",registerPage.postalCodeAndMunicipalityTextFieldFirstOwner.getAttribute("value") + " " +registerPage.streetTextFieldFirstOwner.getAttribute("value"));
            registrationFieldMap.put("streetNumber",registerPage.streetNumberTextFieldFirstOwner.getAttribute("value"));
            registrationFieldMap.put("email",registerPage.emailTextFieldFirstOwner.getAttribute("value"));
        }

        fillCatIdentifier(registerPage.firstIdentifier);
        String passportNumberDate = new SimpleDateFormat("ddMMyyyyHHmmss").format(new Date());
        String passportText = tcFeatureListNumber  + passportNumberDate;
        logger.log(LogStatus.INFO,passportText);
        setText(registerPage.passportNumber, passportText);

        //Fill cat identification
        String timeStamp = new SimpleDateFormat("dd.MM.yyyy").format(new Date());
        setText(registerPage.birthDate,timeStamp);
        setText(registerPage.identificationDate,timeStamp);
        setText(registerPage.sterilizationDate, timeStamp);
        setText(registerPage.colorCatTextField, "black");
        setText(registerPage.nameCatTextField,"Minuit");
        setText(registerPage.breedCatTextField, "Ab");
        click(registerPage.selectBreedTooltip);

        //Save in a map the registration info
        registrationFieldMap.put("RRN",registerPage.nationalNumberTextFieldFirstOwner.getAttribute("value"));
        registrationFieldMap.put("identifier",getLastIdentifier());
        registrationFieldMap.put("gender",registerPage.gender.getAttribute("value"));
        registrationFieldMap.put("birthDate",registerPage.birthDate.getAttribute("value"));
        registrationFieldMap.put("identificationDate",registerPage.identificationDate.getAttribute("value"));
        registrationFieldMap.put("sterilizationDate",registerPage.sterilizationDate.getAttribute("value"));
        registrationFieldMap.put("colorCatTextField",registerPage.colorCatTextField.getAttribute("value"));
        registrationFieldMap.put("nameCatTextField",registerPage.nameCatTextField.getAttribute("value"));
        registrationFieldMap.put("breedCatTextField",registerPage.breedCatTextField.getAttribute("value"));

        //Submit the form
        click(registerPage.submitButtonVet);
        if(isLater){
            click(registerPage.signVetLaterOrAccrPendingButton);
        } else {
            click(registerPage.signVetNowOrAccrSendVetButton);
            WebEazyIdPage eazyIdPage = new WebEazyIdPage(driver);
            eazyId(eazyIdPage);
        }
        return registrationFieldMap;
    }

    protected void registerSpecificBreederOrRefugeCat(WebRegisterPage registerPage, WebStartPage startPage, String secondOwner, String identifier) throws Exception {
        click(startPage.registerTabButton);
        WaitForElement(registerPage.transfertOfResponsibility,60);
        //Fill First Responsible Info
        click(registerPage.transfertOfResponsibility);
        setText(registerPage.refNumberTextFieldSecondOwner, secondOwner);
        click(registerPage.selectSecondResponsibleToolTip);

        setText(registerPage.firstIdentifier, identifier);
        String passportNumberDate = new SimpleDateFormat("ddMMyyyyHHmmss").format(new Date());
        String passportText = tcFeatureListNumber  + passportNumberDate;
        logger.log(LogStatus.INFO,passportText);
        setText(registerPage.passportNumber, passportText);

        //Fill cat identification
        String timeStamp = new SimpleDateFormat("dd.MM.yyyy").format(new Date());
        setText(registerPage.birthDate,timeStamp);
        setText(registerPage.identificationDate,timeStamp);
        setText(registerPage.sterilizationDate, timeStamp);
        setText(registerPage.colorCatTextField, "black");
        setText(registerPage.nameCatTextField,"Minuit");
        setText(registerPage.breedCatTextField, "Ab");
        click(registerPage.selectBreedTooltip);

        //Submit the form
        click(registerPage.submitButtonOther);
        click(registerPage.signVetNowOrAccrSendVetButton);
    }

    protected void registerAndTransferNewBreederOrRefugeCat(WebRegisterPage registerPage, WebStartPage startPage, String secondOwner) throws Exception {
        click(startPage.registerTabButton);
        WaitForElement(registerPage.transfertOfResponsibility,60);
        //Fill First Responsible Info
        click(registerPage.transfertOfResponsibility);
        setText(registerPage.refNumberTextFieldSecondOwner, secondOwner);
        click(registerPage.selectSecondResponsibleToolTip);

        fillCatIdentifier(registerPage.firstIdentifier);
        String passportNumberDate = new SimpleDateFormat("ddMMyyyyHHmmss").format(new Date());
        String passportText = tcFeatureListNumber  + passportNumberDate;
        setText(registerPage.passportNumber, passportText);

        //Fill cat identification
        String timeStamp = new SimpleDateFormat("dd.MM.yyyy").format(new Date());
        click(registerPage.birthDate);
        setText(registerPage.birthDate,timeStamp);
        setText(registerPage.identificationDate,timeStamp);
        setText(registerPage.sterilizationDate, timeStamp);
        setText(registerPage.colorCatTextField, "black");
        setText(registerPage.nameCatTextField,"Minuit");
        setText(registerPage.breedCatTextField, "Ab");
        click(registerPage.selectBreedTooltip);

        //Submit the form
        click(registerPage.submitButtonOther);
        click(registerPage.signVetNowOrAccrSendVetButton);
    }

    protected void registerNewBreederOrRefugeCat(WebRegisterPage registerPage, WebStartPage startPage) throws Exception {
        click(startPage.registerTabButton);
        WaitForElement(registerPage.transfertOfResponsibility,60);

        fillCatIdentifier(registerPage.firstIdentifier);
        String passportNumberDate = new SimpleDateFormat("ddMMyyyyHHmmss").format(new Date());
        String passportText = tcFeatureListNumber + passportNumberDate;
        logger.log(LogStatus.INFO,"Passport: " + passportText);
        setText(registerPage.passportNumber, passportText);
        //Fill cat identification
        String timeStamp = new SimpleDateFormat("dd.MM.yyyy").format(new Date());
        setText(registerPage.birthDate,timeStamp);
        setText(registerPage.identificationDate,timeStamp);
        setText(registerPage.sterilizationDate, timeStamp);
        setText(registerPage.colorCatTextField, "black");
        setText(registerPage.nameCatTextField,"Minuit");
        setText(registerPage.breedCatTextField, "Ab");
        click(registerPage.selectBreedTooltip);

        //Submit the form
        click(registerPage.submitButtonOther);
        click(registerPage.signVetNowOrAccrSendVetButton);
    }

    protected void transferFirstCat(WebStartPage startPage, WebApprovedPage approvedPage, WebModifyPage modifyPage, String changeResponsible, int index) throws Exception {
        click(startPage.filterButton);
        click(startPage.filterApprovedBreederAndRefugeTabButton);
        WaitForElement(approvedPage.headerRow,5);
        //descendant sort  direction
        List<WebElement> listHeaderButton = approvedPage.headerRow.findElements(By.tagName("th"));
        click(listHeaderButton.get(0));
        click(listHeaderButton.get(0));

        WebElement we = getRow(index,approvedPage.identifierTableResults);
        click(we);
        click(approvedPage.editButton);
        WaitForElement(modifyPage.changeResponsibleDropDown, 60);
        clickHiddenElement(modifyPage.changeResponsibleDropDown);
        scrollOnElement(modifyPage.changeResponsibleHkRefNumSecondSearchField);
        setText(modifyPage.changeResponsibleHkRefNumSecondSearchField, changeResponsible);
        click(modifyPage.responsibleHkSelectResponsibleToolTip);
        scrollOnElement(modifyPage.submitBreederAndRefugeButton);
        clickHiddenElement((modifyPage.changeResponsibleAgreeBreederRefugeCheckBox));
        scrollOnElement(modifyPage.submitBreederAndRefugeButton);
        click(modifyPage.submitBreederAndRefugeButton);
        click(modifyPage.submitYesOrSignLaterButton);
    }

    protected void transferCatWithSpecifiKeyWord(WebStartPage startPage, WebApprovedPage approvedPage, WebModifyPage modifyPage, String changeResponsible, int index, String keyword) throws Exception {
        click(startPage.filterButton);
        click(startPage.filterApprovedBreederAndRefugeTabButton);
        setText(approvedPage.searchField,keyword);
        sleep(3000);

        click(approvedPage.pageSelectionDropList);
        click(approvedPage.fiftyRowsByPageSelection);
        //descendant sort  direction
        List<WebElement> listHeaderButton = approvedPage.headerRow.findElements(By.tagName("th"));
        click(listHeaderButton.get(0));
        click(listHeaderButton.get(0));


        WebElement we = getRow(index,approvedPage.identifierTableResults);
        scrollOnElement(we);
        click(we);
        click(approvedPage.editButton);
        WaitForElement(modifyPage.changeResponsibleDropDown, 60);
        clickHiddenElement(modifyPage.changeResponsibleDropDown);
        scrollOnElement(modifyPage.changeResponsibleHkRefNumSecondSearchField);
        setText(modifyPage.changeResponsibleHkRefNumSecondSearchField, changeResponsible);
        click(modifyPage.responsibleHkSelectResponsibleToolTip);
        scrollOnElement(modifyPage.submitBreederAndRefugeButton);
        clickHiddenElement((modifyPage.changeResponsibleAgreeBreederRefugeCheckBox));
        scrollOnElement(modifyPage.submitBreederAndRefugeButton);
        click(modifyPage.submitBreederAndRefugeButton);
        click(modifyPage.submitYesOrSignLaterButton);
    }

    protected void registerNewRefugeCat(WebRegisterPage registerPage, WebStartPage startPage) throws Exception {
        click(startPage.registerTabButton);
        WaitForElement(registerPage.transfertOfResponsibility,60);
        //Fill First Responsible Info
        click(registerPage.transfertOfResponsibility);
        setText(registerPage.searchSecondOwnerPrivatePersonField, "71715100070");
        clickHiddenElement(registerPage.searchSecondOwnerPrivatePersonButton);

        fillCatIdentifier(registerPage.firstIdentifier);
        String passportNumberDate = new SimpleDateFormat("ddMMyyyyHHmmss").format(new Date());
        String passportText = tcFeatureListNumber  + passportNumberDate;
        logger.log(LogStatus.INFO,passportText);
        setText(registerPage.passportNumber, passportText);

        //Fill cat identification
        String timeStamp = new SimpleDateFormat("dd.MM.yyyy").format(new Date());
        setText(registerPage.birthDate,timeStamp);
        setText(registerPage.identificationDate,timeStamp);
        setText(registerPage.sterilizationDate, timeStamp);
        setText(registerPage.colorCatTextField, "black");
        setText(registerPage.nameCatTextField,"Minuit");
        setText(registerPage.breedCatTextField, "Ab");
        click(registerPage.selectBreedTooltip);

        //Submit the form
        click(registerPage.submitButtonOther);
        click(registerPage.signVetNowOrAccrSendVetButton);
    }

    protected void registerPendingBreederOrRefugeCat(WebRegisterPage registerPage, WebStartPage startPage) throws Exception {
        click(startPage.registerTabButton);
        WaitForElement(registerPage.transfertOfResponsibility,60);
        //Submit the form
        click(registerPage.submitButtonOther);
        click(registerPage.signVetLaterOrAccrPendingButton);
    }

    protected void registerSentForApprovalBreederOrRefugeCat(WebRegisterPage registerPage, WebStartPage startPage) throws Exception {
        click(startPage.registerTabButton);
        WaitForElement(registerPage.transfertOfResponsibility,60);
        //Submit the form
        click(registerPage.submitButtonOther);
        click(registerPage.signVetNowOrAccrSendVetButton);
    }

    protected void registerSpecificAndValidSentForApprovalBreederOrRefugeCat(WebRegisterPage registerPage, WebStartPage startPage, String identifier) throws Exception {
        click(startPage.registerTabButton);
        WaitForElement(registerPage.transfertOfResponsibility,60);
        setText(registerPage.firstIdentifier, identifier);

        //Fill cat identification
        String timeStamp = new SimpleDateFormat("dd.MM.yyyy").format(new Date());
        setText(registerPage.birthDate,timeStamp);
        setText(registerPage.identificationDate,timeStamp);
        setText(registerPage.sterilizationDate, timeStamp);
        setText(registerPage.colorCatTextField, "black");
        setText(registerPage.nameCatTextField,"Minuit");
        setText(registerPage.breedCatTextField, "Ab");
        click(registerPage.selectBreedTooltip);

        //Submit the form
        click(registerPage.submitButtonOther);
        click(registerPage.signVetNowOrAccrSendVetButton);
    }
}
