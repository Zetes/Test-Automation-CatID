package ElementaryScripts.General;

import Logger.LoggerInterface;
import com.relevantcodes.extentreports.LogStatus;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public abstract class AbstractElementaryScript implements ElementaryScriptInterface {
    protected static int GLOBAL_WAIT_DURATION = 60;
	protected String USER_DIR = System.getProperty("user.dir");
    protected static final String REGEX_PATH = "\\\\([^\\\\]+)\\\\([^\\\\]+)$";
    protected String PATH_TO_IDENTIFIERS = "src/test/java/Resources/IdentifierList";
	
    protected static WebDriver driver;
    protected static LoggerInterface logger;
    protected String baseUrl;
    protected int i = 0;

    // assuming JS is enabled
    protected JavascriptExecutor js = (JavascriptExecutor)driver;
    protected WebElement lastElem = null;
    protected String lastBorder = null;

    protected static final String SCRIPT_GET_ELEMENT_BORDER = "var elem = arguments[0]; \n" +
            "if (elem.currentStyle) {\n" +
            "    // Branch for IE 6,7,8. No idea how this works on IE9, but the script\n" +
            "    // should take care of it.\n" +
            "    var style = elem.currentStyle;\n" +
            "    var border = style['borderTopWidth']\n" +
            "            + ' ' + style['borderTopStyle']\n" +
            "            + ' ' + style['borderTopColor']\n" +
            "            + ';' + style['borderRightWidth']\n" +
            "            + ' ' + style['borderRightStyle']\n" +
            "            + ' ' + style['borderRightColor']\n" +
            "            + ';' + style['borderBottomWidth']\n" +
            "            + ' ' + style['borderBottomStyle']\n" +
            "            + ' ' + style['borderBottomColor']\n" +
            "            + ';' + style['borderLeftWidth']\n" +
            "            + ' ' + style['borderLeftStyle']\n" +
            "            + ' ' + style['borderLeftColor'];\n" +
            "} else if (window.getComputedStyle) {\n" +
            "    // Branch for FF, Chrome, Opera\n" +
            "    var style = document.defaultView.getComputedStyle(elem);\n" +
            "    var border = style.getPropertyValue('border-top-width')\n" +
            "            + ' ' + style.getPropertyValue('border-top-style')\n" +
            "            + ' ' + style.getPropertyValue('border-top-color')\n" +
            "            + ';' + style.getPropertyValue('border-right-width')\n" +
            "            + ' ' + style.getPropertyValue('border-right-style')\n" +
            "            + ' ' + style.getPropertyValue('border-right-color')\n" +
            "            + ';' + style.getPropertyValue('border-bottom-width')\n" +
            "            + ' ' + style.getPropertyValue('border-bottom-style')\n" +
            "            + ' ' + style.getPropertyValue('border-bottom-color')\n" +
            "            + ';' + style.getPropertyValue('border-left-width')\n" +
            "            + ' ' + style.getPropertyValue('border-left-style')\n" +
            "            + ' ' + style.getPropertyValue('border-left-color');\n" +
            "}\n" +
            "// highlight the element\n" +
            "elem.style.border = '2px solid red';\n" +
            "return border;";
    protected static final String SCRIPT_UNHIGHLIGHT_ELEMENT = "var elem = arguments[0];\n" +
            "var borders = arguments[1].split(';');\n" +
            "elem.style.borderTop = borders[0];\n" +
            "elem.style.borderRight = borders[1];\n" +
            "elem.style.borderBottom = borders[2];\n" +
            "elem.style.borderLeft = borders[3];";

    public void setDriver(WebDriver driver) {
        this.driver = driver;
    }

    public void setParams() {
        new Exception("this function should be implemented");
    }

    public abstract void run() throws Exception;

    public void setScreenshotFolderLocation(String path){
        USER_DIR = path;
    }

    public void restoreScreenshotFolderLocation(){
        USER_DIR = System.getProperty("user.dir");
    }

    public void setLogger(LoggerInterface logger) {
        this.logger = logger;
    }

    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    protected String getScreenShot() {
        String name = System.currentTimeMillis() + "-" + i;
        String path = USER_DIR + "/img/" + name + ".png";
              
        File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
        try {
            FileUtils.copyFile(scrFile, new File(path));
        } catch (Exception e) {
            logger.log(LogStatus.ERROR, "something went wrong while taking a screenshot\r\n ERROR: " + e.toString());
        }
                
        return getWebPath(name);
    }

    protected String getScreenShot(String path) {
        path = path + ".png";
        String name = System.currentTimeMillis() + "-" + i;

        File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
        try {
            FileUtils.copyFile(scrFile, new File(path));
        } catch (Exception e) {
            logger.log(LogStatus.ERROR, "something went wrong while taking a screenshot\r\n ERROR: " + e.toString());
        }

        return getWebPath(name);
    }
    
    private String getWebPath(String fileName) {
        Pattern p = Pattern.compile(REGEX_PATH);
        Matcher m = p.matcher(USER_DIR);       
//        String directory = "";
//        String directory = USER_DIR;

        File file = new File(USER_DIR + "/img/" + fileName + ".png" );
        String directory = file.getAbsolutePath();
        
        if (m.find()) {
        	directory = m.group(0);
        	directory = directory.replaceAll("\\\\", "/");
        }        
//        String webPath = directory + "/img/" + fileName + ".png";
        String webPath = directory;
        return webPath;
    }

    protected boolean hoverElement(WebElement e) {
        if (e == null) {
            logger.log(LogStatus.ERROR, "Hover The webelement field can not be null");
            return false;
        }
        if (!WaitForElement(e, 5)) {
            logger.logScreenshot(LogStatus.ERROR, "the element is not present " + e.toString(), getScreenShot());
            return false;
        }
        try {
             //WebElement webHoverElement = driver.findElement(By.xpath("//*[@id=\'nav-link-accountList\']/span[1]"));
            Actions builder = new Actions(driver);
            builder.moveToElement(e).build().perform();
            logger.logScreenshot(LogStatus.INFO, getScreenShot());
            return true;
        } catch (Exception ex) {
            logger.logScreenshot(LogStatus.ERROR, "Exception during setText: " + ex.toString(), getScreenShot());
            return false;
        }
    }

    protected void OpenBaseURL() {
        logger.log(LogStatus.INFO, "Opening the baseURL");
        driver.get(baseUrl);
    }

    protected String getAttribute(WebElement e, String attribute) {
        if (e == null) {
            logger.log(LogStatus.ERROR, "getAttribute The webelement field can not be null");
            return null;
        }
        if (attribute == null) {
            logger.log(LogStatus.ERROR, "getAttribute The attribute field can not be null");
            return null;
        }

        if (!WaitForElement(e, 5)) {
            logger.logScreenshot(LogStatus.ERROR, "the element is not present " + e.toString(), getScreenShot());
            return null;
        }

        return e.getAttribute(attribute);
    }

    protected String getText(WebElement e) {
        if (e == null) {
            logger.log(LogStatus.ERROR, "getText The webelement field can not be null");
            return null;
        }
        if (!WaitForElement(e, 5)) {
            logger.logScreenshot(LogStatus.ERROR, "the element is not present " + e.toString(), getScreenShot());
            return null;
        }
        return e.getText();
    }

    protected boolean WaitForElement(WebElement e, int seconds) {
        if (isElementPresent(e)) {
            return true;
        }
        (new WebDriverWait(driver, seconds))
                .until(ExpectedConditions.visibilityOf(e));
        (new WebDriverWait(driver, seconds))
                .until(ExpectedConditions.elementToBeClickable(e));
        return isElementPresent(e);
    }

    protected boolean isElementPresent(WebElement e) {
        try {
            return e.isDisplayed();
        } catch (Exception ex){
            return false;
        }
    }

    protected boolean selectDropDown(WebElement e, String title) {
        if (e == null) {
            logger.log(LogStatus.ERROR, "selectDropDown The webelement field can not be null");
            return false;
        }
        if (title == null) {
            logger.log(LogStatus.ERROR, "selectDropDown The title field can not be null");
            return false;
        }
        if (!WaitForElement(e, GLOBAL_WAIT_DURATION)) {
            logger.logScreenshot(LogStatus.ERROR, "the element is not present " + e.toString(), getScreenShot());
            return false;
        }

        Select select = new Select(e);
        select.selectByVisibleText(title);
        return true;
    }

    protected boolean setText(WebElement e, String txt) {
        if (e == null) {
            logger.log(LogStatus.ERROR, "SetText The webelement field can not be null");
            return false;
        }
        if (txt == null) {
            logger.log(LogStatus.ERROR, "SetText The txt field can not be null");
            return false;
        }
        if (!WaitForElement(e, GLOBAL_WAIT_DURATION)) {
            logger.logScreenshot(LogStatus.ERROR, "the element is not present " + e.toString(), getScreenShot());
            return false;
        }

        try {
            e.clear();
            highlightElement(e);
            e.sendKeys(txt);
            logger.logScreenshot(LogStatus.INFO, getScreenShot());
            return true;
        } catch (Exception ex) {
            logger.logScreenshot(LogStatus.ERROR, "Exception during setText: " + ex.toString(), getScreenShot());
            return false;
        }
    }

    public boolean setTextReadOnlyField(WebElement e, String txt){
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].removeAttribute('readonly')", e);
        return setText(e,txt);
    }

    public boolean setInputFileText(WebElement e, String txt){
        if (e == null) {
            logger.log(LogStatus.ERROR, "SetText The webelement field can not be null");
            return false;
        }
        if (txt == null) {
            logger.log(LogStatus.ERROR, "SetText The txt field can not be null");
            return false;
        }

        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].setAttribute('style', 'display:flex')", e);

        if (!WaitForElement(e, GLOBAL_WAIT_DURATION)) {
            logger.logScreenshot(LogStatus.ERROR, "the element is not present " + e.toString(), getScreenShot());
            return false;
        }
        try {
            e.clear();
            e.sendKeys(txt);
            logger.logScreenshot(LogStatus.INFO, getScreenShot());
            return true;
        } catch (Exception ex) {
            logger.logScreenshot(LogStatus.ERROR, "Exception during setInputFileText: " + ex.toString(), getScreenShot());
            return false;
        }
    }

    //Check if it is edditable trying to re-input the field with its own value
    public boolean isEditable(WebElement e){
        try {
            String currentString = e.getAttribute("value");
            if(currentString.equals("")){
                //Input spaces because some fields are usually sensitive to alphabetic characters and can initiate an additional behavior (eg: send a request with written characters)
                currentString = "   ";
            }
            WaitForElement(e, GLOBAL_WAIT_DURATION);
            e.clear();
            e.sendKeys(currentString);
            logger.logScreenshot(LogStatus.INFO, getScreenShot());

            return true;
        } catch (Exception ex) {
            logger.logScreenshot(LogStatus.INFO, getScreenShot());
            return false;
        }
    }

    //Check if it is edditable trying to verify the html field is readonly or not
    public boolean isReadOnly(WebElement e){
        try {
            boolean isEdditable = true;
            click(e);
            String readonly = e.getAttribute("readonly");
            if(readonly != null && !readonly.equals("")){
                isEdditable = false;
            }
            return isEdditable;
        } catch (Exception ex) {
            logger.logScreenshot(LogStatus.ERROR, getScreenShot());
            return false;
        }
    }

    public boolean isEditable(WebElement e, String title){
        logger.log(LogStatus.INFO, "isEditable " + title);
       return isEditable(e);
    }

    public boolean isFilled(WebElement e){
        logger.logScreenshot(LogStatus.INFO, getScreenShot());
        return !e.getAttribute("value").equals("");
    }

    public boolean isFilled(WebElement e, String title){
        logger.log(LogStatus.INFO, "isFilled " + title);
        return isFilled(e);
    }

    protected boolean sleep(int milliseconds) {
        try {
            Thread.sleep(milliseconds);
            return true;
        } catch (Exception e) {
            logger.logScreenshot(LogStatus.ERROR, "Exception during sleep: " + e.toString(), getScreenShot());
            return false;
        }
    }

    protected WebElement ByToWebelement(By by) {
        try {
            WebElement output = driver.findElement(by);
            if (output != null)
                return output;
            logger.logScreenshot(LogStatus.ERROR, "cant find By element : " + by.toString(), getScreenShot());
            return null;
        } catch (Exception e) {
            logger.logScreenshot(LogStatus.ERROR, "cant find By element : " + by.toString(), getScreenShot());
            return null;
        }
    }

    protected boolean click(WebElement e){
        if (e == null) {
            logger.log(LogStatus.ERROR, "getText The webelement field can not be null");
            return false;
        }
        try {
            WaitForElement(e, GLOBAL_WAIT_DURATION);
            highlightElement(e);
            e.click();
            logger.logScreenshot(LogStatus.INFO, getScreenShot());
            sleep(500);
            return true;
        } catch (Exception ex){
            logger.logScreenshot(LogStatus.FAIL, "Click The webelement field can not be found "+ e.toString() + " " + ex.getMessage(), getScreenShot());
            return false;
        }
    }

    protected boolean clickHiddenElement(WebElement e){
        if (e == null) {
            logger.log(LogStatus.ERROR, "getText The webelement field can not be null");
            return false;
        }
        try {
            JavascriptExecutor js = (JavascriptExecutor)driver;
            highlightElement(e);
            js.executeScript("arguments[0].click();", e);
            logger.logScreenshot(LogStatus.INFO, getScreenShot());
            return true;
        } catch (Exception ex){
            logger.logScreenshot(LogStatus.FAIL, "Click The webelement field can not be found "+ e.toString() + " " + ex.getMessage(), getScreenShot());
            return false;
        }
    }

    protected boolean click(By b){
        WebElement e = ByToWebelement(b);
        if (e != null) {
            return click(e);
        }
        return false;
    }
    public void scrollOnElement(WebElement Element){
        WaitForElement(Element,GLOBAL_WAIT_DURATION);
        logger.log(LogStatus.INFO,"Scrolling on an object");
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", Element);
    }

    public boolean createFolder(String path){
        return (new File(path)).mkdirs();
    }

    void highlightElement(WebElement elem) {
        unhighlightLast();

        // remember the new element
        lastElem = elem;
        lastBorder = (String)(js.executeScript(SCRIPT_GET_ELEMENT_BORDER, elem));
    }

    void unhighlightLast() {
        if (lastElem != null && isElementPresent(lastElem)) {
            try {
                // if there already is a highlighted element, unhighlight it
                js.executeScript(SCRIPT_UNHIGHLIGHT_ELEMENT, lastElem, lastBorder);
            } catch (StaleElementReferenceException ignored) {
                // the page got reloaded, the element isn't there
            } finally {
                // element either restored or wasn't valid, nullify in both cases
                lastElem = null;
            }
        }
    }
}
