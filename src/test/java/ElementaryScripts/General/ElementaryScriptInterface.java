package ElementaryScripts.General;

import Logger.LoggerInterface;
import org.openqa.selenium.WebDriver;

/**
 * Created by rstevens on 26/09/2016.
 */
public interface ElementaryScriptInterface {

    void setDriver(WebDriver driver);

    //TODO: think of a way to give the params to the elementary scrips e.g. some kind of map
    void setParams();

    void run() throws Exception;

    void setLogger(LoggerInterface logger);

    void setBaseUrl(String homePage);


}
