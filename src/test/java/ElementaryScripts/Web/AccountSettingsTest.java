package ElementaryScripts.Web;

import DataBaseFramework.SqlOracleMiddleware;
import ElementaryScripts.General.AbstractCatIdElementaryScript;
import Screens.WebAccountSettingsPage;
import Screens.WebStartPage;
import com.relevantcodes.extentreports.LogStatus;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by thierry.foulon on 5/18/2018.
 */
public class AccountSettingsTest extends AbstractCatIdElementaryScript {

    private enum NUMBER_TYPE{ phone, mobile, fax };

    public void run() throws Exception{
        try {
            WebStartPage startPage = new WebStartPage(driver);
            WebAccountSettingsPage accountSettingsPage = new WebAccountSettingsPage(driver);

            click(startPage.accountSettings);
            WaitForElement(accountSettingsPage.saveButtonAccountDetails, 60);

            //Check if name, number, address,... are present
            checkAccountInformationPresent(accountSettingsPage);

            //Check if all fields are editable
            checkAccountSettingsFieldsEditable(accountSettingsPage);

            //Check phone number format
            checkNumberFormat(accountSettingsPage, NUMBER_TYPE.phone);

            //Check mobile number format
            checkNumberFormat(accountSettingsPage, NUMBER_TYPE.mobile);

            //Check fax number format
            checkNumberFormat(accountSettingsPage, NUMBER_TYPE.fax);

            //Check email format
            checkEmailFormat(accountSettingsPage);

            //Check warning quota format/values
            checkQuotaValues(accountSettingsPage);

            //Breeder and Refuge specific: contract veterinary operations
            if(!registrator.equals("Veterinary")) {
                checkContractVetOperations(accountSettingsPage);
            }
        } catch (Exception e) {
            logger.logScreenshot(LogStatus.ERROR, "ERROR stopping the test", getScreenShot());
            throw e;
        }
    }

    private void checkAccountInformationPresent(WebAccountSettingsPage accountSettingsPage) {
        //by default use Refuge 1's reference number
        String refnumber = "HK12341234";
        if(registrator.equals("Veterinary")){
            refnumber = "F0032";
        }
        if(registrator.equals("Breeder 1")){
            refnumber = "HK43214321";
        }

        //get correct values
        SqlOracleMiddleware catIdMiddleWare = new SqlOracleMiddleware(logger, "CATID", "CATID", "jdbc:oracle:thin:@192.168.17.6:1521:PREPROD");
        ArrayList<HashMap> rs = new ArrayList<HashMap>();
        try {
            rs = catIdMiddleWare.executeSqlQuery("select addr.street || ' ' || addr.housenumber || ' ' || addr.zip || ' ' || addr.city as \"Address\", \n" +
                    "    (pid.firstname || ' ' || pid.lastname) as \"Name\",\n" +
                    "    person.refnum as \"RefNo\"\n" +
                    "from ABIEC_PERSON person\n" +
                    "join ABIEC_PERSON_ID pid on pid.id = person.personidentification_id\n" +
                    "join ABIEC_ADDRESS addr on addr.id = person.addressid\n" +
                    "where refnum = '" + refnumber + "'");
        } catch (SQLException e) {
            logger.log(LogStatus.ERROR, "SQL query for name and address lookup failed\n"+e.getMessage());
        }

        if (rs.size() == 0) {
            logger.log(LogStatus.ERROR, "Something is wrong with SQL statement or DB connection");
        } else {
            if(!registrator.equals("Veterinary")){
                if(!accountSettingsPage.labelAddress.getText().equals("Address - " + rs.get(0).get("Address"))){
                    logger.log(LogStatus.ERROR, "Address field is not filled in correctly\n" +
                            "Result: "+accountSettingsPage.labelAddress.getText()+"\n" +
                            "Expected: " + "Address - " + rs.get(0).get("Address"));
                }else{
                    logger.log(LogStatus.INFO, "Address field filled in correctly");
                }
            }
            if(accountSettingsPage.labelAccountName.getText().contains("Name - " + rs.get(0).get("Name"))
                    && accountSettingsPage.labelAccreditationNumber.getText().contains("Accreditation number - " + rs.get(0).get("RefNo"))){
                logger.log(LogStatus.INFO, "Name and accreditation number fields are filled in correctly");
            }else{
                logger.log(LogStatus.ERROR, "Name and accreditation number fields are not filled in correctly\n" +
                        "Result Name: "+accountSettingsPage.labelAccountName.getText()+"\n" +
                        "Expected Name: Name - " + rs.get(0).get("Name") +"\n" +
                        "Result Accreditation Number: "+accountSettingsPage.labelAccreditationNumber.getText()+"\n"+
                        "Expected Accreditation Number: Accreditation number - "+rs.get(0).get("RefNo"));
            }
        }
    }

    private void checkContractVetOperations(WebAccountSettingsPage accountSettingsPage) {
        //Add a vet
        //Searching for unexisting vet
        accountSettingsPage.addVetTextFieldForBreederAndRefuge.sendKeys("---------");
        click(accountSettingsPage.addVetTextFieldForBreederAndRefuge);
        if(isRed(getRgbString(accountSettingsPage.addVetTextFieldForBreederAndRefuge))){
            logger.log(LogStatus.INFO, "Searching for unexisting veterinary \"---------\" yielded no results. Text field colored red");
        }else{
            logger.log(LogStatus.ERROR, "Searching for unexisting veterinary \"---------\" did not cause the text field to be colored red");
        }

        //Adding a vet without an account
        accountSettingsPage.addVetTextFieldForBreederAndRefuge.clear();
        accountSettingsPage.addVetTextFieldForBreederAndRefuge.sendKeys("00112255");
        click(accountSettingsPage.newVetSearchFirstResult);
        WaitForElement(accountSettingsPage.yesAddVetButtonForBreederAndRefuge,10);
        click(accountSettingsPage.yesAddVetButtonForBreederAndRefuge);
        try{
            WaitForElement(accountSettingsPage.messagePopup,10);
            String message = accountSettingsPage.messagePopup.getAttribute("textContent").replaceAll("\\s+", "");
            if (message.equals("Theveterinarianhasnoaccount")) {
                logger.log(LogStatus.INFO, "Veterinary searched by \"00112266\" was not added because it has no account");
            } else {
                logger.log(LogStatus.ERROR, "Page returned unexpected confirmation.\nExpected: The veterinarian has no account\n" +
                        "Received: "+message);
            }
        }catch (Exception e){
            logger.log(LogStatus.ERROR, "Veterinary searched by \"00112266\" did not cause a popup when added");
        }

        //Adding an existing vet
        setText(accountSettingsPage.addVetTextFieldForBreederAndRefuge,"0572");
        //Select first result from autocomplete
        click(accountSettingsPage.newVetSearchFirstResult);

        //Confirm adding the vet
        WaitForElement(accountSettingsPage.yesAddVetButtonForBreederAndRefuge,10);
        click(accountSettingsPage.yesAddVetButtonForBreederAndRefuge);
        try {
            WaitForElement(accountSettingsPage.messagePopup, 10);
            String message = accountSettingsPage.messagePopup.getAttribute("textContent").replaceAll("\\s+", "");
            if (message.equals("TheVethasbeensuccessfullyadded.")) {
                logger.log(LogStatus.INFO, "Veterinary searched by \"0572\" was successfully added");
            } else {
                logger.log(LogStatus.ERROR, "Unable to add existing veterinary searched by \"0572\"");
            }
        }catch(Exception e){
            logger.log(LogStatus.ERROR, "Unable to add existing veterinary searched by \"0572\"");
        }

        //If vet was added, it will be present in the dropdownlist
        WebElement newvet = null;
        try {
            newvet = accountSettingsPage.newSecondVetAdded;
            logger.log(LogStatus.INFO, "Veterinary was added to the contract veterinary dropdownlist");
        }catch (Exception e){
            logger.log(LogStatus.ERROR, "Veterinary was not added to the contract veterinary dropdownlist");
        }

        //Make new veterinary default
        click(accountSettingsPage.vetTextFieldForBreederAndRefuge);
        click(newvet);
        click(accountSettingsPage.setDefaultVetButtonForBreederAndRefuge);
        try{
            WaitForElement(accountSettingsPage.messagePopup,10);
            String message = accountSettingsPage.messagePopup.getAttribute("textContent").replaceAll("\\s+", "");
            if (message.equals("TheVetWasMarkedAsDefault")) {
                logger.log(LogStatus.INFO, "Veterinary successfully set as default");
            }else{
                logger.log(LogStatus.ERROR, "Page returned unexpected confirmation.\nExpected: The Vet Was Marked As Default\n" +
                        "Received: "+message);
            }
        }catch (Exception e){
            logger.log(LogStatus.ERROR, "Selected contract veterinary was not set as default");
        }

        //Delete new veterinary
        click(accountSettingsPage.deleteVetButtonForBreederAndRefuge);
        try{
            WaitForElement(accountSettingsPage.messagePopup,10);
            String message = accountSettingsPage.messagePopup.getAttribute("textContent").replaceAll("\\s+", "");
            if (message.equals("ContractVetDeleted")) {
                logger.log(LogStatus.INFO, "Veterinary successfully deleted");
            }else{
                logger.log(LogStatus.ERROR, "Page returned unexpected confirmation.\nExpected: Contract Vet Deleted\n" +
                        "Received: "+message);
            }
        }catch (Exception e){
            logger.log(LogStatus.ERROR, "Selected veterinary was not deleted");
        }

        //Check that the vet was deleted
        List<WebElement> listVet = accountSettingsPage.listContainerVeterinary.findElements(By.tagName("div"));
        boolean found = false;
        for (WebElement we: listVet) {
            if(we.getText().contains("F0572")){
                found = true;
                break;
            }
        }
        if(!found){
            logger.log(LogStatus.INFO, "Veterinary no longer present in veterinary list");
        }else{
            logger.log(LogStatus.ERROR, "Veterinary is still in veterinary list");
        }

        logger.log(LogStatus.INFO, "Cancel adding a veterinary");
        //Cancel adding a vet
        accountSettingsPage.addVetTextFieldForBreederAndRefuge.sendKeys("0572");
        //Select first result from autocomplete
        click(accountSettingsPage.newVetSearchFirstResult);
        WaitForElement(accountSettingsPage.noAddVetButtonForBreederAndRefuge,10);
        click(accountSettingsPage.noAddVetButtonForBreederAndRefuge);

        //Check that the vet is not added to veterinary list;
        if(!isPresentInVeterinaryList(accountSettingsPage, "F0572")){
            logger.log(LogStatus.INFO, "Veterinary not present in veterinary list");
        }else{
            logger.log(LogStatus.ERROR, "Selected veterinary was added in veterinary list");
        }
    }

    private boolean isPresentInVeterinaryList(WebAccountSettingsPage accountSettingsPage, String vetcode){
        List<WebElement> listVet = accountSettingsPage.listContainerVeterinary.findElements(By.tagName("div"));
        for (WebElement we: listVet) {
            if(we.getText().contains(vetcode)){
                return true;
            }
        }
        return false;
    }

    private void checkQuotaValues(WebAccountSettingsPage accountSettingsPage) {
        logger.log(LogStatus.INFO, "Checking quota values");
        WebElement quotafield = accountSettingsPage.editTextFieldForQuotaWarning;
        String current = quotafield.getAttribute("value");

        //Valid quota values
        quotaCheckValid(accountSettingsPage, quotafield,"0");
        quotaCheckValid(accountSettingsPage, quotafield,"42");

        //Invalid quota values
        quotaCheckInValid(accountSettingsPage, quotafield,"abcdef");
        quotaCheckInValid(accountSettingsPage, quotafield,"-10");
        quotaCheckInValid(accountSettingsPage, quotafield,"-+/*-++/");

        setText(quotafield, current);
        click(accountSettingsPage.saveButtonAccountDetails);
    }

    //email addresses courtesy of Wikipedia
    //https://en.wikipedia.org/wiki/Email_address#Examples
    private void checkEmailFormat(WebAccountSettingsPage accountSettingsPage) {
        logger.log(LogStatus.INFO, "Checking email address format");
        WebElement emailfield = accountSettingsPage.editTextFieldForEmailAddress;
        String current = emailfield.getAttribute("value");

        //Valid addresses
        emailCheckValid(accountSettingsPage, emailfield,"simple@example.com");
        emailCheckValid(accountSettingsPage, emailfield,"very.common@example.com");
        emailCheckValid(accountSettingsPage, emailfield,"disposable.style.email.with+symbol@example.com");
        emailCheckValid(accountSettingsPage, emailfield,"other.email-with-dash@example.com");
        emailCheckValid(accountSettingsPage, emailfield,"fully-qualified-domain@example.com");
        emailCheckValid(accountSettingsPage, emailfield,"user.name+tag+sorting@example.com");
        emailCheckValid(accountSettingsPage, emailfield,"x@example.com");
        emailCheckValid(accountSettingsPage, emailfield,"\"very.(),:;<>[]\\\".VERY.\\\"very@\\\\ \\\"very\\\".unusual\"@strange.example.com");
        emailCheckValid(accountSettingsPage, emailfield,"example-indeed@strange-example.com");
        emailCheckValid(accountSettingsPage, emailfield,"#!$%&'*+-/=?^_`{}|~@example.org");
        emailCheckValid(accountSettingsPage, emailfield,"\"()<>[]:,;@\\\\\\\"!#$%&'-/=?^_`{}| ~.a\"@example.org");
        emailCheckValid(accountSettingsPage, emailfield,"example@s.solutions");

        //Invalid addresses
        emailCheckInValid(accountSettingsPage, emailfield,"Abc.example.com");
        emailCheckInValid(accountSettingsPage, emailfield,"A@b@c@example.com");
        emailCheckInValid(accountSettingsPage, emailfield,"a\"b(c)d,e:f;g<h>i[j\\k]l@example.com");
        emailCheckInValid(accountSettingsPage, emailfield,"just\"not\"right@example.com");
        emailCheckInValid(accountSettingsPage, emailfield,"this is\"not\\allowed@example.com");
        emailCheckInValid(accountSettingsPage, emailfield,"this\\ still\\\"not\\\\allowed@example.com");
        emailCheckInValid(accountSettingsPage, emailfield,"john..doe@example.com");
        emailCheckInValid(accountSettingsPage, emailfield,"john.doe@example..com");

        setText(emailfield, current);
        click(accountSettingsPage.saveButtonAccountDetails);
    }

    //regular: String numbertype="phone"
    //mobile: String numbertype="mobile"
    //fax: String numbertype="fax"
    //Fax uses a different error message "Fax number invalid", where phone and mobile numbers both use the same format:
    //  "Invalid phone number" or "Invalid mobile number"
    private void checkNumberFormat(WebAccountSettingsPage accountSettingsPage, NUMBER_TYPE numbertype) {
        logger.log(LogStatus.INFO,"Checking " + numbertype + " number format");
        WebElement numberfield = null;
        switch (numbertype){
            case phone: numberfield = accountSettingsPage.editTextFieldForPhoneNumber;
                        break;
            case mobile: numberfield = accountSettingsPage.editTextFieldForMobileNumber;
                        break;
            case fax: numberfield = accountSettingsPage.editTextFieldForFaxNumber;
        }
        String current = numberfield.getAttribute("value");

        numberCheckValid(accountSettingsPage, numberfield, numbertype, "0123/456789");
        numberCheckValid(accountSettingsPage, numberfield, numbertype,"+32123/456789");
        numberCheckValid(accountSettingsPage, numberfield, numbertype,"0123/45 67 89");
        numberCheckValid(accountSettingsPage, numberfield, numbertype,"0123/456 789");
        numberCheckValid(accountSettingsPage, numberfield, numbertype,"0123456789");
        numberCheckValid(accountSettingsPage, numberfield, numbertype, "+/797979");

        numberCheckInValid(accountSettingsPage, numberfield, numbertype, "abcdef");
        numberCheckInValid(accountSettingsPage, numberfield, numbertype, "-+/*-++/");

        setText(numberfield, current);
        click(accountSettingsPage.saveButtonAccountDetails);
    }

    private void checkAccountSettingsFieldsEditable(WebAccountSettingsPage accountSettingsPage){
        logger.log(LogStatus.INFO,"Checking if fields on the account settings page are editable");
        if(isEditable(accountSettingsPage.editTextFieldForPhoneNumber)
                && isEditable(accountSettingsPage.editTextFieldForMobileNumber)
                && isEditable(accountSettingsPage.editTextFieldForFaxNumber)
                && isEditable(accountSettingsPage.editTextFieldForEmailAddress)
                && isEditable(accountSettingsPage.editTextFieldForQuotaWarning)){
            //For veterinary
            if(registrator.equals("Veterinary")){
                if(isEditable(accountSettingsPage.editTextFieldForPostalCodeAndMunicipality)
                        && isEditable(accountSettingsPage.editTextFieldForStreet)
                        && isEditable(accountSettingsPage.editTextFieldForStreetNumber)) {
                    logger.log(LogStatus.INFO, "All fields are editable");
                }else{
                    logger.log(LogStatus.ERROR, "Not all fields are editable");
                }
            }else{ //For breeder and refuge
                if(isEditable(accountSettingsPage.addVetTextFieldForBreederAndRefuge)
                        && accountSettingsPage.vetTextFieldForBreederAndRefuge.isEnabled()){
                    logger.log(LogStatus.INFO, "All fields are editable");
                }else{
                    logger.log(LogStatus.ERROR, "Not all fields are editable");
                }
            }
        }else{
            logger.log(LogStatus.ERROR, "Not all fields are editable");
        }
    }

    private void numberCheckValid(WebAccountSettingsPage accountSettingsPage, WebElement numberfield, NUMBER_TYPE numbertype, String value){
        setText(numberfield, value);
        click(accountSettingsPage.saveButtonAccountDetails);
        try {
            WaitForElement(accountSettingsPage.messagePopup, 10);
            String message = accountSettingsPage.messagePopup.getAttribute("textContent").replaceAll("\\s+", "");
            if (message.equals("Datasuccessfullysaved.")) {
                logger.log(LogStatus.INFO, value + " was accepted as a valid " + numbertype + " number");
            } else {
                logger.log(LogStatus.ERROR, "Unexpected confirmation message\n" +
                        "Expected: Data successfully saved\n" +
                        "Received: " + message);
            }
        }catch (Exception e){
            logger.log(LogStatus.ERROR, value + " was not accepted as a valid " + numbertype + " number");
        }
    }

    private void numberCheckInValid(WebAccountSettingsPage accountSettingsPage, WebElement numberfield, NUMBER_TYPE numbertype, String value){
        setText(numberfield, value);
        click(accountSettingsPage.saveButtonAccountDetails);
        try{
            WaitForElement(accountSettingsPage.messageError, 10);
            String message = accountSettingsPage.messageError.getAttribute("textContent").replaceAll("\\s+", "");
            if(message.equals("Invalid" + numbertype + "number")){
                logger.log(LogStatus.INFO, value+" was not accepted as a valid " + numbertype + " number");
            }else{
                logger.log(LogStatus.ERROR, "Unexpected error message\n" +
                        "Expected: Invalid " + numbertype + " number\n" +
                        "Result: " + message);
            }
        }catch (Exception e){
            logger.log(LogStatus.ERROR, value + " was accepted as a valid " + numbertype + " number");
        }
    }

    private void emailCheckValid(WebAccountSettingsPage accountSettingsPage, WebElement emailfield, String value){
        setText(emailfield,value);
        click(accountSettingsPage.saveButtonAccountDetails);
        try {
            WaitForElement(accountSettingsPage.messagePopup, 10);
            String message = accountSettingsPage.messagePopup.getAttribute("textContent").replaceAll("\\s+", "");
            if (message.equals("Datasuccessfullysaved.")) {
                logger.log(LogStatus.INFO, value + " was accepted as a valid email address");
            } else {
                logger.log(LogStatus.ERROR, "Unexpected confirmation message\n" +
                        "Expected: Data successfully saved\n" +
                        "Received: " + message);
            }
        }catch (Exception e){
            logger.log(LogStatus.ERROR, value + " was not accepted as a valid email address");
        }
    }

    private void emailCheckInValid(WebAccountSettingsPage accountSettingsPage, WebElement emailfield, String value){
        setText(emailfield, value);
        click(accountSettingsPage.saveButtonAccountDetails);
        try {
            WaitForElement(accountSettingsPage.messageError, 10);
            String message = accountSettingsPage.messageError.getAttribute("textContent").replaceAll("\\s+", "");
            if (message.equals("Pleaseprovideavalidemailaddress")) {
                logger.log(LogStatus.INFO, value + " was not accepted as a valid email address");
            } else {
                logger.log(LogStatus.ERROR, "Unexpected error message\n" +
                        "Expected: Please provide a valid email address" +
                        "Result: " + message);
            }
        }catch (Exception e){
            logger.log(LogStatus.ERROR, value +" was accepted as a valid email address]");
        }
    }

    private void quotaCheckValid(WebAccountSettingsPage accountSettingsPage, WebElement quotafield, String value){
        setText(quotafield, value);
        click(accountSettingsPage.saveButtonAccountDetails);
        try {
            WaitForElement(accountSettingsPage.messagePopup, 10);
            String message = accountSettingsPage.messagePopup.getAttribute("textContent").replaceAll("\\s+", "");
            if (message.equals("Datasuccessfullysaved.")) {
                logger.log(LogStatus.INFO, value + " was accepted as a valid quota warning value");
            } else {
                logger.log(LogStatus.ERROR, "Unexpected confirmation message\n" +
                        "Expected: Data successfully saved\n" +
                        "Received: " + message);
            }
        }catch (Exception e){
            logger.log(LogStatus.ERROR, value + " was not accepted as a valid quota warning value");
        }
    }

    private void quotaCheckInValid(WebAccountSettingsPage accountSettingsPage, WebElement quotafield, String value){
        setText(quotafield, value);
        click(accountSettingsPage.saveButtonAccountDetails);
        try {
            WaitForElement(accountSettingsPage.messageError, 10);
            String message = accountSettingsPage.messageError.getAttribute("textContent").replaceAll("\\s+", "");
            if (message.equals("Quotawarningmustbeonlydigits")) {
                logger.log(LogStatus.INFO, value + " was not accepted as a valid quota warning value");
            } else {
                logger.log(LogStatus.ERROR, "Unexpected error message\n" +
                        "Expected: Quota warning must be only digits" +
                        "Result: " + message);
            }
        }catch (Exception e){
            logger.log(LogStatus.ERROR, value + " was accepted as a valid quota warning value");
        }
    }
}
