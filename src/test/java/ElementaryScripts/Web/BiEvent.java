package ElementaryScripts.Web;

import Csv.CsvHandler;
import DataBaseFramework.SqlMySqlMiddleware;
import ElementaryScripts.General.AbstractCatIdElementaryScript;
import Screens.*;
import com.relevantcodes.extentreports.LogStatus;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class BiEvent extends AbstractCatIdElementaryScript{
    private CsvHandler csvWriteHandler;
    private static String REGISTRATION = "Registration";
    private static List<String> listIdentifiers;
    private static String MODIFY = "Modify";
    private static List<String>  listAnimalID;
    private static int numberRegistration = 1;
    public void run() throws Exception {
        try {
            //Init CSV result file
            initCsvResultOutputFile();

            listIdentifiers = new ArrayList<String>();
            listAnimalID = new ArrayList<String>();

            //BI Registration
            registrationBiTestKnownUser();
            registrationBiTestNewUser();
            //BI Modify

            //The modify event on the BI need to be tested on several itteration, because several fields raise the same event...
            modifiyBiTestFirstBasicAttribute();
            //Those tests are focusing on identifier and passport events
            modifyBiTestAddNewIdentifierAndPassport();
            modifyBiTestUpdateIdentifier();
            modifyBiTestAddPassport();
            for(int i = 0; i < 4; i++){ //To test the cat transfer between several region
                modifyBiTestFirstOwnerInfoUpdte();
            }
            modifyBiTestChangeResponsible();

        }catch (Exception e) {
            logger.logScreenshot(LogStatus.ERROR, "ERROR stopping the test", getScreenShot());
            throw e;
        }
    }

    private void initCsvResultOutputFile(){
        csvWriteHandler = new CsvHandler(logger);
        csvWriteHandler.setCsvPath(USER_DIR + "/BiOutputTest.csv");
        ArrayList<String> colunmList =  new ArrayList<String>();
        colunmList.add("GUI flow (registration or modify)");
        colunmList.add("Is new owner (new in DB)");
        colunmList.add("Owner's RRN or HK number");
        colunmList.add("Cat Identifier");
        colunmList.add("Cat animal ID");
        colunmList.add("GUI Action (field modified)");
        colunmList.add("GUI Action date and hour");
        colunmList.add("BI Action type");
        colunmList.add("BI label");
        colunmList.add("BI raw data");
        colunmList.add("BI entityID");
        colunmList.add("BI received date (\"receivedOn\")");

        try {
            csvWriteHandler.writeNextRow(colunmList);
        } catch (Exception e){
            logger.logScreenshot(LogStatus.ERROR, "ERROR during CSV init " + e.getMessage(), getScreenShot());
        }
    }

    private void collectBiData(String flow, Boolean isNewOwner, String RRN, String identifier, String animalId, List<String> actionList, ArrayList<HashMap> rs){
        for(int i = 0; i < rs.size(); i++) {
            ArrayList<String> resultData =  new ArrayList<String>();
            //GUI flow (registration or modify)
            resultData.add(flow);
            //Is new owner (new in DB)
            resultData.add(isNewOwner.toString());
            //Owner RRN
            resultData.add(RRN);
            //Cat Identifier
            resultData.add(identifier);

            //Cat animal Id
            resultData.add(animalId);

            //GUI Action (field modified)
            String actionType = (String) rs.get(i).get("label");
            if( flow.equals(REGISTRATION)){
                String actionString = "";
                if(actionList.size() > 0){
                    for (String str : actionList) {
                        actionString = actionString + str + "| ";
                    }
                }
                if (actionType.equals("CREATE_REQUEST")) {
                    resultData.add(actionString);
                } else {
                    resultData.add("No GUI action expected");
                }
            } else { //MODIFY
                if (actionType.equals("CREATE_REQUEST")) {
                    resultData.add("No GUI action expected");
                } else {
                    resultData.add(actionList.get(i));
                }

            }


            //GUI Action date and hour
            String timeStamp = new SimpleDateFormat("dd-MM-yyyy:HH-mm").format(new Date());
            resultData.add(timeStamp);

            //BI Action type

            resultData.add(actionType);

            //BI label
            String data = (String)rs.get(i).get("data");
            if (actionType.equals("CREATE_REQUEST")) {
                resultData.add("");
            } else {
                //Extract the action type if needed
                Pattern pattern = Pattern.compile("<actionType>(.*)</actionType>");
                Matcher matcher = pattern.matcher(data);
                if(matcher.find()){
                    resultData.add(matcher.group(1));
                } else {
                    logger.log(LogStatus.ERROR,"ERROR: No action type found in raw data");
                    resultData.add("");
                }
            }

            //BI raw data
            resultData.add(data);

            //BI entityID
            resultData.add((String)rs.get(i).get("entityId"));

            //BI received date
            String nonFormattedStringDate = "\"" +(String)rs.get(i).get("receivedOn") + "\"";  //In order to avoid miss interpretation on Excel ... by default
            resultData.add(nonFormattedStringDate);

            try {
                csvWriteHandler.writeNextRow(resultData);
            } catch (Exception e) {
                logger.logScreenshot(LogStatus.ERROR, "ERROR collect CSV data " + e.getMessage(), getScreenShot());
            }
        }
    }

    private void triggerBiEventToLandingZone(){
        sleep(5000);
        //Serge makes the trigger automatic from now
        WebBiTriggerPage triggerPage = new WebBiTriggerPage(driver);
        this.setBaseUrl("http://192.168.17.21:8080/bi-lz/ui/service/schedules");
        //http://192.168.17.21:8080/scheduler/service/scheduler/jobs/BI.dbmessages
        OpenBaseURL();
        driver.manage().window().maximize();
        WaitForElement(triggerPage.runButton,60);
        click(triggerPage.runButton);
        WaitForElement(triggerPage.endBiMessageTrigger,60);
    }

    private void relogin(){

        try {
            logger.log(LogStatus.INFO,"Re-login to CatID page");
            OpenHomepage openHomepage = new OpenHomepage();
            openHomepage.setScreenshotFolderLocation(USER_DIR);
            openHomepage.setDriver(driver);
            openHomepage.setLogger(logger);
            //The eaazyID session is still open session is still open, in theory no need to relogin
//            Login loginPopup = new Login();
//            loginPopup.setScreenshotFolderLocation(USER_DIR);
//            loginPopup.setUserRole(registrator);
//            loginPopup.setDriver(driver);
//            loginPopup.setLogger(logger);
            logger.log(LogStatus.INFO,"Opening home page");
            openHomepage.run();
//            logger.log(LogStatus.INFO,"Login to CatID page");
//            loginPopup.run();
        } catch (Exception e) {
            logger.logScreenshot(LogStatus.ERROR, "ERROR during relogin " + e.getMessage(), getScreenShot());
        }
    }

    //The modify event on the BI need to be tested on several iterations, because several fields raise the same event...
    private void modifiyBiTestFirstBasicAttribute() throws Exception{
        try{
            WebStartPage startPage = new WebStartPage(driver);
            WebModifyPage modifyPage = new WebModifyPage(driver);
            SqlMySqlMiddleware mySql = new SqlMySqlMiddleware(logger, "bus-bi-test", "MweneZWLsmKDa55T", "jdbc:mysql://192.168.1.9:3306");
            relogin();
            String RRN;
            HashMap<String,String> actionTypeInput = new HashMap<String, String>();

            if(listIdentifiers.size() > 0 && listAnimalID.size() > 0){
                String identifier = "123456789" + listIdentifiers.get(0);
                String catAnimalId = listAnimalID.get(0);
                //Open modify page
                click(startPage.modifyTabButton);
                setText(modifyPage.searchField,identifier);
                click(modifyPage.buttonSearch);

                //Change idenfiier - ANIMAL_IDENTIFIER + IDENTIFIER_STATUS
//                fillCatIdentifier(modifyPage.firstCatIdentifierVet);
//                actionTypeInput.put("ANIMAL_IDENTIFIER","identifier");
//                actionTypeInput.put("IDENTIFIER_STATUS","identifier");

                //Change cat name - ANIMAL_NAME
                setText(modifyPage.catName,"Midnight");
                actionTypeInput.put("ANIMAL_NAME","nameCatTextField");

                //Change fur - ANIMAL_COAT
                setText(modifyPage.catColor,"Pink");
                actionTypeInput.put("ANIMAL_COAT","colorCatTextField");

                //Change cat status - ANIMAL_STATUS
                click(modifyPage.catVetStatusContainer);
                click(modifyPage.catStatusLostVet);
                actionTypeInput.put("ANIMAL_STATUS","catStatusField");

                //Change cat gender - ANIMAL_SEX
                //TODO ask to the dev to un-hide this element. I cannot handle it at ALL
//                Select genderDropdown = new Select(modifyPage.catGender);
//                if(genderDropdown.getFirstSelectedOption().getAttribute("textContent").equals("Male")){
//                    for(WebElement element : genderDropdown.getOptions()){
//                        System.out.println(element.getAttribute("value"));
//                    }
//                    genderDropdown.selectByVisibleText("Female");
//                    System.out.println("Toto");
//                }
//                click(modifyPage.catGenderContainer);
//                click(modifyPage.catVetGenderFemale);
//                actionTypeInput.put("ANIMAL_SEX","gender");

                //Change cat birthday - ANIMAL_BIRTHDATE
                String timeStamp = new SimpleDateFormat("dd-MM-yyyy").format(new Date());
                setText(modifyPage.catBirthDate,timeStamp);
                actionTypeInput.put("ANIMAL_BIRTHDATE","birthDate");

                //Change cat sterilization date - ANIMAL_STERILIZATION
                setText(modifyPage.stereilizedDate,timeStamp);
                actionTypeInput.put("ANIMAL_STERILIZATION","stereilizedDate");

                //Change Identification date - ANIMAL_IDENTIFIER
                setText(modifyPage.firstCatIdentificationDateVet,timeStamp);
                actionTypeInput.put("ANIMAL_IDENTIFIER","identificationDate");

                RRN = modifyPage.responsibleNationalNumber.getAttribute("value");

                //Cat breed - ANIMAL_BREED
                setText(modifyPage.catBreed,"British Shorthair");
                click(modifyPage.catBreedToolTip);
                actionTypeInput.put("ANIMAL_BREED","catBreed");

                //Submit cat modifications
                click(modifyPage.submitVetButton);
                click(modifyPage.submitSignNowButton);
                WebEazyIdPage eazyIdPage = new WebEazyIdPage(driver);
                eazyId(eazyIdPage);

                triggerBiEventToLandingZone();
                ArrayList<HashMap> rs;
                //Get the cat ANIMAL_NAME update on the landing zone linked to a specific animalID and extract the CREATE_REQUEST entityId (to get all action linked to it)
                rs = mySql.executeSqlQuery("SELECT * FROM `bus-bi-preprod`.BiEvent WHERE applicationCode = 'CatID' AND DATA LIKE '%ANIMAL_NAME%" + listAnimalID.get(0) + "%' AND LABEL = 'CREATE_ACTION' ORDER BY ID DESC LIMIT 100");
                extractAllActionForSpecificRequest(rs,mySql,actionTypeInput,RRN,identifier,catAnimalId);
            }

        } catch (Exception e) {
            logger.logScreenshot(LogStatus.ERROR, "ERROR during BI modify test", getScreenShot());
            throw e;
        }
    }

    //This test is focusing on identifier and passport events
    private void modifyBiTestAddNewIdentifierAndPassport() throws Exception{
        WebStartPage startPage = new WebStartPage(driver);
        WebModifyPage modifyPage = new WebModifyPage(driver);
        SqlMySqlMiddleware mySql = new SqlMySqlMiddleware(logger, "bus-bi-test", "MweneZWLsmKDa55T", "jdbc:mysql://192.168.1.9:3306");
        WebEazyIdPage eazyIdPage = new WebEazyIdPage(driver);
        String RRN;
        HashMap<String,String> actionTypeInput = new HashMap<String, String>();
        ArrayList<HashMap> rs;


        if(listIdentifiers.size() > 0 && listAnimalID.size() > 0){
            String identifier = "123456789" + listIdentifiers.get(0);
            String catAnimalId = listAnimalID.get(0);
            //Open modify page
            relogin();
            click(startPage.modifyTabButton);
            setText(modifyPage.searchField,identifier);
            click(modifyPage.buttonSearch);

            RRN = modifyPage.responsibleNationalNumber.getAttribute("value");

            //Add new identifier test and passport update test

            //Add new identifier - ANIMAL_IDENTIFIER & IDENTIFIER_STATUS
            click(modifyPage.addSecondCatIdentifierDropDownVet);
            fillCatIdentifier(modifyPage.addCatIdentifierFieldVet);
            String timeStamp = new SimpleDateFormat("dd-MM-yyyy").format(new Date());
            setText(modifyPage.addCatIdentifierIdentificationDateVet,timeStamp);
            identifier = "123456789" + getLastIdentifier();
            actionTypeInput.put("ANIMAL_IDENTIFIER","newIdentifier");
            actionTypeInput.put("IDENTIFIER_STATUS","newIdentifierStatus");

            //Passport update - ANIMAL_PASSPORT (if the field is eddited REMOVAL_ANIMAL_PASSPORT and then ANIMAL_PASSPORT)
            //Because on V6 the field won't be edditable anymore only the add passport is tested here
            String passportNumberDate = new SimpleDateFormat("ddMMyyyyHHmmss").format(new Date());
            String passportText = tcFeatureListNumber  + passportNumberDate;
            click(modifyPage.addCatPassportDropDownVet);
            setText(modifyPage.addCatPassportNumberFieldVet, passportText);
            actionTypeInput.put("ANIMAL_PASSPORT","newPassport");

            //Submit cat modifications
            click(modifyPage.submitVetButton);
            click(modifyPage.submitSignNowButton);
            eazyId(eazyIdPage);
            triggerBiEventToLandingZone();

            //Get the cat ANIMAL_NAME update on the landing zone linked to a specific animalID and extract the CREATE_REQUEST entityId (to get all action linked to it)
            rs = mySql.executeSqlQuery("SELECT * FROM `bus-bi-preprod`.BiEvent WHERE applicationCode = 'CatID' AND DATA LIKE '%" + identifier + "%' AND LABEL = 'CREATE_ACTION' ORDER BY ID DESC LIMIT 100");
            extractAllActionForSpecificRequest(rs,mySql,actionTypeInput,RRN,identifier,catAnimalId);
        }
    }

    private void modifyBiTestUpdateIdentifier() throws Exception{
        WebStartPage startPage = new WebStartPage(driver);
        WebModifyPage modifyPage = new WebModifyPage(driver);
        SqlMySqlMiddleware mySql = new SqlMySqlMiddleware(logger, "bus-bi-test", "MweneZWLsmKDa55T", "jdbc:mysql://192.168.1.9:3306");
        String RRN;
        HashMap<String,String> actionTypeInput = new HashMap<String, String>();
        ArrayList<HashMap> rs;
        WebEazyIdPage eazyIdPage = new WebEazyIdPage(driver);

        if(listIdentifiers.size() > 0 && listAnimalID.size() > 0){
            String identifier = "123456789" + listIdentifiers.get(0);
            String catAnimalId = listAnimalID.get(0);
            //Open modify page
            relogin();
            click(startPage.modifyTabButton);
            setText(modifyPage.searchField,identifier);
            click(modifyPage.buttonSearch);

            RRN = modifyPage.responsibleNationalNumber.getAttribute("value");

            //Update current identifier identifier test and passport update test -  ANIMAL_IDENTIFIER & IDENTIFIER_STATUS
            fillCatIdentifier(modifyPage.firstCatIdentifierVet);
            actionTypeInput.put("ANIMAL_IDENTIFIER","identifierUpdate");
            actionTypeInput.put("IDENTIFIER_STATUS","newIdentifierStatus");
            actionTypeInput.put("REMOVE_ANIMAL_IDENTIFIER","identifierUpdate");

            identifier = "123456789" + getLastIdentifier();
            //Submit cat modifications
            click(modifyPage.submitVetButton);
            click(modifyPage.submitSignNowButton);
            eazyId(eazyIdPage);

            triggerBiEventToLandingZone();

            //Get the cat ANIMAL_NAME update on the landing zone linked to a specific animalID and extract the CREATE_REQUEST entityId (to get all action linked to it)
            rs = mySql.executeSqlQuery("SELECT * FROM `bus-bi-preprod`.BiEvent WHERE applicationCode = 'CatID' AND DATA LIKE '%" + identifier + "%' AND LABEL = 'CREATE_ACTION' ORDER BY ID DESC LIMIT 100");
            extractAllActionForSpecificRequest(rs,mySql,actionTypeInput,RRN,identifier,catAnimalId);
        }
    }

    private void modifyBiTestAddPassport() throws Exception{
        WebStartPage startPage = new WebStartPage(driver);
        WebModifyPage modifyPage = new WebModifyPage(driver);
        SqlMySqlMiddleware mySql = new SqlMySqlMiddleware(logger, "bus-bi-test", "MweneZWLsmKDa55T", "jdbc:mysql://192.168.1.9:3306");
        String RRN;
        HashMap<String,String> actionTypeInput = new HashMap<String, String>();
        ArrayList<HashMap> rs;
        WebEazyIdPage eazyIdPage = new WebEazyIdPage(driver);

        if(listIdentifiers.size() > 0 && listAnimalID.size() > 0){
            String identifier = "123456789" + getLastIdentifier();
            String catAnimalId = listAnimalID.get(0);
            //Open modify page
            relogin();
            click(startPage.modifyTabButton);
            setText(modifyPage.searchField,identifier);
            click(modifyPage.buttonSearch);

            RRN = modifyPage.responsibleNationalNumber.getAttribute("value");

            //Update current identifier identifier test and passport update test -  ANIMAL_IDENTIFIER & IDENTIFIER_STATUS
            String passportNumberDate = new SimpleDateFormat("ddMMyyyyHHmmss").format(new Date());
            String passportText = tcFeatureListNumber  + passportNumberDate;
            click(modifyPage.addCatPassportDropDownVet);
            setText(modifyPage.addCatPassportNumberFieldVet, passportText);
            actionTypeInput.put("ANIMAL_PASSPORT","newPassport");

            //Submit cat modifications
            click(modifyPage.submitVetButton);
            click(modifyPage.submitSignNowButton);
            eazyId(eazyIdPage);

            triggerBiEventToLandingZone();

            //Get the cat ANIMAL_NAME update on the landing zone linked to a specific animalID and extract the CREATE_REQUEST entityId (to get all action linked to it)
            rs = mySql.executeSqlQuery("SELECT * FROM `bus-bi-preprod`.BiEvent WHERE applicationCode = 'CatID' AND DATA LIKE '%" + passportText + "%' AND LABEL = 'CREATE_ACTION' ORDER BY ID DESC LIMIT 100");
            extractAllActionForSpecificRequest(rs,mySql,actionTypeInput,RRN,identifier,catAnimalId);
        }
    }

    private void modifyBiTestFirstOwnerInfoUpdte() throws Exception{
        WebStartPage startPage = new WebStartPage(driver);
        WebModifyPage modifyPage = new WebModifyPage(driver);
        SqlMySqlMiddleware mySql = new SqlMySqlMiddleware(logger, "bus-bi-test", "MweneZWLsmKDa55T", "jdbc:mysql://192.168.1.9:3306");
        String RRN;
        HashMap<String,String> actionTypeInput = new HashMap<String, String>();
        ArrayList<HashMap> rs;

        if(listIdentifiers.size() > 0 && listAnimalID.size() > 0){
            String identifier = "123456789" + getLastIdentifier();
            String catAnimalId = listAnimalID.get(0);
            //Open modify page
            relogin();
            click(startPage.modifyTabButton);
            setText(modifyPage.searchField,identifier);
            click(modifyPage.buttonSearch);

            RRN = modifyPage.responsibleNationalNumber.getAttribute("value");

            //Update owner's address and information - ACTOR_ADDRESS & ACTOR_INFORMATION
            if(modifyPage.responsibleStreet.getAttribute("value").contains("1000 Bruxelles")){ // Bruxelles --> Flandre
                setText(modifyPage.responsibleCity,"2000 Antwerpen");
                setText(modifyPage.responsibleStreet,"Leopoldstraat");
                setText(modifyPage.responsibleStreetNumber,"12");
                setText(modifyPage.responsiblePhone,"12345678");
                setText(modifyPage.responsibleFax,"12345678");
                setText(modifyPage.responsibleMobile,"12345678");
                setText(modifyPage.responsibleMail,"alice.specimen@zetes.com");

            } else if(modifyPage.responsibleStreet.getAttribute("value").contains("1000 Antwerpen")){ // Flandre --> Flandre
                setText(modifyPage.responsibleCity,"9100 Sint-Niklaas");
                setText(modifyPage.responsibleStreet,"Nobels-Peelmanstraat");
                setText(modifyPage.responsibleStreetNumber,"13");
                setText(modifyPage.responsiblePhone,"789455");
                setText(modifyPage.responsibleFax,"789455");
                setText(modifyPage.responsibleMobile,"789455");
                setText(modifyPage.responsibleMail,"specimen@zetes.com");
            } else if(modifyPage.responsibleStreet.getAttribute("value").contains("9100 Sint-Niklaas")){ // Flandre --> Wallonie
                setText(modifyPage.responsibleCity,"5000 Namur");
                setText(modifyPage.responsibleStreet,"Rue De Fer");
                setText(modifyPage.responsibleStreetNumber,"14");
                setText(modifyPage.responsiblePhone,"12345671");
                setText(modifyPage.responsibleFax,"12345671");
                setText(modifyPage.responsibleMobile,"12345671");
                setText(modifyPage.responsibleMail,"a.specimen@zetes.com");
            } else { // ??? --> Bruxelles (in theory Wallonie --> Bruxelles)
                setText(modifyPage.responsibleCity,"1000 Bruxelles");
                setText(modifyPage.responsibleStreet,"Chaussee D'ixelles");
                setText(modifyPage.responsibleStreetNumber,"64");
                setText(modifyPage.responsiblePhone,"789456");
                setText(modifyPage.responsibleFax,"789456");
                setText(modifyPage.responsibleMobile,"789456");
                setText(modifyPage.responsibleMail,"alice@zetes.com");
            }
            actionTypeInput.put("ACTOR_ADDRESS","addressZip_Street_StreetNumber");
            actionTypeInput.put("ACTOR_INFORMATION","phone_Fax_Mobile_Email");

            //Submit cat modifications
            click(modifyPage.submitVetButton);
            click(modifyPage.submitSignNowButton);

            triggerBiEventToLandingZone();

            //Get the cat ANIMAL_NAME update on the landing zone linked to a specific animalID and extract the CREATE_REQUEST entityId (to get all action linked to it)
            rs = mySql.executeSqlQuery("SELECT * FROM `bus-bi-preprod`.BiEvent WHERE applicationCode = 'CatID' AND DATA LIKE '%ACTOR_INFORMATION%" + RRN + "%' AND LABEL = 'CREATE_ACTION' ORDER BY ID DESC LIMIT 100");
            extractAllActionForSpecificRequest(rs,mySql,actionTypeInput,RRN,identifier,catAnimalId);
        }
    }

    private void modifyBiTestChangeResponsible() throws Exception{
        WebStartPage startPage = new WebStartPage(driver);
        WebModifyPage modifyPage = new WebModifyPage(driver);
        SqlMySqlMiddleware mySql = new SqlMySqlMiddleware(logger, "bus-bi-test", "MweneZWLsmKDa55T", "jdbc:mysql://192.168.1.9:3306");
        String RRN;
        HashMap<String,String> actionTypeInput = new HashMap<String, String>();
        ArrayList<HashMap> rs;
        WebEazyIdPage eazyIdPage = new WebEazyIdPage(driver);

        if(listIdentifiers.size() > 0 && listAnimalID.size() > 0){
            String identifier = "123456789" + getLastIdentifier();
            String catAnimalId = listAnimalID.get(0);
            //Open modify page
            relogin();
            click(startPage.modifyTabButton);
            setText(modifyPage.searchField,identifier);
            click(modifyPage.buttonSearch);

            //Change cat responsible - ANIMAL_RESPONSIBLE
            click(modifyPage.changeResponsibleDropDown);
            setText(modifyPage.changeResponsibleHkRefNumSecondSearchField,"HK4321");
            click(modifyPage.changeResponsibleHkRefNumSecondSearchTooltip);
            actionTypeInput.put("ANIMAL_RESPONSIBLE","changeResponsible");

            RRN = "HK43214321";

            //Submit cat modifications
            click(modifyPage.submitVetButton);
            click(modifyPage.submitSignNowButton);
            eazyId(eazyIdPage);

            triggerBiEventToLandingZone();

            //Get the cat ANIMAL_NAME update on the landing zone linked to a specific animalID and extract the CREATE_REQUEST entityId (to get all action linked to it)
            rs = mySql.executeSqlQuery("SELECT * FROM `bus-bi-preprod`.BiEvent WHERE applicationCode = 'CatID' AND DATA LIKE '%ANIMAL_RESPONSIBLE%" + catAnimalId + "%' AND LABEL = 'CREATE_ACTION' ORDER BY ID DESC LIMIT 100");
            extractAllActionForSpecificRequest(rs,mySql,actionTypeInput,RRN,identifier,catAnimalId);
        }
    }

    private void extractAllActionForSpecificRequest(ArrayList<HashMap> rs, SqlMySqlMiddleware mySql, HashMap<String,String> actionTypeInput, String RRN, String identifier, String catAnimalId) throws Exception{
        String entityId;
        ArrayList<String> actionList;
        if(rs.size() > 0){
            Pattern pattern = Pattern.compile("<requestId>(.*)</requestId>");
            Matcher matcher = pattern.matcher((String) rs.get(0).get("data"));
            if (matcher.find()) {
                entityId = matcher.group(1);

                //Get all actionType
                rs = mySql.executeSqlQuery("SELECT * FROM `bus-bi-preprod`.BiEvent WHERE applicationCode = 'CatID' AND DATA LIKE '%" + entityId + "%'  ORDER BY ID DESC LIMIT 100");
                int expectedNumber = actionTypeInput.size(); //Depending of the GUI modification implemented
                if (rs.size() == expectedNumber) {
                    logger.log(LogStatus.INFO, "The number of CREATE_ACTION linked to CREATE_REQUEST " + entityId);
                } else {
                    logger.log(LogStatus.ERROR, "ERROR: Incorrect number of CREATE_ACTION detected found: " + rs.size() + " expected: " + expectedNumber);
                }
                actionList = new ArrayList<String>();
                for(HashMap actionTypeMap : rs){
                    pattern = Pattern.compile("<actionType>(.*)</actionType>");
                    matcher = pattern.matcher((String) actionTypeMap.get("data"));
                    if (matcher.find() && actionTypeInput.containsKey(matcher.group(1))) {
                        //Save the CREATE_ACTION discover for the entityId wanted
                        actionList.add(actionTypeInput.get(matcher.group(1)));

                    } else {
                        actionList.add("ERROR:  No gui action found for Label " + matcher.group(1));
                        logger.log(LogStatus.ERROR, "ERROR:  No gui action found for Label " + matcher.group(1));
                    }
                }
                collectBiData(MODIFY, false, RRN, identifier, catAnimalId, actionList, rs);

                //Get the Original request linked to the identifier
                rs = mySql.executeSqlQuery("SELECT * FROM `bus-bi-preprod`.BiEvent WHERE applicationCode = 'CatID' AND ENTITYID = '" + entityId + "' AND LABEL = 'CREATE_REQUEST'  ORDER BY ID DESC LIMIT 100");
                if (rs.size() == 1) {
                    logger.log(LogStatus.INFO, "The CREATE_REQUEST with ID: " + entityId + " is created");
                    actionList = new ArrayList<String>();
                    actionList.add("");
                    collectBiData(MODIFY, false, RRN, identifier, catAnimalId, actionList, rs);
                } else {
                    logger.log(LogStatus.ERROR, "ERROR: DB statement for CREATE_REQUEST failed");
                }
            } else {
                logger.log(LogStatus.ERROR, "ERROR: No entityId found");
            }
        } else {
            logger.log(LogStatus.ERROR,"ERROR: no modification found for animalID " + listAnimalID.get(0) + " and new identifier " + identifier);
        }

    }

    private void registrationBiTestKnownUser() throws Exception{
        try{
            WebStartPage startPage = new WebStartPage(driver);
            WebRegisterPage registerPage = new WebRegisterPage(driver);
            Pattern pattern;
            Matcher matcher;
            SqlMySqlMiddleware mySql = new SqlMySqlMiddleware(logger, "bus-bi-test", "MweneZWLsmKDa55T", "jdbc:mysql://192.168.1.9:3306");
            HashMap<String,HashMap<String,String>> registrationInfo = new HashMap<String,HashMap<String,String>>();
            for(int i = 0; i < numberRegistration; i++){
                Date pastDate;
                Calendar calendar = Calendar.getInstance();
                calendar.add(Calendar.DATE, -2);
                pastDate = calendar.getTime();

                HashMap<String,String> registrationReturnValue = registerNewVetCatToPro(registerPage, startPage, "71715100070", false, pastDate, true);
                listIdentifiers.add(getLastIdentifier());
                registrationInfo.put(getLastIdentifier(), registrationReturnValue);
            }

            triggerBiEventToLandingZone();
            ArrayList<HashMap> rs;
            String entityId;

            //BI Registration
            for(String identifier : listIdentifiers){
                ArrayList<HashMap> collectDataList =  new ArrayList<HashMap>();
                String catAnimalID = "";
                //Get the create action where the cat identifier occurs
                // Get Actions NEW_ANIMAL
                rs = mySql.executeSqlQuery("SELECT * FROM `bus-bi-preprod`.BiEvent WHERE applicationCode = 'CatID' AND DATA LIKE '%" + identifier + "%' AND LABEL = 'CREATE_ACTION' AND DATA LIKE '%NEW_ANIMAL%' ORDER BY ID DESC LIMIT 100");
                if (rs.size() == 1) {
                    //Prepare collect data to CSV
                    collectDataList.add(rs.get(0));
                    //Get animal ID
                    pattern = Pattern.compile("<animalID>(.*)</animalID>");
                    matcher = pattern.matcher((String)rs.get(0).get("data"));
                    if(matcher.find()){
                        catAnimalID = matcher.group(1);
                        listAnimalID.add(catAnimalID);
                        logger.log(LogStatus.INFO,"Animal ID: " + catAnimalID + " is linked to identifier: " + identifier);
                    } else {
                        logger.log(LogStatus.ERROR,"ERROR: No animalID found");
                    }
                    // Get Actions IDENTIFIER_STATUS
                    rs = mySql.executeSqlQuery("SELECT * FROM `bus-bi-preprod`.BiEvent WHERE applicationCode = 'CatID' AND DATA LIKE '%" + identifier + "%' AND LABEL = 'CREATE_ACTION' AND DATA LIKE '%IDENTIFIER_STATUS%' ORDER BY ID DESC LIMIT 100");
                    if (rs.size() == 1) {
                        collectDataList.add(rs.get(0));
                        //Get Request information
                        pattern = Pattern.compile("<requestId>(.*)</requestId>");
                        matcher = pattern.matcher((String) rs.get(0).get("data"));
                        if (matcher.find()) {
                            entityId = matcher.group(1);
                            logger.log(LogStatus.INFO, "The CREATE_ACTION: NEW_ANIMAL and IDENTIFIER_STATUS linked to " + entityId + " are created");
                            //Get the other action linked to the cat identifier and the BI entityID linked to it
                            rs = mySql.executeSqlQuery("SELECT * FROM `bus-bi-preprod`.BiEvent WHERE applicationCode = 'CatID' AND DATA LIKE '%" + entityId + "%' AND DATA LIKE '%ANIMAL_STATUS%' ORDER BY ID DESC LIMIT 100");
                            if (rs.size() == 1) { // Get Actions NEW_ANIMAL and IDENTIFIER_STATUS
                                collectDataList.add(rs.get(0));
                                logger.log(LogStatus.INFO, "The CREATE_ACTION: ANIMAL_STATUS linked to " + entityId + " is created");
                            } else {
                                logger.log(LogStatus.ERROR, "ERROR: DB statement for ANIMAL_STATUS failed");
                            }
                            //Get the Original request linked to the identifier
                            rs = mySql.executeSqlQuery("SELECT * FROM `bus-bi-preprod`.BiEvent WHERE applicationCode = 'CatID' AND ENTITYID = '" + entityId + "' AND LABEL = 'CREATE_REQUEST'  ORDER BY ID DESC LIMIT 100");
                            if (rs.size() == 1) { // Get Actions NEW_ANIMAL and IDENTIFIER_STATUS
                                collectDataList.add(rs.get(0));
                                logger.log(LogStatus.INFO, "The CREATE_REQUEST with ID: " + entityId + " is created");
                            } else {
                                logger.log(LogStatus.ERROR, "ERROR: DB statement for CREATE_REQUEST failed");
                            }
                        } else {
                            logger.log(LogStatus.ERROR, "ERROR: No entityId found");
                        }
                    }  else {
                        logger.log(LogStatus.ERROR,"ERROR: DB statement for  IDENTIFIER_STATUS failed");
                    }

                } else {
                    logger.log(LogStatus.ERROR,"ERROR: DB statement for  NEW_ANIMAL failed");
                }

                //Collect output to CSV file
                String RRN = registrationInfo.get(identifier).get("RRN");
                ArrayList<String> actionList = new ArrayList<String>();
                actionList.addAll(registrationInfo.get(identifier).keySet());
                collectBiData(REGISTRATION, false, RRN, "123456789" + identifier, catAnimalID, actionList, collectDataList);
            }

        } catch (Exception e) {
            logger.logScreenshot(LogStatus.ERROR, "ERROR during BI registration test", getScreenShot());
            throw e;
        }

    }

    private void registrationBiTestNewUser() throws Exception{
        try{
            WebStartPage startPage = new WebStartPage(driver);
            WebRegisterPage registerPage = new WebRegisterPage(driver);
            Pattern pattern;
            Matcher matcher;
            SqlMySqlMiddleware mySql = new SqlMySqlMiddleware(logger, "bus-bi-test", "MweneZWLsmKDa55T", "jdbc:mysql://192.168.1.9:3306");

            HashMap<String,HashMap<String,String>> registrationInfo = new HashMap<String,HashMap<String,String>>();
            Date pastDate;
            Calendar calendar = Calendar.getInstance();
            calendar.add(Calendar.DATE, -2);
            pastDate = calendar.getTime();

            relogin();
            HashMap<String,String> registrationReturnValue = registerNewVetCatToPro(registerPage, startPage, computeRandomRRN(), false, pastDate, true);
            int i = 0;
            while(!registrationReturnValue.containsKey("firstOwnerFirstName") && i < 5){
                //Register 5 times maximum a new cat forms
                logger.log(LogStatus.WARNING,"Recompute RRN... Current value is already used:" + registrationReturnValue.get("RRN"));
                i++;
                registrationReturnValue = registerNewVetCatToPro(registerPage, startPage, computeRandomRRN(), false, pastDate, true);
            }
            listIdentifiers.add(getLastIdentifier());
            registrationInfo.put(getLastIdentifier(), registrationReturnValue);

            triggerBiEventToLandingZone();
            ArrayList<HashMap> rs;
            String entityId;

            //BI Registration
            String identifier = getLastIdentifier();
            ArrayList<HashMap> collectDataList =  new ArrayList<HashMap>();
            String catAnimalID = "";
            //Get the create action where the cat identifier occurs
            // Get Actions NEW_ANIMAL
            rs = mySql.executeSqlQuery("SELECT * FROM `bus-bi-preprod`.BiEvent WHERE applicationCode = 'CatID' AND DATA LIKE '%" + identifier + "%' AND LABEL = 'CREATE_ACTION' AND DATA LIKE '%NEW_ANIMAL%' ORDER BY ID DESC LIMIT 100");
            if (rs.size() == 1) {
                //Prepare collect data to CSV
                collectDataList.add(rs.get(0));
                //Get animal ID
                pattern = Pattern.compile("<animalID>(.*)</animalID>");
                matcher = pattern.matcher((String)rs.get(0).get("data"));
                if(matcher.find()){
                    catAnimalID = matcher.group(1);
                    listAnimalID.add(catAnimalID);
                    logger.log(LogStatus.INFO,"Animal ID: " + catAnimalID + " is linked to identifier: " + identifier);
                } else {
                    logger.log(LogStatus.ERROR,"ERROR: No animalID found");
                }
                // Get Actions IDENTIFIER_STATUS
                rs = mySql.executeSqlQuery("SELECT * FROM `bus-bi-preprod`.BiEvent WHERE applicationCode = 'CatID' AND DATA LIKE '%" + identifier + "%' AND LABEL = 'CREATE_ACTION' AND DATA LIKE '%IDENTIFIER_STATUS%' ORDER BY ID DESC LIMIT 100");
                if (rs.size() == 1) {
                    collectDataList.add(rs.get(0));
                    //Get Request information
                    pattern = Pattern.compile("<requestId>(.*)</requestId>");
                    matcher = pattern.matcher((String) rs.get(0).get("data"));
                    if (matcher.find()) {
                        entityId = matcher.group(1);
                        logger.log(LogStatus.INFO, "The CREATE_ACTION: NEW_ANIMAL and IDENTIFIER_STATUS linked to " + entityId + " are created");
                        //Get the other action linked to the cat identifier and the BI entityID linked to it
                        rs = mySql.executeSqlQuery("SELECT * FROM `bus-bi-preprod`.BiEvent WHERE applicationCode = 'CatID' AND DATA LIKE '%" + entityId + "%' AND DATA LIKE '%ANIMAL_STATUS%' ORDER BY ID DESC LIMIT 100");
                        if (rs.size() == 1) { // Get Actions NEW_ANIMAL and IDENTIFIER_STATUS
                            collectDataList.add(rs.get(0));
                            logger.log(LogStatus.INFO, "The CREATE_ACTION: ANIMAL_STATUS linked to " + entityId + " is created");
                        } else {
                            logger.log(LogStatus.ERROR, "ERROR: DB statement for ANIMAL_STATUS failed");
                        }
                        //Get the new actor request
                        rs = mySql.executeSqlQuery("SELECT * FROM `bus-bi-preprod`.BiEvent WHERE applicationCode = 'CatID' AND DATA LIKE '%" + entityId + "%' AND DATA LIKE '%NEW_ACTOR%' ORDER BY ID DESC LIMIT 100");
                        if (rs.size() == 1) {
                            collectDataList.add(rs.get(0));
                            logger.log(LogStatus.INFO, "The CREATE_ACTION: NEW_ACTOR linked to " + entityId + " is created");
                        } else {
                            logger.log(LogStatus.ERROR, "ERROR: DB statement for NEW_ACTOR failed");
                        }

                        //Get the Original request linked to the identifier
                        rs = mySql.executeSqlQuery("SELECT * FROM `bus-bi-preprod`.BiEvent WHERE applicationCode = 'CatID' AND ENTITYID = '" + entityId + "' AND LABEL = 'CREATE_REQUEST'  ORDER BY ID DESC LIMIT 100");
                        if (rs.size() == 1) {
                            collectDataList.add(rs.get(0));
                            logger.log(LogStatus.INFO, "The CREATE_REQUEST with ID: " + entityId + " is created");
                        } else {
                            logger.log(LogStatus.ERROR, "ERROR: DB statement for CREATE_REQUEST failed");
                        }
                    } else {
                        logger.log(LogStatus.ERROR, "ERROR: No entityId found");
                    }
                }  else {
                    logger.log(LogStatus.ERROR,"ERROR: DB statement for  IDENTIFIER_STATUS failed");
                }

            } else {
                logger.log(LogStatus.ERROR,"ERROR: DB statement for  NEW_ANIMAL failed");
            }

            //Collect output to CSV file
            String RRN = registrationInfo.get(identifier).get("RRN");
            ArrayList<String> actionList = new ArrayList<String>();
            actionList.addAll(registrationInfo.get(identifier).keySet());
            if(registrationReturnValue.containsKey("firstOwnerFirstName")){
                collectBiData(REGISTRATION, true, RRN, "123456789" + identifier, catAnimalID, actionList, collectDataList);
                logger.log(LogStatus.INFO,"New user with random RRN is successfully registered");
            } else {
                collectBiData(REGISTRATION, false, RRN, "123456789" + identifier, catAnimalID, actionList, collectDataList);
                logger.log(LogStatus.ERROR,"The RRN computed was not new ...");
            }

        } catch (Exception e) {
            logger.logScreenshot(LogStatus.ERROR, "ERROR during BI registration test", getScreenShot());
            throw e;
        }

    }
}
