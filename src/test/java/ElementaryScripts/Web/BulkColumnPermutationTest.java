package ElementaryScripts.Web;

import Screens.WebStartPage;
import com.relevantcodes.extentreports.LogStatus;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

public class BulkColumnPermutationTest extends BulkRegistrationColumnTest {
    public void run() throws Exception{
        try {
            WebStartPage startPage = new WebStartPage(driver);
            if(registrator.contains("Refuge")){
                click(startPage.bulkTabRefugeButton);
            } else {
                click(startPage.bulkTabVetAndBreederButton);
            }

            HashMap<String,Block> blockMap ;
            //First registration
            blockMap = initBlockMap();
            sendBlocks("BulkFirstBlockRegistration",blockMap.get("First"),blockMap.get("Second"),blockMap.get("Third"));

            //Second registration
            blockMap = initBlockMap();
            sendBlocks("BulkSecondBlockRegistration",blockMap.get("Second"),blockMap.get("Third"),blockMap.get("First"));

            //Third registration
            blockMap = initBlockMap();
            sendBlocks("BulkThirdBlockRegistration",blockMap.get("Third"),blockMap.get("First"),blockMap.get("Second"));

            click(startPage.logoutButton);
        } catch (Exception e) {
            logger.logScreenshot(LogStatus.ERROR, "ERROR stopping the test", getScreenShot());
            throw e;
        }
    }

    protected HashMap<String,Block> initBlockMap() throws Exception {
        HashMap<String,Block> mapBlock = new HashMap<String, Block>();
        //Block 1
        //First registration
        ArrayList<String> firstInitBlockList = new ArrayList<String>();
        ArrayList<String> firstAddingRowBlockList = new ArrayList<String>();
        firstInitBlockList.add("identification_date");
        firstInitBlockList.add("confidential_flag");
        firstInitBlockList.add("name");
        firstInitBlockList.add("breed");
        firstInitBlockList.add("crossing");
        firstInitBlockList.add("fur_tc");
        firstInitBlockList.add("birth_date");
        firstInitBlockList.add("sterilized");
        firstInitBlockList.add("sterilization_date");
        firstInitBlockList.add("gender");
        firstInitBlockList.add("id-1:location");
        firstInitBlockList.add("id-1:value");
        firstInitBlockList.add("id-2:location");
        firstInitBlockList.add("id-2:value");
        firstInitBlockList.add("passport_nr");

        //identification_date
        String timeStamp = new SimpleDateFormat("dd/MM/yyyy").format(new Date());
        firstAddingRowBlockList.add(timeStamp);
        //confidential_flag
        firstAddingRowBlockList.add("0");
        //name
        firstAddingRowBlockList.add("Minuit");
        //breed
        firstAddingRowBlockList.add("Abyssinian");
        //crossing
        firstAddingRowBlockList.add("0");
        //fur_tc
        firstAddingRowBlockList.add("black");
        //birth_date
        firstAddingRowBlockList.add(timeStamp);
        //sterilized
        firstAddingRowBlockList.add("1");
        //sterilization_date
        firstAddingRowBlockList.add(timeStamp);
        //gender
        firstAddingRowBlockList.add("M");
        //id-1:location
        firstAddingRowBlockList.add("5");
        //id-1:value
        int lastIdentifierInteger = Integer.parseInt(this.getLastIdentifier());
        int firstNewIdentifier = lastIdentifierInteger + 1;
        String newFirstIdentifierString = "123456789" + String.valueOf(firstNewIdentifier);
        firstAddingRowBlockList.add(newFirstIdentifierString);
        addNewIdentifier(String.valueOf(firstNewIdentifier));
        //id-2:location
        //colunmList.add("5");
        firstAddingRowBlockList.add("");
        //id-2:value
        //int secondNewIdentifier = firstNewIdentifier + 1;
        //String newSecondIdentifierString = "123456789" + String.valueOf(secondNewIdentifier);
        //colunmList.add(newSecondIdentifierString);
        //addNewIdentifier(String.valueOf(secondNewIdentifier));
        firstAddingRowBlockList.add("");
        //passport_nr
        firstAddingRowBlockList.add("");

        //Block 2
        ArrayList<String> secondInitBlockList =  new ArrayList<String>();
        ArrayList<String> secondAddingRowBlockList =  new ArrayList<String>();
        secondInitBlockList.add("resp-1:hk-nr");
        secondInitBlockList.add("resp-1:rrn");
        secondInitBlockList.add("resp-1:first_name");
        secondInitBlockList.add("resp-1:last_name");
        secondInitBlockList.add("resp-1:postal_code");
        secondInitBlockList.add("resp-1:municipality");
        secondInitBlockList.add("resp-1:street");
        secondInitBlockList.add("resp-1:number");
        secondInitBlockList.add("resp-1:phone");
        secondInitBlockList.add("resp-1:mobile");
        secondInitBlockList.add("resp-1:fax");
        secondInitBlockList.add("resp-1:email");

        //resp-1:hk-nr
        secondAddingRowBlockList.add("");
        //resp-1:rrn
        secondAddingRowBlockList.add("71715100070");
        //resp-1:first_name
        secondAddingRowBlockList.add("Alice Geldigekaart");
        //resp-1:last_name
        secondAddingRowBlockList.add("SPECIMEN");
        //resp-1:postal_code
        secondAddingRowBlockList.add("1000");
        //resp-1:municipality
        secondAddingRowBlockList.add("Bruxelles");
        //resp-1:street
        secondAddingRowBlockList.add("Chaussee D'ixelles");
        //resp-1:number
        secondAddingRowBlockList.add("64");
        //resp-1:phone
        secondAddingRowBlockList.add("789456");
        //resp-1:mobile
        secondAddingRowBlockList.add("789456");
        //resp-1:fax
        secondAddingRowBlockList.add("789456");
        //resp-1:email
        secondAddingRowBlockList.add("alice@zetes.com");

        //Block 3
        ArrayList<String> thirdInitBlockList =  new ArrayList<String>();
        ArrayList<String> thirdAddingRowBlockList =  new ArrayList<String>();
        thirdInitBlockList.add("resp-2:transfer_date");
        thirdInitBlockList.add("resp-2:hk-nr");
        thirdInitBlockList.add("resp-2:rrn");
        thirdInitBlockList.add("resp-2:first_name");
        thirdInitBlockList.add("resp-2:last_name");
        thirdInitBlockList.add("resp-2:postal_code");
        thirdInitBlockList.add("resp-2:municipality");
        thirdInitBlockList.add("resp-2:street");
        thirdInitBlockList.add("resp-2:number");
        thirdInitBlockList.add("resp-2:phone");
        thirdInitBlockList.add("resp-2:mobile");
        thirdInitBlockList.add("resp-2:fax");
        thirdInitBlockList.add("resp-2:email");
        thirdInitBlockList.add("vetid");

        //resp-2:transfer_date
        thirdAddingRowBlockList.add(timeStamp);
        //resp-2:hk-nr
        thirdAddingRowBlockList.add("");
        //resp-2:rrn
        thirdAddingRowBlockList.add("89041952724");
        //resp-2:first_name
        thirdAddingRowBlockList.add("Thierry");
        //resp-2:last_name
        thirdAddingRowBlockList.add("Foulon");
        //resp-2:postal_code
        thirdAddingRowBlockList.add("5004");
        //resp-2:municipality
        thirdAddingRowBlockList.add("Bouge");
        //resp-2:street
        thirdAddingRowBlockList.add("Chaussée De Louvain");
        //resp-2:number
        thirdAddingRowBlockList.add("222");
        //resp-2:phone
        thirdAddingRowBlockList.add("0810834585");
        //resp-2:mobile
        thirdAddingRowBlockList.add("0810834585");
        //resp-2:fax
        thirdAddingRowBlockList.add("0810834585");
        //esp-2:email
        thirdAddingRowBlockList.add("thierry.foulon@zetes.com");
        //vetid
        String vetID = "";
        if (registrator.equals("Veterinary 2")) {
            vetID = "F0024";
        } else if (registrator.equals("Veterinary 3")) {
            vetID = "F1560";
        } else {
            vetID = "F0032";
        }
        thirdAddingRowBlockList.add(vetID);
        Block first = new Block();
        Block second = new Block();
        Block third = new Block();

        first.initBlock.addAll(firstInitBlockList);
        first.addingRowBlock.addAll(firstAddingRowBlockList);
        mapBlock.put("First",first);
        second.initBlock.addAll(secondInitBlockList);
        second.addingRowBlock.addAll(secondAddingRowBlockList);
        mapBlock.put("Second",second);
        third.initBlock.addAll(thirdInitBlockList);
        third.addingRowBlock.addAll(thirdAddingRowBlockList);
        mapBlock.put("Third",third);
        return mapBlock;

    }

    protected void sendBlocks(String csvFile, Block first, Block second, Block third) throws Exception {
        ArrayList<String> firstInitList =  new ArrayList<String>();
        ArrayList<String> firstAddList =  new ArrayList<String>();
        firstInitList.addAll(first.initBlock);
        firstInitList.addAll(second.initBlock);
        firstInitList.addAll(third.initBlock);
        initCsvInputFile(csvFile, firstInitList);
        firstAddList.addAll(first.addingRowBlock);
        firstAddList.addAll(second.addingRowBlock);
        firstAddList.addAll(third.addingRowBlock);
        addBulkRegistrationRow(firstAddList);
        sendCsvFile(true);
    }

    private class Block{
        protected ArrayList<String> initBlock;
        protected ArrayList<String> addingRowBlock;
        protected Block(){
            initBlock = new ArrayList<String>();
            addingRowBlock = new ArrayList<String>();
        }

        protected void clear(){
            initBlock.clear();
            addingRowBlock.clear();
        }
    }

    protected void addBulkRegistrationRow( ArrayList<String> colunmList) throws Exception{
        try {
            csvWriteHandler.writeNextRow(colunmList);
        } catch (Exception e){
            logger.logScreenshot(LogStatus.ERROR, "ERROR during CSV add rows " + e.getMessage(), getScreenShot());
        }
    }
}
