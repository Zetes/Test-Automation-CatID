package ElementaryScripts.Web;


import Csv.CsvHandler;
import com.relevantcodes.extentreports.LogStatus;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public abstract class BulkRegistrationColumnTest extends BulkTest {

    protected void addBulkRegistrationRow(ResponsibleInfo firstResponsibleInfo, ResponsibleInfo newResponsibleInfo, boolean isNewResponsibleTransfer) throws Exception{
        csvWriteHandler = new CsvHandler(logger,';');
        if("".equals(this.csvFileName)){
            throw new Exception("Csv file name is empty");
        }
        csvWriteHandler.setCsvPath(USER_DIR + "/" + csvFileName + ".csv");
        ArrayList<String> colunmList =  new ArrayList<String>();
        String timeStamp = new SimpleDateFormat("dd/MM/yyyy").format(new Date());
        //identification_date
        colunmList.add(timeStamp);
        //confidential_flag
        colunmList.add("0");
        //name
        colunmList.add("Minuit");
        //breed
        colunmList.add("Abyssinian");
        //crossing
        colunmList.add("0");
        //fur_tc
        colunmList.add("black");
        //birth_date
        colunmList.add(timeStamp);
        //sterilized
        colunmList.add("1");
        //sterilization_date
        colunmList.add(timeStamp);
        //gender
        colunmList.add("M");
        //id-1:location
        colunmList.add("5");
        //id-1:value
        int lastIdentifierInteger = Integer.parseInt(this.getLastIdentifier());
        int firstNewIdentifier = lastIdentifierInteger + 1;
        String newFirstIdentifierString = "123456789" + String.valueOf(firstNewIdentifier);
        colunmList.add(newFirstIdentifierString);
        addNewIdentifier(String.valueOf(firstNewIdentifier));
        //id-2:location
        //colunmList.add("5");
        colunmList.add("");
        //id-2:value
        //int secondNewIdentifier = firstNewIdentifier + 1;
        //String newSecondIdentifierString = "123456789" + String.valueOf(secondNewIdentifier);
        //colunmList.add(newSecondIdentifierString);
        //addNewIdentifier(String.valueOf(secondNewIdentifier));
        colunmList.add("");
        //passport_nr
        colunmList.add("");
        //resp-1:hk-nr
        colunmList.add(firstResponsibleInfo.responsibleAccreditationNumber);
        //resp-1:rrn
        colunmList.add(firstResponsibleInfo.responsibleRRN);
        //resp-1:first_name
        colunmList.add(firstResponsibleInfo.responsibleFirstName);
        //resp-1:last_name
        colunmList.add(firstResponsibleInfo.responsibleLastName);
        //resp-1:postal_code
        colunmList.add(firstResponsibleInfo.responsibleZipCode);
        //resp-1:municipality
        colunmList.add(firstResponsibleInfo.responsibleMunicipality);
        //resp-1:street
        colunmList.add(firstResponsibleInfo.responsibleStreetName);
        //resp-1:number
        colunmList.add(firstResponsibleInfo.responsibleStreetNumber);
        //resp-1:phone
        colunmList.add(firstResponsibleInfo.responsiblePhone);
        //resp-1:mobile
        colunmList.add(firstResponsibleInfo.responsibleMobile);
        //resp-1:fax
        colunmList.add(firstResponsibleInfo.responsibleFax);
        //resp-1:email
        colunmList.add(firstResponsibleInfo.responsibleEMail);
        if(isNewResponsibleTransfer) {
            //resp-2:transfer_date
            colunmList.add(timeStamp);
            //resp-2:hk-nr
            colunmList.add(newResponsibleInfo.responsibleAccreditationNumber);
            //resp-2:rrn
            colunmList.add(newResponsibleInfo.responsibleRRN);
            //resp-2:first_name
            colunmList.add(newResponsibleInfo.responsibleFirstName);
            //resp-2:last_name
            colunmList.add(newResponsibleInfo.responsibleLastName);
            //resp-2:postal_code
            colunmList.add(newResponsibleInfo.responsibleZipCode);
            //resp-2:municipality
            colunmList.add(newResponsibleInfo.responsibleMunicipality);
            //resp-2:street
            colunmList.add(newResponsibleInfo.responsibleStreetName);
            //resp-2:number
            colunmList.add(newResponsibleInfo.responsibleStreetNumber);
            //resp-2:phone
            colunmList.add(newResponsibleInfo.responsiblePhone);
            //resp-2:mobile
            colunmList.add(newResponsibleInfo.responsibleMobile);
            //resp-2:fax
            colunmList.add(newResponsibleInfo.responsibleFax);
            //esp-2:email
            colunmList.add(newResponsibleInfo.responsibleEMail);
        }
        //vetid
        String vetID;
        if (registrator.equals("Veterinary 2")) {
            vetID = "F0024";
        } else if (registrator.equals("Veterinary 3")) {
            vetID = "F1560";
        } else {
            vetID = "F0032";
        }
        colunmList.add(vetID);

        try {
            csvWriteHandler.writeNextRow(colunmList);
        } catch (Exception e){
            logger.logScreenshot(LogStatus.ERROR, "ERROR during CSV init " + e.getMessage(), getScreenShot());
        }
    }
}
