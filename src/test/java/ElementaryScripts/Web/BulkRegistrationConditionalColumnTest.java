package ElementaryScripts.Web;

import Screens.WebBulkUploadPage;
import Screens.WebStartPage;
import com.relevantcodes.extentreports.LogStatus;

public class BulkRegistrationConditionalColumnTest extends BulkRegistrationColumnTest {

    public void run() throws Exception{
        try {
            WebStartPage startPage = new WebStartPage(driver);
            WebBulkUploadPage bulkUploadPage = new WebBulkUploadPage(driver);
            if(registrator.contains("Refuge")){
                click(startPage.bulkTabRefugeButton);
            } else {
                click(startPage.bulkTabVetAndBreederButton);
            }
            //Test missing last name
            ResponsibleInfo firstResponsibleInfo = new ResponsibleInfo();
            //Accreditation number
            firstResponsibleInfo.responsibleAccreditationNumber = "HK43214321";

            ResponsibleInfo newResponsibleInfo = new ResponsibleInfo();
            //Accreditation number
            newResponsibleInfo.responsibleAccreditationNumber = "HK12341234";

            //First responsible test
            initCsvInputFile("BulkFirstResponsibleTestHkAndPersonalInfo1", false);
            addBulkRegistrationRow(firstResponsibleInfo, newResponsibleInfo, false);
            sendCsvFile(false);
            if(detectErrorMessages("missing last name") && detectErrorMessages("resp-1:last_name")){
                logger.log(LogStatus.INFO,"Unexpected personal info are detected for first responsible");
            } else {
                logger.log(LogStatus.ERROR, "Unexpected personal info are not detected for first responsible");
            }

            //New responsible test
            firstResponsibleInfo.responsibleLastName = "SPECIMEN";
            initCsvInputFile("BulkNewResponsibleTestHkAndPersonalInfo2", true);
            addBulkRegistrationRow(firstResponsibleInfo, newResponsibleInfo, true);
            sendCsvFile(false);
            if(detectErrorMessages("missing last name") && detectErrorMessages("resp-2:last_name")){
                logger.log(LogStatus.INFO,"Unexpected personal info are detected for new responsible");
            } else {
                logger.log(LogStatus.ERROR, "Unexpected personal info are not detected for new responsible");
            }

            //Test conditional columns without warnings

            //Personal info for particular
            firstResponsibleInfo.responsibleRRN = "71715100070";
            firstResponsibleInfo.responsibleFirstName = "Alice Geldigekaart";
            firstResponsibleInfo.responsibleLastName = "SPECIMEN";
            firstResponsibleInfo.responsibleZipCode = "1000";
            firstResponsibleInfo.responsibleMunicipality = "Bruxelles";
            firstResponsibleInfo.responsibleStreetName = "Chaussee D'ixelles";
            firstResponsibleInfo.responsibleStreetNumber = "64";
            firstResponsibleInfo.responsiblePhone = "789456";
            firstResponsibleInfo.responsibleMobile = "789456";
            firstResponsibleInfo.responsibleFax = "789456";
            firstResponsibleInfo.responsibleEMail = "alice@zetes.com";
            //Accreditation number
            firstResponsibleInfo.responsibleAccreditationNumber = "";

            //Personal info for particular
            newResponsibleInfo.responsibleRRN = "89041952724";
            newResponsibleInfo.responsibleFirstName = "Thierry";
            newResponsibleInfo.responsibleLastName = "Foulon";
            newResponsibleInfo.responsibleZipCode = "5004";
            newResponsibleInfo.responsibleMunicipality = "Bouge";
            newResponsibleInfo.responsibleStreetName = "Chaussée De Louvain";
            newResponsibleInfo.responsibleStreetNumber = "222";
            newResponsibleInfo.responsiblePhone = "0810834585";
            newResponsibleInfo.responsibleMobile = "0470834585";
            newResponsibleInfo.responsibleFax = "0810834586";
            newResponsibleInfo.responsibleEMail = "thierry.foulon@zetes.com";
            //Accreditation number
            newResponsibleInfo.responsibleAccreditationNumber = "";
            initCsvInputFile("BulkResponsibleTestHkAndPersonalInfo3", true);
            addBulkRegistrationRow(firstResponsibleInfo, newResponsibleInfo, true);
            sendCsvFile(false);
            if(bulkUploadPage.validationOrWarningMessage.getText().contains("The file passed validation. Please proceed to submit or re-upload a different file")){
                logger.log(LogStatus.INFO,"Conditional column for private person (first and new) are correctly filled");
            } else {
                logger.log(LogStatus.ERROR, "Conditional column for private person (first and new) are not correctly filled");
            }

            //Test conditional columns with warnings
            //Personal info for particular
            firstResponsibleInfo.responsibleRRN = "71715100070";
            firstResponsibleInfo.responsibleFirstName = "Alice Geldigekaart";
            firstResponsibleInfo.responsibleLastName = "SPECIMEN";
            firstResponsibleInfo.responsibleZipCode = "1000";
            firstResponsibleInfo.responsibleMunicipality = "Bruxelles";
            firstResponsibleInfo.responsibleStreetName = "Chaussee D'ixelles";
            firstResponsibleInfo.responsibleStreetNumber = "64";
            firstResponsibleInfo.responsiblePhone = "789456";
            firstResponsibleInfo.responsibleMobile = "789456";
            firstResponsibleInfo.responsibleFax = "789456";
            firstResponsibleInfo.responsibleEMail = "alice@zetes.com";
            //Accreditation number
            firstResponsibleInfo.responsibleAccreditationNumber = "";

            //Personal info for particular
            newResponsibleInfo.responsibleRRN = "89041952724";
            newResponsibleInfo.responsibleFirstName = "Thierry";
            newResponsibleInfo.responsibleLastName = "Foulon";
            newResponsibleInfo.responsibleZipCode = "5004";
            newResponsibleInfo.responsibleMunicipality = "Bouge";
            newResponsibleInfo.responsibleStreetName = "Chaussée De Louvain";
            newResponsibleInfo.responsibleStreetNumber = "222";
            newResponsibleInfo.responsiblePhone = "0810834585";
            newResponsibleInfo.responsibleMobile = "0470834585";
            newResponsibleInfo.responsibleFax = "0810834586";
            newResponsibleInfo.responsibleEMail = "thierry.foulon@zetes.com";
            //Accreditation number
            newResponsibleInfo.responsibleAccreditationNumber = "BR88888888";

            //First responsible test
            initCsvInputFile("BulkFirstResponsibleTestHkAndPersonalInfo4", true);
            addBulkRegistrationRow(firstResponsibleInfo, newResponsibleInfo, true);
            sendCsvFile(false);
            if(detectWarningMessages("postal code is ignored when accreditation number is filled in") &&
                    detectWarningMessages("municipality is ignored when accreditation number is filled in") &&
                    detectWarningMessages("street name is ignored when accreditation number is filled in") &&
                    detectWarningMessages("housenumber is ignored when accreditation number is filled in") &&
                    detectWarningMessages("national number is ignored when accreditation number is filled in") &&
                    detectWarningMessages("firstname is ignored when accreditation number is filled in")){
                logger.log(LogStatus.INFO,"Unexpected personal info are detected for first responsible");
            } else {
                logger.log(LogStatus.ERROR, "Unexpected personal info are not detected for first responsible");
            }

            //New responsible test
            firstResponsibleInfo.responsibleAccreditationNumber = "";
            initCsvInputFile("BulkNewResponsibleTestHkAndPersonalInfo5", true);
            addBulkRegistrationRow(firstResponsibleInfo, newResponsibleInfo, true);
            sendCsvFile(false);
            if(detectWarningMessages("postal code is ignored when accreditation number is filled in") &&
                    detectWarningMessages("municipality is ignored when accreditation number is filled in") &&
                    detectWarningMessages("street name is ignored when accreditation number is filled in") &&
                    detectWarningMessages("housenumber is ignored when accreditation number is filled in") &&
                    detectWarningMessages("national number is ignored when accreditation number is filled in") &&
                    detectWarningMessages("firstname is ignored when accreditation number is filled in")){
                logger.log(LogStatus.INFO,"Unexpected personal info are detected for new responsible");
            } else {
                logger.log(LogStatus.ERROR, "Unexpected personal info are not detected for new responsible");
            }

            click(startPage.logoutButton);
        } catch (Exception e) {
            logger.logScreenshot(LogStatus.ERROR, "ERROR stopping the test", getScreenShot());
            throw e;
        }
    }
}
