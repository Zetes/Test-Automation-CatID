package ElementaryScripts.Web;

import Csv.CsvHandler;
import Screens.WebStartPage;
import com.relevantcodes.extentreports.LogStatus;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class BulkRegistrationMatchValueTest extends BulkTest {
    public void run() throws Exception {
        try {
            WebStartPage startPage = new WebStartPage(driver);

            if (registrator.contains("Refuge")) {
                click(startPage.bulkTabRefugeButton);
            } else {
                click(startPage.bulkTabVetAndBreederButton);
            }
            //Check each column matches with one value (if not an error should be raised). Linked to  CATREG-867
            //Init a csv file containing all columns (mandatory and optional)
            initCsvInputFile(tcFeatureListNumber + "BulkRegistrationMatchValueTest", false);

            //Add a row with missing values (meaning there is a mismatch between header columns and the current values (eg: 10 headers columns and 8 values)
            //Each column should find at least a filled or empty value (eg: Last Name; First Name; ; ;   ==> 4 values here)
            //This function add a row to the csv file with 2 missing values
            addIncompleteBulkRegistrationRow(2);
            sendCsvFile(false);

            //Check error missing values
            //TODO once CATREG-867 is resolved, get the error messages and check the content

            click(startPage.logoutButton);
        } catch (Exception e) {
            logger.logScreenshot(LogStatus.ERROR, "ERROR stopping the test", getScreenShot());
            throw e;
        }
    }

    //This function add a row to the csv file with 2 missing values
    private void addIncompleteBulkRegistrationRow(int numberMissingValue) throws Exception {
        csvWriteHandler = new CsvHandler(logger, ';');
        if ("".equals(this.csvFileName)) {
            throw new Exception("Csv file name is empty");
        }
        csvWriteHandler.setCsvPath(USER_DIR + "/" + csvFileName + ".csv");
        ArrayList<String> colunmList = new ArrayList<String>();
        String timeStamp = new SimpleDateFormat("dd/MM/yyyy").format(new Date());
        //identification_date
        colunmList.add(timeStamp);
        //confidential_flag
        colunmList.add("0");
        //name
        colunmList.add("Minuit");
        //breed
        colunmList.add("Abyssinian");
        //crossing
        colunmList.add("0");
        //fur_tc
        colunmList.add("black");
        //birth_date
        colunmList.add(timeStamp);
        //sterilized
        colunmList.add("1");
        //sterilization_date
        colunmList.add(timeStamp);
        //gender
        colunmList.add("M");
        //id-1:location
        colunmList.add("5");
        //id-1:value
        int lastIdentifierInteger = Integer.parseInt(this.getLastIdentifier());
        int firstNewIdentifier = lastIdentifierInteger + 1;
        String newFirstIdentifierString = "123456789" + String.valueOf(firstNewIdentifier);
        colunmList.add(newFirstIdentifierString);
        addNewIdentifier(String.valueOf(firstNewIdentifier));
        //id-2:location
        //colunmList.add("5");
        colunmList.add("");
        //id-2:value
        //int secondNewIdentifier = firstNewIdentifier + 1;
        //String newSecondIdentifierString = "123456789" + String.valueOf(secondNewIdentifier);
        //colunmList.add(newSecondIdentifierString);
        //addNewIdentifier(String.valueOf(secondNewIdentifier));
        colunmList.add("");
        //passport_nr
        colunmList.add("");


        //resp-1:hk-nr
        colunmList.add("HK43214321");
        //resp-1:rrn
        colunmList.add("");
        //resp-1:first_name
        colunmList.add("");
        //resp-1:last_name
        colunmList.add("SPECIMEN");
        //resp-1:postal_code
        colunmList.add("");
        //resp-1:municipality
        colunmList.add("");
        //resp-1:street
        colunmList.add("");
        //resp-1:number
        //Missing value
        if (0 - numberMissingValue >= 0) {
            colunmList.add("");
        }
        //resp-1:phone
        //Missing value
        if (1 - numberMissingValue >= 0) {
            colunmList.add("");
        }
        //resp-1:mobile
        colunmList.add("");
        //resp-1:fax
        if (2 - numberMissingValue >= 0) {
            colunmList.add("");
        }
        //resp-1:email
        if (3 - numberMissingValue >= 0) {
            colunmList.add("");
        }
        //resp-2:transfer_date
        if (4 - numberMissingValue >= 0) {
            colunmList.add("");
        }
        //resp-2:hk-nr
        if (5 - numberMissingValue >= 0) {
            colunmList.add("");
        }
        //resp-2:rrn
        if (6 - numberMissingValue >= 0) {
            colunmList.add("");
        }
        //resp-2:first_name
        if (7 - numberMissingValue >= 0) {
            colunmList.add("");
        }
        //resp-2:last_name
        colunmList.add("");
        //resp-2:postal_code
        colunmList.add("");
        //resp-2:municipality
        colunmList.add("");
        //resp-2:street
        colunmList.add("");
        //resp-2:number
        colunmList.add("");
        //resp-2:phone
        colunmList.add("");
        //resp-2:mobile
        colunmList.add("");
        //resp-2:fax
        colunmList.add("");
        //esp-2:email
        colunmList.add("");
        //vetid
        String vetID;
        if (registrator.equals("Veterinary 2")) {
            vetID = "F0024";
        } else if (registrator.equals("Veterinary 3")) {
            vetID = "F1560";
        } else {
            vetID = "F0032";
        }
        colunmList.add(vetID);

        try {
            csvWriteHandler.writeNextRow(colunmList);
        } catch (Exception e) {
            logger.logScreenshot(LogStatus.ERROR, "ERROR during CSV init " + e.getMessage(), getScreenShot());
        }
    }
}