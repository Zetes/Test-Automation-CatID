package ElementaryScripts.Web;

import Screens.WebStartPage;
import com.relevantcodes.extentreports.LogStatus;

import java.text.SimpleDateFormat;
import java.util.Date;

public class BulkRegistrationPassportIDCheckBetweenFilesTest extends BulkRegistrationPassportIDCheckTest{
    public void run() throws Exception {
        try {
            WebStartPage startPage = new WebStartPage(driver);
            if(registrator.contains("Refuge")){
                click(startPage.bulkTabRefugeButton);
            } else {
                click(startPage.bulkTabVetAndBreederButton);
            }

            //Check same identifier between 2 csv file
            String newFirstIdentifierString = getNextIdentifier();
            initCsvInputFile("BulkRegistrationIDCheckBetweenFilesTest1", true);
            addBulkRegistrationRow(newFirstIdentifierString, "");
            sendCsvFile(true);
            sendCsvFile(false);
            if(detectErrorMessages("identification number is already assigned")){
                logger.log(LogStatus.INFO,"Already used ID is detected");
            } else {
                logger.log(LogStatus.ERROR,"Already used ID is not detected");
            }

            //Check same passport between 2 csv file
            String passportNumberDate = new SimpleDateFormat("ddMMyyyyHHmmssSSS").format(new Date());
            String passportText = tcFeatureListNumber  + passportNumberDate;
            initCsvInputFile("BulkRegistrationIDCheckBetweenFilesTest2-1", true);
            addBulkRegistrationRow(getNextIdentifier(), passportText);
            sendCsvFile(true);
            initCsvInputFile("BulkRegistrationIDCheckBetweenFilesTest2-2", true);
            addBulkRegistrationRow(getNextIdentifier(), passportText);
            sendCsvFile(false);
            if(detectErrorMessages("already used passport")){
                logger.log(LogStatus.INFO,"already used passport is detected");
            } else {
                logger.log(LogStatus.ERROR,"already used passport is not detected");
            }

            //Check same passport and identifier between 2 csv file
            initCsvInputFile("BulkRegistrationIDCheckBetweenFilesTest3", true);
            addBulkRegistrationRow(newFirstIdentifierString, passportText);
            sendCsvFile(false);
            if(detectErrorMessages("identification number is already assigned") && detectErrorMessages("already used passport")){
                logger.log(LogStatus.INFO,"Already used ID and already used passport are detected");
            } else {
                logger.log(LogStatus.ERROR,"Already used ID and already used passport are detected");
            }

            click(startPage.logoutButton);
        } catch (Exception e) {
            logger.logScreenshot(LogStatus.ERROR, "ERROR stopping the test", getScreenShot());
            throw e;
        }
    }
}
