package ElementaryScripts.Web;

import Screens.WebStartPage;
import com.relevantcodes.extentreports.LogStatus;

import java.text.SimpleDateFormat;
import java.util.Date;

public class BulkRegistrationPassportIDCheckSameFileTest extends BulkRegistrationPassportIDCheckTest{
    public void run() throws Exception {
        try {
            WebStartPage startPage = new WebStartPage(driver);
            if(registrator.contains("Refuge")){
                click(startPage.bulkTabRefugeButton);
            } else {
                click(startPage.bulkTabVetAndBreederButton);
            }

            //Check same identifier between 2 csv file
            //Same identifier
            String newFirstIdentifierString = getNextIdentifier();
            initCsvInputFile("BulkRegistrationIDCheckBetweenFilesTest1", true);
            addBulkRegistrationRow(newFirstIdentifierString, "");
            addBulkRegistrationRow(newFirstIdentifierString, "");
            sendCsvFile(false);
            if(detectErrorMessages("reoccurring identification number")){
                logger.log(LogStatus.INFO,"Reoccurring identification number is detected");
            } else {
                logger.log(LogStatus.ERROR,"Reoccurring identification number is not detected");
            }

            //Same passport
            String passportNumberDate = new SimpleDateFormat("ddMMyyyyHHmmssSSS").format(new Date());
            String passportText = tcFeatureListNumber  + passportNumberDate;
            initCsvInputFile("BulkRegistrationIDCheckBetweenFilesTest2", true);
            addBulkRegistrationRow(getNextIdentifier(), passportText);
            addBulkRegistrationRow(getNextIdentifier(), passportText);
            sendCsvFile(false);

            if(detectErrorMessages("reoccurring passport number")){
                logger.log(LogStatus.INFO,"Reoccurring passport number is detected");
            } else {
                logger.log(LogStatus.ERROR,"Reoccurring passport number is not detected");
            }//

            click(startPage.logoutButton);
        } catch (Exception e) {
            logger.logScreenshot(LogStatus.ERROR, "ERROR stopping the test", getScreenShot());
            throw e;
        }
    }
}
