package ElementaryScripts.Web;

import Csv.CsvHandler;
import com.relevantcodes.extentreports.LogStatus;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;


public abstract class BulkRegistrationPassportIDCheckTest extends BulkTest{

    protected void addBulkRegistrationRow(String identifier, String passport) throws Exception{

        csvWriteHandler = new CsvHandler(logger,';');
        if("".equals(this.csvFileName)){
            throw new Exception("Csv file name is empty");
        }
        csvWriteHandler.setCsvPath(USER_DIR + "/" + csvFileName + ".csv");
        ArrayList<String> colunmList =  new ArrayList<String>();
        String timeStamp = new SimpleDateFormat("dd/MM/yyyy").format(new Date());
        //identification_date
        colunmList.add(timeStamp);
        //confidential_flag
        colunmList.add("0");
        //name
        colunmList.add("Minuit");
        //breed
        colunmList.add("Abyssinian");
        //crossing
        colunmList.add("0");
        //fur_tc
        colunmList.add("black");
        //birth_date
        colunmList.add(timeStamp);
        //sterilized
        colunmList.add("1");
        //sterilization_date
        colunmList.add(timeStamp);
        //gender
        colunmList.add("M");
        //id-1:location
        colunmList.add("5");
        //id-1:value
        colunmList.add(identifier);
        //id-2:location
        //colunmList.add("5");
        colunmList.add("");
        //id-2:value
        //int secondNewIdentifier = firstNewIdentifier + 1;
        //String newSecondIdentifierString = "123456789" + String.valueOf(secondNewIdentifier);
        //colunmList.add(newSecondIdentifierString);
        //addNewIdentifier(String.valueOf(secondNewIdentifier));
        colunmList.add("");
        //passport_nr
        colunmList.add(passport);
        //resp-1:hk-nr
        colunmList.add("");
        //resp-1:rrn
        colunmList.add("71715100070");
        //resp-1:first_name
        colunmList.add("Alice Geldigekaart");
        //resp-1:last_name
        colunmList.add("SPECIMEN");
        //resp-1:postal_code
        colunmList.add("1000");
        //resp-1:municipality
        colunmList.add("Bruxelles");
        //resp-1:street
        colunmList.add("Chaussee D'ixelles");
        //resp-1:number
        colunmList.add("64");
        //resp-1:phone
        colunmList.add("789456");
        //resp-1:mobile
        colunmList.add("789456");
        //resp-1:fax
        colunmList.add("789456");
        //resp-1:email
        colunmList.add("alice@zetes.com");
        //resp-2:transfer_date
        colunmList.add(timeStamp);
        //resp-2:hk-nr
        colunmList.add("HK43214321");
        //resp-2:rrn
        colunmList.add("");
        //resp-2:first_name
        colunmList.add("");
        //resp-2:last_name
        colunmList.add("SPECIMEN");
        //resp-2:postal_code
        colunmList.add("");
        //resp-2:municipality
        colunmList.add("");
        //resp-2:street
        colunmList.add("");
        //resp-2:number
        colunmList.add("");
        //resp-2:phone
        colunmList.add("");
        //resp-2:mobile
        colunmList.add("");
        //resp-2:fax
        colunmList.add("");
        //esp-2:email
        colunmList.add("");
        //vetid
        String vetID;
        if (registrator.equals("Veterinary 2")) {
            vetID = "F0024";
        } else if (registrator.equals("Veterinary 3")) {
            vetID = "F1560";
        } else {
            vetID = "F0032";
        }
        colunmList.add(vetID);

        try {
            csvWriteHandler.writeNextRow(colunmList);
        } catch (Exception e){
            logger.logScreenshot(LogStatus.ERROR, "ERROR during CSV init " + e.getMessage(), getScreenShot());
        }
    }
}
