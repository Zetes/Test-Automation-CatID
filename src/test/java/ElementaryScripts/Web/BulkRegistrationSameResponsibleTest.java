package ElementaryScripts.Web;

import Screens.WebStartPage;
import com.relevantcodes.extentreports.LogStatus;

/**
 * Created by thierry.foulon on 4/18/2018.
 */
public class BulkRegistrationSameResponsibleTest extends BulkRegistrationColumnTest{
    public void run() throws Exception{
        try {
            WebStartPage startPage = new WebStartPage(driver);
            if(registrator.contains("Refuge")){
                click(startPage.bulkTabRefugeButton);
            } else {
                click(startPage.bulkTabVetAndBreederButton);
            }
            ResponsibleInfo firstResponsibleInfo = new ResponsibleInfo();
            //Personal info for particular
            firstResponsibleInfo.responsibleRRN = "71715100070";
            firstResponsibleInfo.responsibleFirstName = "Alice Geldigekaart";
            firstResponsibleInfo.responsibleLastName = "SPECIMEN";
            firstResponsibleInfo.responsibleZipCode = "1000";
            firstResponsibleInfo.responsibleMunicipality = "Bruxelles";
            firstResponsibleInfo.responsibleStreetName = "Chaussee D'ixelles";
            firstResponsibleInfo.responsibleStreetNumber = "64";
            firstResponsibleInfo.responsiblePhone = "789456";
            firstResponsibleInfo.responsibleMobile = "789456";
            firstResponsibleInfo.responsibleFax = "789456";
            firstResponsibleInfo.responsibleEMail = "alice@zetes.com";

            //Personal info for particular
            ResponsibleInfo newResponsibleInfo = new ResponsibleInfo();
            newResponsibleInfo.responsibleRRN = "71715100070";
            newResponsibleInfo.responsibleFirstName = "Alice Geldigekaart";
            newResponsibleInfo.responsibleLastName = "SPECIMEN";
            newResponsibleInfo.responsibleZipCode = "1000";
            newResponsibleInfo.responsibleMunicipality = "Bruxelles";
            newResponsibleInfo.responsibleStreetName = "Chaussee D'ixelles";
            newResponsibleInfo.responsibleStreetNumber = "64";
            newResponsibleInfo.responsiblePhone = "789456";
            newResponsibleInfo.responsibleMobile = "789456";
            newResponsibleInfo.responsibleFax = "789456";
            newResponsibleInfo.responsibleEMail = "alice@zetes.com";

            //First responsible test
            initCsvInputFile("BulkFirstResponsibleTestHkAndPersonalInfo", true);
            addBulkRegistrationRow(firstResponsibleInfo, newResponsibleInfo, true);
            sendCsvFile(false);
            if(detectErrorMessages("national number refers to same person as previous responsible")){
                logger.log(LogStatus.INFO,"Same person info are detected");
            } else {
                logger.log(LogStatus.ERROR, "Same person info are not detected");
            }

            click(startPage.logoutButton);
        } catch (Exception e) {
            logger.logScreenshot(LogStatus.ERROR, "ERROR stopping the test", getScreenShot());
            throw e;
        }
    }
}
