package ElementaryScripts.Web;

import Csv.CsvHandler;
import Screens.WebStartPage;
import com.relevantcodes.extentreports.LogStatus;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class BulkRegistrationTest extends BulkTest {

    public void run() throws Exception{
        try {
            WebStartPage startPage = new WebStartPage(driver);

            boolean isNewResponsibleTransfer = false;
            if(registrator.contains("Refuge")){
                click(startPage.bulkTabRefugeButton);
            } else {
                click(startPage.bulkTabVetAndBreederButton);
            }
            ResponsibleInfo firstResponsibleInfo = new ResponsibleInfo();
            //Prepare First Responsible Info
            if (firstResponsible.equals("Private Person")) {
                firstResponsibleInfo.responsibleRRN = "71715100070";
                firstResponsibleInfo.responsibleFirstName = "Alice Geldigekaart";
                firstResponsibleInfo.responsibleLastName = "SPECIMEN";
                firstResponsibleInfo.responsibleZipCode = "1000";
                firstResponsibleInfo.responsibleMunicipality = "Bruxelles";
                firstResponsibleInfo.responsibleStreetName = "Chaussee D'ixelles";
                firstResponsibleInfo.responsibleStreetNumber = "64";
                firstResponsibleInfo.responsiblePhone = "789456";
                firstResponsibleInfo.responsibleMobile = "789456";
                firstResponsibleInfo.responsibleFax = "789456";
                firstResponsibleInfo.responsibleEMail = "alice@zetes.com";
            } else if (firstResponsible.equals("Breeder 1")) {
                firstResponsibleInfo.responsibleAccreditationNumber = "HK43214321";
                firstResponsibleInfo.responsibleLastName = "SPECIMEN";
            } else if (firstResponsible.equals("Refuge 1")) {
                firstResponsibleInfo.responsibleAccreditationNumber = "HK12341234";
                firstResponsibleInfo.responsibleLastName = "SPECIMEN";
            } else if (firstResponsible.equals("Breeder 2")) {
                firstResponsibleInfo.responsibleAccreditationNumber = "BR88888888";
                firstResponsibleInfo.responsibleLastName = "Foulon";
            } else if (firstResponsible.equals("Breeder 3")) {
                firstResponsibleInfo.responsibleAccreditationNumber = "HK10101235";
                firstResponsibleInfo.responsibleLastName = "SPECIMEN";
            } else if (firstResponsible.equals("Breeder 4")) {
                firstResponsibleInfo.responsibleAccreditationNumber = "HK159357";
                firstResponsibleInfo.responsibleLastName = "SPECIMEN";
            } else if (firstResponsible.equals("Refuge 2")) {
                firstResponsibleInfo.responsibleAccreditationNumber = "RE88888888";
                firstResponsibleInfo.responsibleLastName = "Foulon";
            } else if (firstResponsible.equals("Refuge 3")) {
                firstResponsibleInfo.responsibleAccreditationNumber = "HK852456";
                firstResponsibleInfo.responsibleLastName = "SPECIMEN";
            }

            //Prepare New Responsible Info
            ResponsibleInfo newResponsibleInfo = new ResponsibleInfo();
            if (!newResponsible.equals("N/A")) {
                isNewResponsibleTransfer = true;
                if (newResponsible.equals("Private Person")) {
                    newResponsibleInfo.responsibleRRN = "89041952724";
                    newResponsibleInfo.responsibleFirstName = "Thierry";
                    newResponsibleInfo.responsibleLastName = "Foulon";
                    newResponsibleInfo.responsibleZipCode = "5004";
                    newResponsibleInfo.responsibleMunicipality = "Bouge";
                    newResponsibleInfo.responsibleStreetName = "Chaussée De Louvain";
                    newResponsibleInfo.responsibleStreetNumber = "222";
                    newResponsibleInfo.responsiblePhone = "0810834585";
                    newResponsibleInfo.responsibleMobile = "0470834585";
                    newResponsibleInfo.responsibleFax = "0810834586";
                    newResponsibleInfo.responsibleEMail = "thierry.foulon@zetes.com";
                } else if (newResponsible.equals("Breeder 1")) {
                    newResponsibleInfo.responsibleAccreditationNumber = "HK43214321";
                    newResponsibleInfo.responsibleLastName = "SPECIMEN";
                } else if (newResponsible.equals("Refuge 1")) {
                    newResponsibleInfo.responsibleAccreditationNumber = "HK12341234";
                    newResponsibleInfo.responsibleLastName = "SPECIMEN";
                } else if (newResponsible.equals("Breeder 2")) {
                    newResponsibleInfo.responsibleAccreditationNumber = "BR88888888";
                    newResponsibleInfo.responsibleLastName = "Foulon";
                } else if (newResponsible.equals("Breeder 3")) {
                    newResponsibleInfo.responsibleAccreditationNumber = "HK10101235";
                    newResponsibleInfo.responsibleLastName = "SPECIMEN";
                } else if (newResponsible.equals("Breeder 4")) {
                    newResponsibleInfo.responsibleAccreditationNumber = "HK159357";
                    newResponsibleInfo.responsibleLastName = "SPECIMEN";
                } else if (newResponsible.equals("Refuge 2")) {
                    newResponsibleInfo.responsibleAccreditationNumber = "RE88888888";
                    newResponsibleInfo.responsibleLastName = "Foulon";
                } else if (newResponsible.equals("Refuge 3")) {
                    newResponsibleInfo.responsibleAccreditationNumber = "HK852456";
                    newResponsibleInfo.responsibleLastName = "SPECIMEN";
                }
            }

            //Generate a file containing 1, 3 and 10 rows

            //CSV file with 1 row
            addMultipleRows("BulkInputTest1", 1, firstResponsibleInfo, newResponsibleInfo, isNewResponsibleTransfer);
            sendCsvFile(true);

            //CSV file with 3 rows
            addMultipleRows("BulkInputTest3", 3, firstResponsibleInfo, newResponsibleInfo, isNewResponsibleTransfer);
            sendCsvFile(true);

            //CSV file with 10 rows
            addMultipleRows("BulkInputTest10", 10, firstResponsibleInfo, newResponsibleInfo, isNewResponsibleTransfer);
            sendCsvFile(true);

            click(startPage.logoutButton);
        } catch (Exception e) {
            logger.logScreenshot(LogStatus.ERROR, "ERROR stopping the test", getScreenShot());
            throw e;
        }
    }

    protected void addMultipleRows(String csvTitle, int numberOfLine, ResponsibleInfo firstResponsibleInfo, ResponsibleInfo newResponsibleInfo, boolean isNewResponsibleTransfer) throws Exception{
        initCsvInputFile(tcFeatureListNumber + csvTitle, isNewResponsibleTransfer);
        for(int i = 0; i < numberOfLine; i++) {
            addBulkRegistrationRow(firstResponsibleInfo, newResponsibleInfo, isNewResponsibleTransfer, false);
        }
    }

    protected void addBulkRegistrationRow(ResponsibleInfo firstResponsibleInfo, ResponsibleInfo newResponsibleInfo, boolean isNewResponsibleTransfer) throws Exception{
        addBulkRegistrationRow(firstResponsibleInfo, newResponsibleInfo, isNewResponsibleTransfer, false);
    }

    protected void addBulkRegistrationRow(ResponsibleInfo firstResponsibleInfo, ResponsibleInfo newResponsibleInfo, boolean isNewResponsibleTransfer, boolean withPassport) throws Exception{
        csvWriteHandler = new CsvHandler(logger,';');
        if("".equals(this.csvFileName)){
            throw new Exception("Csv file name is empty");
        }
        csvWriteHandler.setCsvPath(USER_DIR + "/" + csvFileName + ".csv");
        ArrayList<String> colunmList =  new ArrayList<String>();
        String timeStamp = new SimpleDateFormat("dd/MM/yyyy").format(new Date());
        //identification_date
        colunmList.add(timeStamp);
        //confidential_flag
        colunmList.add("0");
        //name
        colunmList.add("Minuit");
        //breed
        colunmList.add("Abyssinian");
        //crossing
        colunmList.add("0");
        //fur_tc
        colunmList.add("black");
        //birth_date
        colunmList.add(timeStamp);
        //sterilized
        colunmList.add("1");
        //sterilization_date
        colunmList.add(timeStamp);
        //gender
        colunmList.add("M");
        //id-1:location
        colunmList.add("5");
        //id-1:value
        int lastIdentifierInteger = Integer.parseInt(this.getLastIdentifier());
        int firstNewIdentifier = lastIdentifierInteger + 1;
        String newFirstIdentifierString = "123456789" + String.valueOf(firstNewIdentifier);
        colunmList.add(newFirstIdentifierString);
        addNewIdentifier(String.valueOf(firstNewIdentifier));
        //id-2:location
        //colunmList.add("5");
        colunmList.add("");
        //id-2:value
        //int secondNewIdentifier = firstNewIdentifier + 1;
        //String newSecondIdentifierString = "123456789" + String.valueOf(secondNewIdentifier);
        //colunmList.add(newSecondIdentifierString);
        //addNewIdentifier(String.valueOf(secondNewIdentifier));
        colunmList.add("");
        //passport_nr
        if(withPassport){
            String passportNumberDate = new SimpleDateFormat("ddMMyyyyHHmmssSSS").format(new Date());
            String passportText = tcFeatureListNumber  + passportNumberDate;
            colunmList.add(passportText);
        } else {
            colunmList.add("");
        }

        //resp-1:hk-nr
        colunmList.add(firstResponsibleInfo.responsibleAccreditationNumber);
        //resp-1:rrn
        colunmList.add(firstResponsibleInfo.responsibleRRN);
        //resp-1:first_name
        colunmList.add(firstResponsibleInfo.responsibleFirstName);
        //resp-1:last_name
        colunmList.add(firstResponsibleInfo.responsibleLastName);
        //resp-1:postal_code
        colunmList.add(firstResponsibleInfo.responsibleZipCode);
        //resp-1:municipality
        colunmList.add(firstResponsibleInfo.responsibleMunicipality);
        //resp-1:street
        colunmList.add(firstResponsibleInfo.responsibleStreetName);
        //resp-1:number
        colunmList.add(firstResponsibleInfo.responsibleStreetNumber);
        //resp-1:phone
        colunmList.add(firstResponsibleInfo.responsiblePhone);
        //resp-1:mobile
        colunmList.add(firstResponsibleInfo.responsibleMobile);
        //resp-1:fax
        colunmList.add(firstResponsibleInfo.responsibleFax);
        //resp-1:email
        colunmList.add(firstResponsibleInfo.responsibleEMail);
        if(isNewResponsibleTransfer) {
            //resp-2:transfer_date
            colunmList.add(timeStamp);
            //resp-2:hk-nr
            colunmList.add(newResponsibleInfo.responsibleAccreditationNumber);
            //resp-2:rrn
            colunmList.add(newResponsibleInfo.responsibleRRN);
            //resp-2:first_name
            colunmList.add(newResponsibleInfo.responsibleFirstName);
            //resp-2:last_name
            colunmList.add(newResponsibleInfo.responsibleLastName);
            //resp-2:postal_code
            colunmList.add(newResponsibleInfo.responsibleZipCode);
            //resp-2:municipality
            colunmList.add(newResponsibleInfo.responsibleMunicipality);
            //resp-2:street
            colunmList.add(newResponsibleInfo.responsibleStreetName);
            //resp-2:number
            colunmList.add(newResponsibleInfo.responsibleStreetNumber);
            //resp-2:phone
            colunmList.add(newResponsibleInfo.responsiblePhone);
            //resp-2:mobile
            colunmList.add(newResponsibleInfo.responsibleMobile);
            //resp-2:fax
            colunmList.add(newResponsibleInfo.responsibleFax);
            //esp-2:email
            colunmList.add(newResponsibleInfo.responsibleEMail);
        }
        //vetid
        String vetID = "";
        if (registrator.equals("Veterinary 2")) {
            vetID = "F0024";
        } else if (registrator.equals("Veterinary 3")) {
            vetID = "F1560";
        } else {
            vetID = "F0032";
        }
        colunmList.add(vetID);

        try {
            csvWriteHandler.writeNextRow(colunmList);
        } catch (Exception e){
            logger.logScreenshot(LogStatus.ERROR, "ERROR during CSV add rows " + e.getMessage(), getScreenShot());
        }
    }

    public void generateCsvWithNRows(int n) throws Exception {
        initCsvInputFile(tcFeatureListNumber + "BulkGenerationFile", true);
        ResponsibleInfo firstResponsibleInfo = new ResponsibleInfo();
        firstResponsibleInfo.responsibleAccreditationNumber = "HK43214321";
        firstResponsibleInfo.responsibleLastName = "SPECIMEN";

        ResponsibleInfo newResponsibleInfo = new ResponsibleInfo();
        newResponsibleInfo.responsibleAccreditationNumber = "HK12341234";
        newResponsibleInfo.responsibleLastName = "SPECIMEN";
        for(int i = 0; i < n; i++) {
            addBulkRegistrationRow(firstResponsibleInfo, newResponsibleInfo, true, false);
        }
    }
}
