package ElementaryScripts.Web;

import Csv.CsvHandler;
import ElementaryScripts.General.AbstractCatIdElementaryScript;
import Screens.WebBulkUploadPage;
import Screens.WebEazyIdPage;
import com.relevantcodes.extentreports.LogStatus;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.List;


public abstract class BulkTest extends AbstractCatIdElementaryScript {
    protected CsvHandler csvWriteHandler;
    protected String csvFileName = "";
    
    protected class ResponsibleInfo {
        public String responsibleRRN;
        public String responsibleAccreditationNumber;
        public String responsibleFirstName;
        public String responsibleLastName;
        public String responsibleZipCode;
        public String responsibleMunicipality;
        public String responsibleStreetName;
        public String responsibleStreetNumber;
        public String responsiblePhone;
        public String responsibleMobile;
        public String responsibleFax;
        public String responsibleEMail;

        public ResponsibleInfo (){
            responsibleRRN = "";
            responsibleAccreditationNumber = "";
            responsibleFirstName = "";
            responsibleLastName = "";
            responsibleZipCode = "";
            responsibleMunicipality = "";
            responsibleStreetName = "";
            responsibleStreetNumber = "";
            responsiblePhone = "";
            responsibleMobile = "";
            responsibleFax = "";
            responsibleEMail = "";
        }
    }

    protected void initCsvInputFile(String csvFileName, boolean isNewResponsibleTransfer){
        csvWriteHandler = new CsvHandler(logger,';');
        this.csvFileName = csvFileName;
        csvWriteHandler.setCsvPath(USER_DIR + "/" + this.csvFileName + ".csv");
        ArrayList<String> colunmList =  new ArrayList<String>();
        colunmList.add("identification_date");
        colunmList.add("confidential_flag");
        colunmList.add("name");
        colunmList.add("breed");
        colunmList.add("crossing");
        colunmList.add("fur_tc");
        colunmList.add("birth_date");
        colunmList.add("sterilized");
        colunmList.add("sterilization_date");
        colunmList.add("gender");
        colunmList.add("id-1:location");
        colunmList.add("id-1:value");
        colunmList.add("id-2:location");
        colunmList.add("id-2:value");
        colunmList.add("passport_nr");
        colunmList.add("resp-1:hk-nr");
        colunmList.add("resp-1:rrn");
        colunmList.add("resp-1:first_name");
        colunmList.add("resp-1:last_name");
        colunmList.add("resp-1:postal_code");
        colunmList.add("resp-1:municipality");
        colunmList.add("resp-1:street");
        colunmList.add("resp-1:number");
        colunmList.add("resp-1:phone");
        colunmList.add("resp-1:mobile");
        colunmList.add("resp-1:fax");
        colunmList.add("resp-1:email");
        if(isNewResponsibleTransfer){
            colunmList.add("resp-2:transfer_date");
            colunmList.add("resp-2:hk-nr");
            colunmList.add("resp-2:rrn");
            colunmList.add("resp-2:first_name");
            colunmList.add("resp-2:last_name");
            colunmList.add("resp-2:postal_code");
            colunmList.add("resp-2:municipality");
            colunmList.add("resp-2:street");
            colunmList.add("resp-2:number");
            colunmList.add("resp-2:phone");
            colunmList.add("resp-2:mobile");
            colunmList.add("resp-2:fax");
            colunmList.add("resp-2:email");
        }
        colunmList.add("vetid");

        try {
            csvWriteHandler.writeNextRow(colunmList);
        } catch (Exception e){
            logger.logScreenshot(LogStatus.ERROR, "ERROR during CSV init " + e.getMessage(), getScreenShot());
        }
    }

    protected void initCsvInputFile(String csvFileName, ArrayList<String> colunmList){
        csvWriteHandler = new CsvHandler(logger,';');
        this.csvFileName = csvFileName;
        csvWriteHandler.setCsvPath(USER_DIR + "/" + this.csvFileName + ".csv");
        try {
            csvWriteHandler.writeNextRow(colunmList);
        } catch (Exception e){
            logger.logScreenshot(LogStatus.ERROR, "ERROR during CSV init " + e.getMessage(), getScreenShot());
        }
    }

    protected boolean detectErrorMessages(String errorMessage) throws Exception{
        WebBulkUploadPage bulkUploadPage = new WebBulkUploadPage(driver);
        boolean isErrorDetected = false;
        List<WebElement> lineErrorMessage = bulkUploadPage.errorTabList.findElements(By.tagName("tr"));
        for(WebElement element : lineErrorMessage){
            List<WebElement> columnErrorMessage = element.findElements(By.tagName("td"));
            if(columnErrorMessage.size() > 2 && (columnErrorMessage.get(1).getText().contains(errorMessage) || columnErrorMessage.get(2).getText().contains(errorMessage) )){
                isErrorDetected = true;
                break; //stop the loop
            }
        }
        return isErrorDetected;
    }

    protected boolean detectWarningMessages(String errorMessage)throws Exception{
        WebBulkUploadPage bulkUploadPage = new WebBulkUploadPage(driver);
        boolean isErrorDetected = false;
        List<WebElement> lineErrorMessage = bulkUploadPage.warningTabList.findElements(By.tagName("tr"));
        for(WebElement element : lineErrorMessage){
            List<WebElement> columnErrorMessage = element.findElements(By.tagName("td"));
            if(columnErrorMessage.size() > 2 && (columnErrorMessage.get(1).getText().contains(errorMessage) || columnErrorMessage.get(2).getText().contains(errorMessage) )){
                isErrorDetected = true;
                break; //stop the loop
            }
        }
        return isErrorDetected;
    }

    protected void sendCsvFile(boolean withSignature) throws Exception {
        WebEazyIdPage eazyIdPage = new WebEazyIdPage(driver);
        WebBulkUploadPage bulkUploadPage = new WebBulkUploadPage(driver);

        setInputFileText(bulkUploadPage.bulkUploadFileInputFileField, csvWriteHandler.getAbsoluteCsvPath());
        click(bulkUploadPage.uploadButton);
        if(registrator.contains("Veterinary") && withSignature) {
            click(bulkUploadPage.submitVetFileButton);
            click(bulkUploadPage.signVetNowOrSendToEndButton);
            eazyId(eazyIdPage);
            click(eazyIdPage.closeValidSubmitMessage);
        } else if (withSignature){
            click(bulkUploadPage.submitRefugeAndBreederFileButton);
            click(bulkUploadPage.signVetNowOrSendToEndButton);
        }
    }


}
