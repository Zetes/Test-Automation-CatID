package ElementaryScripts.Web;

import Csv.CsvHandler;
import DataBaseFramework.SqlOracleMiddleware;
import Screens.WebApprovedPage;
import Screens.WebModifyPage;
import Screens.WebStartPage;
import com.relevantcodes.extentreports.LogStatus;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.io.File;
import java.util.*;

public class FilterApprovedPage extends FilterPage {
    private int  duration = 5000;
    public void run() throws Exception {
        try {
            WebStartPage startPage = new WebStartPage(driver);
            WebApprovedPage approvedPage = new WebApprovedPage(driver);
            WebModifyPage modifyPage = new WebModifyPage(driver);

            SqlOracleMiddleware catIdMiddleWare = new SqlOracleMiddleware(logger, "CATID", "CATID", "jdbc:oracle:thin:@192.168.17.6:1521:PREPROD");
//            SqlOracleMiddleware metaformMiddleWare = new SqlOracleMiddleware(logger, "MF_CATS", "MF_CATS", "jdbc:oracle:thin:@192.168.17.6:1521:PREPROD");

            //Accessibility check
            checkApprovedFilterAccessibility( approvedPage, startPage);

            //Approved filter check
//            approvedFilterCheck(approvedPage, startPage, eazyIdPage, metaformMiddleWare, modifyPage);
            approvedFilterCheck(approvedPage, startPage, catIdMiddleWare, modifyPage);

            //logout for next page
            click(startPage.logoutButton);
        }catch (Exception e) {
            logger.logScreenshot(LogStatus.ERROR, "ERROR stopping the test", getScreenShot());
            throw e;
        }
    }

    private void ascendingSortCheck(WebApprovedPage approvedPage, WebStartPage startPage) throws Exception {
        reloadApprovedFilterPageWithCatIdentifier(approvedPage, startPage);
        List<WebElement> listHeaderButton = approvedPage.headerRow.findElements(By.tagName("th"));
        //Check all columns
        List<WebElement> pageNumberList;
        List<String> buildColumnStrings;
        List<WebElement> listTrWebElement;
        List<WebElement> listTdWebElement;
        String content;
        int tdIndex = 0;
        //Select 50 rows per page
        click(approvedPage.pageSelectionDropList);
        click(approvedPage.fiftyRowsByPageSelection);
        String headerName = "";
        for (WebElement header : listHeaderButton){
            buildColumnStrings = new ArrayList<String>();
            headerName = header.getAttribute("textContent").replaceAll("\\s+", "");
            scrollOnElement(approvedPage.searchField);
            //Sort the column as ascending
            click(header); //Click for ascendant sort
            pageNumberList = approvedPage.paginationMenu.findElements(By.tagName("a"));
            //Click on the last page
            if(pageNumberList.size() > 5) {
                click(pageNumberList.get(pageNumberList.size() - 2)); //Next button is the last button at index "size - 1"
                click(approvedPage.previousButton); // to make sure there are probably 50 rows
            }
            sleep(duration);//need time to load 50 rows
            listTrWebElement = approvedPage.identifierTableResults.findElements(By.tagName("tr"));
            //Check the data is correctly sort (10 results max)
            for(int trIndex = 0; trIndex < listTrWebElement.size(); trIndex++){
                listTdWebElement = listTrWebElement.get(trIndex).findElements(By.tagName("td"));
                content = listTdWebElement.get(tdIndex).getAttribute("textContent");

                if(registrator.contains("Veterinary") && headerName.equals("Ownerinformation")){
                    String splittedString [] = content.split(" ");
                    if(splittedString.length > 1){
                        if(splittedString[0].startsWith("HK") || splittedString[0].startsWith("BP") || splittedString[0].startsWith("BR") || splittedString[0].startsWith("RE")){
                            content = splittedString[1];//set the content with only the first name
                        } else {
                            content = splittedString[0];//set the content with only the first name
                        }
                    }
                }

                if(headerName.equals("Identifier") && content.contains(",")){
                    content = content.replaceAll("\\s+", "");
                    String identifiers[] = content.split(",");
                    if(identifiers.length == 2){
                        content = identifiers[0];
                    }
                }

                //Removed Vet number of name string
                if(headerName.equals("Vet")){
                    String splittedValue[] = content.split(" ", 2);
                    if(splittedValue.length >= 2){
                        content = splittedValue[1];
                    }
                }
                buildColumnStrings.add(content);
            }
            boolean isDate = false;
            if(headerName.contains("Date") ||headerName.contains("date")){
                isDate = true;
            }
            if(isAscendingListOfString(buildColumnStrings, isDate)){
                logger.log(LogStatus.INFO,"Ascendant Column " + headerName + " is correctly sorted");
            } else {
                logger.log(LogStatus.ERROR,"ERROR: Ascendant Column " + headerName + " is not correctly sorted");
            }
            //Increase the index to check tbody/td[index]
            tdIndex++;
        }
    }

    private void descendingSortCheck(WebApprovedPage approvedPage, WebStartPage startPage) throws Exception {
        reloadApprovedFilterPageWithCatIdentifier(approvedPage, startPage);
        List<WebElement> listHeaderButton = approvedPage.headerRow.findElements(By.tagName("th"));
        //Check all columns
        List<WebElement> pageNumberList;
        List<String> buildColumnStrings;
        List<WebElement> listTrWebElement;
        List<WebElement> listTdWebElement;
        String content;
        int tdIndex = 0;
        //Select 50 rows per page
        click(approvedPage.pageSelectionDropList);
        click(approvedPage.fiftyRowsByPageSelection);
        String headerName = "";
        for (WebElement header : listHeaderButton){
            buildColumnStrings = new ArrayList<String>();
            headerName = header.getAttribute("textContent").replaceAll("\\s+", "");
            scrollOnElement(approvedPage.searchField);
            //Sort the column as ascending
            click(header);
            click(header); // double click to descendant sort
            sleep(duration);
            listTrWebElement = approvedPage.identifierTableResults.findElements(By.tagName("tr"));
            //Check the data is correctly sort (10 results max)
            for(int trIndex = 0; trIndex < listTrWebElement.size(); trIndex++){
                listTdWebElement = listTrWebElement.get(trIndex).findElements(By.tagName("td"));
//                System.out.println(listTdWebElement.get(tdIndex).getAttribute("textContent"));
                content = listTdWebElement.get(tdIndex).getAttribute("textContent");
                if(registrator.contains("Veterinary") && headerName.equals("Ownerinformation")){
                    String splittedString [] = content.split(" ");
                    if(splittedString.length > 1){
                        if(splittedString[0].startsWith("HK") || splittedString[0].startsWith("BP") || splittedString[0].startsWith("BR") || splittedString[0].startsWith("RE")){
                            content = splittedString[1];//set the content with only the first name
                        } else {
                            content = splittedString[0];//set the content with only the first name
                        }
                    }
                }
                if(headerName.equals("Identifier") && content.contains(",")){
                    content = content.replaceAll("\\s+", "");
                    String identifiers[] = content.split(",");
                    if(identifiers.length == 2){
                        content = identifiers[0];
                    }
                }

                //Removed Vet number of name string
                if(headerName.equals("Vet")){
                    String splittedValue[] = content.split(" ", 2);
                    if(splittedValue.length >= 2){
                        content = splittedValue[1];
                    }
                }
                buildColumnStrings.add(content);
            }
            boolean isDate = false;
            if(headerName.contains("Date") ||headerName.contains("date")){
                isDate = true;
            }
            if(isDescendingListOfString(buildColumnStrings, isDate)){
                logger.log(LogStatus.INFO,"Descendant Column " + headerName + " is correctly sorted");
            } else {
                logger.log(LogStatus.ERROR,"ERROR: Descendant Column " + headerName + " is not correctly sorted");
            }
//            System.out.println("############################################");
            //Increase the index to check tbody/td[index]
            tdIndex++;
        }
    }

    private void reloadApprovedFilterPageWithCatIdentifier(WebApprovedPage approvedPage, WebStartPage startPage){
        click(startPage.filterButton);
        if(registrator.contains("Veterinary")) {
            click(startPage.filterApprovedVetTabButton);
        } else {
            click(startPage.filterApprovedBreederAndRefugeTabButton);
        }
        WaitForElement(approvedPage.searchField,5);
    }

    private void checkApprovedFilterAccessibility(WebApprovedPage approvedPage, WebStartPage startPage){
        click(startPage.filterButton);
        if(registrator.contains("Veterinary")) {
            click(startPage.filterApprovedVetActionButton);
        } else {
            click(startPage.filterApprovedBreederAndRefugeActionButton);
        }
        try{
            WaitForElement(approvedPage.searchField,5);
            logger.log(LogStatus.INFO,"Action button for filter Approved is reachable");
        } catch (Exception e){
            logger.log(LogStatus.ERROR,"ERROR: Action button for filter Approved is not reachable");
        }
        click(startPage.homeButton);
        click(startPage.filterButton);
        if(registrator.contains("Veterinary")) {
            click(startPage.filterApprovedVetTabButton);
        } else {
            click(startPage.filterApprovedBreederAndRefugeTabButton);
        }
        try{
            WaitForElement(approvedPage.searchField,5);
            logger.log(LogStatus.INFO,"Tab button for filter Approved is reachable");
        } catch (Exception e){
            logger.log(LogStatus.ERROR,"ERROR: Tab button for filter Approved is not reachable");
        }
    }

    private boolean detectValidClickEdditButton(WebApprovedPage approvedPage, WebModifyPage modifyPage){
        boolean isValid;
        click(approvedPage.editButton);
        try{
            WaitForElement(modifyPage.searchField,10);
            isValid = true;
            logger.log(LogStatus.INFO, "Valid button click");
        } catch (Exception e){
            logger.log(LogStatus.ERROR, "ERROR: Invalid button click");
            sleep(5000);
            isValid = false;
        }
        return isValid;
    }

    private boolean detectValidClickCreateCsvtButton(WebApprovedPage approvedPage, List<String> expectedIdentifiers){
        boolean isValid;
        click(approvedPage.createCsvButton);
        String path = System.getProperty("user.dir")+"/src/test/java/Results";
        File latestModifiedFile = getLatestFilefromDir(path);
        if(latestModifiedFile != null && latestModifiedFile.getName().contains("Data export")){
            isValid = true;
            logger.log(LogStatus.INFO, "Valid create CSV button click for " + latestModifiedFile.getName());
            CsvHandler csvHandler = new CsvHandler(logger);
            csvHandler.setCsvPath(latestModifiedFile.getAbsolutePath());
            List<String[]> entries = csvHandler.readAll();
            if((entries.size() - 1) == expectedIdentifiers.size()){ // (entries.size() - 1) because there is the title row
                for(int i = 0; i < expectedIdentifiers.size(); i++){
                    String[] entry = entries.get(i + 1);
                    if(!entry[0].equals(expectedIdentifiers.get(i))){//Compare the first column of the csv row and the expected identifier
                        isValid = false;
                        System.out.println(entries.get(i + 1)[0] + " " + expectedIdentifiers.get(i));
                        logger.log(LogStatus.ERROR, "ERROR: CSV file doesn't match with selected identifiers");
                        break;
                    }
                    logger.log(LogStatus.INFO, "CSV entry: " + Arrays.toString(entry));
                }
                logger.log(LogStatus.INFO, "CSV file matches with selected identifiers");
            }
            //Delete CSV file because useless now
            try {
                if(latestModifiedFile.delete()){
                    logger.log(LogStatus.INFO, "CSV file is deleted");
                } else {
                    logger.log(LogStatus.ERROR, "CSV file is not deleted, please clean the file manually");
                }
            } catch (Exception e) {
                logger.log(LogStatus.ERROR, "ERROR: CSV file cannot be deleted");
            }
        } else {
            logger.log(LogStatus.ERROR, "ERROR: Invalid create CSV  button click");
            sleep(5000);
            isValid = false;
        }
        return isValid;
    }

    private boolean detectValidClickCreateCertificateButton(WebApprovedPage approvedPage){
        boolean isValid;
        //Certificate generation takes a lot of time (more or less 30s)

        click(approvedPage.createCertificateButton);
        sleep(40000);//Wait 40s to make sure the certificate is correctly generated and downloaded
        String path = System.getProperty("user.dir")+"/src/test/java/Results";
        File latestModifiedFile = getLatestFilefromDir(path);
        if(latestModifiedFile != null && latestModifiedFile.getName().contains("Certificate_")){
            isValid = true;
            logger.log(LogStatus.INFO, "Valid create Certificate button click for " + latestModifiedFile.getName());

            //Delete CSV file because useless now
            try {
                if(latestModifiedFile.delete()){
                    logger.log(LogStatus.INFO, "Certificate file is deleted");
                } else {
                    logger.log(LogStatus.ERROR, "Certificate file is not deleted, please clean the file manually");
                }
            } catch (Exception e) {
                logger.log(LogStatus.ERROR, "ERROR: Certificate file cannot be deleted");
            }
        } else {

            if(latestModifiedFile != null){
                logger.log(LogStatus.ERROR, "ERROR: Invalid create Certificate  button click ___ " + latestModifiedFile.getName());
            } else {
                logger.log(LogStatus.ERROR, "ERROR: Invalid create Certificate  button click");
            }
            sleep(5000);
            isValid = false;
        }
        return isValid;
    }

    private void checkButtonMenu(WebApprovedPage approvedPage, WebStartPage startPage, WebModifyPage modifyPage){
        reloadApprovedFilterPageWithCatIdentifier(approvedPage, startPage);

        //buttons menu are available
        //select all
        //user can click on the button
        //case no lines of the current page are selected
        //all lines becomes selected
        click(approvedPage.selectALLButton);
        int maxNumberRow = approvedPage.identifierTableResults.findElements(By.tagName("tr")).size();
        boolean areAllSelected = true;
        for(int i = 0; i < maxNumberRow; i++){
           if(!isSpecificColor(getRgbString(getRow(i,approvedPage.identifierTableResults)), "52", "69", "78")){ // gray color
               areAllSelected = false;
               break;
           }
        }
        if(areAllSelected){
            logger.log(LogStatus.INFO,"All lines become selected");
        } else {
            logger.log(LogStatus.ERROR,"ERROR: All lines don't become selected");
        }
        //case some lines of the current page are selected
        //all lines becomes selected
        if(maxNumberRow == 10) {
            //Unselect few lines
            click(getRow(1, approvedPage.identifierTableResults));
            click(getRow(5, approvedPage.identifierTableResults));
            click(getRow(8, approvedPage.identifierTableResults));
            click(approvedPage.selectALLButton);
            areAllSelected = true;
            for (int i = 0; i < 10; i++) {
                if (!isSpecificColor(getRgbString(getRow(i, approvedPage.identifierTableResults)), "52", "69", "78")) { // gray color
                    areAllSelected = false;
                    break;
                }
            }
            if (areAllSelected) {
                logger.log(LogStatus.INFO, "All lines become selected");
            } else {
                logger.log(LogStatus.ERROR, "ERROR: All lines don't become selected");
            }
            //case all lines of the current page are already selected
            //nothing change, all lines stay selected
            click(approvedPage.selectALLButton);
            areAllSelected = true;
            for (int i = 0; i < 10; i++) {
                if (!isSpecificColor(getRgbString(getRow(i, approvedPage.identifierTableResults)), "52", "69", "78")) { // gray color
                    areAllSelected = false;
                    break;
                }
            }
            if (areAllSelected) {
                logger.log(LogStatus.INFO, "All lines become selected");
            } else {
                logger.log(LogStatus.ERROR, "ERROR: All lines don't become selected");
            }

            //unselect all
            //by default, button is disable
            reloadApprovedFilterPageWithCatIdentifier(approvedPage, startPage);
            if (approvedPage.unselectAllButton.getAttribute("class").equals("ui button buttons-select-none disabled")) {
                logger.log(LogStatus.INFO, "Menu Unselect All is disabled");
            } else {
                logger.log(LogStatus.ERROR, "ERROR: Menu Unselect All is enabled");
            }
            //case no line of the current page are selected
            //button stay disable
            boolean areNotAllSelected = true;
            for (int i = 0; i < 10; i++) {
                if (!isNoColor(getRgbString(getRow(i, approvedPage.identifierTableResults)))) {
                    areNotAllSelected = false;
                    break;
                }
            }
            if (areNotAllSelected && approvedPage.unselectAllButton.getAttribute("class").equals("ui button buttons-select-none disabled")) {
                logger.log(LogStatus.INFO, "Unselect button stays disable");
            } else {
                logger.log(LogStatus.ERROR, "ERROR: Unselect don't button stays disable");
            }
            //case one line of the current page is selected
            //button becomes enable
            click(getRow(5, approvedPage.identifierTableResults));
            if (areNotAllSelected && approvedPage.unselectAllButton.getAttribute("class").equals("ui button buttons-select-none")) {
                logger.log(LogStatus.INFO, "Unselect button becomes enable");
            } else {
                logger.log(LogStatus.ERROR, "ERROR: Unselect stays disable");
            }
            //user can click on button
            click(approvedPage.unselectAllButton);
            //selected line becomes unselected
            if (isNoColor(getRgbString(getRow(5, approvedPage.identifierTableResults))) && approvedPage.unselectAllButton.getAttribute("class").equals("ui button buttons-select-none disabled")) {
                logger.log(LogStatus.INFO, "Selected line becomes unselected");
            } else {
                logger.log(LogStatus.ERROR, "ERROR: Selected line doesn't become unselected");
            }
            //case some lines of the current page  are selected
            click(getRow(1, approvedPage.identifierTableResults));
            click(getRow(5, approvedPage.identifierTableResults));
            click(getRow(8, approvedPage.identifierTableResults));
            //button becomes enable
            if (approvedPage.unselectAllButton.getAttribute("class").equals("ui button buttons-select-none")) {
                logger.log(LogStatus.INFO, "Menu Unselect All is enable");
            } else {
                logger.log(LogStatus.ERROR, "ERROR: Menu Unselect All is disable");
            }
            //user can click on button
            click(approvedPage.unselectAllButton);
            //selected line becomes unselected
            if (isNoColor(getRgbString(getRow(1, approvedPage.identifierTableResults))) &&
                    isNoColor(getRgbString(getRow(5, approvedPage.identifierTableResults))) &&
                    isNoColor(getRgbString(getRow(8, approvedPage.identifierTableResults))) &&
                    approvedPage.unselectAllButton.getAttribute("class").equals("ui button buttons-select-none disabled")) {
                logger.log(LogStatus.INFO, "Selected lines become unselected");
            } else {
                logger.log(LogStatus.ERROR, "ERROR: Selected lines don't become unselected");
            }
            //case all lines of the current page are selected
            click(approvedPage.selectALLButton);
            //button becomes enable
            if (approvedPage.unselectAllButton.getAttribute("class").equals("ui button buttons-select-none")) {
                logger.log(LogStatus.INFO, "Menu Unselect All is enable");
            } else {
                logger.log(LogStatus.ERROR, "ERROR: Menu Unselect All is disable");
            }
            //user can click on button
            click(approvedPage.unselectAllButton);
            //selected line becomes unselected
            areNotAllSelected = true;
            for (int i = 0; i < 10; i++) {
                if (!isNoColor(getRgbString(getRow(i, approvedPage.identifierTableResults)))) {
                    areNotAllSelected = false;
                    break;
                }
            }
            if (areNotAllSelected && approvedPage.unselectAllButton.getAttribute("class").equals("ui button buttons-select-none disabled")) {
                logger.log(LogStatus.INFO, "Selected line becomes unselected");
            } else {
                logger.log(LogStatus.ERROR, "ERROR: Selected line stay selected");
            }

            //Edit
            //By default, button is enabled
            //user can click on the button
            //case no line is selected
            detectInvalidClickButton(approvedPage.editButton, approvedPage, "Please select from the list");//This test valid that by default the button is enable
            //case one line is selected
            reloadApprovedFilterPageWithCatIdentifier(approvedPage, startPage); //reload just in case of error...
            click(getRow(1, approvedPage.identifierTableResults));
            detectValidClickEdditButton(approvedPage, modifyPage);
            //case several lines are selected
            reloadApprovedFilterPageWithCatIdentifier(approvedPage, startPage); //reload just in case of error...
            click(getRow(1, approvedPage.identifierTableResults));
            click(getRow(4, approvedPage.identifierTableResults));
            click(getRow(7, approvedPage.identifierTableResults));
            detectInvalidClickButton(approvedPage.editButton, approvedPage, "Select only one");

            //Create Certificate
            //By default, button is enabled
            //user can click on the button
            //case no line is selected
            //error message is displayed "Please select from the list"
            reloadApprovedFilterPageWithCatIdentifier(approvedPage, startPage); //reload just in case of error...
            detectInvalidClickButton(approvedPage.createCertificateButton, approvedPage, "Please select from the list");//This test valid that by default the button is enable
            //case one line is selected
            //a certificate is created and downloaded
            reloadApprovedFilterPageWithCatIdentifier(approvedPage, startPage); //reload just in case of error...
            click(getRow(1, approvedPage.identifierTableResults));
            detectValidClickCreateCertificateButton(approvedPage);
            //case several lines are selected
            //error message is displayed : "select only one"
            reloadApprovedFilterPageWithCatIdentifier(approvedPage, startPage); //reload just in case of error...
            click(getRow(5, approvedPage.identifierTableResults));
            click(getRow(8, approvedPage.identifierTableResults));
            detectInvalidClickButton(approvedPage.createCertificateButton, approvedPage, "Select only one");//This test valid that by default the button is enable
            //create CSV
            //By default, button is enabled
            //user can click on the button
            //case no line is selected
            //Error message is displayed
            reloadApprovedFilterPageWithCatIdentifier(approvedPage, startPage); //reload just in case of error...
            detectInvalidClickButton(approvedPage.createCsvButton, approvedPage, "Please select from the list");//This test valid that by default the button is enable
            //case one line is selected
            //a CSV file is generated
            reloadApprovedFilterPageWithCatIdentifier(approvedPage, startPage); //reload just in case of error...
            click(getRow(1, approvedPage.identifierTableResults));
            List<String> expectedIdentifiers = new ArrayList<String>();
            expectedIdentifiers.add(getCell(1, 0, approvedPage.identifierTableResults).getAttribute("textContent"));
            detectValidClickCreateCsvtButton(approvedPage, expectedIdentifiers);
            //file contains the line selected
            //case several lines are selected
            //a CSV file is generated
            //file contains the lines selected
            reloadApprovedFilterPageWithCatIdentifier(approvedPage, startPage); //reload just in case of error...
            click(getRow(1, approvedPage.identifierTableResults));
            click(getRow(2, approvedPage.identifierTableResults));
            expectedIdentifiers = new ArrayList<String>();
            expectedIdentifiers.add(getCell(1, 0, approvedPage.identifierTableResults).getAttribute("textContent"));
            expectedIdentifiers.add(getCell(2, 0, approvedPage.identifierTableResults).getAttribute("textContent"));
            detectValidClickCreateCsvtButton(approvedPage, expectedIdentifiers);
            //case selected line has multiple identifier
            //user must be able to edit CSV
            //both identifier must be set in column identifier
            reloadApprovedFilterPageWithCatIdentifier(approvedPage, startPage); //reload just in case of error...
            setText(approvedPage.searchField, ",");
            sleep(1000);
            click(getRow(1, approvedPage.identifierTableResults));
            click(getRow(2, approvedPage.identifierTableResults));
            expectedIdentifiers = new ArrayList<String>();
            expectedIdentifiers.add(getCell(1, 0, approvedPage.identifierTableResults).getAttribute("textContent"));
            expectedIdentifiers.add(getCell(2, 0, approvedPage.identifierTableResults).getAttribute("textContent"));
            detectValidClickCreateCsvtButton(approvedPage, expectedIdentifiers);

        }
    }

    private void checkSelectAndUnSelectLine(WebApprovedPage approvedPage, WebStartPage startPage){
        reloadApprovedFilterPageWithCatIdentifier(approvedPage, startPage);
        //User can select lines
        //by clicking on a line
        //line becomes gray
        WebElement firstRow = getRow(0,approvedPage.identifierTableResults);
        click(firstRow);
        if(isSpecificColor(getRgbString(firstRow), "52", "69", "78")){
            logger.log(LogStatus.INFO,"First line is gray when selected (by clicking)");
        } else {
            logger.log(LogStatus.ERROR,"ERROR: First line is not gray when selected (by clicking)");
        }
        //Label row selected is incremented by 1
        if(approvedPage.pageInfo.getAttribute("textContent").contains(("1 row selected"))){
            logger.log(LogStatus.INFO,"Label row selected is incremented by 1");
        } else {
            logger.log(LogStatus.ERROR,"ERROR: Label row selected is not incremented by 1");
        }
        //Menu Unselect All is enabled
        if(approvedPage.unselectAllButton.getAttribute("class").equals("ui button buttons-select-none")){
            logger.log(LogStatus.INFO,"Menu Unselect All is enabled");
        } else {
            logger.log(LogStatus.ERROR,"ERROR: Menu Unselect All is disabled");
        }

        WebElement thirdRow = getRow(2,approvedPage.identifierTableResults);
        click(thirdRow);
        if(isSpecificColor(getRgbString(thirdRow), "52", "69", "78")){
            logger.log(LogStatus.INFO,"Third line is gray when selected (by clicking)");
        } else {
            logger.log(LogStatus.ERROR,"ERROR: Third line is not gray when selected (by clicking)");
        }
        //Label row selected is incremented by 1
        if(approvedPage.pageInfo.getAttribute("textContent").contains(("2 rows selected"))){
            logger.log(LogStatus.INFO,"Label row selected is incremented by 1");
        } else {
            logger.log(LogStatus.ERROR,"ERROR: Label row selected is not incremented by 1");
        }

        //User can deselect lines
        //line must be selected and color gray
        //user click on the line
        //line become deselected
        //color line becomes white
        click(approvedPage.firstIdentifierTableResult);
        if(isNoColor(getRgbString(approvedPage.firstIdentifierTableResult))){
            logger.log(LogStatus.INFO,"First line is white when selected (by clicking)");
        } else {
            logger.log(LogStatus.ERROR,"ERROR: First line is not white when selected (by clicking)");
        }
        //Label row selected is decremented by 1
        if(approvedPage.pageInfo.getAttribute("textContent").contains(("1 row selected"))){
            logger.log(LogStatus.INFO,"Label row selected is decremented by 1");
        } else {
            logger.log(LogStatus.ERROR,"ERROR: Label row selected is not decremented by 1");
        }

        click(thirdRow);
        if(isNoColor(getRgbString(thirdRow))){
            logger.log(LogStatus.INFO,"Third line is white when selected (by clicking)");
        } else {
            logger.log(LogStatus.ERROR,"ERROR: Third line is not white when selected (by clicking)");
        }

        //Label row selected is decremented by 1
        if(!approvedPage.pageInfo.getAttribute("textContent").contains(("selected"))){
            logger.log(LogStatus.INFO,"Label row selected is decremented by 1");
        } else {
            logger.log(LogStatus.ERROR,"ERROR: Label row selected is not decremented by 1");
        }
        //case no more lines are selected
        //menu unselect all is disabled
        if(approvedPage.unselectAllButton.getAttribute("class").equals("ui button buttons-select-none disabled")){
            logger.log(LogStatus.INFO,"Menu Unselect All is disabled");
        } else {
            logger.log(LogStatus.ERROR,"ERROR: Menu Unselect All is enabled");
        }
    }

    private void checkSearchField(WebApprovedPage approvedPage, WebStartPage startPage){
        reloadApprovedFilterPageWithCatIdentifier(approvedPage, startPage);
        HashMap<String,String> expectedResult = new HashMap<String, String>();
        HashMap<String,String> searchWords = new HashMap<String, String>();
        if(registrator.equals("Refuge 3")){
            expectedResult.put("Identifier", "123456789502285");
            expectedResult.put("PassportNumber", "TC0030012018153811");
            expectedResult.put("DateOfBirth", "30/01/2018");
            expectedResult.put("IdentificationDate", "30/01/2018");
            expectedResult.put("Gender", "Male");
            expectedResult.put("CatBreed", "Abyssinian");
            expectedResult.put("Sterelized", "Yes");
            expectedResult.put("SterelizationDate", "30/01/2018");
            expectedResult.put("Info", "F0024 Alice Geldigekaart SPECIMEN");
            searchWords.put("Identifier", "502285");
            searchWords.put("PassportNumber", "30012018153811");
            searchWords.put("DateOfBirth", "30/01/2018");
            searchWords.put("IdentificationDate", "30/01/2018");
            searchWords.put("Gender", "Male");
            searchWords.put("CatBreed", "Abyssinian");
            searchWords.put("Sterelized", "Yes");
            searchWords.put("SterelizationDate", "30/01/2018");
            searchWords.put("Info", "F0024");

        } else if(registrator.equals("Breeder 4")){
            expectedResult.put("Identifier", "123456789501884");
            expectedResult.put("PassportNumber", "TC130012018112145");
            expectedResult.put("DateOfBirth", "30/01/2018");
            expectedResult.put("IdentificationDate", "30/01/2018");
            expectedResult.put("Gender", "Male");
            expectedResult.put("CatBreed", "Abyssinian");
            expectedResult.put("Sterelized", "Yes");
            expectedResult.put("SterelizationDate", "30/01/2018");
            expectedResult.put("Info", "F0024 Alice Geldigekaart SPECIMEN");
            searchWords.put("Identifier", "501884");
            searchWords.put("PassportNumber", "30012018112145");
            searchWords.put("DateOfBirth", "30/01/2018");
            searchWords.put("IdentificationDate", "30/01/2018");
            searchWords.put("Gender", "Male");
            searchWords.put("CatBreed", "Abyssinian");
            searchWords.put("Sterelized", "Yes");
            searchWords.put("SterelizationDate", "30/01/2018");
            searchWords.put("Info", "F0024");
        } else {
            expectedResult.put("Identifier", "123456789501685");
            expectedResult.put("PassportNumber", "TC1030012018113232");
            expectedResult.put("DateOfBirth", "01/05/2017");
            expectedResult.put("IdentificationDate", "08/05/2017");
            expectedResult.put("Gender", "Female");
            expectedResult.put("CatBreed", "Sibirer");
            expectedResult.put("Sterelized", "Yes");
            expectedResult.put("SterelizationDate", "05/06/2017");
            expectedResult.put("Info", "Thierry Foulon");
            searchWords.put("Identifier", "501685");
            searchWords.put("PassportNumber", "30012018113232");
            searchWords.put("DateOfBirth", "01/05/2017");
            searchWords.put("IdentificationDate", "08/05/2017");
            searchWords.put("Gender", "Female");
            searchWords.put("CatBreed", "Sibirer");
            searchWords.put("Sterelized", "Yes");
            searchWords.put("SterelizationDate", "05/06/2017");
            searchWords.put("Info", "Thierry");
        }

        //User can perform searches
        //by enconding string in field Search
        //results are displayed after each character encoded
        //search will fetch on column
        //identifier  -> "500319" to search for "123456789500319"
        setText(approvedPage.searchField,searchWords.get("Identifier"));
        sleep(duration); // Wait 1s to let the result to be refreshed
        getScreenShot();
        String identifier = lookForExpectedResult(expectedResult.get("Identifier"), approvedPage.identifierTableResults, 0);
        if(identifier.contains(expectedResult.get("Identifier"))){
            logger.log(LogStatus.INFO,"Identifier search for " + searchWords.get("Identifier") + " works and the result is " + identifier);
        } else {
            logger.log(LogStatus.ERROR,"ERROR: Identifier search for " + searchWords.get("Identifier") + " doesn't work and the result is " + identifier);
        }
        //passport number
        setText(approvedPage.searchField,searchWords.get("PassportNumber"));
        sleep(duration); // Wait 1s to let the result to be refreshed
        getScreenShot();
        String passportResult = lookForExpectedResult(expectedResult.get("PassportNumber"), approvedPage.identifierTableResults, 1);
        if(passportResult.contains(expectedResult.get("PassportNumber"))){
            logger.log(LogStatus.INFO,"Passport search for " + searchWords.get("PassportNumber") + " works and the result is " + passportResult);
        } else {
            logger.log(LogStatus.ERROR,"ERROR: Passport search for " + searchWords.get("PassportNumber") + " doesn't work and the result is " + passportResult);
        }
        //date of birth
        setText(approvedPage.searchField,searchWords.get("DateOfBirth"));
        sleep(duration); // Wait 1s to let the result to be refreshed
        getScreenShot();
        String birthDateResult = lookForExpectedResult(expectedResult.get("DateOfBirth"), approvedPage.identifierTableResults, 2);
        if(birthDateResult.contains(expectedResult.get("DateOfBirth"))){
            logger.log(LogStatus.INFO,"Date of birth search for " + searchWords.get("DateOfBirth") + " works and the result is " + birthDateResult);
        } else {
            logger.log(LogStatus.ERROR,"ERROR: Date of birth search for "+ searchWords.get("DateOfBirth") + " doesn't work and the result is " + birthDateResult);
        }
        //identification date
        setText(approvedPage.searchField,searchWords.get("IdentificationDate"));
        sleep(duration); // Wait 1s to let the result to be refreshed
        getScreenShot();
        String identificationDateResult = lookForExpectedResult(expectedResult.get("IdentificationDate"), approvedPage.identifierTableResults, 3);
        if(identificationDateResult.contains(expectedResult.get("IdentificationDate"))){
            logger.log(LogStatus.INFO,"Date of identification search for " + searchWords.get("IdentificationDate") + " works and the result is " + identificationDateResult);
        } else {
            logger.log(LogStatus.ERROR,"ERROR: Date of identification search for " + searchWords.get("IdentificationDate") + " doesn't work and the result is " + identificationDateResult);
        }
        //Gender
        setText(approvedPage.searchField,searchWords.get("Gender"));
        sleep(duration); // Wait 1s to let the result to be refreshed
        getScreenShot();
        String genderResult = lookForExpectedResult(expectedResult.get("Gender"), approvedPage.identifierTableResults, 4);
        if(genderResult.contains(expectedResult.get("Gender"))){
            logger.log(LogStatus.INFO,"Gender search for " + searchWords.get("Gender") + " works and the result is " + genderResult);
        } else {
            logger.log(LogStatus.ERROR,"ERROR: Gender search for " + searchWords.get("Gender") + " doesn't work and the result is " + genderResult);
        }
        //Cat Breed
        setText(approvedPage.searchField,searchWords.get("CatBreed"));
        sleep(duration); // Wait 1s to let the result to be refreshed
        getScreenShot();
        String breedResult = lookForExpectedResult(expectedResult.get("CatBreed"), approvedPage.identifierTableResults, 5);
        if(breedResult.contains(expectedResult.get("CatBreed"))){
            logger.log(LogStatus.INFO,"Breed search for " + searchWords.get("CatBreed") + " works and the result is " + breedResult);
        } else {
            logger.log(LogStatus.ERROR,"ERROR: Breed search for " + searchWords.get("CatBreed") + " doesn't work and the result is " + breedResult);
        }
        //Sterilized
        setText(approvedPage.searchField,searchWords.get("Sterelized"));
        sleep(duration); // Wait 1s to let the result to be refreshed
        getScreenShot();
        String sterilizedResult = lookForExpectedResult(expectedResult.get("Sterelized"), approvedPage.identifierTableResults, 6);
        if(sterilizedResult.contains(expectedResult.get("Sterelized"))){
            logger.log(LogStatus.INFO,"Sterilzed search for " + searchWords.get("Sterelized") + " works and the result is " + sterilizedResult);
        } else {
            logger.log(LogStatus.ERROR,"ERROR: Sterilzed search for " + searchWords.get("Sterelized") + " doesn't work and the result is " + sterilizedResult);
        }
        //sterilized date
        setText(approvedPage.searchField,searchWords.get("SterelizationDate"));
        sleep(duration); // Wait 1s to let the result to be refreshed
        getScreenShot();
        String sterilizedDateResult = lookForExpectedResult(expectedResult.get("SterelizationDate"), approvedPage.identifierTableResults, 7);
        if(sterilizedDateResult.contains(expectedResult.get("SterelizationDate"))){
            logger.log(LogStatus.INFO,"Sterilzed date search for " + searchWords.get("SterelizationDate") + " works and the result is " + sterilizedDateResult);
        } else {
            logger.log(LogStatus.ERROR,"ERROR: Sterilzed date search for " + searchWords.get("SterelizationDate") + " doesn't work and the result is " + sterilizedDateResult);
        }
        //owner information
        setText(approvedPage.searchField,searchWords.get("Info"));
        sleep(duration); // Wait 1s to let the result to be refreshed
        getScreenShot();
        String ownerInfoResult = lookForExpectedResult(expectedResult.get("Info"), approvedPage.identifierTableResults, 8);
        if(ownerInfoResult.contains(searchWords.get("Info"))){
            logger.log(LogStatus.INFO,"Owner Information search for " + searchWords.get("Info") + " works and the result is " + ownerInfoResult);
        } else {
            logger.log(LogStatus.ERROR,"ERROR: Owner Information search for " + searchWords.get("Info") + " doesn't work and the result is " + ownerInfoResult);
        }
    }

//    private void approvedFilterCheck(WebApprovedPage approvedPage, WebStartPage startPage, WebEazyIdPage eazyIdPage, SqlOracleMiddleware metaformMiddleWare, WebModifyPage modifyPage) throws SQLException {
    private void approvedFilterCheck(WebApprovedPage approvedPage, WebStartPage startPage, SqlOracleMiddleware catIdMiddleWare, WebModifyPage modifyPage) throws Exception {
        click(startPage.homeButton);
        //by clicking on the menu filters - Approved
        //The all cats page is displayed
        //all cats where vet are linked and approved by system are displayed
        //total number of cats is the sum of
        String numberCatApprovedDisplayed = "";
        String numberCatApprovedSql = "";
        String refnum = "";
        ArrayList<HashMap> rs;
        if(registrator.contains("Veterinary")){
            WaitForElement(startPage.filterApprovedVetActionButton,10);
            numberCatApprovedDisplayed = startPage.filterApprovedVetActionButton.getAttribute("textContent").replaceAll("\\s+", "");
            refnum = "F0024";
            rs = catIdMiddleWare.executeSqlQuery("SELECT COUNT(ANIMAL.ID) FROM ABIEC_ANIMAL animal, ABIEC_PERSON pers WHERE pers.ID = animal.VETERINARYID AND pers.REFNUM = '" + refnum + "'");
        } else {
            if(registrator.equals("Breeder 4")){ //Breeder
                refnum = "HK159357";
            } else { //Refuge
                refnum = "HK852456";
            }
            WaitForElement(startPage.filterApprovedBreederAndRefugeActionButton,5);
            numberCatApprovedDisplayed = startPage.filterApprovedBreederAndRefugeActionButton.getAttribute("textContent").replaceAll("\\s+", "");
            rs = catIdMiddleWare.executeSqlQuery("SELECT COUNT(ANIMAL.ID) FROM ABIEC_ANIMAL animal, ABIEC_PERSON pers WHERE pers.ID = animal.OWNERID AND pers.REFNUM = '" + refnum + "'");
        }

//        ArrayList<HashMap> rs = metaformMiddleWare.executeSqlQuery("SELECT COUNT(*) from METAFORM_ABIEC_ANIMAL where MF_STATUS = 256 AND VET_REFNUM = '" + refnum + "'");
        if (rs.size() == 0) {
            logger.log(LogStatus.ERROR, "Something is wrong with SQL statement or DB connection");
        } else {
            numberCatApprovedSql = (String)rs.get(0).get("COUNT(ANIMAL.ID)");
            if(numberCatApprovedSql != null && numberCatApprovedDisplayed != null && numberCatApprovedSql.equals(numberCatApprovedDisplayed)){
                logger.log(LogStatus.INFO,"All cats where vet is linked and approved by system are displayed");
            } else {
                logger.log(LogStatus.ERROR,"ERROR: All cats where vet is not linked or approved by system are not displayed");
            }
        }
        //Page is showing 10 cats per page by default
        if(registrator.contains("Veterinary")){
            click(startPage.filterApprovedVetActionButton);
        } else {
            click(startPage.filterApprovedBreederAndRefugeActionButton);
        }
        List<WebElement> listResults = approvedPage.identifierTableResults.findElements(By.tagName("tr"));
        //Wait a little bit the page is completely loaded to take a screenshot
        WaitForElement(approvedPage.firstIdentifierTableResult,5);
        if(listResults.size() == 10){
            logger.log(LogStatus.INFO,"Page is showing 10 cats per page by default");
            if(isElementPresent(approvedPage.nextButton)){
                //case of more than 10 cats
                //next button is displayed
                logger.log(LogStatus.INFO,"Next button is displayed");
                //case there are more than one page available
                //case there are more than 10 cats
                //pages number are displayed
                List<WebElement> pageNumberList = approvedPage.paginationMenu.findElements(By.tagName("a"));
                if(pageNumberList.size() > 5){
                    //is it possible to click on the specific page
                    //selected page is displayed
                    //other available pages are still available for click
                    //page 1 display results from 1 to 10
                    //page 2 display result from 11 to 20
                    //page 3 display result from 21 to 30

                    logger.log(LogStatus.INFO,"Pages number are displayed");
                    if(approvedPage.pageInfo.getAttribute("textContent").contains("1 to 10")){
                        logger.log(LogStatus.INFO,"Page 1 display results from 1 to 10");
                    } else {
                        logger.log(LogStatus.ERROR,"ERROR: Page 1 doesn't display results from 1 to 10: " + approvedPage.pageInfo.getAttribute("textContent"));
                    }
                    click(pageNumberList.get(1)); //Click page 2
                    WaitForElement(approvedPage.firstIdentifierTableResult,5);
                    logger.log(LogStatus.INFO,"Click page 2");
                    if(approvedPage.pageInfo.getAttribute("textContent").contains("11 to 20")){
                        logger.log(LogStatus.INFO,"Page 2 display results from 11 to 20");
                    } else {
                        logger.log(LogStatus.ERROR,"ERROR: Page 2 doesn't display results from 11 to 20: " + approvedPage.pageInfo.getAttribute("textContent"));
                    }
                    pageNumberList = approvedPage.paginationMenu.findElements(By.tagName("a"));
                    //Indexes change between multiple clicks
                    click(pageNumberList.get(3)); //Click page 3
                    WaitForElement(approvedPage.firstIdentifierTableResult,5);
                    logger.log(LogStatus.INFO,"Click page 3");
                    if(approvedPage.pageInfo.getAttribute("textContent").contains("21 to")){
                        logger.log(LogStatus.INFO,"Page 3 display results from 21 to");
                    } else {
                        logger.log(LogStatus.ERROR,"ERROR: Page 3 doesn't display results from 21 to 30: " + approvedPage.pageInfo.getAttribute("textContent"));
                    }
                    pageNumberList = approvedPage.paginationMenu.findElements(By.tagName("a"));
                    click(pageNumberList.get(1)); //Click page 1
                    WaitForElement(approvedPage.firstIdentifierTableResult,5);
                    logger.log(LogStatus.INFO,"Click page 1");
                } else {
                    logger.log(LogStatus.INFO,"There are not enough forms in the filter to test multiple pages");
                }

            } else {
                logger.log(LogStatus.INFO,"Next button is not displayed");
            }
            if(approvedPage.numberResultByPageSelectionDroplist.getAttribute("textContent").contains("10")
                    && approvedPage.numberResultByPageSelectionDroplist.getAttribute("textContent").contains("25")
                    && approvedPage.numberResultByPageSelectionDroplist.getAttribute("textContent").contains("50")
                    && approvedPage.numberResultByPageSelectionDroplist.getAttribute("textContent").contains("100")){

                logger.log(LogStatus.INFO,"Number of result by page can be modify between {10, 25, 50, 100} rows");
            } else {
                logger.log(LogStatus.ERROR,"ERROR: Number of result by page cannot be modify between {10, 25, 50, 100} rows");
            }
            if(isElementPresent(approvedPage.previousButton)) {
                //case of more than 10 cats
                //previous button is displayed
                logger.log(LogStatus.INFO, "Previous button is displayed");
            } else {
                logger.log(LogStatus.ERROR, "ERROR: Previous button is not displayed");
            }


        } else {
            logger.log(LogStatus.INFO,"There are not enough forms in the filter to test more than 10 pages");
        }
        ascendingSortCheck(approvedPage, startPage);
        descendingSortCheck(approvedPage, startPage);
        checkSearchField(approvedPage,startPage);
        checkSelectAndUnSelectLine(approvedPage,startPage);
        checkButtonMenu(approvedPage,startPage,modifyPage);
    }
}
