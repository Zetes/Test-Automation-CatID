package ElementaryScripts.Web;

import DataBaseFramework.SqlOracleMiddleware;
import Screens.*;
import com.relevantcodes.extentreports.LogStatus;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class FilterIncompletePage extends FilterPage {
    private int duration = 5000;
    public void run() throws Exception {
        try {
            WebEazyIdPage eazyIdPage = new WebEazyIdPage(driver);
            WebStartPage startPage = new WebStartPage(driver);
            WebIncompletePage incompletePage = new WebIncompletePage(driver);
            WebModifyPage modifyPage = new WebModifyPage(driver);
            WebRegisterPage registerPage = new WebRegisterPage(driver);

            SqlOracleMiddleware metaformMiddleWare = new SqlOracleMiddleware(logger, "MF_CATS", "MF_CATS", "jdbc:oracle:thin:@192.168.17.6:1521:PREPROD");

            //Accessibility check
            checkIncompleteFilterAccessibility( incompletePage, startPage);

            //Incomplete filter check
            incompleteFilterCheck(incompletePage, startPage, metaformMiddleWare, registerPage);

            //logout for next page
            click(startPage.logoutButton);
        }catch (Exception e) {
            logger.logScreenshot(LogStatus.ERROR, "ERROR stopping the test", getScreenShot());
            throw e;
        }
    }

    private void ascendingSortCheck(WebIncompletePage incomplete, WebStartPage startPage) throws Exception {
        reloadIncompleteFilterPageWithCatIdentifier(incomplete, startPage);
        List<WebElement> listHeaderButton = incomplete.headerRow.findElements(By.tagName("th"));
        //Check all columns
        List<WebElement> pageNumberList;
        List<String> buildColumnStrings;
        List<WebElement> listTrWebElement;
        List<WebElement> listTdWebElement;
        String content;
        int tdIndex = 0;
        //Select 50 rows per page
        click(incomplete.pageSelectionDropList);
        click(incomplete.fiftyRowsByPageSelection);
        String headerName = "";
        for (WebElement header : listHeaderButton){
            buildColumnStrings = new ArrayList<String>();
            headerName = header.getAttribute("textContent").replaceAll("\\s+", "");
            scrollOnElement(incomplete.searchField);
            //Sort the column as ascending
            click(header); //Click for ascendant sort
            pageNumberList = incomplete.paginationMenu.findElements(By.tagName("a"));
            //Click on the last page
            if(pageNumberList.size() > 5) {
                click(pageNumberList.get(pageNumberList.size() - 2)); //Next button is the last button at index "size - 1"
                click(incomplete.previousButton); // to make sure there are probably 50 rows
            }
            sleep(duration);//need time to load 50 rows
            listTrWebElement = incomplete.identifierTableResults.findElements(By.tagName("tr"));
            //Check the data is correctly sort (10 results max)
            for(int trIndex = 0; trIndex < listTrWebElement.size(); trIndex++){
                listTdWebElement = listTrWebElement.get(trIndex).findElements(By.tagName("td"));
//                System.out.println(listTdWebElement.get(tdIndex).getAttribute("textContent"));
                content = listTdWebElement.get(tdIndex).getAttribute("textContent");

                if(headerName.equals("Identifier") && content.contains(",")){
                    content = content.replaceAll("\\s+", "");
                    String identifiers[] = content.split(",");
                    if(identifiers.length == 2){
                        if(identifiers.length == 2){
//                        if(identifiers[0].compareTo(identifiers[1]) > 0){
//                            content = identifiers[1];
//                        } else {
//                            content = identifiers[0];
//                        }
                            content = identifiers[0];
                        }
                    }
                }
                buildColumnStrings.add(content);
            }
            boolean isDate = false;
            if(headerName.contains("Date") ||headerName.contains("date")){
                isDate = true;
            }
            if(isAscendingListOfString(buildColumnStrings, isDate)){
                logger.log(LogStatus.INFO,"Ascendant Column " + headerName + " is correctly sorted");
            } else {
                logger.log(LogStatus.ERROR,"ERROR: Ascendant Column " + headerName + " is not correctly sorted");
            }
//            System.out.println("############################################");
            //Increase the index to check tbody/td[index]
            tdIndex++;
        }
    }

    private void descendingSortCheck(WebIncompletePage incomplete, WebStartPage startPage) throws Exception {
        reloadIncompleteFilterPageWithCatIdentifier(incomplete, startPage);
        List<WebElement> listHeaderButton = incomplete.headerRow.findElements(By.tagName("th"));
        //Check all columns
        List<WebElement> pageNumberList;
        List<String> buildColumnStrings;
        List<WebElement> listTrWebElement;
        List<WebElement> listTdWebElement;
        String content;
        int tdIndex = 0;
        //Select 50 rows per page
        click(incomplete.pageSelectionDropList);
        click(incomplete.fiftyRowsByPageSelection);
        String headerName = "";
        for (WebElement header : listHeaderButton){
            buildColumnStrings = new ArrayList<String>();
            headerName = header.getAttribute("textContent").replaceAll("\\s+", "");
            scrollOnElement(incomplete.searchField);
            //Sort the column as ascending
            click(header);
            click(header); // double click to descendant sort
            sleep(duration);
            listTrWebElement = incomplete.identifierTableResults.findElements(By.tagName("tr"));
            //Check the data is correctly sort (10 results max)
            for(int trIndex = 0; trIndex < listTrWebElement.size(); trIndex++){
                listTdWebElement = listTrWebElement.get(trIndex).findElements(By.tagName("td"));
//                System.out.println(listTdWebElement.get(tdIndex).getAttribute("textContent"));
                content = listTdWebElement.get(tdIndex).getAttribute("textContent");
                if(headerName.equals("Identifier") && content.contains(",")){
                    content = content.replaceAll("\\s+", "");
                    String identifiers[] = content.split(",");
                    if(identifiers.length == 2){
                        content = identifiers[0];
                    }
                }
                buildColumnStrings.add(content);
            }
            boolean isDate = false;
            if(headerName.contains("Date") ||headerName.contains("date")){
                isDate = true;
            }
            if(isDescendingListOfString(buildColumnStrings, isDate)){
                logger.log(LogStatus.INFO,"Descendant Column " + headerName + " is correctly sorted");
            } else {
                logger.log(LogStatus.ERROR,"ERROR: Descendant Column " + headerName + " is not correctly sorted");
            }
//            System.out.println("############################################");
            //Increase the index to check tbody/td[index]
            tdIndex++;
        }
    }

    private void reloadIncompleteFilterPageWithCatIdentifier(WebIncompletePage incompletePage, WebStartPage startPage){
        click(startPage.filterButton);
        click(startPage.filterIncompleteVetTabButton);
        WaitForElement(incompletePage.searchField,5);
    }

    private void checkIncompleteFilterAccessibility(WebIncompletePage incompletePage, WebStartPage startPage){
        click(startPage.filterButton);
        click(startPage.filterIncompleteVetActionButton);
        try{
            WaitForElement(incompletePage.searchField,5);
            logger.log(LogStatus.INFO,"Action button for filter Approved is reachable");
        } catch (Exception e){
            logger.log(LogStatus.ERROR,"ERROR: Action button for filter Approved is not reachable");
        }
        click(startPage.homeButton);
        click(startPage.filterButton);
        click(startPage.filterIncompleteVetTabButton);
        try{
            WaitForElement(incompletePage.searchField,5);
            logger.log(LogStatus.INFO,"Tab button for filter Approved is reachable");
        } catch (Exception e){
            logger.log(LogStatus.ERROR,"ERROR: Tab button for filter Approved is not reachable");
        }
    }

    private boolean detectValidClickEdditButton(WebIncompletePage incompletePage, WebRegisterPage modifyPage){
        boolean isValid;
        click(incompletePage.editButton);
        try{
            WaitForElement(modifyPage.transfertOfResponsibility,10);
            isValid = true;
            logger.log(LogStatus.INFO, "Valid button click");
        } catch (Exception e){
            logger.log(LogStatus.ERROR, "ERROR: Invalid button click");
            sleep(5000);
            isValid = false;
        }
        return isValid;
    }

    private void checkButtonMenu(WebIncompletePage incompletePage, WebStartPage startPage, WebRegisterPage registerPage){
        reloadIncompleteFilterPageWithCatIdentifier(incompletePage, startPage);

        //buttons menu are available
        //select all
        //user can click on the button
        //case no lines of the current page are selected
        //all lines becomes selected
        click(incompletePage.selectAllButton);
        int maxNumberRow = incompletePage.identifierTableResults.findElements(By.tagName("tr")).size();
        boolean areAllSelected = true;
        for(int i = 0; i < maxNumberRow; i++){
            if(!isSpecificColor(getRgbString(getRow(i,incompletePage.identifierTableResults)), "52", "69", "78")){ // gray color
                areAllSelected = false;
                break;
            }
        }
        if(areAllSelected){
            logger.log(LogStatus.INFO,"All lines become selected");
        } else {
            logger.log(LogStatus.ERROR,"ERROR: All lines don't become selected");
        }
        //case some lines of the current page are selected
        //all lines becomes selected
        if(maxNumberRow == 10) {
            //Unselect few lines
            click(getRow(1, incompletePage.identifierTableResults));
            click(getRow(5, incompletePage.identifierTableResults));
            click(getRow(8, incompletePage.identifierTableResults));
            click(incompletePage.selectAllButton);
            areAllSelected = true;
            for (int i = 0; i < 10; i++) {
                if (!isSpecificColor(getRgbString(getRow(i, incompletePage.identifierTableResults)), "52", "69", "78")) { // gray color
                    areAllSelected = false;
                    break;
                }
            }
            if (areAllSelected) {
                logger.log(LogStatus.INFO, "All lines become selected");
            } else {
                logger.log(LogStatus.ERROR, "ERROR: All lines don't become selected");
            }
            //case all lines of the current page are already selected
            //nothing change, all lines stay selected
            click(incompletePage.selectAllButton);
            areAllSelected = true;
            for (int i = 0; i < 10; i++) {
                if (!isSpecificColor(getRgbString(getRow(i, incompletePage.identifierTableResults)), "52", "69", "78")) { // gray color
                    areAllSelected = false;
                    break;
                }
            }
            if (areAllSelected) {
                logger.log(LogStatus.INFO, "All lines become selected");
            } else {
                logger.log(LogStatus.ERROR, "ERROR: All lines don't become selected");
            }

            //unselect all
            //by default, button is disable
            reloadIncompleteFilterPageWithCatIdentifier(incompletePage, startPage);
            if (incompletePage.unselectAllButton.getAttribute("class").equals("ui button buttons-select-none disabled")) {
                logger.log(LogStatus.INFO, "Menu Unselect All is disabled");
            } else {
                logger.log(LogStatus.ERROR, "ERROR: Menu Unselect All is enabled");
            }
            //case no line of the current page are selected
            //button stay disable
            boolean areNotAllSelected = true;
            for (int i = 0; i < 10; i++) {
                if (!isNoColor(getRgbString(getRow(i, incompletePage.identifierTableResults)))) {
                    areNotAllSelected = false;
                    break;
                }
            }
            if (areNotAllSelected && incompletePage.unselectAllButton.getAttribute("class").equals("ui button buttons-select-none disabled")) {
                logger.log(LogStatus.INFO, "Unselect button stays disable");
            } else {
                logger.log(LogStatus.ERROR, "ERROR: Unselect don't button stays disable");
            }
            //case one line of the current page is selected
            //button becomes enable
            click(getRow(5, incompletePage.identifierTableResults));
            if (areNotAllSelected && incompletePage.unselectAllButton.getAttribute("class").equals("ui button buttons-select-none")) {
                logger.log(LogStatus.INFO, "Unselect button becomes enable");
            } else {
                logger.log(LogStatus.ERROR, "ERROR: Unselect stays disable");
            }
            //user can click on button
            click(incompletePage.unselectAllButton);
            //selected line becomes unselected
            if (isNoColor(getRgbString(getRow(5, incompletePage.identifierTableResults))) && incompletePage.unselectAllButton.getAttribute("class").equals("ui button buttons-select-none disabled")) {
                logger.log(LogStatus.INFO, "Selected line becomes unselected");
            } else {
                logger.log(LogStatus.ERROR, "ERROR: Selected line doesn't become unselected");
            }
            //case some lines of the current page  are selected
            click(getRow(1, incompletePage.identifierTableResults));
            click(getRow(5, incompletePage.identifierTableResults));
            click(getRow(8, incompletePage.identifierTableResults));
            //button becomes enable
            if (incompletePage.unselectAllButton.getAttribute("class").equals("ui button buttons-select-none")) {
                logger.log(LogStatus.INFO, "Menu Unselect All is enable");
            } else {
                logger.log(LogStatus.ERROR, "ERROR: Menu Unselect All is disable");
            }
            //user can click on button
            click(incompletePage.unselectAllButton);
            //selected line becomes unselected
            if (isNoColor(getRgbString(getRow(1, incompletePage.identifierTableResults))) &&
                    isNoColor(getRgbString(getRow(5, incompletePage.identifierTableResults))) &&
                    isNoColor(getRgbString(getRow(8, incompletePage.identifierTableResults))) &&
                    incompletePage.unselectAllButton.getAttribute("class").equals("ui button buttons-select-none disabled")) {
                logger.log(LogStatus.INFO, "Selected lines become unselected");
            } else {
                logger.log(LogStatus.ERROR, "ERROR: Selected lines don't become unselected");
            }
            //case all lines of the current page are selected
            click(incompletePage.selectAllButton);
            //button becomes enable
            if (incompletePage.unselectAllButton.getAttribute("class").equals("ui button buttons-select-none")) {
                logger.log(LogStatus.INFO, "Menu Unselect All is enable");
            } else {
                logger.log(LogStatus.ERROR, "ERROR: Menu Unselect All is disable");
            }
            //user can click on button
            click(incompletePage.unselectAllButton);
            //selected line becomes unselected
            areNotAllSelected = true;
            for (int i = 0; i < 10; i++) {
                if (!isNoColor(getRgbString(getRow(i, incompletePage.identifierTableResults)))) {
                    areNotAllSelected = false;
                    break;
                }
            }
            if (areNotAllSelected && incompletePage.unselectAllButton.getAttribute("class").equals("ui button buttons-select-none disabled")) {
                logger.log(LogStatus.INFO, "Selected line becomes unselected");
            } else {
                logger.log(LogStatus.ERROR, "ERROR: Selected line stay selected");
            }

            //Edit
            //By default, button is enabled
            //user can click on the button
            //case no line is selected
            detectInvalidClickButton(incompletePage.editButton, incompletePage, "Please select from the list");//This test valid that by default the button is enable
            //case one line is selected
            reloadIncompleteFilterPageWithCatIdentifier(incompletePage, startPage); //reload just in case of error...
            click(getRow(1, incompletePage.identifierTableResults));
            detectValidClickEdditButton(incompletePage, registerPage);
            //case several lines are selected
            reloadIncompleteFilterPageWithCatIdentifier(incompletePage, startPage); //reload just in case of error...
            click(getRow(1, incompletePage.identifierTableResults));
            click(getRow(4, incompletePage.identifierTableResults));
            click(getRow(7, incompletePage.identifierTableResults));
            detectInvalidClickButton(incompletePage.editButton, incompletePage, "Select only one");
        }
//        //Delete
//        //Register 13 cats before deletion test
//        for (int i = 0; i < 13; i++) {
//            try {
//                if(i < 10){
//                    registerSpecificVetCat(registerPage,startPage,"HK43214321", "99999999999900" + i);
//                    logger.log(LogStatus.INFO, "Cat number: " + "99999999999900" + i + " is registered");
//                } else {
//                    registerSpecificVetCat(registerPage,startPage,"HK43214321", "9999999999990" + i);
//                    logger.log(LogStatus.INFO, "Cat number: " + "9999999999990" + i + " is registered");
//                }
//
//            } catch (Exception e) {
//                if(i < 10) {
//                    logger.log(LogStatus.ERROR, "Cat number: " + "99999999999900" + i + " is not registered");
//                } else {
//                    logger.log(LogStatus.ERROR, "Cat number: " + "9999999999990" + i + " is not registered");
//                }
//            }
//        }
//        reloadIncompleteFilterPageWithCatIdentifier(incompletePage,startPage);
//        //by default, button is enable
//        //case no line of the current page are selected
//        //user can click on the button
//        //an error message "please select from the list" is displayed
//        detectInvalidClickButton(incompletePage.deleteButton, incompletePage,"Please select from the list");//This test valid that by default the button is enable
//
//        //case one line of the current page is selected
//        //user can click on the button
//        reloadIncompleteFilterPageWithCatIdentifier(incompletePage,startPage);
//        click(getRow(0,incompletePage.identifierTableResults));
//        String beforeDeletionNumberOfForms = incompletePage.pageInfo.getAttribute("textContent").split(" ")[5];
//        logger.log(LogStatus.INFO, "There are currently " + beforeDeletionNumberOfForms + " forms before deletion test");
//        click(incompletePage.deleteButton);
//        //user is prompted to confirm deletion
//        //case Cancel
//        click(incompletePage.refuseDeletionButton);
//        String afterCancelDeletion = incompletePage.pageInfo.getAttribute("textContent").split(" ")[5];
//
//        //nothing append, no deletion occurs
//        if(beforeDeletionNumberOfForms.equals(afterCancelDeletion)){
//            logger.log(LogStatus.INFO, "Nothing append, no deletion occurred");
//        } else {
//            logger.log(LogStatus.ERROR, "ERROR: Something append, deletion occurred");
//        }
//        //case OK
//        sleep(5000);//Because the dialog message can hide the filter button (visible so "wait for" return true)
//        reloadIncompleteFilterPageWithCatIdentifier(incompletePage,startPage);
//        click(getRow(0,incompletePage.identifierTableResults));
//        String identifierDeletionFormBeforeDeletion = getCellString(0,0,incompletePage.identifierTableResults);
//        beforeDeletionNumberOfForms = incompletePage.pageInfo.getAttribute("textContent").split(" ")[5];
//        click(incompletePage.deleteButton);
//        click(incompletePage.acceptDeletionButton);
//        sleep(1000);
//        String identifierDeletionFormAfterDeletion = getCellString(0,0,incompletePage.identifierTableResults);
//        String afterDeletion = incompletePage.pageInfo.getAttribute("textContent").split(" ")[5];
//        //selected line is deleted
//        if(!identifierDeletionFormBeforeDeletion.equals(identifierDeletionFormAfterDeletion)){
//            logger.log(LogStatus.INFO, "Selected line is deleted");
//        } else {
//            logger.log(LogStatus.ERROR, "ERROR: Selected line is not deleted");
//        }
//        //page is refreshed
//        //entries counter is updated -1
//        int refreshCounter = 0;
//        try{
//            int beforeInt = Integer.parseInt(beforeDeletionNumberOfForms);
//            int afterInt = Integer.parseInt(afterDeletion);
//            refreshCounter = afterInt - beforeInt;
//        } catch (Exception e){
//            logger.log(LogStatus.ERROR, "ERROR: Integer parsing failed");
//        }
//        if(refreshCounter == -1){
//            logger.log(LogStatus.INFO, "Page is refreshed, entries counter is updated -1");
//        } else {
//            logger.log(LogStatus.ERROR, "ERROR: Page is refreshed, entries counter is not updated -1");
//        }
//        //case some lines of the current page  are selected
//        click(getRow(0,incompletePage.identifierTableResults));
//        click(getRow(1,incompletePage.identifierTableResults));
//        //user can click on the button
//        beforeDeletionNumberOfForms = incompletePage.pageInfo.getAttribute("textContent").split(" ")[5];
//        logger.log(LogStatus.INFO, "There are currently " + beforeDeletionNumberOfForms + " forms before deletion test");
//        click(incompletePage.deleteButton);
//        //user is prompted to confirm deletion
//        //case Cancel
//        click(incompletePage.refuseDeletionButton);
//        afterCancelDeletion = incompletePage.pageInfo.getAttribute("textContent").split(" ")[5];
//        //nothing append, no deletion occurs
//        if(beforeDeletionNumberOfForms.equals(afterCancelDeletion)){
//            logger.log(LogStatus.INFO, "Nothing append, no deletions occurred");
//        } else {
//            logger.log(LogStatus.ERROR, "ERROR: Something append, deletions occurred");
//        }
//        //case OK
//        sleep(5000);//Because the dialog message can hide the filter button (visible so "wait for" return true)
//        reloadIncompleteFilterPageWithCatIdentifier(incompletePage,startPage);
//        click(getRow(0,incompletePage.identifierTableResults));
//        click(getRow(1,incompletePage.identifierTableResults));
//        identifierDeletionFormBeforeDeletion = getCellString(0,0,incompletePage.identifierTableResults);
//        beforeDeletionNumberOfForms = incompletePage.pageInfo.getAttribute("textContent").split(" ")[5];
//        click(incompletePage.deleteButton);
//        click(incompletePage.acceptDeletionButton);
//        sleep(1000);
//        identifierDeletionFormAfterDeletion = getCellString(0,0,incompletePage.identifierTableResults);
//        afterDeletion = incompletePage.pageInfo.getAttribute("textContent").split(" ")[5];
//        //selected lines are deleted
//        if(!identifierDeletionFormBeforeDeletion.equals(identifierDeletionFormAfterDeletion)){
//            logger.log(LogStatus.INFO, "Selected line is deleted");
//        } else {
//            logger.log(LogStatus.ERROR, "ERROR: Selected line is not deleted");
//        }
//        //page is refreshed
//        //entries counter is updated minus number of lines selected
//        refreshCounter = 0;
//        try{
//            int beforeInt = Integer.parseInt(beforeDeletionNumberOfForms);
//            int afterInt = Integer.parseInt(afterDeletion);
//            refreshCounter = afterInt - beforeInt;
//        } catch (Exception e){
//            logger.log(LogStatus.ERROR, "ERROR: Integer parsing failed");
//        }
//        if(refreshCounter == -2){
//            logger.log(LogStatus.INFO, "Page is refreshed, entries counter is updated minus number of lines selected");
//        } else {
//            logger.log(LogStatus.ERROR, "ERROR: Page is refreshed, entries counter is not updated minus number of lines selected");
//        }
//        //case all lines of the current page  are selected
//        click(incompletePage.selectAllButton);
//        //user can click on the button
//        beforeDeletionNumberOfForms = incompletePage.pageInfo.getAttribute("textContent").split(" ")[5];
//        logger.log(LogStatus.INFO, "There are currently " + beforeDeletionNumberOfForms + " forms before deletion test");
//        click(incompletePage.deleteButton);
//        //user is prompted to confirm deletion
//        //case Cancel
//        click(incompletePage.refuseDeletionButton);
//        afterCancelDeletion = incompletePage.pageInfo.getAttribute("textContent").split(" ")[5];
//        //nothing append, no deletion occurs
//        if(beforeDeletionNumberOfForms.equals(afterCancelDeletion)){
//            logger.log(LogStatus.INFO, "Nothing append, no deletions occurred");
//        } else {
//            logger.log(LogStatus.ERROR, "ERROR: Something append, deletions occurred");
//        }
//        //case OK
//        sleep(5000);//Because the dialog message can hide the filter button (visible so "wait for" return true)
//        reloadIncompleteFilterPageWithCatIdentifier(incompletePage,startPage);
//        click(incompletePage.selectAllButton);
//        identifierDeletionFormBeforeDeletion = getCellString(0,0,incompletePage.identifierTableResults);
//        beforeDeletionNumberOfForms = incompletePage.pageInfo.getAttribute("textContent").split(" ")[5];
//        click(incompletePage.deleteButton);
//        click(incompletePage.acceptDeletionButton);
//        sleep(1000);
//        identifierDeletionFormAfterDeletion = getCellString(0,0,incompletePage.identifierTableResults);
//        afterDeletion = incompletePage.pageInfo.getAttribute("textContent").split(" ")[5];
//        //all  lines of the current page are deleted
//        if(!identifierDeletionFormBeforeDeletion.equals(identifierDeletionFormAfterDeletion)){
//            logger.log(LogStatus.INFO, "Selected line is deleted");
//        } else {
//            logger.log(LogStatus.ERROR, "ERROR: Selected line is not deleted");
//        }
//        //page is refreshed
//
//        //all next lines will come on the first page
//        //entries counter is updated minus 10
//        refreshCounter = 0;
//        try{
//            int beforeInt = Integer.parseInt(beforeDeletionNumberOfForms);
//            int afterInt = Integer.parseInt(afterDeletion);
//            refreshCounter = afterInt - beforeInt;
//        } catch (Exception e){
//            logger.log(LogStatus.ERROR, "ERROR: Integer parsing failed");
//        }
//        if(refreshCounter == -10){
//            logger.log(LogStatus.INFO, "Page is refreshed, entries counter is updated -10");
//        } else {
//            logger.log(LogStatus.ERROR, "ERROR: Page is refreshed, entries counter is not updated -10");
//        }
//        //entries counter is updated to 0
//        //button unselect all becomes disable
//        //no more lines are available
//        String count = afterDeletion = incompletePage.pageInfo.getAttribute("textContent").split(" ")[5];
//        if(count.equals("0")){
//            logger.log(LogStatus.INFO, "No more lines are available");
//            if(incompletePage.unselectAllButton.getAttribute("class").equals("ui button buttons-select-none")){
//                logger.log(LogStatus.INFO,"Menu Unselect All is enabled");
//            } else {
//                logger.log(LogStatus.ERROR,"ERROR: Menu Unselect All is disabled");
//            }
//        } else {
//            logger.log(LogStatus.INFO, "There are still lines available");
//        }


    }

    private void checkSelectAndUnSelectLine(WebIncompletePage incomplete, WebStartPage startPage){
        reloadIncompleteFilterPageWithCatIdentifier(incomplete, startPage);
        //User can select lines
        //by clicking on a line
        //line becomes gray
        WebElement firstRow = getRow(0,incomplete.identifierTableResults);
        click(firstRow);
        if(isSpecificColor(getRgbString(firstRow), "52", "69", "78")){
            logger.log(LogStatus.INFO,"First line is gray when selected (by clicking)");
        } else {
            logger.log(LogStatus.ERROR,"ERROR: First line is not gray when selected (by clicking)");
        }
        //Label row selected is incremented by 1
        if(incomplete.pageInfo.getAttribute("textContent").contains(("1 row selected"))){
            logger.log(LogStatus.INFO,"Label row selected is incremented by 1");
        } else {
            logger.log(LogStatus.ERROR,"ERROR: Label row selected is not incremented by 1");
        }
        //Menu Unselect All is enabled
        if(incomplete.unselectAllButton.getAttribute("class").equals("ui button buttons-select-none")){
            logger.log(LogStatus.INFO,"Menu Unselect All is enabled");
        } else {
            logger.log(LogStatus.ERROR,"ERROR: Menu Unselect All is disabled");
        }

        WebElement thirdRow = getRow(2,incomplete.identifierTableResults);
        click(thirdRow);
        if(isSpecificColor(getRgbString(thirdRow), "52", "69", "78")){
            logger.log(LogStatus.INFO,"Third line is gray when selected (by clicking)");
        } else {
            logger.log(LogStatus.ERROR,"ERROR: Third line is not gray when selected (by clicking)");
        }
        //Label row selected is incremented by 1
        if(incomplete.pageInfo.getAttribute("textContent").contains(("2 rows selected"))){
            logger.log(LogStatus.INFO,"Label row selected is incremented by 1");
        } else {
            logger.log(LogStatus.ERROR,"ERROR: Label row selected is not incremented by 1");
        }

        //User can deselect lines
        //line must be selected and color gray
        //user click on the line
        //line become deselected
        //color line becomes white
        click(incomplete.firstIdentifierTableResult);
        if(isNoColor(getRgbString(incomplete.firstIdentifierTableResult))){
            logger.log(LogStatus.INFO,"First line is white when selected (by clicking)");
        } else {
            logger.log(LogStatus.ERROR,"ERROR: First line is not white when selected (by clicking)");
        }
        //Label row selected is decremented by 1
        if(incomplete.pageInfo.getAttribute("textContent").contains(("1 row selected"))){
            logger.log(LogStatus.INFO,"Label row selected is decremented by 1");
        } else {
            logger.log(LogStatus.ERROR,"ERROR: Label row selected is not decremented by 1");
        }

        click(thirdRow);
        if(isNoColor(getRgbString(thirdRow))){
            logger.log(LogStatus.INFO,"Third line is white when selected (by clicking)");
        } else {
            logger.log(LogStatus.ERROR,"ERROR: Third line is not white when selected (by clicking)");
        }

        //Label row selected is decremented by 1
        if(!incomplete.pageInfo.getAttribute("textContent").contains(("selected"))){
            logger.log(LogStatus.INFO,"Label row selected is decremented by 1");
        } else {
            logger.log(LogStatus.ERROR,"ERROR: Label row selected is not decremented by 1");
        }
        //case no more lines are selected
        //menu unselect all is disabled
        if(incomplete.unselectAllButton.getAttribute("class").equals("ui button buttons-select-none disabled")){
            logger.log(LogStatus.INFO,"Menu Unselect All is disabled");
        } else {
            logger.log(LogStatus.ERROR,"ERROR: Menu Unselect All is enabled");
        }
    }

    private void checkSearchField(WebIncompletePage incomplete, WebStartPage startPage){
        int DURATION = 2000;
        reloadIncompleteFilterPageWithCatIdentifier(incomplete, startPage);
        HashMap<String,String> expectedResult = new HashMap<String, String>();
        HashMap<String,String> searchWords = new HashMap<String, String>();
        expectedResult.put("Identifier", "123456789200055");
        expectedResult.put("PassportNumber", "TH124567893");
        expectedResult.put("DateOfBirth", "30/01/2018");
        expectedResult.put("IdentificationDate", "16/01/2018");
        expectedResult.put("Gender", "Male");
        expectedResult.put("CatBreed", "Ocicat");
        expectedResult.put("Sterelized", "No");
        expectedResult.put("SterelizationDate", "30/01/2018");
        expectedResult.put("Info", "HK159357");
        expectedResult.put("Status", "Incomplete");
        searchWords.put("Identifier", "789200055");
        searchWords.put("PassportNumber", "TH124567893");
        searchWords.put("DateOfBirth", "30/01/2018");
        searchWords.put("IdentificationDate", "16/01/2018");
        searchWords.put("Gender", "Male");
        searchWords.put("CatBreed", "Ocicat");
        searchWords.put("Sterelized", "No");
        searchWords.put("SterelizationDate", "30/01/2018");
        searchWords.put("Info", "HK159357");
        searchWords.put("Status", "Incomplete");

        //User can perform searches
        //by enconding string in field Search
        //results are displayed after each character encoded
        //search will fetch on column
        //identifier  -> "500319" to search for "123456789500319"
        setText(incomplete.searchField,searchWords.get("Identifier"));
        sleep(DURATION); // Wait 1s to let the result to be refreshed
        getScreenShot();
        String identifier = lookForExpectedResult(expectedResult.get("Identifier"), incomplete.identifierTableResults, 0);
        if(identifier.contains(expectedResult.get("Identifier"))){
            logger.log(LogStatus.INFO,"Identifier search for " + searchWords.get("Identifier") + " works and the result is " + identifier);
        } else {
            logger.log(LogStatus.ERROR,"ERROR: Identifier search for " + searchWords.get("Identifier") + " doesn't work and the result is " + identifier);
        }
        //passport number
        setText(incomplete.searchField,searchWords.get("PassportNumber"));
        sleep(DURATION); // Wait 1s to let the result to be refreshed
        getScreenShot();
        String passportResult = lookForExpectedResult(expectedResult.get("PassportNumber"), incomplete.identifierTableResults, 1);
        if(passportResult.contains(expectedResult.get("PassportNumber"))){
            logger.log(LogStatus.INFO,"Passport search for " + searchWords.get("PassportNumber") + " works and the result is " + passportResult);
        } else {
            logger.log(LogStatus.ERROR,"ERROR: Passport search for " + searchWords.get("PassportNumber") + " doesn't work and the result is " + passportResult);
        }
        //date of birth
        setText(incomplete.searchField,searchWords.get("DateOfBirth"));
        sleep(DURATION); // Wait 1s to let the result to be refreshed
        getScreenShot();
        String birthDateResult = lookForExpectedResult(expectedResult.get("DateOfBirth"), incomplete.identifierTableResults, 2);
        if(birthDateResult.contains(searchWords.get("DateOfBirth"))){
            logger.log(LogStatus.INFO,"Date of birth search for " + searchWords.get("DateOfBirth") + " works and the result is " + birthDateResult);
        } else {
            logger.log(LogStatus.ERROR,"ERROR: Date of birth search for "+ searchWords.get("DateOfBirth") + " doesn't work and the result is " + birthDateResult);
        }
        //identification date
        setText(incomplete.searchField,searchWords.get("IdentificationDate"));
        sleep(DURATION); // Wait 1s to let the result to be refreshed
        getScreenShot();
        String identificationDateResult = lookForExpectedResult(expectedResult.get("IdentificationDate"), incomplete.identifierTableResults, 3);
        if(identificationDateResult.contains(expectedResult.get("IdentificationDate"))){
            logger.log(LogStatus.INFO,"Date of identification search for " + searchWords.get("IdentificationDate") + " works and the result is " + identificationDateResult);
        } else {
            logger.log(LogStatus.ERROR,"ERROR: Date of identification search for " + searchWords.get("IdentificationDate") + " doesn't work and the result is " + identificationDateResult);
        }
        //Gender
        setText(incomplete.searchField,searchWords.get("Gender"));
        sleep(1000); // Wait 1s to let the result to be refreshed
        getScreenShot();
        String genderResult =lookForExpectedResult(expectedResult.get("Gender"), incomplete.identifierTableResults, 4);
        if(genderResult.contains(expectedResult.get("Gender"))){
            logger.log(LogStatus.INFO,"Gender search for " + searchWords.get("Gender") + " works and the result is " + genderResult);
        } else {
            logger.log(LogStatus.ERROR,"ERROR: Gender search for " + searchWords.get("Gender") + " doesn't work and the result is " + genderResult);
        }
        //Cat Breed
        setText(incomplete.searchField,searchWords.get("CatBreed"));
        sleep(DURATION); // Wait 1s to let the result to be refreshed
        getScreenShot();
        String breedResult = lookForExpectedResult(expectedResult.get("CatBreed"), incomplete.identifierTableResults, 5);
        if(breedResult.contains(expectedResult.get("CatBreed"))){
            logger.log(LogStatus.INFO,"Breed search for " + searchWords.get("CatBreed") + " works and the result is " + breedResult);
        } else {
            logger.log(LogStatus.ERROR,"ERROR: Breed search for " + searchWords.get("CatBreed") + " doesn't work and the result is " + breedResult);
        }
        //Sterilized
        setText(incomplete.searchField,searchWords.get("Sterelized"));
        sleep(DURATION); // Wait 1s to let the result to be refreshed
        getScreenShot();
        String sterilizedResult = lookForExpectedResult(expectedResult.get("Sterelized"), incomplete.identifierTableResults, 6);
        if(sterilizedResult.contains(expectedResult.get("Sterelized"))){
            logger.log(LogStatus.INFO,"Sterilzed search for " + searchWords.get("Sterelized") + " works and the result is " + sterilizedResult);
        } else {
            logger.log(LogStatus.ERROR,"ERROR: Sterilzed search for " + searchWords.get("Sterelized") + " doesn't work and the result is " + sterilizedResult);
        }
        //sterilized date
        setText(incomplete.searchField,searchWords.get("SterelizationDate"));
        sleep(DURATION); // Wait 1s to let the result to be refreshed
        getScreenShot();
        String sterilizedDateResult = lookForExpectedResult(expectedResult.get("SterelizationDate"), incomplete.identifierTableResults, 7);
        if(sterilizedDateResult.contains(expectedResult.get("SterelizationDate"))){
            logger.log(LogStatus.INFO,"Sterilzed date search for " + searchWords.get("SterelizationDate") + " works and the result is " + sterilizedDateResult);
        } else {
            logger.log(LogStatus.ERROR,"ERROR: Sterilzed date search for " + searchWords.get("SterelizationDate") + " doesn't work and the result is " + sterilizedDateResult);
        }
        //owner information
        setText(incomplete.searchField,searchWords.get("Info"));
        sleep(DURATION); // Wait 1s to let the result to be refreshed
        getScreenShot();
        String ownerInfoResult = lookForExpectedResult(expectedResult.get("Info"), incomplete.identifierTableResults, 8);
        if(ownerInfoResult.contains(searchWords.get("Info"))){
            logger.log(LogStatus.INFO,"Owner Information search for " + searchWords.get("Info") + " works and the result is " + ownerInfoResult);
        } else {
            logger.log(LogStatus.ERROR,"ERROR: Owner Information search for " + searchWords.get("Info") + " doesn't work and the result is " + ownerInfoResult);
        }
        //status
        setText(incomplete.searchField,searchWords.get("Status"));
        sleep(DURATION); // Wait 1s to let the result to be refreshed
        getScreenShot();
        String statusResult = lookForExpectedResult(expectedResult.get("Status"), incomplete.identifierTableResults, 9);
        if(statusResult.contains(searchWords.get("Status"))){
            logger.log(LogStatus.INFO,"Status search for " + searchWords.get("Status") + " works and the result is " + statusResult);
        } else {
            logger.log(LogStatus.ERROR,"ERROR: Status search for " + searchWords.get("Status") + " doesn't work and the result is " + statusResult);
        }
    }

    private void incompleteFilterCheck(WebIncompletePage incompletePage, WebStartPage startPage, SqlOracleMiddleware metaformMiddleWare, WebRegisterPage registerPage) throws Exception {
        click(startPage.homeButton);
        //by clicking on the menu filters - Approved
        //The all cats page is displayed
        //all cats where vet are linked and approved by system are displayed
        //total number of cats is the sum of
        String numberCatApprovedDisplayed = "";
        String numberCatApprovedSql = "";
        WaitForElement(startPage.filterIncompleteVetActionButton,10);
        numberCatApprovedDisplayed = startPage.filterIncompleteVetActionButton.getAttribute("textContent").replaceAll("\\s+", "");
        ArrayList<HashMap> rs = metaformMiddleWare.executeSqlQuery("SELECT COUNT(*) from METAFORM_ABIEC_ANIMAL where MF_STATUS = 512 AND VET_REFNUM = 'F0024'");
        if (rs.size() == 0) {
            logger.log(LogStatus.ERROR, "Something is wrong with SQL statement or DB connection");
        } else {
            numberCatApprovedSql = (String)rs.get(0).get("COUNT(*)");
            if(numberCatApprovedSql != null && numberCatApprovedDisplayed != null && numberCatApprovedSql.equals(numberCatApprovedDisplayed)){
                logger.log(LogStatus.INFO,"All cats where vet is linked and incomplete are displayed");
            } else {
                logger.log(LogStatus.ERROR,"ERROR: All cats where vet is not linked or incomplete are not displayed. SQL:" + numberCatApprovedSql + " displayed: " + numberCatApprovedDisplayed);
            }
        }
        //Page is showing 10 cats per page by default
        click(startPage.filterIncompleteVetActionButton);
        List<WebElement> listResults = incompletePage.identifierTableResults.findElements(By.tagName("tr"));
        //Wait a little bit the page is completely loaded to take a screenshot
        WaitForElement(incompletePage.firstIdentifierTableResult,5);
        if(listResults.size() == 10){
            logger.log(LogStatus.INFO,"Page is showing 10 cats per page by default");
            if(isElementPresent(incompletePage.nextButton)){
                //case of more than 10 cats
                //next button is displayed
                logger.log(LogStatus.INFO,"Next button is displayed");
                //case there are more than one page available
                //case there are more than 10 cats
                //pages number are displayed
                List<WebElement> pageNumberList = incompletePage.paginationMenu.findElements(By.tagName("a"));
                if(pageNumberList.size() > 5){
                    //is it possible to click on the specific page
                    //selected page is displayed
                    //other available pages are still available for click
                    //page 1 display results from 1 to 10
                    //page 2 display result from 11 to 20
                    //page 3 display result from 21 to 30

                    logger.log(LogStatus.INFO,"Pages number are displayed");
                    if(incompletePage.pageInfo.getAttribute("textContent").contains("1 to 10")){
                        logger.log(LogStatus.INFO,"Page 1 display results from 1 to 10");
                    } else {
                        logger.log(LogStatus.ERROR,"ERROR: Page 1 doesn't display results from 1 to 10: " + incompletePage.pageInfo.getAttribute("textContent"));
                    }
                    click(pageNumberList.get(1)); //Click page 2
                    WaitForElement(incompletePage.firstIdentifierTableResult,5);
                    logger.log(LogStatus.INFO,"Click page 2");
                    if(incompletePage.pageInfo.getAttribute("textContent").contains("11 to 20")){
                        logger.log(LogStatus.INFO,"Page 2 display results from 11 to 20");
                    } else {
                        logger.log(LogStatus.ERROR,"ERROR: Page 2 doesn't display results from 11 to 20: " + incompletePage.pageInfo.getAttribute("textContent"));
                    }
                    pageNumberList = incompletePage.paginationMenu.findElements(By.tagName("a"));
                    //Indexes change between multiple clicks
                    click(pageNumberList.get(3)); //Click page 3
                    WaitForElement(incompletePage.firstIdentifierTableResult,5);
                    logger.log(LogStatus.INFO,"Click page 3");
                    if(incompletePage.pageInfo.getAttribute("textContent").contains("21 to")){
                        logger.log(LogStatus.INFO,"Page 3 display results from 21");
                    } else {
                        logger.log(LogStatus.ERROR,"ERROR: Page 3 doesn't display results from 21 to 30: " + incompletePage.pageInfo.getAttribute("textContent"));
                    }
                    pageNumberList = incompletePage.paginationMenu.findElements(By.tagName("a"));
                    click(pageNumberList.get(1)); //Click page 1
                    WaitForElement(incompletePage.firstIdentifierTableResult,5);
                    logger.log(LogStatus.INFO,"Click page 1");
                } else {
                    logger.log(LogStatus.INFO,"There are not enough forms in the filter to test multiple pages");
                }

            } else {
                logger.log(LogStatus.INFO,"Next button is not displayed");
            }
            if(incompletePage.numberResultByPageSelectionDroplist.getAttribute("textContent").contains("10")
                    && incompletePage.numberResultByPageSelectionDroplist.getAttribute("textContent").contains("25")
                    && incompletePage.numberResultByPageSelectionDroplist.getAttribute("textContent").contains("50")
                    && incompletePage.numberResultByPageSelectionDroplist.getAttribute("textContent").contains("100")){

                logger.log(LogStatus.INFO,"Number of result by page can be modify between {10, 25, 50, 100} rows");
            } else {
                logger.log(LogStatus.ERROR,"ERROR: Number of result by page cannot be modify between {10, 25, 50, 100} rows");
            }
            if(isElementPresent(incompletePage.previousButton)) {
                //case of more than 10 cats
                //previous button is displayed
                logger.log(LogStatus.INFO, "Previous button is displayed");
            } else {
                logger.log(LogStatus.ERROR, "ERROR: Previous button is not displayed");
            }


        } else {
            logger.log(LogStatus.INFO,"There are not enough forms in the filter to test more than 10 pages");
        }
        ascendingSortCheck(incompletePage, startPage);
        descendingSortCheck(incompletePage, startPage);
        checkSearchField(incompletePage,startPage);
        checkSelectAndUnSelectLine(incompletePage,startPage);
        checkButtonMenu(incompletePage,startPage, registerPage);
    }
}

