package ElementaryScripts.Web;

import ElementaryScripts.General.AbstractCatIdElementaryScript;
import Screens.*;
import com.relevantcodes.extentreports.LogStatus;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public abstract class FilterPage extends AbstractCatIdElementaryScript {
    //TODO prepare 3 user with FIXED filters ...

    protected boolean isAscendingListOfString(List<String> list, boolean isDate) throws Exception {
        boolean isSorted = true;
        if(list.size() > 0) {
            if (isDate) {
                SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");

                Date previousDate = format.parse("01/01/1500");
                for (String current : list) {
                    if(!current.equals("")) {
                        String date = current.replaceAll("-","/");
                        Date currentDate = format.parse(date);
                        if (currentDate.before(previousDate)) {
                            isSorted = false;
                            logger.log(LogStatus.ERROR, "previous: " + previousDate + " current: " + currentDate);
                            break;
                        }
                        previousDate = currentDate;
                    }
                }
            } else {
                String previous = " ";// empty string: guaranteed to be less than or equal to any other
                for (String current : list) {
                    current = current.toLowerCase();
                    if (!current.equals("") && current.compareTo(previous) < 0 || !previous.equals(" ") && current.equals(" ")) {
                        isSorted = false;
                        logger.log(LogStatus.ERROR, "previous: " + previous + " current: " + current);
                        break;
                    }
                    previous = current;
                }
            }
        } else {
            isSorted = false;
        }

        return isSorted;
//        return Ordering.natural().isOrdered(list);

    }

    protected boolean isDescendingListOfString(List<String> list, boolean isDate) throws Exception {
        boolean isSorted = true;
        if(list.size() > 0) {
            if (isDate) {
                SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");

                Date previousDate = new Date();
                for (String current : list) {
                    if(!current.equals("")) {
                        String date = current.replaceAll("-","/");
                        Date currentDate = format.parse(date);
                        if (currentDate.after(previousDate)) {
                            isSorted = false;
                            logger.log(LogStatus.ERROR, "previous: " + previousDate + " current: " + currentDate);
                            break;
                        }
                        previousDate = currentDate;
                    }
                }
            } else {
                String previous = list.get(0).toLowerCase();// empty string: guaranteed to be less than or equal to any other
                for (String current : list) {
                    current = current.toLowerCase();
                    if (!previous.equals("") && current.compareTo(previous) > 0 || !current.equals(" ") && previous.equals(" ")) {
                        isSorted = false;
                        logger.log(LogStatus.ERROR, "previous: " + previous + " current: " + current);
                        break;
                    }
                    previous = current;
                }
            }
        }
        else {
            isSorted = false;
        }
        return isSorted;
//        return Ordering.natural().isOrdered(list);

    }

    protected String lookForExpectedResult(String expected, WebElement tableResult, int columnIndex){
        String found = "";
        String tempResult = "";
        for(int i = 0; i < 10; i++){
            tempResult = getCellString(i, columnIndex, tableResult);
            if(tempResult.contains(expected)){
                found = tempResult;
                break;
            }
        }
        return found;
    }

    protected boolean detectInvalidClickButton(WebElement we, WebFilterPage filterPage, String errorMessage){
        boolean isValid;
        click(we);
        WaitForElement(filterPage.messageDialog,5);
        logger.log(LogStatus.INFO,"Found: " + filterPage.messageDialog.getAttribute("textContent"));
        logger.log(LogStatus.INFO,"Expected: " + errorMessage);
        if(filterPage.messageDialog.getAttribute("textContent").contains(errorMessage)){ //because there are something a lot of space before the sentence
            logger.logScreenshot(LogStatus.INFO, "Error case correctly detected for " + registrator, getScreenShot());
            sleep(5000);//Wait for the end of the dialog message to not disturb any click
            isValid = true;
        } else {
            logger.logScreenshot(LogStatus.ERROR, "ERROR: Error case message is wrong... for " + registrator, getScreenShot());
            sleep(5000);
            isValid = false;
        }
        return isValid;
    }
}
