package ElementaryScripts.Web;

import Csv.CsvHandler;
import DataBaseFramework.SqlOracleMiddleware;
import Screens.*;
import com.relevantcodes.extentreports.LogStatus;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class FilterPendingPage extends FilterPage {
    private int duration = 5000;
    public void run() throws Exception {
        try {
            WebStartPage startPage = new WebStartPage(driver);
            WebPendingPage pendingPage = new WebPendingPage(driver);
            WebRegisterPage registerPage = new WebRegisterPage(driver);

            SqlOracleMiddleware metaformMiddleWare = new SqlOracleMiddleware(logger, "MF_CATS", "MF_CATS", "jdbc:oracle:thin:@192.168.17.6:1521:PREPROD");

            //Accessibility check
            checkPendingFilterAccessibility( pendingPage, startPage);

            //Pending filter check
            pendingFilterCheck(pendingPage, startPage, metaformMiddleWare, registerPage);
            //logout for next page
            click(startPage.logoutButton);
        }catch (Exception e) {
            logger.logScreenshot(LogStatus.ERROR, "ERROR stopping the test", getScreenShot());
            throw e;
        }
    }

    private void ascendingSortCheck(WebPendingPage pendingPage, WebStartPage startPage) throws Exception {
        reloadPendingFilterPageWithCatIdentifier(pendingPage, startPage);
        List<WebElement> listHeaderButton = pendingPage.headerRow.findElements(By.tagName("th"));
        //Check all columns
        List<WebElement> pageNumberList;
        List<String> buildColumnStrings;
        List<WebElement> listTrWebElement;
        List<WebElement> listTdWebElement;
        String content;
        int tdIndex = 0;
        //Select 50 rows per page
        click(pendingPage.pageSelectionDropList);
        click(pendingPage.fiftyRowsByPageSelection);
        String headerName = "";
        for (WebElement header : listHeaderButton){
            buildColumnStrings = new ArrayList<String>();
            headerName = header.getAttribute("textContent").replaceAll("\\s+", "");
            scrollOnElement(pendingPage.searchField);
            //Sort the column as ascending
            click(header); //Click for ascendant sort
            pageNumberList = pendingPage.paginationMenu.findElements(By.tagName("a"));
            //Click on the last page
            if(pageNumberList.size() > 5) {
                click(pageNumberList.get(pageNumberList.size() - 2)); //Next button is the last button at index "size - 1"
                click(pendingPage.previousButton); // to make sure there are probably 50 rows
            }
            sleep(duration);//need time to load 50 rows
            listTrWebElement = pendingPage.identifierTableResults.findElements(By.tagName("tr"));
            //Check the data is correctly sort (10 results max)
            for(int trIndex = 0; trIndex < listTrWebElement.size(); trIndex++){
                listTdWebElement = listTrWebElement.get(trIndex).findElements(By.tagName("td"));
//                System.out.println(listTdWebElement.get(tdIndex).getAttribute("textContent"));
                content = listTdWebElement.get(tdIndex).getAttribute("textContent");

                if(headerName.equals("Identifier") && content.contains(",")){
                    content = content.replaceAll("\\s+", "");
                    String identifiers[] = content.split(",");
                    if(identifiers.length == 2){
                        if(identifiers.length == 2){
//                        if(identifiers[0].compareTo(identifiers[1]) > 0){
//                            content = identifiers[1];
//                        } else {
//                            content = identifiers[0];
//                        }
                            content = identifiers[0];
                        }
                    }
                }
                buildColumnStrings.add(content);
            }
            boolean isDate = false;
            if(headerName.contains("Date") ||headerName.contains("date")){
                isDate = true;
            }
            if(isAscendingListOfString(buildColumnStrings, isDate)){
                logger.log(LogStatus.INFO,"Ascendant Column " + headerName + " is correctly sorted");
            } else {
                logger.log(LogStatus.ERROR,"ERROR: Ascendant Column " + headerName + " is not correctly sorted");
            }
//            System.out.println("############################################");
            //Increase the index to check tbody/td[index]
            tdIndex++;
        }
    }

    private void descendingSortCheck(WebPendingPage pendingPage, WebStartPage startPage) throws Exception {
        reloadPendingFilterPageWithCatIdentifier(pendingPage, startPage);
        List<WebElement> listHeaderButton = pendingPage.headerRow.findElements(By.tagName("th"));
        //Check all columns
        List<WebElement> pageNumberList;
        List<String> buildColumnStrings;
        List<WebElement> listTrWebElement;
        List<WebElement> listTdWebElement;
        String content;
        int tdIndex = 0;
        //Select 50 rows per page
        click(pendingPage.pageSelectionDropList);
        click(pendingPage.fiftyRowsByPageSelection);
        String headerName = "";
        for (WebElement header : listHeaderButton){
            buildColumnStrings = new ArrayList<String>();
            headerName = header.getAttribute("textContent").replaceAll("\\s+", "");
            scrollOnElement(pendingPage.searchField);
            //Sort the column as ascending
            click(header);
            click(header); // double click to descendant sort
            sleep(duration);
            listTrWebElement = pendingPage.identifierTableResults.findElements(By.tagName("tr"));
            //Check the data is correctly sort (10 results max)
            for(int trIndex = 0; trIndex < listTrWebElement.size(); trIndex++){
                listTdWebElement = listTrWebElement.get(trIndex).findElements(By.tagName("td"));
//                System.out.println(listTdWebElement.get(tdIndex).getAttribute("textContent"));
                content = listTdWebElement.get(tdIndex).getAttribute("textContent");
                if(headerName.equals("Identifier") && content.contains(",")){
                    content = content.replaceAll("\\s+", "");
                    String identifiers[] = content.split(",");
                    if(identifiers.length == 2){
                        content = identifiers[0];
                    }
                }
                buildColumnStrings.add(content);
            }
            boolean isDate = false;
            if(headerName.contains("Date") ||headerName.contains("date")){
                isDate = true;
            }
            if(isDescendingListOfString(buildColumnStrings, isDate)){
                logger.log(LogStatus.INFO,"Descendant Column " + headerName + " is correctly sorted");
            } else {
                logger.log(LogStatus.ERROR,"ERROR: Descendant Column " + headerName + " is not correctly sorted");
            }
//            System.out.println("############################################");
            //Increase the index to check tbody/td[index]
            tdIndex++;
        }
    }

    private void reloadPendingFilterPageWithCatIdentifier(WebPendingPage pendingPage, WebStartPage startPage){
        click(startPage.filterButton);
        click(startPage.filterPendingBreederAndRefugeTabButton);
        WaitForElement(pendingPage.searchField,5);
    }

    private void checkPendingFilterAccessibility(WebPendingPage pendingPage, WebStartPage startPage){
        click(startPage.filterButton);
        click(startPage.filterPendingBreederAndRefugeTabButton);
        try{
            WaitForElement(pendingPage.searchField,5);
            logger.log(LogStatus.INFO,"Action button for filter Approved is reachable");
        } catch (Exception e){
            logger.log(LogStatus.ERROR,"ERROR: Action button for filter Approved is not reachable");
        }
        click(startPage.homeButton);
        click(startPage.filterButton);
        click(startPage.filterPendingBreederAndRefugeTabButton);
        try{
            WaitForElement(pendingPage.searchField,5);
            logger.log(LogStatus.INFO,"Tab button for filter Approved is reachable");
        } catch (Exception e){
            logger.log(LogStatus.ERROR,"ERROR: Tab button for filter Approved is not reachable");
        }
    }

    private boolean detectValidClickCreateCsvtButton(WebPendingPage pendingPage, List<String> expectedIdentifiers){
        boolean isValid;
        click(pendingPage.createCsvButton);
        String path = System.getProperty("user.dir")+"/src/test/java/Results";
        sleep(5000);
        File latestModifiedFile = getLatestFilefromDir(path);
        if(latestModifiedFile != null && latestModifiedFile.getName().contains("Data export")){
            isValid = true;
            logger.log(LogStatus.INFO, "Valid create CSV button click for " + latestModifiedFile.getName());
            CsvHandler csvHandler = new CsvHandler(logger);
            csvHandler.setCsvPath(latestModifiedFile.getAbsolutePath());
            List<String[]> entries = csvHandler.readAll();
            if((entries.size() - 1) == expectedIdentifiers.size()){ // (entries.size() - 1) because there is the title row
                for(int i = 0; i < expectedIdentifiers.size(); i++){
                    String[] entry = entries.get(i + 1);
                    if(!entry[0].equals(expectedIdentifiers.get(i))){//Compare the first column of the csv row and the expected identifier
                        isValid = false;
                        System.out.println(entries.get(i + 1)[0] + " " + expectedIdentifiers.get(i));
                        logger.log(LogStatus.ERROR, "ERROR: CSV file doesn't match with selected identifiers");
                        break;
                    }
                    logger.log(LogStatus.INFO, "CSV entry: " + Arrays.toString(entry));
                }
                logger.log(LogStatus.INFO, "CSV file matches with selected identifiers");
            }
            //Delete CSV file because useless now
            try {
                if(latestModifiedFile.delete()){
                    logger.log(LogStatus.INFO, "CSV file is deleted");
                } else {
                    logger.log(LogStatus.ERROR, "CSV file is not deleted, please clean the file manually");
                }
            } catch (Exception e) {
                logger.log(LogStatus.ERROR, "ERROR: CSV file cannot be deleted");
            }
        } else {
            logger.log(LogStatus.ERROR, "ERROR: Invalid create CSV  button click");
            sleep(5000);
            isValid = false;
        }
        return isValid;
    }

    private boolean detectValidClickEdditButton(WebPendingPage pendingPage, WebRegisterPage registerPage){
        boolean isValid;
        click(pendingPage.editButton);
        try{
            WaitForElement(registerPage.transfertOfResponsibility,10);
            isValid = true;
            logger.log(LogStatus.INFO, "Valid button click");
        } catch (Exception e){
            logger.log(LogStatus.ERROR, "ERROR: Invalid button click");
            sleep(5000);
            isValid = false;
        }
        return isValid;
    }

    private void checkButtonMenu(WebPendingPage pendingPage, WebStartPage startPage, WebRegisterPage registerPage){
        reloadPendingFilterPageWithCatIdentifier(pendingPage, startPage);

        //buttons menu are available
        //select all
        //user can click on the button
        //case no lines of the current page are selected
        //all lines becomes selected
        click(pendingPage.selectAllButton);
        int maxNumberRow = pendingPage.identifierTableResults.findElements(By.tagName("tr")).size();
        boolean areAllSelected = true;
        for(int i = 0; i < maxNumberRow; i++){
            if(!isSpecificColor(getRgbString(getRow(i,pendingPage.identifierTableResults)), "52", "69", "78")){ // gray color
                areAllSelected = false;
                break;
            }
        }
        if(areAllSelected){
            logger.log(LogStatus.INFO,"All lines become selected");
        } else {
            logger.log(LogStatus.ERROR,"ERROR: All lines don't become selected");
        }
        //case some lines of the current page are selected
        //all lines becomes selected
        if(maxNumberRow == 10) {
            //Unselect few lines
            click(getRow(1, pendingPage.identifierTableResults));
            click(getRow(5, pendingPage.identifierTableResults));
            click(getRow(8, pendingPage.identifierTableResults));
            click(pendingPage.selectAllButton);
            areAllSelected = true;
            for (int i = 0; i < 10; i++) {
                if (!isSpecificColor(getRgbString(getRow(i, pendingPage.identifierTableResults)), "52", "69", "78")) { // gray color
                    areAllSelected = false;
                    break;
                }
            }
            if (areAllSelected) {
                logger.log(LogStatus.INFO, "All lines become selected");
            } else {
                logger.log(LogStatus.ERROR, "ERROR: All lines don't become selected");
            }
            //case all lines of the current page are already selected
            //nothing change, all lines stay selected
            click(pendingPage.selectAllButton);
            areAllSelected = true;
            for (int i = 0; i < 10; i++) {
                if (!isSpecificColor(getRgbString(getRow(i, pendingPage.identifierTableResults)), "52", "69", "78")) { // gray color
                    areAllSelected = false;
                    break;
                }
            }
            if (areAllSelected) {
                logger.log(LogStatus.INFO, "All lines become selected");
            } else {
                logger.log(LogStatus.ERROR, "ERROR: All lines don't become selected");
            }

            //unselect all
            //by default, button is disable
            reloadPendingFilterPageWithCatIdentifier(pendingPage, startPage);
            if (pendingPage.unselectAllButton.getAttribute("class").equals("ui button buttons-select-none disabled")) {
                logger.log(LogStatus.INFO, "Menu Unselect All is disabled");
            } else {
                logger.log(LogStatus.ERROR, "ERROR: Menu Unselect All is enabled");
            }
            //case no line of the current page are selected
            //button stay disable
            boolean areNotAllSelected = true;
            for (int i = 0; i < 10; i++) {
                if (!isNoColor(getRgbString(getRow(i, pendingPage.identifierTableResults)))) {
                    areNotAllSelected = false;
                    break;
                }
            }
            if (areNotAllSelected && pendingPage.unselectAllButton.getAttribute("class").equals("ui button buttons-select-none disabled")) {
                logger.log(LogStatus.INFO, "Unselect button stays disable");
            } else {
                logger.log(LogStatus.ERROR, "ERROR: Unselect don't button stays disable");
            }
            //case one line of the current page is selected
            //button becomes enable
            click(getRow(5, pendingPage.identifierTableResults));
            if (areNotAllSelected && pendingPage.unselectAllButton.getAttribute("class").equals("ui button buttons-select-none")) {
                logger.log(LogStatus.INFO, "Unselect button becomes enable");
            } else {
                logger.log(LogStatus.ERROR, "ERROR: Unselect stays disable");
            }
            //user can click on button
            click(pendingPage.unselectAllButton);
            //selected line becomes unselected
            if (isNoColor(getRgbString(getRow(5, pendingPage.identifierTableResults))) && pendingPage.unselectAllButton.getAttribute("class").equals("ui button buttons-select-none disabled")) {
                logger.log(LogStatus.INFO, "Selected line becomes unselected");
            } else {
                logger.log(LogStatus.ERROR, "ERROR: Selected line doesn't become unselected");
            }
            //case some lines of the current page  are selected
            click(getRow(1, pendingPage.identifierTableResults));
            click(getRow(5, pendingPage.identifierTableResults));
            click(getRow(8, pendingPage.identifierTableResults));
            //button becomes enable
            if (pendingPage.unselectAllButton.getAttribute("class").equals("ui button buttons-select-none")) {
                logger.log(LogStatus.INFO, "Menu Unselect All is enable");
            } else {
                logger.log(LogStatus.ERROR, "ERROR: Menu Unselect All is disable");
            }
            //user can click on button
            click(pendingPage.unselectAllButton);
            //selected line becomes unselected
            if (isNoColor(getRgbString(getRow(1, pendingPage.identifierTableResults))) &&
                    isNoColor(getRgbString(getRow(5, pendingPage.identifierTableResults))) &&
                    isNoColor(getRgbString(getRow(8, pendingPage.identifierTableResults))) &&
                    pendingPage.unselectAllButton.getAttribute("class").equals("ui button buttons-select-none disabled")) {
                logger.log(LogStatus.INFO, "Selected lines become unselected");
            } else {
                logger.log(LogStatus.ERROR, "ERROR: Selected lines don't become unselected");
            }
            //case all lines of the current page are selected
            click(pendingPage.selectAllButton);
            //button becomes enable
            if (pendingPage.unselectAllButton.getAttribute("class").equals("ui button buttons-select-none")) {
                logger.log(LogStatus.INFO, "Menu Unselect All is enable");
            } else {
                logger.log(LogStatus.ERROR, "ERROR: Menu Unselect All is disable");
            }
            //user can click on button
            click(pendingPage.unselectAllButton);
            //selected line becomes unselected
            areNotAllSelected = true;
            for (int i = 0; i < 10; i++) {
                if (!isNoColor(getRgbString(getRow(i, pendingPage.identifierTableResults)))) {
                    areNotAllSelected = false;
                    break;
                }
            }
            if (areNotAllSelected && pendingPage.unselectAllButton.getAttribute("class").equals("ui button buttons-select-none disabled")) {
                logger.log(LogStatus.INFO, "Selected line becomes unselected");
            } else {
                logger.log(LogStatus.ERROR, "ERROR: Selected line stay selected");
            }

            //Edit
            //By default, button is enabled
            //user can click on the button
            //case no line is selected
            detectInvalidClickButton(pendingPage.editButton, pendingPage, "Please select from the list");//This test valid that by default the button is enable
            //case one line is selected
            reloadPendingFilterPageWithCatIdentifier(pendingPage, startPage); //reload just in case of error...
            click(getRow(1, pendingPage.identifierTableResults));
            detectValidClickEdditButton(pendingPage, registerPage);
            //case several lines are selected
            reloadPendingFilterPageWithCatIdentifier(pendingPage, startPage); //reload just in case of error...
            click(getRow(1, pendingPage.identifierTableResults));
            click(getRow(4, pendingPage.identifierTableResults));
            click(getRow(7, pendingPage.identifierTableResults));
            detectInvalidClickButton(pendingPage.editButton, pendingPage, "Select only one");

            //Send to Vet menu is available
            try {
                WaitForElement(pendingPage.sendToVetButton, 5);
                logger.log(LogStatus.INFO, "Send to Vet menu is available");
            } catch (Exception e) {
                logger.log(LogStatus.ERROR, "ERROR: Send to Vet menu is not available");
            }

            //Delete
            //Register 3 cats before deletion test
            generatePendingForms(registerPage, startPage);
            sleep(5000); // wait for the end of dialog message to click on filter tab
            reloadPendingFilterPageWithCatIdentifier(pendingPage, startPage);
            //by default, button is enable
            //case no line of the current page are selected
            //user can click on the button
            //an error message "please select from the list" is displayed
            detectInvalidClickButton(pendingPage.deleteButton, pendingPage, "Please select from the list");//This test valid that by default the button is enable

            //case one line of the current page is selected
            //user can click on the button
            reloadPendingFilterPageWithCatIdentifier(pendingPage, startPage);
            click(getRow(0, pendingPage.identifierTableResults));
            String beforeDeletionNumberOfForms = pendingPage.pageInfo.getAttribute("textContent").split(" ")[5];
            logger.log(LogStatus.INFO, "There are currently " + beforeDeletionNumberOfForms + " forms before deletion test");
            click(pendingPage.deleteButton);
            //user is prompted to confirm deletion
            //case Cancel
            click(pendingPage.refuseDeletionButton);
            String afterCancelDeletion = pendingPage.pageInfo.getAttribute("textContent").split(" ")[5];

            //nothing append, no deletion occurs
            if (beforeDeletionNumberOfForms.equals(afterCancelDeletion)) {
                logger.log(LogStatus.INFO, "Nothing append, no deletion occurred");
            } else {
                logger.log(LogStatus.ERROR, "ERROR: Something append, deletion occurred");
            }
            //case OK
            sleep(5000);//Because the dialog message can hide the filter button (visible so "wait for" return true)
            reloadPendingFilterPageWithCatIdentifier(pendingPage, startPage);
            click(getRow(0, pendingPage.identifierTableResults));
            beforeDeletionNumberOfForms = pendingPage.pageInfo.getAttribute("textContent").split(" ")[5];
            click(pendingPage.deleteButton);
            click(pendingPage.acceptDeletionButton);
            sleep(1000);
            String afterDeletion = pendingPage.pageInfo.getAttribute("textContent").split(" ")[5];
            //selected line is deleted
            //page is refreshed
            //entries counter is updated -1
            int refreshCounter = 0;
            try {
                int beforeInt = Integer.parseInt(beforeDeletionNumberOfForms);
                int afterInt = Integer.parseInt(afterDeletion);
                refreshCounter = afterInt - beforeInt;
            } catch (Exception e) {
                logger.log(LogStatus.ERROR, "ERROR: Integer parsing failed");
            }
            if (refreshCounter == -1) {
                logger.log(LogStatus.INFO, "Page is refreshed, entries counter is updated -1");
            } else {
                logger.log(LogStatus.ERROR, "ERROR: Page is refreshed, entries counter is not updated -1");
            }
            //case some lines of the current page  are selected
            reloadPendingFilterPageWithCatIdentifier(pendingPage, startPage);
            click(getRow(0, pendingPage.identifierTableResults));
            click(getRow(1, pendingPage.identifierTableResults));
            //user can click on the button
            beforeDeletionNumberOfForms = pendingPage.pageInfo.getAttribute("textContent").split(" ")[5];
            logger.log(LogStatus.INFO, "There are currently " + beforeDeletionNumberOfForms + " forms before deletion test");
            click(pendingPage.deleteButton);
            //user is prompted to confirm deletion
            //case Cancel
            click(pendingPage.refuseDeletionButton);
            afterCancelDeletion = pendingPage.pageInfo.getAttribute("textContent").split(" ")[5];
            //nothing append, no deletion occurs
            if (beforeDeletionNumberOfForms.equals(afterCancelDeletion)) {
                logger.log(LogStatus.INFO, "Nothing append, no deletions occurred");
            } else {
                logger.log(LogStatus.ERROR, "ERROR: Something append, deletions occurred");
            }
            //case OK
            sleep(5000);//Because the dialog message can hide the filter button (visible so "wait for" return true)
            reloadPendingFilterPageWithCatIdentifier(pendingPage, startPage);
            click(getRow(0, pendingPage.identifierTableResults));
            click(getRow(1, pendingPage.identifierTableResults));
            beforeDeletionNumberOfForms = pendingPage.pageInfo.getAttribute("textContent").split(" ")[5];
            click(pendingPage.deleteButton);
            click(pendingPage.acceptDeletionButton);
            sleep(1000);
            afterDeletion = pendingPage.pageInfo.getAttribute("textContent").split(" ")[5];
            //selected lines are deleted
            //page is refreshed
            //entries counter is updated minus number of lines selected
            refreshCounter = 0;
            try {
                int beforeInt = Integer.parseInt(beforeDeletionNumberOfForms);
                int afterInt = Integer.parseInt(afterDeletion);
                refreshCounter = afterInt - beforeInt;
            } catch (Exception e) {
                logger.log(LogStatus.ERROR, "ERROR: Integer parsing failed");
            }
            if (refreshCounter == -2) {
                logger.log(LogStatus.INFO, "Page is refreshed, entries counter is updated minus number of lines selected");
            } else {
                logger.log(LogStatus.ERROR, "ERROR: Page is refreshed, entries counter is not updated minus number of lines selected");
            }
            //entries counter is updated to 0
            //button unselect all becomes disable
            //no more lines are available
            String count = afterDeletion = pendingPage.pageInfo.getAttribute("textContent").split(" ")[5];
            if (count.equals("0")) {
                logger.log(LogStatus.INFO, "No more lines are available");
                if (pendingPage.unselectAllButton.getAttribute("class").equals("ui button buttons-select-none")) {
                    logger.log(LogStatus.INFO, "Menu Unselect All is enabled");
                } else {
                    logger.log(LogStatus.ERROR, "ERROR: Menu Unselect All is disabled");
                }
            } else {
                logger.log(LogStatus.INFO, "There are still lines available");
            }

            //create CSV
            //By default, button is enabled
            //user can click on the button
            //case no line is selected
            //Error message is displayed
            reloadPendingFilterPageWithCatIdentifier(pendingPage, startPage); //reload just in case of error...
            detectInvalidClickButton(pendingPage.createCsvButton, pendingPage, "Please select from the list");//This test valid that by default the button is enable
            //case one line is selected
            //a CSV file is generated
            reloadPendingFilterPageWithCatIdentifier(pendingPage, startPage); //reload just in case of error...
            setText(pendingPage.searchField, "123456789100");
            sleep(1000);
            click(getRow(1, pendingPage.identifierTableResults));
            List<String> expectedIdentifiers = new ArrayList<String>();
            expectedIdentifiers.add(getCell(1, 0, pendingPage.identifierTableResults).getAttribute("textContent"));
            detectValidClickCreateCsvtButton(pendingPage, expectedIdentifiers);
            //file contains the line selected
            //case several lines are selected
            //a CSV file is generated
            //file contains the lines selected
            reloadPendingFilterPageWithCatIdentifier(pendingPage, startPage); //reload just in case of error...
            setText(pendingPage.searchField, "123456789100");
            sleep(1000);
            click(getRow(1, pendingPage.identifierTableResults));
            click(getRow(2, pendingPage.identifierTableResults));
            expectedIdentifiers = new ArrayList<String>();
            expectedIdentifiers.add(getCell(1, 0, pendingPage.identifierTableResults).getAttribute("textContent"));
            expectedIdentifiers.add(getCell(2, 0, pendingPage.identifierTableResults).getAttribute("textContent"));
            detectValidClickCreateCsvtButton(pendingPage, expectedIdentifiers);
            //case selected line has multiple identifier
            //user must be able to edit CSV
            //both identifier must be set in column identifier
            reloadPendingFilterPageWithCatIdentifier(pendingPage, startPage); //reload just in case of error...
            setText(pendingPage.searchField, ",");
            sleep(1000);
            click(getRow(0, pendingPage.identifierTableResults));
            expectedIdentifiers = new ArrayList<String>();
            expectedIdentifiers.add(getCell(0, 0, pendingPage.identifierTableResults).getAttribute("textContent"));
            detectValidClickCreateCsvtButton(pendingPage, expectedIdentifiers);
        }
    }

    protected void generatePendingForms(WebRegisterPage registerPage, WebStartPage startPage){
        if(registrator.equals("Breeder 4")) { //Breeder 1 create 4 waiting for approval form for refuge 1

            try {
                registerPendingBreederOrRefugeCat(registerPage, startPage);
                registerPendingBreederOrRefugeCat(registerPage, startPage);
                registerPendingBreederOrRefugeCat(registerPage, startPage);
            } catch (Exception e) {
                logger.log(LogStatus.ERROR, "ERROR: registration failed " + e.getMessage());
            }

        } else if(registrator.equals("Refuge 3")) {//Refuge 1 create 4 waiting for approval form for breeder 1
            try {
                registerPendingBreederOrRefugeCat(registerPage, startPage);
                registerPendingBreederOrRefugeCat(registerPage, startPage);
                registerPendingBreederOrRefugeCat(registerPage, startPage);
            } catch (Exception e) {
                logger.log(LogStatus.ERROR, "ERROR: registration failed " + e.getMessage());
            }
        }
    }

    private void checkSelectAndUnSelectLine(WebPendingPage pendingPage, WebStartPage startPage){
        reloadPendingFilterPageWithCatIdentifier(pendingPage, startPage);
        //User can select lines
        //by clicking on a line
        //line becomes gray
        WebElement firstRow = getRow(0,pendingPage.identifierTableResults);
        click(firstRow);
        if(isSpecificColor(getRgbString(firstRow), "52", "69", "78")){
            logger.log(LogStatus.INFO,"First line is gray when selected (by clicking)");
        } else {
            logger.log(LogStatus.ERROR,"ERROR: First line is not gray when selected (by clicking)");
        }
        //Label row selected is incremented by 1
        if(pendingPage.pageInfo.getAttribute("textContent").contains(("1 row selected"))){
            logger.log(LogStatus.INFO,"Label row selected is incremented by 1");
        } else {
            logger.log(LogStatus.ERROR,"ERROR: Label row selected is not incremented by 1");
        }
        //Menu Unselect All is enabled
        if(pendingPage.unselectAllButton.getAttribute("class").equals("ui button buttons-select-none")){
            logger.log(LogStatus.INFO,"Menu Unselect All is enabled");
        } else {
            logger.log(LogStatus.ERROR,"ERROR: Menu Unselect All is disabled");
        }

        WebElement thirdRow = getRow(2,pendingPage.identifierTableResults);
        click(thirdRow);
        if(isSpecificColor(getRgbString(thirdRow), "52", "69", "78")){
            logger.log(LogStatus.INFO,"Third line is gray when selected (by clicking)");
        } else {
            logger.log(LogStatus.ERROR,"ERROR: Third line is not gray when selected (by clicking)");
        }
        //Label row selected is incremented by 1
        if(pendingPage.pageInfo.getAttribute("textContent").contains(("2 rows selected"))){
            logger.log(LogStatus.INFO,"Label row selected is incremented by 1");
        } else {
            logger.log(LogStatus.ERROR,"ERROR: Label row selected is not incremented by 1");
        }

        //User can deselect lines
        //line must be selected and color gray
        //user click on the line
        //line become deselected
        //color line becomes white
        click(pendingPage.firstIdentifierTableResult);
        if(isNoColor(getRgbString(pendingPage.firstIdentifierTableResult))){
            logger.log(LogStatus.INFO,"First line is white when selected (by clicking)");
        } else {
            logger.log(LogStatus.ERROR,"ERROR: First line is not white when selected (by clicking)");
        }
        //Label row selected is decremented by 1
        if(pendingPage.pageInfo.getAttribute("textContent").contains(("1 row selected"))){
            logger.log(LogStatus.INFO,"Label row selected is decremented by 1");
        } else {
            logger.log(LogStatus.ERROR,"ERROR: Label row selected is not decremented by 1");
        }

        click(thirdRow);
        if(isNoColor(getRgbString(thirdRow))){
            logger.log(LogStatus.INFO,"Third line is white when selected (by clicking)");
        } else {
            logger.log(LogStatus.ERROR,"ERROR: Third line is not white when selected (by clicking)");
        }

        //Label row selected is decremented by 1
        if(!pendingPage.pageInfo.getAttribute("textContent").contains(("selected"))){
            logger.log(LogStatus.INFO,"Label row selected is decremented by 1");
        } else {
            logger.log(LogStatus.ERROR,"ERROR: Label row selected is not decremented by 1");
        }
        //case no more lines are selected
        //menu unselect all is disabled
        if(pendingPage.unselectAllButton.getAttribute("class").equals("ui button buttons-select-none disabled")){
            logger.log(LogStatus.INFO,"Menu Unselect All is disabled");
        } else {
            logger.log(LogStatus.ERROR,"ERROR: Menu Unselect All is enabled");
        }
    }

    private void checkSearchField(WebPendingPage pendingPage, WebStartPage startPage){
        int DURATION = 2000;
        reloadPendingFilterPageWithCatIdentifier(pendingPage, startPage);
        HashMap<String,String> expectedResult = new HashMap<String, String>();
        HashMap<String,String> searchWords = new HashMap<String, String>();
        if(registrator.equals("Refuge 3")){
            expectedResult.put("Identifier", "123456789200071");
            expectedResult.put("PassportNumber", "TV67984613");
            expectedResult.put("DateOfBirth", "08/01/2018");
            expectedResult.put("IdentificationDate", "16/01/2018");
            expectedResult.put("Gender", "Male");
            expectedResult.put("CatBreed", "Abyssin");
            expectedResult.put("Sterelized", "Yes");
            expectedResult.put("SterelizationDate", "30/01/2018");
            expectedResult.put("Info", "F0024");
            expectedResult.put("Status", "Pending");
            searchWords.put("Identifier", "789200071");
            searchWords.put("PassportNumber", "67984613");
            searchWords.put("DateOfBirth", "08/01/2018");
            searchWords.put("IdentificationDate", "16/01/2018");
            searchWords.put("Gender", "Male");
            searchWords.put("CatBreed", "Abyssin");
            searchWords.put("Sterelized", "Yes");
            searchWords.put("SterelizationDate", "30/01/2018");
            searchWords.put("Info", "F0024");
            searchWords.put("Status", "Pending");
        } else {
            expectedResult.put("Identifier", "123456789200073");
            expectedResult.put("PassportNumber", "TC465789413");
            expectedResult.put("DateOfBirth", "09/01/2018");
            expectedResult.put("IdentificationDate", "24/01/2018");
            expectedResult.put("Gender", "Male");
            expectedResult.put("CatBreed", "Exotic");
            expectedResult.put("Sterelized", "Yes");
            expectedResult.put("SterelizationDate", "22/01/2018");
            expectedResult.put("Info", "F0024");
            expectedResult.put("Status", "Pending");
            searchWords.put("Identifier", "789200073");
            searchWords.put("PassportNumber", "TC465789413");
            searchWords.put("DateOfBirth", "09/01/2018");
            searchWords.put("IdentificationDate", "24/01/2018");
            searchWords.put("Gender", "Male");
            searchWords.put("CatBreed", "Exotic");
            searchWords.put("Sterelized", "Yes");
            searchWords.put("SterelizationDate", "22/01/2018");
            searchWords.put("Info", "F0024");
            searchWords.put("Status", "Pending");
        }

        //User can perform searches
        //by enconding string in field Search
        //results are displayed after each character encoded
        //search will fetch on column
        //identifier  -> "500319" to search for "123456789500319"
        setText(pendingPage.searchField,searchWords.get("Identifier"));
        sleep(DURATION); // Wait 1s to let the result to be refreshed
        getScreenShot();
        String indeitifierResult = lookForExpectedResult(expectedResult.get("Identifier"), pendingPage.identifierTableResults, 0);
        if(indeitifierResult.contains(expectedResult.get("Identifier"))){
            logger.log(LogStatus.INFO,"Identifier search for " + searchWords.get("Identifier") + " works and the result is " + indeitifierResult);
        } else {
            logger.log(LogStatus.ERROR,"ERROR: Identifier search for " + searchWords.get("Identifier") + " doesn't work and the result is " + indeitifierResult);
        }
        //passport number
        setText(pendingPage.searchField,searchWords.get("PassportNumber"));
        sleep(DURATION); // Wait 1s to let the result to be refreshed
        getScreenShot();
        String passportResult = lookForExpectedResult(expectedResult.get("PassportNumber"), pendingPage.identifierTableResults, 1);
        if(passportResult.contains(expectedResult.get("PassportNumber"))){
            logger.log(LogStatus.INFO,"Passport search for " + searchWords.get("PassportNumber") + " works and the result is " + passportResult);
        } else {
            logger.log(LogStatus.ERROR,"ERROR: Passport search for " + searchWords.get("PassportNumber") + " doesn't work and the result is " + passportResult);
        }
        //date of birth
        setText(pendingPage.searchField,searchWords.get("DateOfBirth"));
        sleep(DURATION); // Wait 1s to let the result to be refreshed
        getScreenShot();
        String birthDateResult = lookForExpectedResult(expectedResult.get("DateOfBirth"), pendingPage.identifierTableResults, 2);
        if(birthDateResult.contains(searchWords.get("DateOfBirth"))){
            logger.log(LogStatus.INFO,"Date of birth search for " + searchWords.get("DateOfBirth") + " works and the result is " + birthDateResult);
        } else {
            logger.log(LogStatus.ERROR,"ERROR: Date of birth search for "+ searchWords.get("DateOfBirth") + " doesn't work and the result is " + birthDateResult);
        }
        //identification date
        setText(pendingPage.searchField,searchWords.get("IdentificationDate"));
        sleep(DURATION); // Wait 1s to let the result to be refreshed
        getScreenShot();
        String identificationDateResult = lookForExpectedResult(expectedResult.get("IdentificationDate"), pendingPage.identifierTableResults, 3);
        if(identificationDateResult.contains(expectedResult.get("IdentificationDate"))){
            logger.log(LogStatus.INFO,"Date of identification search for " + searchWords.get("IdentificationDate") + " works and the result is " + identificationDateResult);
        } else {
            logger.log(LogStatus.ERROR,"ERROR: Date of identification search for " + searchWords.get("IdentificationDate") + " doesn't work and the result is " + identificationDateResult);
        }
        //Gender
        setText(pendingPage.searchField,searchWords.get("Gender"));
        sleep(1000); // Wait 1s to let the result to be refreshed
        getScreenShot();
        String genderResult = lookForExpectedResult(expectedResult.get("Gender"), pendingPage.identifierTableResults, 4);
        if(genderResult.contains(expectedResult.get("Gender"))){
            logger.log(LogStatus.INFO,"Gender search for " + searchWords.get("Gender") + " works and the result is " + genderResult);
        } else {
            logger.log(LogStatus.ERROR,"ERROR: Gender search for " + searchWords.get("Gender") + " doesn't work and the result is " + genderResult);
        }
        //Cat Breed
        setText(pendingPage.searchField,searchWords.get("CatBreed"));
        sleep(DURATION); // Wait 1s to let the result to be refreshed
        getScreenShot();
        String breedResult = lookForExpectedResult(expectedResult.get("CatBreed"), pendingPage.identifierTableResults, 5);
        if(breedResult.contains(expectedResult.get("CatBreed"))){
            logger.log(LogStatus.INFO,"Breed search for " + searchWords.get("CatBreed") + " works and the result is " + breedResult);
        } else {
            logger.log(LogStatus.ERROR,"ERROR: Breed search for " + searchWords.get("CatBreed") + " doesn't work and the result is " + breedResult);
        }
        //Sterilized
        setText(pendingPage.searchField,searchWords.get("Sterelized"));
        sleep(DURATION); // Wait 1s to let the result to be refreshed
        getScreenShot();
        String sterilizedResult = lookForExpectedResult(expectedResult.get("Sterelized"), pendingPage.identifierTableResults, 6);
        if(sterilizedResult.contains(expectedResult.get("Sterelized"))){
            logger.log(LogStatus.INFO,"Sterilzed search for " + searchWords.get("Sterelized") + " works and the result is " + sterilizedResult);
        } else {
            logger.log(LogStatus.ERROR,"ERROR: Sterilzed search for " + searchWords.get("Sterelized") + " doesn't work and the result is " + sterilizedResult);
        }
        //sterilized date
        setText(pendingPage.searchField,searchWords.get("SterelizationDate"));
        sleep(DURATION); // Wait 1s to let the result to be refreshed
        getScreenShot();
        String sterilizedDateResult = lookForExpectedResult(expectedResult.get("SterelizationDate"), pendingPage.identifierTableResults, 7);
        if(sterilizedDateResult.contains(expectedResult.get("SterelizationDate"))){
            logger.log(LogStatus.INFO,"Sterilzed date search for " + searchWords.get("SterelizationDate") + " works and the result is " + sterilizedDateResult);
        } else {
            logger.log(LogStatus.ERROR,"ERROR: Sterilzed date search for " + searchWords.get("SterelizationDate") + " doesn't work and the result is " + sterilizedDateResult);
        }
        //owner information
        setText(pendingPage.searchField,searchWords.get("Info"));
        sleep(DURATION); // Wait 1s to let the result to be refreshed
        getScreenShot();
        String ownerInfoResult = lookForExpectedResult(expectedResult.get("Info"), pendingPage.identifierTableResults, 8);
        if(ownerInfoResult.contains(searchWords.get("Info"))){
            logger.log(LogStatus.INFO,"Owner Information search for " + searchWords.get("Info") + " works and the result is " + ownerInfoResult);
        } else {
            logger.log(LogStatus.ERROR,"ERROR: Owner Information search for " + searchWords.get("Info") + " doesn't work and the result is " + ownerInfoResult);
        }
        //status
        setText(pendingPage.searchField,searchWords.get("Status"));
        sleep(DURATION); // Wait 1s to let the result to be refreshed
        getScreenShot();
        String statusResult = lookForExpectedResult(expectedResult.get("Status"), pendingPage.identifierTableResults, 9);
        if(statusResult.contains(searchWords.get("Status"))){
            logger.log(LogStatus.INFO,"Status search for " + searchWords.get("Status") + " works and the result is " + statusResult);
        } else {
            logger.log(LogStatus.ERROR,"ERROR: Status search for " + searchWords.get("Status") + " doesn't work and the result is " + statusResult);
        }
    }

    private void pendingFilterCheck(WebPendingPage pendingPage, WebStartPage startPage, SqlOracleMiddleware metaformMiddleWare, WebRegisterPage registerPage) throws Exception {
        click(startPage.homeButton);
        //by clicking on the menu filters - Approved
        //The all cats page is displayed
        //all cats where vet are linked and approved by system are displayed
        //total number of cats is the sum of
        String numberCatApprovedDisplayed = "";
        String numberCatApprovedSql = "";
        String refnum = "";
        WaitForElement(startPage.filterPendingBreederAndRefugeActionButton,10);
        numberCatApprovedDisplayed = startPage.filterPendingBreederAndRefugeActionButton.getAttribute("textContent").replaceAll("\\s+", "");
        if(registrator.equals("Breeder 4")){ //Breeder
            refnum = "HK159357";
        } else { //Refuge
            refnum = "HK852456";
        }
        ArrayList<HashMap> rs = metaformMiddleWare.executeSqlQuery("SELECT count(*) from METAFORM_ABIEC_ANIMAL where (MF_STATUS = 1 OR  MF_STATUS = 8192) AND OWNER_REFNUM = '" + refnum + "'");
        if (rs.size() == 0) {
            logger.log(LogStatus.ERROR, "Something is wrong with SQL statement or DB connection");
        } else {
            numberCatApprovedSql = (String)rs.get(0).get("COUNT(*)");
            if(numberCatApprovedSql != null && numberCatApprovedDisplayed != null && numberCatApprovedSql.equals(numberCatApprovedDisplayed)){
                logger.log(LogStatus.INFO,"All cats where vet is linked and in pending are displayed");
            } else {
                logger.log(LogStatus.ERROR,"ERROR: All cats where vet is not linked or in pending are not displayed");
            }
        }
        //Page is showing 10 cats per page by default
        click(startPage.filterPendingBreederAndRefugeActionButton);
        List<WebElement> listResults = pendingPage.identifierTableResults.findElements(By.tagName("tr"));
        //Wait a little bit the page is completely loaded to take a screenshot
        WaitForElement(pendingPage.firstIdentifierTableResult,5);
        if(listResults.size() == 10){
            logger.log(LogStatus.INFO,"Page is showing 10 cats per page by default");
            if(isElementPresent(pendingPage.nextButton)){
                //case of more than 10 cats
                //next button is displayed
                logger.log(LogStatus.INFO,"Next button is displayed");
                //case there are more than one page available
                //case there are more than 10 cats
                //pages number are displayed
                List<WebElement> pageNumberList = pendingPage.paginationMenu.findElements(By.tagName("a"));
                if(pageNumberList.size() > 5){
                    //is it possible to click on the specific page
                    //selected page is displayed
                    //other available pages are still available for click
                    //page 1 display results from 1 to 10
                    //page 2 display result from 11 to 20
                    //page 3 display result from 21 to 30

                    logger.log(LogStatus.INFO,"Pages number are displayed");
                    if(pendingPage.pageInfo.getAttribute("textContent").contains("1 to 10")){
                        logger.log(LogStatus.INFO,"Page 1 display results from 1 to 10");
                    } else {
                        logger.log(LogStatus.ERROR,"ERROR: Page 1 doesn't display results from 1 to 10: " + pendingPage.pageInfo.getAttribute("textContent"));
                    }
                    click(pageNumberList.get(1)); //Click page 2
                    WaitForElement(pendingPage.firstIdentifierTableResult,5);
                    logger.log(LogStatus.INFO,"Click page 2");
                    if(pendingPage.pageInfo.getAttribute("textContent").contains("11 to 20")){
                        logger.log(LogStatus.INFO,"Page 2 display results from 11 to 20");
                    } else {
                        logger.log(LogStatus.ERROR,"ERROR: Page 2 doesn't display results from 11 to 20: " + pendingPage.pageInfo.getAttribute("textContent"));
                    }
                    pageNumberList = pendingPage.paginationMenu.findElements(By.tagName("a"));
                    //Indexes change between multiple clicks
                    click(pageNumberList.get(3)); //Click page 3
                    WaitForElement(pendingPage.firstIdentifierTableResult,5);
                    logger.log(LogStatus.INFO,"Click page 3");
                    if(pendingPage.pageInfo.getAttribute("textContent").contains("21 to")){
                        logger.log(LogStatus.INFO,"Page 3 display results from 21");
                    } else {
                        logger.log(LogStatus.ERROR,"ERROR: Page 3 doesn't display results from 21 to 30: " + pendingPage.pageInfo.getAttribute("textContent"));
                    }
                    pageNumberList = pendingPage.paginationMenu.findElements(By.tagName("a"));
                    click(pageNumberList.get(1)); //Click page 1
                    WaitForElement(pendingPage.firstIdentifierTableResult,5);
                    logger.log(LogStatus.INFO,"Click page 1");
                } else {
                    logger.log(LogStatus.INFO,"There are not enough forms in the filter to test multiple pages");
                }

            } else {
                logger.log(LogStatus.INFO,"Next button is not displayed");
            }
            if(pendingPage.numberResultByPageSelectionDroplist.getAttribute("textContent").contains("10")
                    && pendingPage.numberResultByPageSelectionDroplist.getAttribute("textContent").contains("25")
                    && pendingPage.numberResultByPageSelectionDroplist.getAttribute("textContent").contains("50")
                    && pendingPage.numberResultByPageSelectionDroplist.getAttribute("textContent").contains("100")){

                logger.log(LogStatus.INFO,"Number of result by page can be modify between {10, 25, 50, 100} rows");
            } else {
                logger.log(LogStatus.ERROR,"ERROR: Number of result by page cannot be modify between {10, 25, 50, 100} rows");
            }
            if(isElementPresent(pendingPage.previousButton)) {
                //case of more than 10 cats
                //previous button is displayed
                logger.log(LogStatus.INFO, "Previous button is displayed");
            } else {
                logger.log(LogStatus.ERROR, "ERROR: Previous button is not displayed");
            }
        } else {
            logger.log(LogStatus.INFO,"There are not enough forms in the filter to test more than 10 pages");
        }
        ascendingSortCheck(pendingPage, startPage);
        descendingSortCheck(pendingPage, startPage);
        checkSearchField(pendingPage,startPage);
        checkSelectAndUnSelectLine(pendingPage,startPage);
        checkButtonMenu(pendingPage,startPage, registerPage);
    }
}
