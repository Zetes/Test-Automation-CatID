package ElementaryScripts.Web;

import Csv.CsvHandler;
import DataBaseFramework.SqlOracleMiddleware;
import Screens.*;
import com.relevantcodes.extentreports.LogStatus;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.io.File;
import java.util.*;

public class FilterRejectPage extends FilterPage {
    private int MAX_REJECT = 0;
    private int duration = 5000;
    public void run() throws Exception {
        try {
            WebStartPage startPage = new WebStartPage(driver);
            WebRejectPage rejectPage = new WebRejectPage(driver);
            WebRegisterPage registerPage = new WebRegisterPage(driver);
            WebWaitingForApprovalPage waitingForApprovalPage = new WebWaitingForApprovalPage(driver);
            SqlOracleMiddleware metaformMiddleWare = new SqlOracleMiddleware(logger, "MF_CATS", "MF_CATS", "jdbc:oracle:thin:@192.168.17.6:1521:PREPROD");

//            //Accessibility check
            checkRejectFilterAccessibility( rejectPage, startPage);

            //Reject filter check
//            rejectFilterCheck(rejectPage, startPage, eazyIdPage, metaformMiddleWare, modifyPage);
            rejectFilterCheck(rejectPage, startPage, metaformMiddleWare, registerPage, waitingForApprovalPage);
            //logout for next page
            click(startPage.logoutButton);
        }catch (Exception e) {
            logger.logScreenshot(LogStatus.ERROR, "ERROR stopping the test", getScreenShot());
            throw e;
        }
    }

    private void ascendingSortCheck(WebRejectPage rejectPage, WebStartPage startPage) throws Exception {
        reloadRejectFilterPageWithCatIdentifier(rejectPage, startPage);
        List<WebElement> listHeaderButton = rejectPage.headerRow.findElements(By.tagName("th"));
        //Check all columns
        List<WebElement> pageNumberList;
        List<String> buildColumnStrings;
        List<WebElement> listTrWebElement;
        List<WebElement> listTdWebElement;
        String content;
        int tdIndex = 0;
        //Select 50 rows per page
        click(rejectPage.pageSelectionDropList);
        click(rejectPage.fiftyRowsByPageSelection);
        sleep(5000);
        String headerName = "";
        for (WebElement header : listHeaderButton){
            buildColumnStrings = new ArrayList<String>();
            headerName = header.getAttribute("textContent").replaceAll("\\s+", "");
            scrollOnElement(rejectPage.searchField);
            //Sort the column as ascending
            click(header); //Click for ascendant sort

            sleep(3000);//need time to load 50 rows
            listTrWebElement = rejectPage.identifierTableResults.findElements(By.tagName("tr"));
            //Check the data is correctly sort (10 results max)
            for(int trIndex = 0; trIndex < MAX_REJECT; trIndex++){
                listTdWebElement = listTrWebElement.get(trIndex).findElements(By.tagName("td"));
                content = listTdWebElement.get(tdIndex).getAttribute("textContent");

                if(registrator.contains("Veterinary") && headerName.equals("Ownerinformation")){
                    String splittedString [] = content.split(" ");
                    if(splittedString.length > 1){
                        if(splittedString[0].startsWith("HK") || splittedString[0].startsWith("BR") || splittedString[0].startsWith("RE")){
                            content = splittedString[1];//set the content with only the first name
                        } else {
                            content = splittedString[0];//set the content with only the first name
                        }
                    }
                }

                if(headerName.equals("Identifier") && content.contains(",")){
                    content = content.replaceAll("\\s+", "");
                    String identifiers[] = content.split(",");
                    if(identifiers.length == 2){
                        if(identifiers.length == 2){
//                        if(identifiers[0].compareTo(identifiers[1]) > 0){
//                            content = identifiers[1];
//                        } else {
//                            content = identifiers[0];
//                        }
                            content = identifiers[0];
                        }
                    }
                }

//                if(headerName.equals("Rejectreason") && content.startsWith("WR00")){
//                    String splittedString [] = content.split(" ");
//                    if(splittedString.length > 1){
//                        content = splittedString[1];//set the content with only the first word after wr0000XXX
//                    }
//                }

                buildColumnStrings.add(content);
            }
            boolean isDate = false;
            if(headerName.contains("Date") ||headerName.contains("date")){
                isDate = true;
            }
            if(isAscendingListOfString(buildColumnStrings, isDate)){
                logger.log(LogStatus.INFO,"Ascendant Column " + headerName + " is correctly sorted");
            } else {
                logger.log(LogStatus.ERROR,"ERROR: Ascendant Column " + headerName + " is not correctly sorted");
            }
            //Increase the index to check tbody/td[index]
            tdIndex++;
        }
    }

    private void descendingSortCheck(WebRejectPage rejectPage, WebStartPage startPage) throws Exception {
        reloadRejectFilterPageWithCatIdentifier(rejectPage, startPage);
        List<WebElement> listHeaderButton = rejectPage.headerRow.findElements(By.tagName("th"));
        //Check all columns
        List<WebElement> pageNumberList;
        List<String> buildColumnStrings;
        List<WebElement> listTrWebElement;
        List<WebElement> listTdWebElement;
        String content;
        int tdIndex = 0;
        //Select 50 rows per page
        click(rejectPage.pageSelectionDropList);
        click(rejectPage.fiftyRowsByPageSelection);
        String headerName = "";
        for (WebElement header : listHeaderButton){
            buildColumnStrings = new ArrayList<String>();
            headerName = header.getAttribute("textContent").replaceAll("\\s+", "");
            scrollOnElement(rejectPage.searchField);
            //Sort the column as ascending
            click(header);
            click(header); // double click to descendant sort
            sleep(duration);
            listTrWebElement = rejectPage.identifierTableResults.findElements(By.tagName("tr"));
            //Check the data is correctly sort (10 results max)
            for(int trIndex = 0; trIndex < MAX_REJECT; trIndex++){
                listTdWebElement = listTrWebElement.get(trIndex).findElements(By.tagName("td"));
//                System.out.println(listTdWebElement.get(tdIndex).getAttribute("textContent"));
                content = listTdWebElement.get(tdIndex).getAttribute("textContent");
                if(registrator.contains("Veterinary") && headerName.equals("Ownerinformation")){
                    String splittedString [] = content.split(" ");
                    if(splittedString.length > 1){
                        if(splittedString[0].startsWith("HK") || splittedString[0].startsWith("BR") || splittedString[0].startsWith("RE")){
                            content = splittedString[1];//set the content with only the first name
                        } else {
                            content = splittedString[0];//set the content with only the first name
                        }
                    }
                }
                if(headerName.equals("Identifier") && content.contains(",")){
                    content = content.replaceAll("\\s+", "");
                    String identifiers[] = content.split(",");
                    if(identifiers.length == 2){
                        content = identifiers[0];
                    }
                }

//                if(headerName.equals("Rejectreason") && content.startsWith("WR00")){
//                    String splittedString [] = content.split(" ");
//                    if(splittedString.length > 1){
//                        content = splittedString[1];//set the content with only the first word after wr0000XXX
//                    }
//                }

                buildColumnStrings.add(content);
            }
            boolean isDate = false;
            if(headerName.contains("Date") ||headerName.contains("date")){
                isDate = true;
            }
            if(isDescendingListOfString(buildColumnStrings, isDate)){
                logger.log(LogStatus.INFO,"Descendant Column " + headerName + " is correctly sorted");
            } else {
                logger.log(LogStatus.ERROR,"ERROR: Descendant Column " + headerName + " is not correctly sorted");
            }
//            System.out.println("############################################");
            //Increase the index to check tbody/td[index]
            tdIndex++;
        }
    }

    private void reloadRejectFilterPageWithCatIdentifier(WebRejectPage rejectPage, WebStartPage startPage){
        click(startPage.filterButton);
        if(registrator.contains("Veterinary")) {
            click(startPage.filterRejectedVetTabButton);
        } else {
            click(startPage.filterRejectsBreederAndRefugeTabButton);
        }
        WaitForElement(rejectPage.searchField,5);
    }

    private void checkRejectFilterAccessibility(WebRejectPage rejectPage, WebStartPage startPage){
        click(startPage.filterButton);
        if(registrator.contains("Veterinary")) {
            click(startPage.filterRejectedVetActionButton);
        } else {
            click(startPage.filterRejectsBreederAndRefugeActionButton);
        }
        try{
            WaitForElement(rejectPage.searchField,5);
            logger.log(LogStatus.INFO,"Action button for filter Reject is reachable");
        } catch (Exception e){
            logger.log(LogStatus.ERROR,"ERROR: Action button for filter Reject is not reachable");
        }
        click(startPage.homeButton);
        click(startPage.filterButton);
        if(registrator.contains("Veterinary")) {
            click(startPage.filterRejectedVetTabButton);
        } else {
            click(startPage.filterRejectsBreederAndRefugeTabButton);
        }
        try{
            WaitForElement(rejectPage.searchField,5);
            logger.log(LogStatus.INFO,"Tab button for filter Reject is reachable");
        } catch (Exception e){
            logger.log(LogStatus.ERROR,"ERROR: Tab button for filter Reject is not reachable");
        }
    }

    private boolean detectValidClickEdditButton(WebRejectPage rejectPage, WebRegisterPage registerPage){
        boolean isValid;
        click(rejectPage.editButton);
        try{
            WaitForElement(registerPage.transfertOfResponsibility,10);
            isValid = true;
            logger.log(LogStatus.INFO, "Valid button click");
        } catch (Exception e){
            logger.log(LogStatus.ERROR, "ERROR: Invalid button click");
            sleep(5000);
            isValid = false;
        }
        return isValid;
    }

    private boolean detectValidClickCreateCsvtButton(WebRejectPage rejectPage, List<String> expectedIdentifiers){
        boolean isValid;
        click(rejectPage.createCsvButton);
        String path = System.getProperty("user.dir")+"/src/test/java/Results";
        File latestModifiedFile = getLatestFilefromDir(path);
        if(latestModifiedFile != null && latestModifiedFile.getName().contains("Data export")){
            isValid = true;
            logger.log(LogStatus.INFO, "Valid create CSV button click for " + latestModifiedFile.getName());
            CsvHandler csvHandler = new CsvHandler(logger);
            csvHandler.setCsvPath(latestModifiedFile.getAbsolutePath());
            List<String[]> entries = csvHandler.readAll();
            if((entries.size() - 1) == expectedIdentifiers.size()){ // (entries.size() - 1) because there is the title row
                for(int i = 0; i < expectedIdentifiers.size(); i++){
                    String[] entry = entries.get(i + 1);
                    if(!entry[0].equals(expectedIdentifiers.get(i))){//Compare the first column of the csv row and the expected identifier
                        isValid = false;
                        System.out.println(entries.get(i + 1)[0] + " " + expectedIdentifiers.get(i));
                        logger.log(LogStatus.ERROR, "ERROR: CSV file doesn't match with selected identifiers");
                        break;
                    }
                    logger.log(LogStatus.INFO, "CSV entry: " + Arrays.toString(entry));
                }
                logger.log(LogStatus.INFO, "CSV file matches with selected identifiers");
            }
            //Delete CSV file because useless now
            try {
                if(latestModifiedFile.delete()){
                    logger.log(LogStatus.INFO, "CSV file is deleted");
                } else {
                    logger.log(LogStatus.ERROR, "CSV file is not deleted, please clean the file manually");
                }
            } catch (Exception e) {
                logger.log(LogStatus.ERROR, "ERROR: CSV file cannot be deleted");
            }
        } else {
            logger.log(LogStatus.ERROR, "ERROR: Invalid create CSV  button click");
            sleep(5000);
            isValid = false;
        }
        return isValid;
    }

    private void checkButtonMenu(WebRejectPage rejectPage, WebStartPage startPage, WebRegisterPage registerPage, WebWaitingForApprovalPage waitingForApprovalPage){
        reloadRejectFilterPageWithCatIdentifier(rejectPage, startPage);

        //buttons menu are available
        //select all
        //user can click on the button
        //case no lines of the current page are selected
        //all lines becomes selected
        click(rejectPage.selectAllButton);
        boolean areAllSelected = true;

        int maxNumberRow = rejectPage.identifierTableResults.findElements(By.tagName("tr")).size();
        for(int i = 0; i < maxNumberRow; i++){
            if(!isSpecificColor(getRgbString(getRow(i,rejectPage.identifierTableResults)), "52", "69", "78")){ // gray color
                areAllSelected = false;
                break;
            }
        }
        if(areAllSelected){
            logger.log(LogStatus.INFO,"All lines become selected");
        } else {
            logger.log(LogStatus.ERROR,"ERROR: All lines don't become selected");
        }
        //case some lines of the current page are selected
        //all lines becomes selected

        if(maxNumberRow == 10) {
            //Unselect few lines
            click(getRow(1, rejectPage.identifierTableResults));
            click(getRow(5, rejectPage.identifierTableResults));
            click(getRow(8, rejectPage.identifierTableResults));
            click(rejectPage.selectAllButton);
            areAllSelected = true;
            for (int i = 0; i < 10; i++) {
                if (!isSpecificColor(getRgbString(getRow(i, rejectPage.identifierTableResults)), "52", "69", "78")) { // gray color
                    areAllSelected = false;
                    break;
                }
            }
            if (areAllSelected) {
                logger.log(LogStatus.INFO, "All lines become selected");
            } else {
                logger.log(LogStatus.ERROR, "ERROR: All lines don't become selected");
            }
            //case all lines of the current page are already selected
            //nothing change, all lines stay selected
            click(rejectPage.selectAllButton);
            areAllSelected = true;
            for (int i = 0; i < 10; i++) {
                if (!isSpecificColor(getRgbString(getRow(i, rejectPage.identifierTableResults)), "52", "69", "78")) { // gray color
                    areAllSelected = false;
                    break;
                }
            }
            if (areAllSelected) {
                logger.log(LogStatus.INFO, "All lines become selected");
            } else {
                logger.log(LogStatus.ERROR, "ERROR: All lines don't become selected");
            }

            //unselect all
            //by default, button is disable
            reloadRejectFilterPageWithCatIdentifier(rejectPage, startPage);
            if (rejectPage.unselectAllButton.getAttribute("class").equals("ui button buttons-select-none disabled")) {
                logger.log(LogStatus.INFO, "Menu Unselect All is disabled");
            } else {
                logger.log(LogStatus.ERROR, "ERROR: Menu Unselect All is enabled");
            }
            //case no line of the current page are selected
            //button stay disable
            boolean areNotAllSelected = true;
            for (int i = 0; i < 10; i++) {
                if (!isNoColor(getRgbString(getRow(i, rejectPage.identifierTableResults)))) {
                    areNotAllSelected = false;
                    break;
                }
            }
            if (areNotAllSelected && rejectPage.unselectAllButton.getAttribute("class").equals("ui button buttons-select-none disabled")) {
                logger.log(LogStatus.INFO, "Unselect button stays disable");
            } else {
                logger.log(LogStatus.ERROR, "ERROR: Unselect don't button stays disable");
            }
            //case one line of the current page is selected
            //button becomes enable
            click(getRow(5, rejectPage.identifierTableResults));
            if (areNotAllSelected && rejectPage.unselectAllButton.getAttribute("class").equals("ui button buttons-select-none")) {
                logger.log(LogStatus.INFO, "Unselect button becomes enable");
            } else {
                logger.log(LogStatus.ERROR, "ERROR: Unselect stays disable");
            }
            //user can click on button
            click(rejectPage.unselectAllButton);
            //selected line becomes unselected
            if (isNoColor(getRgbString(getRow(5, rejectPage.identifierTableResults))) && rejectPage.unselectAllButton.getAttribute("class").equals("ui button buttons-select-none disabled")) {
                logger.log(LogStatus.INFO, "Selected line becomes unselected");
            } else {
                logger.log(LogStatus.ERROR, "ERROR: Selected line doesn't become unselected");
            }
            //case some lines of the current page  are selected
            click(getRow(1, rejectPage.identifierTableResults));
            click(getRow(5, rejectPage.identifierTableResults));
            click(getRow(8, rejectPage.identifierTableResults));
            //button becomes enable
            if (rejectPage.unselectAllButton.getAttribute("class").equals("ui button buttons-select-none")) {
                logger.log(LogStatus.INFO, "Menu Unselect All is enable");
            } else {
                logger.log(LogStatus.ERROR, "ERROR: Menu Unselect All is disable");
            }
            //user can click on button
            click(rejectPage.unselectAllButton);
            //selected line becomes unselected
            if (isNoColor(getRgbString(getRow(1, rejectPage.identifierTableResults))) &&
                    isNoColor(getRgbString(getRow(5, rejectPage.identifierTableResults))) &&
                    isNoColor(getRgbString(getRow(8, rejectPage.identifierTableResults))) &&
                    rejectPage.unselectAllButton.getAttribute("class").equals("ui button buttons-select-none disabled")) {
                logger.log(LogStatus.INFO, "Selected lines become unselected");
            } else {
                logger.log(LogStatus.ERROR, "ERROR: Selected lines don't become unselected");
            }
            //case all lines of the current page are selected
            click(rejectPage.selectAllButton);
            //button becomes enable
            if (rejectPage.unselectAllButton.getAttribute("class").equals("ui button buttons-select-none")) {
                logger.log(LogStatus.INFO, "Menu Unselect All is enable");
            } else {
                logger.log(LogStatus.ERROR, "ERROR: Menu Unselect All is disable");
            }
            //user can click on button
            click(rejectPage.unselectAllButton);
            //selected line becomes unselected
            areNotAllSelected = true;
            for (int i = 0; i < 10; i++) {
                if (!isNoColor(getRgbString(getRow(i, rejectPage.identifierTableResults)))) {
                    areNotAllSelected = false;
                    break;
                }
            }
            if (areNotAllSelected && rejectPage.unselectAllButton.getAttribute("class").equals("ui button buttons-select-none disabled")) {
                logger.log(LogStatus.INFO, "Selected line becomes unselected");
            } else {
                logger.log(LogStatus.ERROR, "ERROR: Selected line stay selected");
            }
            //Edit
            //By default, button is enabled
            //user can click on the button
            //case no line is selected
            detectInvalidClickButton(rejectPage.editButton, rejectPage, "Please select from the list");//This test valid that by default the button is enable
            //case one line is selected
            reloadRejectFilterPageWithCatIdentifier(rejectPage, startPage); //reload just in case of error...
            click(getRow(1, rejectPage.identifierTableResults));
            detectValidClickEdditButton(rejectPage, registerPage);
            //case several lines are selected
            reloadRejectFilterPageWithCatIdentifier(rejectPage, startPage); //reload just in case of error...
            click(getRow(1, rejectPage.identifierTableResults));
            click(getRow(4, rejectPage.identifierTableResults));
            click(getRow(7, rejectPage.identifierTableResults));
            detectInvalidClickButton(rejectPage.editButton, rejectPage, "Select only one");


            //Delete
            if (registrator.contains("Veterinary")) {
                logger.log(LogStatus.INFO, "Reject not testable for vet currently");
            } else {
                //Register 13 cats before deletion test
                for (int i = 0; i < 13; i++) {
                    try {
                        if (i < 10) {
                            registerSpecificAndValidSentForApprovalBreederOrRefugeCat(registerPage, startPage, "99999999999900" + i);
                            logger.log(LogStatus.INFO, "Cat number: " + "99999999999900" + i + " is registered");
                        } else {
                            registerSpecificAndValidSentForApprovalBreederOrRefugeCat(registerPage, startPage, "9999999999990" + i);
                            logger.log(LogStatus.INFO, "Cat number: " + "9999999999990" + i + " is registered");
                        }

                    } catch (Exception e) {
                        if (i < 10) {
                            logger.log(LogStatus.ERROR, "Cat number: " + "99999999999900" + i + " is not registered");
                        } else {
                            logger.log(LogStatus.ERROR, "Cat number: " + "9999999999990" + i + " is not registered");
                        }
                    }
                }
                //Connect as vet to reject the forms previously sent
                try {
                    click(startPage.logoutButton);
                    OpenHomepage openHomepage = new OpenHomepage();
                    openHomepage.setDriver(driver);
                    openHomepage.setLogger(logger);
                    openHomepage.run();
                    Login loginpopup = new Login();
                    loginpopup.setUserRole("Veterinary 2");
                    loginpopup.setDriver(driver);
                    loginpopup.setLogger(logger);
                    loginpopup.run();
                    click(startPage.filterWaitingForApprovalVetActionButton);
                    click(waitingForApprovalPage.pageSelectionDropList);
                    click(waitingForApprovalPage.twentyFiveRowsByPageSelection);
                    setText(waitingForApprovalPage.searchField, "999999999999");
                    sleep(5000);//Wait for the end of the refresh
                    click(waitingForApprovalPage.selectAllButton);
                    click(waitingForApprovalPage.massRejectVetButton);
                    setText(waitingForApprovalPage.rejectTextField, tcFeatureListNumber);
                    setText(waitingForApprovalPage.rejectTextField, "Test filter reject");
                    click(waitingForApprovalPage.acceptRejectButton);
                    sleep(30000);//Wait for the end of the refresh
                    WaitForElement(startPage.logoutButton,60);
                    click(startPage.logoutButton);
                    openHomepage.run();
                    loginpopup.setUserRole(registrator);
                    loginpopup.setDriver(driver);
                    loginpopup.setLogger(logger);
                    loginpopup.run();
                } catch (Exception e) {
                    logger.log(LogStatus.ERROR, "ERROR: Mass reject failed " + e.getMessage());
                }

                reloadRejectFilterPageWithCatIdentifier(rejectPage, startPage);
                //by default, button is enable
                //case no line of the current page are selected
                //user can click on the button
                //an error message "please select from the list" is displayed
                detectInvalidClickButton(rejectPage.deleteButton, rejectPage, "Please select from the list");//This test valid that by default the button is enable

                //case one line of the current page is selected
                //user can click on the button
                reloadRejectFilterPageWithCatIdentifier(rejectPage, startPage);
                click(getRow(0, rejectPage.identifierTableResults));
                String beforeDeletionNumberOfForms = rejectPage.pageInfo.getAttribute("textContent").split(" ")[5];
                logger.log(LogStatus.INFO, "There are currently " + beforeDeletionNumberOfForms + " forms before deletion test");
                click(rejectPage.deleteButton);
                //user is prompted to confirm deletion
                //case Cancel
                click(rejectPage.refuseDeletionButton);
                String afterCancelDeletion = rejectPage.pageInfo.getAttribute("textContent").split(" ")[5];

                //nothing append, no deletion occurs
                if (beforeDeletionNumberOfForms.equals(afterCancelDeletion)) {
                    logger.log(LogStatus.INFO, "Nothing append, no deletion occurred");
                } else {
                    logger.log(LogStatus.ERROR, "ERROR: Something append, deletion occurred");
                }
                //case OK
                sleep(5000);//Because the dialog message can hide the filter button (visible so "wait for" return true)
                reloadRejectFilterPageWithCatIdentifier(rejectPage, startPage);
                click(getRow(0, rejectPage.identifierTableResults));
                String identifierDeletionFormBeforeDeletion = getCellString(0, 0, rejectPage.identifierTableResults);
                beforeDeletionNumberOfForms = rejectPage.pageInfo.getAttribute("textContent").split(" ")[5];
                click(rejectPage.deleteButton);
                click(rejectPage.acceptDeletionButton);
                sleep(duration);
                String identifierDeletionFormAfterDeletion = getCellString(0, 0, rejectPage.identifierTableResults);
                String afterDeletion = rejectPage.pageInfo.getAttribute("textContent").split(" ")[5];
                //selected line is deleted
                if (!identifierDeletionFormBeforeDeletion.equals(identifierDeletionFormAfterDeletion)) {
                    logger.log(LogStatus.INFO, "Selected line is deleted");
                } else {
                    logger.log(LogStatus.ERROR, "ERROR: Selected line is not deleted");
                }
                //page is refreshed
                //entries counter is updated -1
                int refreshCounter = 0;
                try {
                    int beforeInt = Integer.parseInt(beforeDeletionNumberOfForms);
                    int afterInt = Integer.parseInt(afterDeletion);
                    refreshCounter = afterInt - beforeInt;
                } catch (Exception e) {
                    logger.log(LogStatus.ERROR, "ERROR: Integer parsing failed");
                }
                if (refreshCounter == -1) {
                    logger.log(LogStatus.INFO, "Page is refreshed, entries counter is updated -1");
                } else {
                    logger.log(LogStatus.ERROR, "ERROR: Page is refreshed, entries counter is not updated -1");
                }
                //case some lines of the current page  are selected
                sleep(5000);//Because the dialog message can hide the filter button (visible so "wait for" return true)
                reloadRejectFilterPageWithCatIdentifier(rejectPage, startPage);
                click(getRow(0, rejectPage.identifierTableResults));
                click(getRow(1, rejectPage.identifierTableResults));
                //user can click on the button
                beforeDeletionNumberOfForms = rejectPage.pageInfo.getAttribute("textContent").split(" ")[5];
                logger.log(LogStatus.INFO, "There are currently " + beforeDeletionNumberOfForms + " forms before deletion test");
                click(rejectPage.deleteButton);
                //user is prompted to confirm deletion
                //case Cancel
                click(rejectPage.refuseDeletionButton);
                afterCancelDeletion = rejectPage.pageInfo.getAttribute("textContent").split(" ")[5];
                //nothing append, no deletion occurs
                if (beforeDeletionNumberOfForms.equals(afterCancelDeletion)) {
                    logger.log(LogStatus.INFO, "Nothing append, no deletions occurred");
                } else {
                    logger.log(LogStatus.ERROR, "ERROR: Something append, deletions occurred");
                }
                //case OK
                sleep(5000);//Because the dialog message can hide the filter button (visible so "wait for" return true)
                reloadRejectFilterPageWithCatIdentifier(rejectPage, startPage);
                click(getRow(0, rejectPage.identifierTableResults));
                click(getRow(1, rejectPage.identifierTableResults));
                identifierDeletionFormBeforeDeletion = getCellString(0, 0, rejectPage.identifierTableResults);
                beforeDeletionNumberOfForms = rejectPage.pageInfo.getAttribute("textContent").split(" ")[5];
                click(rejectPage.deleteButton);
                click(rejectPage.acceptDeletionButton);
                sleep(5000);
                identifierDeletionFormAfterDeletion = getCellString(0, 0, rejectPage.identifierTableResults);
                afterDeletion = rejectPage.pageInfo.getAttribute("textContent").split(" ")[5];
                //selected lines are deleted
                if (!identifierDeletionFormBeforeDeletion.equals(identifierDeletionFormAfterDeletion)) {
                    logger.log(LogStatus.INFO, "Selected lines are deleted");
                } else {
                    logger.log(LogStatus.ERROR, "ERROR: Selected lines are not deleted");
                }
                //page is refreshed
                //entries counter is updated minus number of lines selected
                refreshCounter = 0;
                try {
                    int beforeInt = Integer.parseInt(beforeDeletionNumberOfForms);
                    int afterInt = Integer.parseInt(afterDeletion);
                    refreshCounter = afterInt - beforeInt;
                } catch (Exception e) {
                    logger.log(LogStatus.ERROR, "ERROR: Integer parsing failed");
                }
                if (refreshCounter == -2) {
                    logger.log(LogStatus.INFO, "Page is refreshed, entries counter is updated minus number of lines selected");
                } else {
                    logger.log(LogStatus.ERROR, "ERROR: Page is refreshed, entries counter is not updated minus number of lines selected");
                }
                //case all lines of the current page  are selected
                sleep(5000);//Because the dialog message can hide the filter button (visible so "wait for" return true)
                reloadRejectFilterPageWithCatIdentifier(rejectPage, startPage);
                click(rejectPage.selectAllButton);
                //user can click on the button
                beforeDeletionNumberOfForms = rejectPage.pageInfo.getAttribute("textContent").split(" ")[5];
                logger.log(LogStatus.INFO, "There are currently " + beforeDeletionNumberOfForms + " forms before deletion test");
                click(rejectPage.deleteButton);
                //user is prompted to confirm deletion
                //case Cancel
                click(rejectPage.refuseDeletionButton);
                afterCancelDeletion = rejectPage.pageInfo.getAttribute("textContent").split(" ")[5];
                //nothing append, no deletion occurs
                if (beforeDeletionNumberOfForms.equals(afterCancelDeletion)) {
                    logger.log(LogStatus.INFO, "Nothing append, no deletions occurred");
                } else {
                    logger.log(LogStatus.ERROR, "ERROR: Something append, deletions occurred");
                }
                //case OK
                sleep(5000);//Because the dialog message can hide the filter button (visible so "wait for" return true)
                reloadRejectFilterPageWithCatIdentifier(rejectPage, startPage);
                click(rejectPage.selectAllButton);
                identifierDeletionFormBeforeDeletion = getCellString(0, 0, rejectPage.identifierTableResults);
                beforeDeletionNumberOfForms = rejectPage.pageInfo.getAttribute("textContent").split(" ")[5];
                click(rejectPage.deleteButton);
                click(rejectPage.acceptDeletionButton);
                sleep(5000);
                identifierDeletionFormAfterDeletion = getCellString(0, 0, rejectPage.identifierTableResults);
                afterDeletion = rejectPage.pageInfo.getAttribute("textContent").split(" ")[5];
                //all  lines of the current page are deleted
                if (!identifierDeletionFormBeforeDeletion.equals(identifierDeletionFormAfterDeletion)) {
                    logger.log(LogStatus.INFO, "Selected lines are deleted");
                } else {
                    logger.log(LogStatus.ERROR, "ERROR: Selected lines are not deleted " + identifierDeletionFormBeforeDeletion + identifierDeletionFormAfterDeletion);
                }
                //page is refreshed

                //all next lines will come on the first page
                //entries counter is updated minus 10
                refreshCounter = 0;
                try {
                    int beforeInt = Integer.parseInt(beforeDeletionNumberOfForms);
                    int afterInt = Integer.parseInt(afterDeletion);
                    refreshCounter = afterInt - beforeInt;
                } catch (Exception e) {
                    logger.log(LogStatus.ERROR, "ERROR: Integer parsing failed");
                }
                if (refreshCounter == -10) {
                    logger.log(LogStatus.INFO, "Page is refreshed, entries counter is updated -10");
                } else {
                    logger.log(LogStatus.ERROR, "ERROR: Page is refreshed, entries counter is not updated -10 ");
                }
                //entries counter is updated to 0
                //button unselect all becomes disable
                //no more lines are available
                String count = afterDeletion = rejectPage.pageInfo.getAttribute("textContent").split(" ")[5];
                if (count.equals("0")) {
                    logger.log(LogStatus.INFO, "No more lines are available");
                    if (rejectPage.unselectAllButton.getAttribute("class").equals("ui button buttons-select-none")) {
                        logger.log(LogStatus.INFO, "Menu Unselect All is enabled");
                    } else {
                        logger.log(LogStatus.ERROR, "ERROR: Menu Unselect All is disabled");
                    }
                } else {
                    logger.log(LogStatus.INFO, "There are still lines available");
                }
            }
            //create CSV
            //By default, button is enabled
            //user can click on the button
            //case no line is selected
            //Error message is displayed
            List<String> expectedIdentifiers = new ArrayList<String>();
            List<WebElement> listTrWebElement = rejectPage.identifierTableResults.findElements(By.tagName("tr"));
            if(listTrWebElement.size() > 0){
                reloadRejectFilterPageWithCatIdentifier(rejectPage, startPage); //reload just in case of error...
                detectInvalidClickButton(rejectPage.createCsvButton, rejectPage, "Please select from the list");//This test valid that by default the button is enable
                //case one line is selected
                //a CSV file is generated
                reloadRejectFilterPageWithCatIdentifier(rejectPage, startPage); //reload just in case of error...
                click(getRow(1, rejectPage.identifierTableResults));
                expectedIdentifiers.add(getCell(1, 0, rejectPage.identifierTableResults).getAttribute("textContent"));
                detectValidClickCreateCsvtButton(rejectPage, expectedIdentifiers);
            }

            //file contains the line selected
            //case several lines are selected
            //a CSV file is generated
            //file contains the lines selected
            reloadRejectFilterPageWithCatIdentifier(rejectPage, startPage); //reload just in case of error...
            listTrWebElement = rejectPage.identifierTableResults.findElements(By.tagName("tr"));
            if(listTrWebElement.size() > 0) {
                click(getRow(1, rejectPage.identifierTableResults));
                click(getRow(2, rejectPage.identifierTableResults));
                expectedIdentifiers = new ArrayList<String>();
                expectedIdentifiers.add(getCell(1, 0, rejectPage.identifierTableResults).getAttribute("textContent"));
                expectedIdentifiers.add(getCell(2, 0, rejectPage.identifierTableResults).getAttribute("textContent"));
                detectValidClickCreateCsvtButton(rejectPage, expectedIdentifiers);
            }
            //case selected line has multiple identifier
            //user must be able to edit CSV
            //both identifier must be set in column identifier
            reloadRejectFilterPageWithCatIdentifier(rejectPage, startPage); //reload just in case of error...
            setText(rejectPage.searchField, ",");
            sleep(5000);
            listTrWebElement = rejectPage.identifierTableResults.findElements(By.tagName("tr"));
            if(listTrWebElement.size() > 3) {
                click(getRow(1, rejectPage.identifierTableResults));
                click(getRow(2, rejectPage.identifierTableResults));
                expectedIdentifiers = new ArrayList<String>();
                expectedIdentifiers.add(getCell(1, 0, rejectPage.identifierTableResults).getAttribute("textContent"));
                expectedIdentifiers.add(getCell(2, 0, rejectPage.identifierTableResults).getAttribute("textContent"));
                detectValidClickCreateCsvtButton(rejectPage, expectedIdentifiers);
            }
        }

    }

    private void checkSelectAndUnSelectLine(WebRejectPage rejectPage, WebStartPage startPage){
        reloadRejectFilterPageWithCatIdentifier(rejectPage, startPage);
        //User can select lines
        //by clicking on a line
        //line becomes gray
        WebElement firstRow = getRow(0,rejectPage.identifierTableResults);
        click(firstRow);
        if(isSpecificColor(getRgbString(firstRow), "52", "69", "78")){
            logger.log(LogStatus.INFO,"First line is gray when selected (by clicking)");
        } else {
            logger.log(LogStatus.ERROR,"ERROR: First line is not gray when selected (by clicking)");
        }
        //Label row selected is incremented by 1
        if(rejectPage.pageInfo.getAttribute("textContent").contains(("1 row selected"))){
            logger.log(LogStatus.INFO,"Label row selected is incremented by 1");
        } else {
            logger.log(LogStatus.ERROR,"ERROR: Label row selected is not incremented by 1");
        }
        //Menu Unselect All is enabled
        if(rejectPage.unselectAllButton.getAttribute("class").equals("ui button buttons-select-none")){
            logger.log(LogStatus.INFO,"Menu Unselect All is enabled");
        } else {
            logger.log(LogStatus.ERROR,"ERROR: Menu Unselect All is disabled");
        }

        WebElement secondRow = getRow(1,rejectPage.identifierTableResults);
        click(secondRow);
        if(isSpecificColor(getRgbString(secondRow), "52", "69", "78")){
            logger.log(LogStatus.INFO,"Second line is gray when selected (by clicking)");
        } else {
            logger.log(LogStatus.ERROR,"ERROR: Second line is not gray when selected (by clicking)");
        }
        //Label row selected is incremented by 1
        if(rejectPage.pageInfo.getAttribute("textContent").contains(("2 rows selected"))){
            logger.log(LogStatus.INFO,"Label row selected is incremented by 1");
        } else {
            logger.log(LogStatus.ERROR,"ERROR: Label row selected is not incremented by 1");
        }

        //User can deselect lines
        //line must be selected and color gray
        //user click on the line
        //line become deselected
        //color line becomes white
        click(rejectPage.firstIdentifierTableResult);
        if(isNoColor(getRgbString(rejectPage.firstIdentifierTableResult))){
            logger.log(LogStatus.INFO,"First line is white when selected (by clicking)");
        } else {
            logger.log(LogStatus.ERROR,"ERROR: First line is not white when selected (by clicking)");
        }
        //Label row selected is decremented by 1
        if(rejectPage.pageInfo.getAttribute("textContent").contains(("1 row selected"))){
            logger.log(LogStatus.INFO,"Label row selected is decremented by 1");
        } else {
            logger.log(LogStatus.ERROR,"ERROR: Label row selected is not decremented by 1");
        }

        click(secondRow);
        if(isNoColor(getRgbString(secondRow))){
            logger.log(LogStatus.INFO,"Second line is white when selected (by clicking)");
        } else {
            logger.log(LogStatus.ERROR,"ERROR: Second line is not white when selected (by clicking)");
        }

        //Label row selected is decremented by 1
        if(!rejectPage.pageInfo.getAttribute("textContent").contains(("selected"))){
            logger.log(LogStatus.INFO,"Label row selected is decremented by 1");
        } else {
            logger.log(LogStatus.ERROR,"ERROR: Label row selected is not decremented by 1");
        }
        //case no more lines are selected
        //menu unselect all is disabled
        if(rejectPage.unselectAllButton.getAttribute("class").equals("ui button buttons-select-none disabled")){
            logger.log(LogStatus.INFO,"Menu Unselect All is disabled");
        } else {
            logger.log(LogStatus.ERROR,"ERROR: Menu Unselect All is enabled");
        }
    }

    private void checkSearchField(WebRejectPage rejectPage, WebStartPage startPage){
        reloadRejectFilterPageWithCatIdentifier(rejectPage, startPage);
        HashMap<String,String> expectedResult = new HashMap<String, String>();
        HashMap<String,String> searchWords = new HashMap<String, String>();
        if(registrator.equals("Refuge 3")){
            expectedResult.put("Identifier", "123456789502238");
            expectedResult.put("PassportNumber", "TC0030012018153130");
            expectedResult.put("DateOfBirth", "30/01/201");
            expectedResult.put("IdentificationDate", "30/01/201");
            expectedResult.put("Gender", "Male");
            expectedResult.put("CatBreed", "Abyssinian");
            expectedResult.put("Sterelized", "Yes");
            expectedResult.put("SterelizationDate", "30/01/201");
            expectedResult.put("Info", "F0024");
            expectedResult.put("Status", "Rejected");
            expectedResult.put("RejectReason", "test rejet");
            searchWords.put("Identifier", "502238");
            searchWords.put("PassportNumber", "30012018153130");
            searchWords.put("DateOfBirth", "30/01/201");
            searchWords.put("IdentificationDate", "30/01/201");
            searchWords.put("Gender", "Male");
            searchWords.put("CatBreed", "Abyssinian");
            searchWords.put("Sterelized", "Yes");
            searchWords.put("SterelizationDate", "30/01/201");
            searchWords.put("Info", "F0024");
            searchWords.put("Status", "Rejected");
            searchWords.put("RejectReason", "test rejet");

        } else if(registrator.equals("Breeder 4")){
            expectedResult.put("Identifier", "123456789501881");
            expectedResult.put("PassportNumber", "30012018133758");
            expectedResult.put("DateOfBirth", "30/01/2018");
            expectedResult.put("IdentificationDate", "30/01/2018");
            expectedResult.put("Gender", "Male");
            expectedResult.put("CatBreed", "Abyssinian");
            expectedResult.put("Sterelized", "Yes");
            expectedResult.put("SterelizationDate", "30/01/2018");
            expectedResult.put("Info", "F0024");
            expectedResult.put("Status", "Rejected");
            expectedResult.put("RejectReason", "Test reject");
            searchWords.put("Identifier", "501881");
            searchWords.put("PassportNumber", "TC0030012018133758");
            searchWords.put("DateOfBirth", "30/01/2018");
            searchWords.put("IdentificationDate", "30/01/2018");
            searchWords.put("Gender", "Male");
            searchWords.put("CatBreed", "Abyssinian");
            searchWords.put("Sterelized", "Yes");
            searchWords.put("SterelizationDate", "30/01/2018");
            searchWords.put("Info", "F0024");
            searchWords.put("Status", "Rejected");
            searchWords.put("RejectReason", "Test reject");
        } else {
            expectedResult.put("Identifier", "123456789502374");
            expectedResult.put("PassportNumber", "TC0030012018155620");
            expectedResult.put("DateOfBirth", "30/01/2018");
            expectedResult.put("IdentificationDate", "30/01/2018");
            expectedResult.put("Gender", "Male");
            expectedResult.put("CatBreed", "Abyssinian");
            expectedResult.put("Sterelized", "Yes");
            expectedResult.put("SterelizationDate", "30/01/2018");
            expectedResult.put("Info", "HK852456");
            expectedResult.put("Status", "Rejected");
            expectedResult.put("RejectReason", "WR000004: identifiers contains already used identification 123456789502383");
            searchWords.put("Identifier", "502374");
            searchWords.put("PassportNumber", "30012018155620");
            searchWords.put("DateOfBirth", "30/01/2018");
            searchWords.put("IdentificationDate", "30/01/2018");
            searchWords.put("Gender", "Male");
            searchWords.put("CatBreed", "Abyssinian");
            searchWords.put("Sterelized", "Yes");
            searchWords.put("SterelizationDate", "30/01/2018");
            searchWords.put("Info", "HK852456");
            searchWords.put("Status", "Rejected");
            searchWords.put("RejectReason", "WR000004:");
        }

        //User can perform searches
        //by enconding string in field Search
        //results are displayed after each character encoded
        //search will fetch on column
        //identifier  -> "500319" to search for "123456789500319"
        setText(rejectPage.searchField,searchWords.get("Identifier"));
        sleep(duration); // Wait 1s to let the result to be refreshed
        getScreenShot();
        String indeitifierResult = lookForExpectedResult(expectedResult.get("Identifier"), rejectPage.identifierTableResults, 0);
        if(indeitifierResult.contains(expectedResult.get("Identifier"))){
            logger.log(LogStatus.INFO,"Identifier search for " + searchWords.get("Identifier") + " works and the result is " + indeitifierResult);
        } else {
            logger.log(LogStatus.ERROR,"ERROR: Identifier search for " + searchWords.get("Identifier") + " doesn't work and the result is " + indeitifierResult);
        }
        //passport number
        setText(rejectPage.searchField,searchWords.get("PassportNumber"));
        sleep(duration); // Wait 1s to let the result to be refreshed
        getScreenShot();
        String passportResult = lookForExpectedResult(expectedResult.get("PassportNumber"), rejectPage.identifierTableResults, 1);
        if(passportResult.contains(expectedResult.get("PassportNumber"))){
            logger.log(LogStatus.INFO,"Passport search for " + searchWords.get("PassportNumber") + " works and the result is " + passportResult);
        } else {
            logger.log(LogStatus.ERROR,"ERROR: Passport search for " + searchWords.get("PassportNumber") + " doesn't work and the result is " + passportResult);
        }
        //date of birth
        setText(rejectPage.searchField,searchWords.get("DateOfBirth"));
        sleep(duration); // Wait 1s to let the result to be refreshed
        getScreenShot();
        String birthDateResult = lookForExpectedResult(expectedResult.get("DateOfBirth"), rejectPage.identifierTableResults, 2);
        if(birthDateResult.contains(searchWords.get("DateOfBirth"))){
            logger.log(LogStatus.INFO,"Date of birth search for " + searchWords.get("DateOfBirth") + " works and the result is " + birthDateResult);
        } else {
            logger.log(LogStatus.ERROR,"ERROR: Date of birth search for " + searchWords.get("DateOfBirth") + " doesn't work and the result is " + birthDateResult);
        }
        //identification date
        setText(rejectPage.searchField,searchWords.get("IdentificationDate"));
        sleep(duration); // Wait 1s to let the result to be refreshed
        getScreenShot();
        String identificationDateResult = lookForExpectedResult(expectedResult.get("IdentificationDate"), rejectPage.identifierTableResults, 3);
        if(identificationDateResult.contains(expectedResult.get("IdentificationDate"))){
            logger.log(LogStatus.INFO,"Date of identification search for " + searchWords.get("IdentificationDate") + " works and the result is " + identificationDateResult);
        } else {
            logger.log(LogStatus.ERROR,"ERROR: Date of identification search for " + searchWords.get("IdentificationDate") + " doesn't work and the result is " + identificationDateResult);
        }
        //Gender
        setText(rejectPage.searchField,searchWords.get("Gender"));
        sleep(duration); // Wait 1s to let the result to be refreshed
        getScreenShot();
        String genderResult = lookForExpectedResult(expectedResult.get("Gender"), rejectPage.identifierTableResults, 4);
        if(genderResult.contains(expectedResult.get("Gender"))){
            logger.log(LogStatus.INFO,"Gender search for " + searchWords.get("Gender") + " works and the result is " + genderResult);
        } else {
            logger.log(LogStatus.ERROR,"ERROR: Gender search for " + searchWords.get("Gender") + " doesn't work and the result is " + genderResult);
        }
        //Cat Breed
        setText(rejectPage.searchField,searchWords.get("CatBreed"));
        sleep(duration); // Wait 1s to let the result to be refreshed
        getScreenShot();
        String breedResult = lookForExpectedResult(expectedResult.get("CatBreed"), rejectPage.identifierTableResults, 5);
        if(breedResult.contains(expectedResult.get("CatBreed"))){
            logger.log(LogStatus.INFO,"Breed search for " + searchWords.get("CatBreed") + " works and the result is " + breedResult);
        } else {
            logger.log(LogStatus.ERROR,"ERROR: Breed search for " + searchWords.get("CatBreed") + " doesn't work and the result is " + breedResult);
        }
        //Sterilized
        setText(rejectPage.searchField,searchWords.get("Sterelized"));
        sleep(duration); // Wait 1s to let the result to be refreshed
        getScreenShot();
        String sterilizedResult = lookForExpectedResult(expectedResult.get("Sterelized"), rejectPage.identifierTableResults, 6);
        if(sterilizedResult.contains(expectedResult.get("Sterelized"))){
            logger.log(LogStatus.INFO,"Sterilzed search for " + searchWords.get("Sterelized") + " works and the result is " + sterilizedResult);
        } else {
            logger.log(LogStatus.ERROR,"ERROR: Sterilzed search for " + searchWords.get("Sterelized") + " doesn't work and the result is " + sterilizedResult);
        }
        //sterilized date
        setText(rejectPage.searchField,searchWords.get("SterelizationDate"));
        sleep(duration); // Wait 1s to let the result to be refreshed
        getScreenShot();
        String sterilizedDateResult = lookForExpectedResult(expectedResult.get("SterelizationDate"), rejectPage.identifierTableResults, 7);
        if(sterilizedDateResult.contains(expectedResult.get("SterelizationDate"))){
            logger.log(LogStatus.INFO,"Sterilzed date search for " + searchWords.get("SterelizationDate") + "works and the result is " + sterilizedDateResult);
        } else {
            logger.log(LogStatus.ERROR,"ERROR: Sterilzed date search for " + searchWords.get("SterelizationDate") + " doesn't work and the result is " + sterilizedDateResult);
        }
        //owner information
        setText(rejectPage.searchField,searchWords.get("Info"));
        sleep(duration); // Wait 1s to let the result to be refreshed
        getScreenShot();
        String ownerInfoResult = lookForExpectedResult(expectedResult.get("Info"), rejectPage.identifierTableResults, 8);
        if(ownerInfoResult.contains(expectedResult.get("Info"))){
            logger.log(LogStatus.INFO,"Owner Information search for " + searchWords.get("Info") + " works and the result is " + ownerInfoResult);
        } else {
            logger.log(LogStatus.ERROR,"ERROR: Owner Information search for " + searchWords.get("Info") + " doesn't work and the result is " + ownerInfoResult);
        }
        //Status
        setText(rejectPage.searchField,searchWords.get("Status"));
        sleep(duration); // Wait 1s to let the result to be refreshed
        getScreenShot();
        String statusResult = lookForExpectedResult(expectedResult.get("Status"), rejectPage.identifierTableResults, 9);
        if(statusResult.contains(expectedResult.get("Status"))){
            logger.log(LogStatus.INFO,"Status  search for " + searchWords.get("Status") + " works and the result is " + statusResult);
        } else {
            logger.log(LogStatus.ERROR,"ERROR: Status search for " + searchWords.get("Status") + " doesn't work and the result is " + statusResult);
        }
        //Reject info
        setText(rejectPage.searchField,searchWords.get("RejectReason"));
        sleep(duration); // Wait 1s to let the result to be refreshed
        getScreenShot();
        String rejectInfoResult = lookForExpectedResult(expectedResult.get("RejectReason"), rejectPage.identifierTableResults, 10);
        if(rejectInfoResult.contains(expectedResult.get("RejectReason"))){
            logger.log(LogStatus.INFO,"Reject information  search for " + searchWords.get("RejectReason") + " works and the result is " + rejectInfoResult);
        } else {
            logger.log(LogStatus.ERROR,"ERROR: Reject information search for " + searchWords.get("RejectReason") + " doesn't work and the result is " + rejectInfoResult);
        }
    }

    private void rejectFilterCheck(WebRejectPage rejectPage, WebStartPage startPage, SqlOracleMiddleware metaformMiddleWare, WebRegisterPage registerPage, WebWaitingForApprovalPage waitingForApprovalPage) throws Exception {
        click(startPage.homeButton);
        //by clicking on the menu filters - Reject
        //The all cats page is displayed
        //all cats where vet are linked and Reject by system are displayed
        //total number of cats is the sum of
        String numberCatRejectDisplayed = "";
        String numberCatRejectSql = "";
        String refnum = "";
        if(registrator.contains("Veterinary")){
            WaitForElement(startPage.filterRejectedVetActionButton,10);
            numberCatRejectDisplayed = startPage.filterRejectedVetActionButton.getAttribute("textContent").replaceAll("\\s+", "");
            refnum = "F0024";
        } else {
            if(registrator.equals("Breeder 4")){ //Breeder
                refnum = "HK159357";
            } else { //Refuge
                refnum = "HK852456";
            }
            WaitForElement(startPage.filterRejectsBreederAndRefugeActionButton,5);
            numberCatRejectDisplayed = startPage.filterRejectsBreederAndRefugeActionButton.getAttribute("textContent").replaceAll("\\s+", "");

        }

        ArrayList<HashMap> rs = new ArrayList<HashMap>();
        if(registrator.contains("Veterinary")){
            rs = metaformMiddleWare.executeSqlQuery("SELECT COUNT(*) FROM METAFORM_ABIEC_ANIMAL WHERE (MF_STATUS = 16 OR  MF_STATUS = 32 OR  MF_STATUS = 32768 OR  MF_STATUS = 2048) AND VET_REFNUM = '" + refnum + "' AND REJECT_COMMENT LIKE 'WR%'");
        } else {
            rs = metaformMiddleWare.executeSqlQuery("SELECT COUNT(*) FROM METAFORM_ABIEC_ANIMAL WHERE (MF_STATUS = 16 OR  MF_STATUS = 32 OR  MF_STATUS = 32768 OR  MF_STATUS = 2048) AND OWNER_REFNUM = '" + refnum + "' AND STARTING_REFNUM = '"  + refnum + "'");
        }
        if (rs.size() == 0) {
            logger.log(LogStatus.ERROR, "Something is wrong with SQL statement or DB connection");
        } else {
            numberCatRejectSql = (String)rs.get(0).get("COUNT(*)");
            MAX_REJECT = Integer.parseInt(numberCatRejectSql);
            if(MAX_REJECT > 50){
                MAX_REJECT = 50;
            }
            if(numberCatRejectSql != null && numberCatRejectDisplayed != null && numberCatRejectSql.equals(numberCatRejectDisplayed)){
                logger.log(LogStatus.INFO,"All cats where vet is linked and rejected by system are displayed");
            } else {
                logger.log(LogStatus.ERROR,"ERROR: All cats where vet is not linked or rejected by system are not displayed");
            }
        }
        //Page is showing 10 cats per page by default
        if(registrator.contains("Veterinary")){
            click(startPage.filterRejectedVetActionButton);
        } else {
            click(startPage.filterRejectsBreederAndRefugeActionButton);
        }
        List<WebElement> listResults = rejectPage.identifierTableResults.findElements(By.tagName("tr"));
        //Wait a little bit the page is completely loaded to take a screenshot
        WaitForElement(rejectPage.firstIdentifierTableResult,5);
        if(listResults.size() == 10){
            logger.log(LogStatus.INFO,"Page is showing 10 cats per page by default");
            if(isElementPresent(rejectPage.nextButton)){
                //case of more than 10 cats
                //next button is displayed
                logger.log(LogStatus.INFO,"Next button is displayed");
                //case there are more than one page available
                //case there are more than 10 cats
                //pages number are displayed
                List<WebElement> pageNumberList = rejectPage.paginationMenu.findElements(By.tagName("a"));
                if(pageNumberList.size() > 5){
                    //is it possible to click on the specific page
                    //selected page is displayed
                    //other available pages are still available for click
                    //page 1 display results from 1 to 10
                    //page 2 display result from 11 to 20
                    //page 3 display result from 21 to 30

                    logger.log(LogStatus.INFO,"Pages number are displayed");
                    if(rejectPage.pageInfo.getAttribute("textContent").contains("1 to 10")){
                        logger.log(LogStatus.INFO,"Page 1 display results from 1 to 10");
                    } else {
                        logger.log(LogStatus.ERROR,"ERROR: Page 1 doesn't display results from 1 to 10: " + rejectPage.pageInfo.getAttribute("textContent"));
                    }
                    click(pageNumberList.get(1)); //Click page 2
                    WaitForElement(rejectPage.firstIdentifierTableResult,5);
                    logger.log(LogStatus.INFO,"Click page 2");
                    if(rejectPage.pageInfo.getAttribute("textContent").contains("11 to 20")){
                        logger.log(LogStatus.INFO,"Page 2 display results from 11 to 20");
                    } else {
                        logger.log(LogStatus.ERROR,"ERROR: Page 2 doesn't display results from 11 to 20: " + rejectPage.pageInfo.getAttribute("textContent"));
                    }
                    pageNumberList = rejectPage.paginationMenu.findElements(By.tagName("a"));
                    //Indexes change between multiple clicks
                    click(pageNumberList.get(3)); //Click page 3
                    WaitForElement(rejectPage.firstIdentifierTableResult,5);
                    logger.log(LogStatus.INFO,"Click page 3");
                    if(rejectPage.pageInfo.getAttribute("textContent").contains("21 to")){
                        logger.log(LogStatus.INFO,"Page 3 display results from 21 to");
                    } else {
                        logger.log(LogStatus.ERROR,"ERROR: Page 3 doesn't display results from 21: " + rejectPage.pageInfo.getAttribute("textContent"));
                    }
                    pageNumberList = rejectPage.paginationMenu.findElements(By.tagName("a"));
                    click(pageNumberList.get(1)); //Click page 1
                    WaitForElement(rejectPage.firstIdentifierTableResult,5);
                    logger.log(LogStatus.INFO,"Click page 1");
                } else {
                    logger.log(LogStatus.INFO,"There are not enough forms in the filter to test multiple pages");
                }

            } else {
                logger.log(LogStatus.INFO,"Next button is not displayed");
            }
            if(rejectPage.numberResultByPageSelectionDroplist.getAttribute("textContent").contains("10")
                    && rejectPage.numberResultByPageSelectionDroplist.getAttribute("textContent").contains("25")
                    && rejectPage.numberResultByPageSelectionDroplist.getAttribute("textContent").contains("50")
                    && rejectPage.numberResultByPageSelectionDroplist.getAttribute("textContent").contains("100")){

                logger.log(LogStatus.INFO,"Number of result by page can be modify between {10, 25, 50, 100} rows");
            } else {
                logger.log(LogStatus.ERROR,"ERROR: Number of result by page cannot be modify between {10, 25, 50, 100} rows");
            }
            if(isElementPresent(rejectPage.previousButton)) {
                //case of more than 10 cats
                //previous button is displayed
                logger.log(LogStatus.INFO, "Previous button is displayed");
            } else {
                logger.log(LogStatus.ERROR, "ERROR: Previous button is not displayed");
            }


        } else {
            logger.log(LogStatus.INFO,"There are not enough forms in the filter to test more than 10 pages");
        }
        ascendingSortCheck(rejectPage, startPage);
        descendingSortCheck(rejectPage, startPage);
        checkSearchField(rejectPage,startPage);
        checkSelectAndUnSelectLine(rejectPage,startPage);
        checkButtonMenu(rejectPage,startPage,registerPage, waitingForApprovalPage);
    }
}
