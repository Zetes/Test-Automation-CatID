package ElementaryScripts.Web;

import Csv.CsvHandler;
import DataBaseFramework.SqlOracleMiddleware;
import Screens.*;
import com.relevantcodes.extentreports.LogStatus;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.io.File;
import java.util.*;

public class FilterWaitingForApprovalPage extends FilterPage {
    private int duration = 5000;
    public void run() throws Exception {
        try {
            WebEazyIdPage eazyIdPage = new WebEazyIdPage(driver);
            WebStartPage startPage = new WebStartPage(driver);
            WebWaitingForApprovalPage waitingForApprovalPage = new WebWaitingForApprovalPage(driver);
            WebRegisterPage registerPage = new WebRegisterPage(driver);
            WebModifyPage modifyPage = new WebModifyPage(driver);

            SqlOracleMiddleware metaformMiddleWare = new SqlOracleMiddleware(logger, "MF_CATS", "MF_CATS", "jdbc:oracle:thin:@192.168.17.6:1521:PREPROD");

            //Accessibility check
            checkWaitingForApprovalFilterAccessibility( waitingForApprovalPage, startPage);

            //Waiting for approval filter check
            waitingForApprovalFilterCheck(waitingForApprovalPage, startPage, eazyIdPage, metaformMiddleWare, registerPage, modifyPage);
            //logout for next page
            click(startPage.logoutButton);
        }catch (Exception e) {
            logger.logScreenshot(LogStatus.ERROR, "ERROR stopping the test", getScreenShot());
            throw e;
        }
    }

    private void ascendingSortCheck(WebWaitingForApprovalPage waitingForApprovalPage, WebStartPage startPage) throws Exception {
        reloadWaitingForApprovalFilterPageWithCatIdentifier(waitingForApprovalPage, startPage);
        List<WebElement> listHeaderButton = waitingForApprovalPage.headerRow.findElements(By.tagName("th"));
        //Check all columns
        List<WebElement> pageNumberList;
        List<String> buildColumnStrings;
        List<WebElement> listTrWebElement;
        List<WebElement> listTdWebElement;
        String content;
        int tdIndex = 0;
        //Select 50 rows per page
        click(waitingForApprovalPage.pageSelectionDropList);
        click(waitingForApprovalPage.fiftyRowsByPageSelection);
        sleep(duration);
        String headerName = "";
        for (WebElement header : listHeaderButton){
            buildColumnStrings = new ArrayList<String>();
            headerName = header.getAttribute("textContent").replaceAll("\\s+", "");
            scrollOnElement(waitingForApprovalPage.searchField);
            //Sort the column as ascending
            click(header); //Click for ascendant sort
            sleep(duration);//need time to load 50 rows
            listTrWebElement = waitingForApprovalPage.identifierTableResults.findElements(By.tagName("tr"));
            //Check the data is correctly sort (10 results max)
            for(int trIndex = 0; trIndex < listTrWebElement.size(); trIndex++){
                listTdWebElement = listTrWebElement.get(trIndex).findElements(By.tagName("td"));
                content = listTdWebElement.get(tdIndex).getAttribute("textContent");

                if(registrator.contains("Veterinary") && headerName.equals("Ownerinformation")){
                    String splittedString [] = content.split(" ");
                    if(splittedString.length > 1){
                        if(splittedString[0].startsWith("HK") || splittedString[0].startsWith("BR") || splittedString[0].startsWith("RE")){
                            content = splittedString[1];//set the content with only the first name
                        } else {
                            content = splittedString[0];//set the content with only the first name
                        }
                    }
                }

                if(headerName.equals("Identifier") && content.contains(",")){
                    content = content.replaceAll("\\s+", "");
                    String identifiers[] = content.split(",");
                    if(identifiers.length == 2){
//                        if(identifiers[0].compareTo(identifiers[1]) > 0){
//                            content = identifiers[1];
//                        } else {
//                            content = identifiers[0];
//                        }
                        content = identifiers[0];
                    }
                }
                buildColumnStrings.add(content);
            }
            boolean isDate = false;
            if(headerName.contains("Date") ||headerName.contains("date")){
                isDate = true;
            }
            if(isAscendingListOfString(buildColumnStrings, isDate)){
                logger.log(LogStatus.INFO,"Ascendant Column " + headerName + " is correctly sorted");
            } else {
                logger.log(LogStatus.ERROR,"ERROR: Ascendant Column " + headerName + " is not correctly sorted");
            }
//            System.out.println("############################################");
            //Increase the index to check tbody/td[index]
            tdIndex++;
        }
    }

    private void descendingSortCheck(WebWaitingForApprovalPage waitingForApprovalPage, WebStartPage startPage) throws Exception {
        reloadWaitingForApprovalFilterPageWithCatIdentifier(waitingForApprovalPage, startPage);
        List<WebElement> listHeaderButton = waitingForApprovalPage.headerRow.findElements(By.tagName("th"));
        //Check all columns
        List<WebElement> pageNumberList;
        List<String> buildColumnStrings;
        List<WebElement> listTrWebElement;
        List<WebElement> listTdWebElement;
        String content;
        int tdIndex = 0;
        //Select 50 rows per page
        click(waitingForApprovalPage.pageSelectionDropList);
        click(waitingForApprovalPage.fiftyRowsByPageSelection);

        String headerName = "";
        for (WebElement header : listHeaderButton){
            buildColumnStrings = new ArrayList<String>();
            headerName = header.getAttribute("textContent").replaceAll("\\s+", "");
            scrollOnElement(waitingForApprovalPage.searchField);
            //Sort the column as ascending
            click(header);
            click(header); // double click to descendant sort
            sleep(duration);
            listTrWebElement = waitingForApprovalPage.identifierTableResults.findElements(By.tagName("tr"));
            //Check the data is correctly sort (10 results max)
            for(int trIndex = 0; trIndex < listTrWebElement.size(); trIndex++){
                listTdWebElement = listTrWebElement.get(trIndex).findElements(By.tagName("td"));
//                System.out.println(listTdWebElement.get(tdIndex).getAttribute("textContent"));
                content = listTdWebElement.get(tdIndex).getAttribute("textContent");
                if(registrator.contains("Veterinary") && headerName.equals("Ownerinformation")){
                    String splittedString [] = content.split(" ");
                    if(splittedString.length > 1){
                        if(splittedString[0].startsWith("HK") || splittedString[0].startsWith("BR") || splittedString[0].startsWith("RE")){
                            content = splittedString[1];//set the content with only the first name
                        } else {
                            content = splittedString[0];//set the content with only the first name
                        }
                    }
                }
                if(headerName.equals("Identifier") && content.contains(",")){
                    content = content.replaceAll("\\s+", "");
                    String identifiers[] = content.split(",");
                    if(identifiers.length == 2){
                            content = identifiers[0];
                    }
                }
                buildColumnStrings.add(content);
            }
            boolean isDate = false;
            if(headerName.contains("Date") ||headerName.contains("date")){
                isDate = true;
            }
            if(isDescendingListOfString(buildColumnStrings, isDate)){
                logger.log(LogStatus.INFO,"Descendant Column " + headerName + " is correctly sorted");
            } else {
                logger.log(LogStatus.ERROR,"ERROR: Descendant Column " + headerName + " is not correctly sorted");
            }
//            System.out.println("############################################");
            //Increase the index to check tbody/td[index]
            tdIndex++;
        }
    }

    private void reloadWaitingForApprovalFilterPageWithCatIdentifier(WebWaitingForApprovalPage waitingForApprovalPage, WebStartPage startPage){
        click(startPage.filterButton);
        if(registrator.contains("Veterinary")) {
            click(startPage.filterWaitingForApprovalVetTabButton);
        } else {
            click(startPage.filterWaitingForApprovalBreederAndRefugeTabButton);
        }
        WaitForElement(waitingForApprovalPage.searchField,5);
    }

    private void checkWaitingForApprovalFilterAccessibility(WebWaitingForApprovalPage approvedPage, WebStartPage startPage){
        click(startPage.filterButton);
        if(registrator.contains("Veterinary")) {
            click(startPage.filterWaitingForApprovalVetActionButton);
        } else {
            click(startPage.filterWaitingForApprovalBreederAndRefugeActionButton);
        }
        try{
            WaitForElement(approvedPage.searchField,5);
            logger.log(LogStatus.INFO,"Action button for filter Waiting for approval is reachable");
        } catch (Exception e){
            logger.log(LogStatus.ERROR,"ERROR: Action button for filter Waiting for approval is not reachable");
        }
        click(startPage.homeButton);
        click(startPage.filterButton);
        if(registrator.contains("Veterinary")) {
            click(startPage.filterWaitingForApprovalVetTabButton);
        } else {
            click(startPage.filterWaitingForApprovalBreederAndRefugeTabButton);
        }
        try{
            WaitForElement(approvedPage.searchField,5);
            logger.log(LogStatus.INFO,"Tab button for filter Waiting for approval is reachable");
        } catch (Exception e){
            logger.log(LogStatus.ERROR,"ERROR: Tab button for filter Waiting for approval is not reachable");
        }
    }

    private boolean detectValidClickReviewVetButton(WebWaitingForApprovalPage waitingForApprovalPage, WebRegisterPage registerPage){
        boolean isValid;
        click(waitingForApprovalPage.reviewButton);
        try{
            WaitForElement(registerPage.transfertOfResponsibility,10);
            isValid = true;
            logger.log(LogStatus.INFO, "Valid button click");
        } catch (Exception e){
            logger.log(LogStatus.ERROR, "ERROR: Invalid button click");
            sleep(5000);
            isValid = false;
        }
        return isValid;
    }

    private boolean detectValidClickReviewBreederOrRefugeButton(WebWaitingForApprovalPage waitingForApprovalPage, WebModifyPage modifyPage){
        boolean isValid;
        click(waitingForApprovalPage.reviewButton);
        try{
            WaitForElement(modifyPage.catName,10);
            isValid = true;
            logger.log(LogStatus.INFO, "Valid button click");
        } catch (Exception e){
            logger.log(LogStatus.ERROR, "ERROR: Invalid button click");
            sleep(5000);
            isValid = false;
        }
        return isValid;
    }

    private boolean detectValidClickCreateCsvtButton(WebWaitingForApprovalPage waitingForApprovalPage, List<String> expectedIdentifiers){
        boolean isValid;
        if(registrator.contains("Veterinary")){
            click(waitingForApprovalPage.createCsvVetButton);
        } else {
            click(waitingForApprovalPage.createCsvBreederOrRefugeButton);
        }

        String path = System.getProperty("user.dir")+"/src/test/java/Results";
        File latestModifiedFile = getLatestFilefromDir(path);
        if(latestModifiedFile != null && latestModifiedFile.getName().contains("Data export")){
            isValid = true;
            logger.log(LogStatus.INFO, "Valid create CSV button click for " + latestModifiedFile.getName());
            CsvHandler csvHandler = new CsvHandler(logger);
            csvHandler.setCsvPath(latestModifiedFile.getAbsolutePath());
            List<String[]> entries = csvHandler.readAll();
            if((entries.size() - 1) == expectedIdentifiers.size()){ // (entries.size() - 1) because there is the title row
                for(int i = 0; i < expectedIdentifiers.size(); i++){
                    String[] entry = entries.get(i + 1);
                    if(!entry[0].equals(expectedIdentifiers.get(i))){//Compare the first column of the csv row and the expected identifier
                        isValid = false;
                        logger.log(LogStatus.ERROR, "ERROR: CSV file doesn't match with selected identifiers");
                        break;
                    }
                    logger.log(LogStatus.INFO, "CSV entry: " + Arrays.toString(entry));
                }
                logger.log(LogStatus.INFO, "CSV file matches with selected identifiers");
            }
            //Delete CSV file because useless now
            try {
                if(latestModifiedFile.delete()){
                    logger.log(LogStatus.INFO, "CSV file is deleted");
                } else {
                    logger.log(LogStatus.ERROR, "CSV file is not deleted, please clean the file manually");
                }
            } catch (Exception e) {
                logger.log(LogStatus.ERROR, "ERROR: CSV file cannot be deleted");
            }
        } else {
            logger.log(LogStatus.ERROR, "ERROR: Invalid create CSV  button click");
            sleep(5000);
            isValid = false;
        }
        return isValid;
    }

    private void checkButtonMenu(WebWaitingForApprovalPage waitingForApprovalPage, WebStartPage startPage, WebRegisterPage registerPage, WebEazyIdPage eazyIdPage, WebModifyPage modifyPage){
        reloadWaitingForApprovalFilterPageWithCatIdentifier(waitingForApprovalPage, startPage);

        //buttons menu are available
        //select all
        //user can click on the button
        //case no lines of the current page are selected
        //all lines becomes selected
        click(waitingForApprovalPage.selectAllButton);
        int maxNumberRow = waitingForApprovalPage.identifierTableResults.findElements(By.tagName("tr")).size();
        boolean areAllSelected = true;
        for(int i = 0; i < maxNumberRow; i++){
            if(!isSpecificColor(getRgbString(getRow(i,waitingForApprovalPage.identifierTableResults)), "52", "69", "78")){ // gray color
                areAllSelected = false;
                break;
            }
        }
        if(areAllSelected){
            logger.log(LogStatus.INFO,"All lines become selected");
        } else {
            logger.log(LogStatus.ERROR,"ERROR: All lines don't become selected");
        }
        //case some lines of the current page are selected
        //all lines becomes selected
        if(maxNumberRow == 10) {
            //Unselect few lines
            click(getRow(1, waitingForApprovalPage.identifierTableResults));
            click(getRow(5, waitingForApprovalPage.identifierTableResults));
            click(getRow(8, waitingForApprovalPage.identifierTableResults));
            click(waitingForApprovalPage.selectAllButton);
            areAllSelected = true;
            for (int i = 0; i < 10; i++) {
                if (!isSpecificColor(getRgbString(getRow(i, waitingForApprovalPage.identifierTableResults)), "52", "69", "78")) { // gray color
                    areAllSelected = false;
                    break;
                }
            }
            if (areAllSelected) {
                logger.log(LogStatus.INFO, "All lines become selected");
            } else {
                logger.log(LogStatus.ERROR, "ERROR: All lines don't become selected");
            }
            //case all lines of the current page are already selected
            //nothing change, all lines stay selected
            click(waitingForApprovalPage.selectAllButton);
            areAllSelected = true;
            for (int i = 0; i < 10; i++) {
                if (!isSpecificColor(getRgbString(getRow(i, waitingForApprovalPage.identifierTableResults)), "52", "69", "78")) { // gray color
                    areAllSelected = false;
                    break;
                }
            }
            if (areAllSelected) {
                logger.log(LogStatus.INFO, "All lines become selected");
            } else {
                logger.log(LogStatus.ERROR, "ERROR: All lines don't become selected");
            }

            //unselect all
            //by default, button is disable
            reloadWaitingForApprovalFilterPageWithCatIdentifier(waitingForApprovalPage, startPage);
            if (waitingForApprovalPage.unselectAllButton.getAttribute("class").equals("ui button buttons-select-none disabled")) {
                logger.log(LogStatus.INFO, "Menu Unselect All is disabled");
            } else {
                logger.log(LogStatus.ERROR, "ERROR: Menu Unselect All is enabled");
            }
            //case no line of the current page are selected
            //button stay disable
            boolean areNotAllSelected = true;
            for (int i = 0; i < 10; i++) {
                if (!isNoColor(getRgbString(getRow(i, waitingForApprovalPage.identifierTableResults)))) {
                    areNotAllSelected = false;
                    break;
                }
            }
            if (areNotAllSelected && waitingForApprovalPage.unselectAllButton.getAttribute("class").equals("ui button buttons-select-none disabled")) {
                logger.log(LogStatus.INFO, "Unselect button stays disable");
            } else {
                logger.log(LogStatus.ERROR, "ERROR: Unselect don't button stays disable");
            }
            //case one line of the current page is selected
            //button becomes enable
            click(getRow(5, waitingForApprovalPage.identifierTableResults));
            if (areNotAllSelected && waitingForApprovalPage.unselectAllButton.getAttribute("class").equals("ui button buttons-select-none")) {
                logger.log(LogStatus.INFO, "Unselect button becomes enable");
            } else {
                logger.log(LogStatus.ERROR, "ERROR: Unselect stays disable");
            }
            //user can click on button
            click(waitingForApprovalPage.unselectAllButton);
            //selected line becomes unselected
            if (isNoColor(getRgbString(getRow(5, waitingForApprovalPage.identifierTableResults))) && waitingForApprovalPage.unselectAllButton.getAttribute("class").equals("ui button buttons-select-none disabled")) {
                logger.log(LogStatus.INFO, "Selected line becomes unselected");
            } else {
                logger.log(LogStatus.ERROR, "ERROR: Selected line doesn't become unselected");
            }
            //case some lines of the current page  are selected
            click(getRow(1, waitingForApprovalPage.identifierTableResults));
            click(getRow(5, waitingForApprovalPage.identifierTableResults));
            click(getRow(8, waitingForApprovalPage.identifierTableResults));
            //button becomes enable
            if (waitingForApprovalPage.unselectAllButton.getAttribute("class").equals("ui button buttons-select-none")) {
                logger.log(LogStatus.INFO, "Menu Unselect All is enable");
            } else {
                logger.log(LogStatus.ERROR, "ERROR: Menu Unselect All is disable");
            }
            //user can click on button
            click(waitingForApprovalPage.unselectAllButton);
            //selected line becomes unselected
            if (isNoColor(getRgbString(getRow(1, waitingForApprovalPage.identifierTableResults))) &&
                    isNoColor(getRgbString(getRow(5, waitingForApprovalPage.identifierTableResults))) &&
                    isNoColor(getRgbString(getRow(8, waitingForApprovalPage.identifierTableResults))) &&
                    waitingForApprovalPage.unselectAllButton.getAttribute("class").equals("ui button buttons-select-none disabled")) {
                logger.log(LogStatus.INFO, "Selected lines become unselected");
            } else {
                logger.log(LogStatus.ERROR, "ERROR: Selected lines don't become unselected");
            }
            //case all lines of the current page are selected
            click(waitingForApprovalPage.selectAllButton);
            //button becomes enable
            if (waitingForApprovalPage.unselectAllButton.getAttribute("class").equals("ui button buttons-select-none")) {
                logger.log(LogStatus.INFO, "Menu Unselect All is enable");
            } else {
                logger.log(LogStatus.ERROR, "ERROR: Menu Unselect All is disable");
            }
            //user can click on button
            click(waitingForApprovalPage.unselectAllButton);
            //selected line becomes unselected
            areNotAllSelected = true;
            for (int i = 0; i < 10; i++) {
                if (!isNoColor(getRgbString(getRow(i, waitingForApprovalPage.identifierTableResults)))) {
                    areNotAllSelected = false;
                    break;
                }
            }
            if (areNotAllSelected && waitingForApprovalPage.unselectAllButton.getAttribute("class").equals("ui button buttons-select-none disabled")) {
                logger.log(LogStatus.INFO, "Selected line becomes unselected");
            } else {
                logger.log(LogStatus.ERROR, "ERROR: Selected line stay selected");
            }

            //Edit
            //By default, button is enabled
            //user can click on the button
            //case no line is selected
            detectInvalidClickButton(waitingForApprovalPage.reviewButton, waitingForApprovalPage, "Please select from the list");//This test valid that by default the button is enable
            //case one line is selected
            reloadWaitingForApprovalFilterPageWithCatIdentifier(waitingForApprovalPage, startPage); //reload just in case of error...
            click(getRow(1, waitingForApprovalPage.identifierTableResults));
            if(registerPage.equals("Veterinary")){
                detectValidClickReviewVetButton(waitingForApprovalPage, registerPage);
            } else {
                detectValidClickReviewBreederOrRefugeButton(waitingForApprovalPage, modifyPage);
            }

            //case several lines are selected
            reloadWaitingForApprovalFilterPageWithCatIdentifier(waitingForApprovalPage, startPage); //reload just in case of error...
            click(getRow(1, waitingForApprovalPage.identifierTableResults));
            click(getRow(4, waitingForApprovalPage.identifierTableResults));
            click(getRow(7, waitingForApprovalPage.identifierTableResults));
            detectInvalidClickButton(waitingForApprovalPage.reviewButton, waitingForApprovalPage, "Select only one");
            if (registrator.contains("Veterinary")) {
                //Delete
                //Register 13 cats before deletion test
                for (int i = 0; i < 13; i++) {
                    try {
                        if (i < 10) {
                            registerSpecificVetCat(registerPage, startPage, "HK159357", "99999999999900" + i);
                            logger.log(LogStatus.INFO, "Cat number: " + "99999999999900" + i + " is registered");
                        } else {
                            registerSpecificVetCat(registerPage, startPage, "HK159357", "9999999999990" + i);
                            logger.log(LogStatus.INFO, "Cat number: " + "9999999999990" + i + " is registered");
                        }

                    } catch (Exception e) {
                        if (i < 10) {
                            logger.log(LogStatus.ERROR, "Cat number: " + "99999999999900" + i + " is not registered");
                        } else {
                            logger.log(LogStatus.ERROR, "Cat number: " + "9999999999990" + i + " is not registered");
                        }
                    }
                }
                reloadWaitingForApprovalFilterPageWithCatIdentifier(waitingForApprovalPage, startPage);
                //by default, button is enable
                //case no line of the current page are selected
                //user can click on the button
                //an error message "please select from the list" is displayed
                detectInvalidClickButton(waitingForApprovalPage.deleteButton, waitingForApprovalPage, "Please select from the list");//This test valid that by default the button is enable

                //case one line of the current page is selected
                //user can click on the button
                reloadWaitingForApprovalFilterPageWithCatIdentifier(waitingForApprovalPage, startPage);
                click(getRow(0, waitingForApprovalPage.identifierTableResults));
                String beforeDeletionNumberOfForms = waitingForApprovalPage.pageInfo.getAttribute("textContent").split(" ")[5];
                logger.log(LogStatus.INFO, "There are currently " + beforeDeletionNumberOfForms + " forms before deletion test");
                click(waitingForApprovalPage.deleteButton);
                //user is prompted to confirm deletion
                //case Cancel
                click(waitingForApprovalPage.refuseDeletionButton);
                String afterCancelDeletion = waitingForApprovalPage.pageInfo.getAttribute("textContent").split(" ")[5];

                //nothing append, no deletion occurs
                if (beforeDeletionNumberOfForms.equals(afterCancelDeletion)) {
                    logger.log(LogStatus.INFO, "Nothing append, no deletion occurred");
                } else {
                    logger.log(LogStatus.ERROR, "ERROR: Something append, deletion occurred");
                }
                //case OK
                sleep(5000);//Because the dialog message can hide the filter button (visible so "wait for" return true)
                reloadWaitingForApprovalFilterPageWithCatIdentifier(waitingForApprovalPage, startPage);
                click(getRow(0, waitingForApprovalPage.identifierTableResults));
                String identifierDeletionFormBeforeDeletion = getCellString(0, 0, waitingForApprovalPage.identifierTableResults);
                beforeDeletionNumberOfForms = waitingForApprovalPage.pageInfo.getAttribute("textContent").split(" ")[5];
                click(waitingForApprovalPage.deleteButton);
                click(waitingForApprovalPage.acceptDeletionButton);
                sleep(1000);
                String identifierDeletionFormAfterDeletion = getCellString(0, 0, waitingForApprovalPage.identifierTableResults);
                String afterDeletion = waitingForApprovalPage.pageInfo.getAttribute("textContent").split(" ")[5];
                //selected line is deleted
                if (!identifierDeletionFormBeforeDeletion.equals(identifierDeletionFormAfterDeletion)) {
                    logger.log(LogStatus.INFO, "Selected line is deleted");
                } else {
                    logger.log(LogStatus.ERROR, "ERROR: Selected line is not deleted");
                }
                //page is refreshed
                //entries counter is updated -1
                int refreshCounter = 0;
                try {
                    int beforeInt = Integer.parseInt(beforeDeletionNumberOfForms);
                    int afterInt = Integer.parseInt(afterDeletion);
                    refreshCounter = afterInt - beforeInt;
                } catch (Exception e) {
                    logger.log(LogStatus.ERROR, "ERROR: Integer parsing failed");
                }
                if (refreshCounter == -1) {
                    logger.log(LogStatus.INFO, "Page is refreshed, entries counter is updated -1");
                } else {
                    logger.log(LogStatus.ERROR, "ERROR: Page is refreshed, entries counter is not updated -1");
                }
                //case some lines of the current page  are selected
                sleep(5000);//Because the dialog message can hide the filter button (visible so "wait for" return true)
                reloadWaitingForApprovalFilterPageWithCatIdentifier(waitingForApprovalPage, startPage);
                click(getRow(0, waitingForApprovalPage.identifierTableResults));
                click(getRow(1, waitingForApprovalPage.identifierTableResults));
                //user can click on the button
                beforeDeletionNumberOfForms = waitingForApprovalPage.pageInfo.getAttribute("textContent").split(" ")[5];
                logger.log(LogStatus.INFO, "There are currently " + beforeDeletionNumberOfForms + " forms before deletion test");
                click(waitingForApprovalPage.deleteButton);
                //user is prompted to confirm deletion
                //case Cancel
                click(waitingForApprovalPage.refuseDeletionButton);
                afterCancelDeletion = waitingForApprovalPage.pageInfo.getAttribute("textContent").split(" ")[5];
                //nothing append, no deletion occurs
                if (beforeDeletionNumberOfForms.equals(afterCancelDeletion)) {
                    logger.log(LogStatus.INFO, "Nothing append, no deletions occurred");
                } else {
                    logger.log(LogStatus.ERROR, "ERROR: Something append, deletions occurred");
                }
                //case OK
                sleep(5000);//Because the dialog message can hide the filter button (visible so "wait for" return true)
                reloadWaitingForApprovalFilterPageWithCatIdentifier(waitingForApprovalPage, startPage);
                click(getRow(0, waitingForApprovalPage.identifierTableResults));
                click(getRow(1, waitingForApprovalPage.identifierTableResults));
                identifierDeletionFormBeforeDeletion = getCellString(0, 0, waitingForApprovalPage.identifierTableResults);
                beforeDeletionNumberOfForms = waitingForApprovalPage.pageInfo.getAttribute("textContent").split(" ")[5];
                click(waitingForApprovalPage.deleteButton);
                click(waitingForApprovalPage.acceptDeletionButton);
                sleep(1000);
                identifierDeletionFormAfterDeletion = getCellString(0, 0, waitingForApprovalPage.identifierTableResults);
                afterDeletion = waitingForApprovalPage.pageInfo.getAttribute("textContent").split(" ")[5];
                //selected lines are deleted
                if (!identifierDeletionFormBeforeDeletion.equals(identifierDeletionFormAfterDeletion)) {
                    logger.log(LogStatus.INFO, "Selected line is deleted");
                } else {
                    logger.log(LogStatus.ERROR, "ERROR: Selected line is not deleted");
                }
                //page is refreshed
                //entries counter is updated minus number of lines selected
                refreshCounter = 0;
                try {
                    int beforeInt = Integer.parseInt(beforeDeletionNumberOfForms);
                    int afterInt = Integer.parseInt(afterDeletion);
                    refreshCounter = afterInt - beforeInt;
                } catch (Exception e) {
                    logger.log(LogStatus.ERROR, "ERROR: Integer parsing failed");
                }
                if (refreshCounter == -2) {
                    logger.log(LogStatus.INFO, "Page is refreshed, entries counter is updated minus number of lines selected");
                } else {
                    logger.log(LogStatus.ERROR, "ERROR: Page is refreshed, entries counter is not updated minus number of lines selected");
                }
                //case all lines of the current page  are selected
                sleep(5000);//Because the dialog message can hide the filter button (visible so "wait for" return true)
                reloadWaitingForApprovalFilterPageWithCatIdentifier(waitingForApprovalPage, startPage);
                click(waitingForApprovalPage.selectAllButton);
                //user can click on the button
                beforeDeletionNumberOfForms = waitingForApprovalPage.pageInfo.getAttribute("textContent").split(" ")[5];
                logger.log(LogStatus.INFO, "There are currently " + beforeDeletionNumberOfForms + " forms before deletion test");
                click(waitingForApprovalPage.deleteButton);
                //user is prompted to confirm deletion
                //case Cancel
                click(waitingForApprovalPage.refuseDeletionButton);
                afterCancelDeletion = waitingForApprovalPage.pageInfo.getAttribute("textContent").split(" ")[5];
                //nothing append, no deletion occurs
                if (beforeDeletionNumberOfForms.equals(afterCancelDeletion)) {
                    logger.log(LogStatus.INFO, "Nothing append, no deletions occurred");
                } else {
                    logger.log(LogStatus.ERROR, "ERROR: Something append, deletions occurred");
                }
                //case OK
                sleep(5000);//Because the dialog message can hide the filter button (visible so "wait for" return true)
                reloadWaitingForApprovalFilterPageWithCatIdentifier(waitingForApprovalPage, startPage);
                click(waitingForApprovalPage.selectAllButton);
                identifierDeletionFormBeforeDeletion = getCellString(0, 0, waitingForApprovalPage.identifierTableResults);
                beforeDeletionNumberOfForms = waitingForApprovalPage.pageInfo.getAttribute("textContent").split(" ")[5];
                click(waitingForApprovalPage.deleteButton);
                click(waitingForApprovalPage.acceptDeletionButton);
                sleep(5000);
                identifierDeletionFormAfterDeletion = getCellString(0, 0, waitingForApprovalPage.identifierTableResults);
                afterDeletion = waitingForApprovalPage.pageInfo.getAttribute("textContent").split(" ")[5];
                //all  lines of the current page are deleted
                if (!identifierDeletionFormBeforeDeletion.equals(identifierDeletionFormAfterDeletion)) {
                    logger.log(LogStatus.INFO, "Selected line is deleted");
                } else {
                    logger.log(LogStatus.ERROR, "ERROR: Selected line is not deleted");
                }
                //page is refreshed

                //all next lines will come on the first page
                //entries counter is updated minus 10
                refreshCounter = 0;
                try {
                    int beforeInt = Integer.parseInt(beforeDeletionNumberOfForms);
                    int afterInt = Integer.parseInt(afterDeletion);
                    refreshCounter = afterInt - beforeInt;
                } catch (Exception e) {
                    logger.log(LogStatus.ERROR, "ERROR: Integer parsing failed");
                }
                if (refreshCounter == -10) {
                    logger.log(LogStatus.INFO, "Page is refreshed, entries counter is updated -10");
                } else {
                    logger.log(LogStatus.ERROR, "ERROR: Page is refreshed, entries counter is not updated -10");
                }
                //entries counter is updated to 0
                //button unselect all becomes disable
                //no more lines are available
                String count = afterDeletion = waitingForApprovalPage.pageInfo.getAttribute("textContent").split(" ")[5];
                if (count.equals("0")) {
                    logger.log(LogStatus.INFO, "No more lines are available");
                    if (waitingForApprovalPage.unselectAllButton.getAttribute("class").equals("ui button buttons-select-none")) {
                        logger.log(LogStatus.INFO, "Menu Unselect All is enabled");
                    } else {
                        logger.log(LogStatus.ERROR, "ERROR: Menu Unselect All is disabled");
                    }
                } else {
                    logger.log(LogStatus.INFO, "There are still lines available");
                }
            }

            //MassApprove
            sleep(5000);//Because the dialog message can hide the filter button (visible so "wait for" return true)
            reloadWaitingForApprovalFilterPageWithCatIdentifier(waitingForApprovalPage, startPage);
            //By default, button is enabled
            //user can click on the button
            //case no line is selected
            //error message is displayed "Please select from the list"
            detectInvalidClickButton(waitingForApprovalPage.deleteButton, waitingForApprovalPage, "Please select from the list");//This test valid that by default the button is enable
            //Register 4 cats for the test
            generateWaitingForApprovalForms(registerPage, startPage, modifyPage);
            sleep(5000);//Because the dialog message can hide the filter button (visible so "wait for" return true)
            reloadWaitingForApprovalFilterPageWithCatIdentifier(waitingForApprovalPage, startPage);
            //case one line is selected
            click(getRow(0, waitingForApprovalPage.identifierTableResults)); //select first identifier registered
            //easylink process is launched
            if(registrator.contains("Veterinary")){
                click(waitingForApprovalPage.massApproveVetButton);
            } else {
                click(waitingForApprovalPage.massApproveBreederOrRefugeButton);
            }
            //selected line is retrieved for signature
            //signature process is successful
            eazyId(eazyIdPage);
            click(eazyIdPage.closeValidSubmitMessage);
            //case several lines are selected
            click(getRow(0, waitingForApprovalPage.identifierTableResults)); //select first identifier registered
            click(getRow(1, waitingForApprovalPage.identifierTableResults)); //select first identifier registered
            click(getRow(2, waitingForApprovalPage.identifierTableResults)); //select first identifier registered
            //easylink process is launched
            if(registrator.contains("Veterinary")){
                click(waitingForApprovalPage.massApproveVetButton);
            } else {
                click(waitingForApprovalPage.massApproveBreederOrRefugeButton);
            }
            //selected lines are retrieved for signature
            //signature process is successful
            //signature process is not successful (depending if form is owning by vet or not)
            eazyId(eazyIdPage);
            click(eazyIdPage.closeValidSubmitMessage);

            //MassReject
            reloadWaitingForApprovalFilterPageWithCatIdentifier(waitingForApprovalPage, startPage);
            //By default, button is enabled
            //user can click on the button
            //case no line is selected
            //error message is displayed "Please select from the list"
            detectInvalidClickButton(waitingForApprovalPage.deleteButton, waitingForApprovalPage, "Please select from the list");//This test valid that by default the button is enable
            //Register 4 cats for the test
            generateWaitingForApprovalForms(registerPage, startPage, modifyPage);
            sleep(5000);//Because the dialog message can hide the filter button (visible so "wait for" return true)
            reloadWaitingForApprovalFilterPageWithCatIdentifier(waitingForApprovalPage, startPage);
            //case one line is selected
            descendingSort(waitingForApprovalPage);
            click(getRow(0, waitingForApprovalPage.identifierTableResults)); //select first identifier registered
            //easylink process is launched
            if(registrator.contains("Veterinary")){
                click(waitingForApprovalPage.massRejectVetButton);
            } else {
                click(waitingForApprovalPage.massRejectBreederOrRefugeButton);
            }
            setText(waitingForApprovalPage.rejectTextField, "TEST Reject " + tcFeatureListNumber);
            click(waitingForApprovalPage.acceptRejectButton);
            sleep(5000); // Wait for the end of reject process
            //case several lines are selected
            reloadWaitingForApprovalFilterPageWithCatIdentifier(waitingForApprovalPage, startPage);
            descendingSort(waitingForApprovalPage);
            click(getRow(0, waitingForApprovalPage.identifierTableResults)); //select first identifier registered
            click(getRow(1, waitingForApprovalPage.identifierTableResults)); //select first identifier registered
            click(getRow(2, waitingForApprovalPage.identifierTableResults)); //select first identifier registered
            //easylink process is launched
            if(registrator.contains("Veterinary")){
                click(waitingForApprovalPage.massRejectVetButton);
            } else {
                click(waitingForApprovalPage.massRejectBreederOrRefugeButton);
            }
            setText(waitingForApprovalPage.rejectTextField, "TEST Reject " + tcFeatureListNumber);
            click(waitingForApprovalPage.acceptRejectButton);
            sleep(5000); // Wait for the end of reject process
            //create CSV
            //By default, button is enabled
            //user can click on the button
            //case no line is selected
            //Error message is displayed
            reloadWaitingForApprovalFilterPageWithCatIdentifier(waitingForApprovalPage, startPage); //reload just in case of error...
            if(registrator.contains("Veterinary")){
                detectInvalidClickButton(waitingForApprovalPage.createCsvVetButton, waitingForApprovalPage, "Please select from the list");//This test valid that by default the button is enable

            } else {
                detectInvalidClickButton(waitingForApprovalPage.createCsvBreederOrRefugeButton, waitingForApprovalPage, "Please select from the list");//This test valid that by default the button is enable

            }
            //case one line is selected
            //a CSV file is generated
            reloadWaitingForApprovalFilterPageWithCatIdentifier(waitingForApprovalPage, startPage); //reload just in case of error...
            click(getRow(1, waitingForApprovalPage.identifierTableResults));
            List<String> expectedIdentifiers = new ArrayList<String>();
            expectedIdentifiers.add(getCell(1, 0, waitingForApprovalPage.identifierTableResults).getAttribute("textContent"));
            detectValidClickCreateCsvtButton(waitingForApprovalPage, expectedIdentifiers);
            //file contains the line selected
            //case several lines are selected
            //a CSV file is generated
            //file contains the lines selected
            reloadWaitingForApprovalFilterPageWithCatIdentifier(waitingForApprovalPage, startPage); //reload just in case of error...
            click(getRow(1, waitingForApprovalPage.identifierTableResults));
            click(getRow(2, waitingForApprovalPage.identifierTableResults));
            expectedIdentifiers = new ArrayList<String>();
            expectedIdentifiers.add(getCell(1, 0, waitingForApprovalPage.identifierTableResults).getAttribute("textContent"));
            expectedIdentifiers.add(getCell(2, 0, waitingForApprovalPage.identifierTableResults).getAttribute("textContent"));
            detectValidClickCreateCsvtButton(waitingForApprovalPage, expectedIdentifiers);
            if(registrator.contains("Veterinary")){
                //case selected line has multiple identifier
                //user must be able to edit CSV
                //both identifier must be set in column identifier
                reloadWaitingForApprovalFilterPageWithCatIdentifier(waitingForApprovalPage, startPage); //reload just in case of error...
                setText(waitingForApprovalPage.searchField, ",");
                click(getRow(1, waitingForApprovalPage.identifierTableResults));
                click(getRow(2, waitingForApprovalPage.identifierTableResults));
                expectedIdentifiers = new ArrayList<String>();
                expectedIdentifiers.add(getCell(1, 0, waitingForApprovalPage.identifierTableResults).getAttribute("textContent"));
                expectedIdentifiers.add(getCell(2, 0, waitingForApprovalPage.identifierTableResults).getAttribute("textContent"));
                detectValidClickCreateCsvtButton(waitingForApprovalPage, expectedIdentifiers);
            }
        }
    }

    private void descendingSort(WebWaitingForApprovalPage waitingForApprovalPage){
        List<WebElement> listHeaderButton = waitingForApprovalPage.headerRow.findElements(By.tagName("th"));
        WebElement identifierTab = listHeaderButton.get(0);//Descending sort for identifier
        click(identifierTab);
        click(identifierTab);
    }

    private void ascendingSort(WebWaitingForApprovalPage waitingForApprovalPage){
        List<WebElement> listHeaderButton = waitingForApprovalPage.headerRow.findElements(By.tagName("th"));
        WebElement identifierTab = listHeaderButton.get(0);//Descending sort for identifier
        click(identifierTab);
        click(identifierTab);
    }

    protected void generateWaitingForApprovalForms(WebRegisterPage registerPage, WebStartPage startPage, WebModifyPage modifyPage){
        if(registrator.contains("Veterinary")) {
            try {
                click(startPage.logoutButton);
                OpenHomepage openHomepage = new OpenHomepage();
                openHomepage.setDriver(driver);
                openHomepage.setLogger(logger);
                openHomepage.run();
                Login loginpopup = new Login();
                loginpopup.setUserRole("Breeder 4");
                loginpopup.setDriver(driver);
                loginpopup.setLogger(logger);
                loginpopup.run();
                registerAndTransferNewBreederOrRefugeCat(registerPage, startPage, "HK852456");
                registerAndTransferNewBreederOrRefugeCat(registerPage, startPage, "HK852456");
                registerAndTransferNewBreederOrRefugeCat(registerPage, startPage, "HK852456");
                registerAndTransferNewBreederOrRefugeCat(registerPage, startPage, "HK852456");
                click(startPage.logoutButton);
                openHomepage.setDriver(driver);
                openHomepage.setLogger(logger);
                openHomepage.run();
                loginpopup.setUserRole("Veterinary 2");
                loginpopup.setDriver(driver);
                loginpopup.setLogger(logger);
                loginpopup.run();
            } catch (Exception e) {
                logger.log(LogStatus.ERROR, "ERROR: registration failed " + e.getMessage());
            }
        } else if(registrator.equals("Refuge 3")) { //Breeder 3 create 4 waiting for approval form for refuge 1

            try {
                click(startPage.logoutButton);
                OpenHomepage openHomepage = new OpenHomepage();
                openHomepage.setDriver(driver);
                openHomepage.setLogger(logger);
                openHomepage.run();
                Login loginpopup = new Login();
                loginpopup.setUserRole("Veterinary 2");
                loginpopup.setDriver(driver);
                loginpopup.setLogger(logger);
                loginpopup.run();
                registerNewVetCatToPro(registerPage, startPage, "HK10101235", false);
                registerNewVetCatToPro(registerPage, startPage, "HK10101235", false);
                registerNewVetCatToPro(registerPage, startPage, "HK10101235", false);
                registerNewVetCatToPro(registerPage, startPage, "HK10101235", false);
                click(startPage.logoutButton);
                openHomepage.setDriver(driver);
                openHomepage.setLogger(logger);
                openHomepage.run();
                loginpopup.setUserRole("Breeder 3");
                loginpopup.setDriver(driver);
                loginpopup.setLogger(logger);
                loginpopup.run();
                WebApprovedPage approvedPage = new WebApprovedPage(driver);
                transferFirstCat(startPage, approvedPage, modifyPage, "HK852456", 0);
                transferFirstCat(startPage, approvedPage, modifyPage, "HK852456", 1);
                transferFirstCat(startPage, approvedPage, modifyPage, "HK852456", 2);
                transferFirstCat(startPage, approvedPage, modifyPage, "HK852456", 3);
                click(startPage.logoutButton);
                openHomepage.setDriver(driver);
                openHomepage.setLogger(logger);
                openHomepage.run();
                loginpopup.setUserRole("Refuge 3");
                loginpopup.setDriver(driver);
                loginpopup.setLogger(logger);
                loginpopup.run();
            } catch (Exception e) {
                logger.log(LogStatus.ERROR, "ERROR: registration failed " + e.getMessage());
            }

        } else if(registrator.equals("Breeder 4")) {//Breeder 3 create 4 waiting for approval form for breeder 1
            try {
                click(startPage.logoutButton);
                OpenHomepage openHomepage = new OpenHomepage();
                openHomepage.setDriver(driver);
                openHomepage.setLogger(logger);
                openHomepage.run();
                Login loginpopup = new Login();
                loginpopup.setUserRole("Veterinary 2");
                loginpopup.setDriver(driver);
                loginpopup.setLogger(logger);
                loginpopup.run();
                registerNewVetCatToPro(registerPage, startPage, "HK10101235", false);
                registerNewVetCatToPro(registerPage, startPage, "HK10101235", false);
                registerNewVetCatToPro(registerPage, startPage, "HK10101235", false);
                registerNewVetCatToPro(registerPage, startPage, "HK10101235", false);
                click(startPage.logoutButton);
                openHomepage.setDriver(driver);
                openHomepage.setLogger(logger);
                openHomepage.run();
                loginpopup.setUserRole("Breeder 3");
                loginpopup.setDriver(driver);
                loginpopup.setLogger(logger);
                loginpopup.run();
                WebApprovedPage approvedPage = new WebApprovedPage(driver);
                transferFirstCat(startPage, approvedPage, modifyPage, "HK159357", 0);
                transferFirstCat(startPage, approvedPage, modifyPage, "HK159357", 1);
                transferFirstCat(startPage, approvedPage, modifyPage, "HK159357", 2);
                transferFirstCat(startPage, approvedPage, modifyPage, "HK159357", 3);
                click(startPage.logoutButton);
                openHomepage.setDriver(driver);
                openHomepage.setLogger(logger);
                openHomepage.run();
                loginpopup.setUserRole("Breeder 4");
                loginpopup.setDriver(driver);
                loginpopup.setLogger(logger);
                loginpopup.run();
            } catch (Exception e) {
                logger.log(LogStatus.ERROR, "ERROR: registration failed " + e.getMessage());
            }
        }
    }

    private void checkSelectAndUnSelectLine(WebWaitingForApprovalPage waitingForApprovalPage, WebStartPage startPage){
        reloadWaitingForApprovalFilterPageWithCatIdentifier(waitingForApprovalPage, startPage);
        //User can select lines
        //by clicking on a line
        //line becomes gray
        WebElement firstRow = getRow(0,waitingForApprovalPage.identifierTableResults);
        click(firstRow);
        if(isSpecificColor(getRgbString(firstRow), "52", "69", "78")){
            logger.log(LogStatus.INFO,"First line is gray when selected (by clicking)");
        } else {
            logger.log(LogStatus.ERROR,"ERROR: First line is not gray when selected (by clicking)");
        }
        //Label row selected is incremented by 1
        if(waitingForApprovalPage.pageInfo.getAttribute("textContent").contains(("1 row selected"))){
            logger.log(LogStatus.INFO,"Label row selected is incremented by 1");
        } else {
            logger.log(LogStatus.ERROR,"ERROR: Label row selected is not incremented by 1");
        }
        //Menu Unselect All is enabled
        if(waitingForApprovalPage.unselectAllButton.getAttribute("class").equals("ui button buttons-select-none")){
            logger.log(LogStatus.INFO,"Menu Unselect All is enabled");
        } else {
            logger.log(LogStatus.ERROR,"ERROR: Menu Unselect All is disabled");
        }

        WebElement thirdRow = getRow(2,waitingForApprovalPage.identifierTableResults);
        click(thirdRow);
        if(isSpecificColor(getRgbString(thirdRow), "52", "69", "78")){
            logger.log(LogStatus.INFO,"Third line is gray when selected (by clicking)");
        } else {
            logger.log(LogStatus.ERROR,"ERROR: Third line is not gray when selected (by clicking)");
        }
        //Label row selected is incremented by 1
        if(waitingForApprovalPage.pageInfo.getAttribute("textContent").contains(("2 rows selected"))){
            logger.log(LogStatus.INFO,"Label row selected is incremented by 1");
        } else {
            logger.log(LogStatus.ERROR,"ERROR: Label row selected is not incremented by 1");
        }

        //User can deselect lines
        //line must be selected and color gray
        //user click on the line
        //line become deselected
        //color line becomes white
        click(waitingForApprovalPage.firstIdentifierTableResult);
        if(isNoColor(getRgbString(waitingForApprovalPage.firstIdentifierTableResult))){
            logger.log(LogStatus.INFO,"First line is white when selected (by clicking)");
        } else {
            logger.log(LogStatus.ERROR,"ERROR: First line is not white when selected (by clicking)");
        }
        //Label row selected is decremented by 1
        if(waitingForApprovalPage.pageInfo.getAttribute("textContent").contains(("1 row selected"))){
            logger.log(LogStatus.INFO,"Label row selected is decremented by 1");
        } else {
            logger.log(LogStatus.ERROR,"ERROR: Label row selected is not decremented by 1");
        }

        click(thirdRow);
        if(isNoColor(getRgbString(thirdRow))){
            logger.log(LogStatus.INFO,"Third line is white when selected (by clicking)");
        } else {
            logger.log(LogStatus.ERROR,"ERROR: Third line is not white when selected (by clicking)");
        }

        //Label row selected is decremented by 1
        if(!waitingForApprovalPage.pageInfo.getAttribute("textContent").contains(("selected"))){
            logger.log(LogStatus.INFO,"Label row selected is decremented by 1");
        } else {
            logger.log(LogStatus.ERROR,"ERROR: Label row selected is not decremented by 1");
        }
        //case no more lines are selected
        //menu unselect all is disabled
        if(waitingForApprovalPage.unselectAllButton.getAttribute("class").equals("ui button buttons-select-none disabled")){
            logger.log(LogStatus.INFO,"Menu Unselect All is disabled");
        } else {
            logger.log(LogStatus.ERROR,"ERROR: Menu Unselect All is enabled");
        }
    }

    private void checkSearchField(WebWaitingForApprovalPage waitingForApprovalPage, WebStartPage startPage){
        int DURATION = 2000;
        reloadWaitingForApprovalFilterPageWithCatIdentifier(waitingForApprovalPage, startPage);
        HashMap<String,String> expectedResult = new HashMap<String, String>();
        HashMap<String,String> searchWords = new HashMap<String, String>();
        if(registrator.equals("Refuge 3")){
            expectedResult.put("Identifier", "123456789501936");
            expectedResult.put("PassportNumber", "TC0030012018134839");
            expectedResult.put("DateOfBirth", "30/01/2018");
            expectedResult.put("IdentificationDate", "30/01/2018");
            expectedResult.put("Gender", "Male");
            expectedResult.put("CatBreed", "Abyssinian");
            expectedResult.put("Sterelized", "Yes");
            expectedResult.put("SterelizationDate", "30/01/2018");
            expectedResult.put("Info", "HK159357");
            expectedResult.put("Status", "Change of responsible");
            searchWords.put("Identifier", "789501936");
            searchWords.put("PassportNumber", "30012018134839");
            searchWords.put("DateOfBirth", "30/01/2018");
            searchWords.put("IdentificationDate", "30/01/2018");
            searchWords.put("Gender", "Male");
            searchWords.put("CatBreed", "Abyssinian");
            searchWords.put("Sterelized", "Yes");
            searchWords.put("SterelizationDate", "30/01/2018");
            searchWords.put("Info", "HK159357");
            searchWords.put("Status", "Change of");
        } else if(registrator.equals("Breeder 4")){
            expectedResult.put("Identifier", "123456789502340");
            expectedResult.put("PassportNumber", "TC0030012018154934");
            expectedResult.put("DateOfBirth", "30/01/2018");
            expectedResult.put("IdentificationDate", "30/01/2018");
            expectedResult.put("Gender", "Male");
            expectedResult.put("CatBreed", "Abyssinian");
            expectedResult.put("Sterelized", "Yes");
            expectedResult.put("SterelizationDate", "30/01/2018");
            expectedResult.put("Info", "HK852456");
            expectedResult.put("Status", "Change of responsible");
            searchWords.put("Identifier", "89502340");
            searchWords.put("PassportNumber", "2018154934");
            searchWords.put("DateOfBirth", "30/01/2018");
            searchWords.put("IdentificationDate", "30/01/2018");
            searchWords.put("Gender", "Male");
            searchWords.put("CatBreed", "Abyssini");
            searchWords.put("Sterelized", "Yes");
            searchWords.put("SterelizationDate", "30/01/2018");
            searchWords.put("Info", "HK852456");
            searchWords.put("Status", "Change of");
        } else {
            expectedResult.put("Identifier", "123456789502443");
            expectedResult.put("PassportNumber", "TC0030012018161307");
            expectedResult.put("DateOfBirth", "01/01/2018");
            expectedResult.put("IdentificationDate", "01/06/2017");
            expectedResult.put("Gender", "Male");
            expectedResult.put("CatBreed", "Exotic");
            expectedResult.put("Sterelized", "Yes");
            expectedResult.put("SterelizationDate", "20/07/2017");
            expectedResult.put("Info", "David Henri, Croix");
            expectedResult.put("Status", "Registration request waiting for approval");
            searchWords.put("Identifier", "789502443");
            searchWords.put("PassportNumber", "0012018161307");
            searchWords.put("DateOfBirth", "01/01/2018");
            searchWords.put("IdentificationDate", "01/06/2017");
            searchWords.put("Gender", "Mal");
            searchWords.put("CatBreed", "Exo");
            searchWords.put("Sterelized", "Yes");
            searchWords.put("SterelizationDate", "20/07/2017");
            searchWords.put("Info", "Croix");
            searchWords.put("Status", "Registration request");
        }

        //User can perform searches
        //by enconding string in field Search
        //results are displayed after each character encoded
        //search will fetch on column
        //identifier  -> "500319" to search for "123456789500319"
        setText(waitingForApprovalPage.searchField,searchWords.get("Identifier"));
        sleep(DURATION); // Wait 1s to let the result to be refreshed
        getScreenShot();
        String indeitifierResult = lookForExpectedResult(expectedResult.get("Identifier"), waitingForApprovalPage.identifierTableResults, 0);
        if(indeitifierResult.contains(expectedResult.get("Identifier"))){
            logger.log(LogStatus.INFO,"Identifier search for " + searchWords.get("Identifier") + " works and the result is " + indeitifierResult);
        } else {
            logger.log(LogStatus.ERROR,"ERROR: Identifier search for " + searchWords.get("Identifier") + " doesn't work and the result is " + indeitifierResult);
        }
        //passport number
        setText(waitingForApprovalPage.searchField,searchWords.get("PassportNumber"));
        sleep(DURATION); // Wait 1s to let the result to be refreshed
        getScreenShot();
        String passportResult = lookForExpectedResult(expectedResult.get("PassportNumber"), waitingForApprovalPage.identifierTableResults, 1);
        if(passportResult.contains(expectedResult.get("PassportNumber"))){
            logger.log(LogStatus.INFO,"Passport search for " + searchWords.get("PassportNumber") + " works and the result is " + passportResult);
        } else {
            logger.log(LogStatus.ERROR,"ERROR: Passport search for " + searchWords.get("PassportNumber") + " doesn't work and the result is " + passportResult);
        }
        //date of birth
        setText(waitingForApprovalPage.searchField,searchWords.get("DateOfBirth"));
        sleep(DURATION); // Wait 1s to let the result to be refreshed
        getScreenShot();
        String birthDateResult = lookForExpectedResult(expectedResult.get("DateOfBirth"), waitingForApprovalPage.identifierTableResults, 2);
        if(birthDateResult.contains(searchWords.get("DateOfBirth"))){
            logger.log(LogStatus.INFO,"Date of birth search for " + searchWords.get("DateOfBirth") + " works and the result is " + birthDateResult);
        } else {
            logger.log(LogStatus.ERROR,"ERROR: Date of birth search for "+ searchWords.get("DateOfBirth") + " doesn't work and the result is " + birthDateResult);
        }
        //identification date
        setText(waitingForApprovalPage.searchField,searchWords.get("IdentificationDate"));
        sleep(DURATION); // Wait 1s to let the result to be refreshed
        getScreenShot();
        String identificationDateResult = lookForExpectedResult(expectedResult.get("IdentificationDate"), waitingForApprovalPage.identifierTableResults, 3);
        if(identificationDateResult.contains(expectedResult.get("IdentificationDate"))){
            logger.log(LogStatus.INFO,"Date of identification search for " + searchWords.get("IdentificationDate") + " works and the result is " + identificationDateResult);
        } else {
            logger.log(LogStatus.ERROR,"ERROR: Date of identification search for " + searchWords.get("IdentificationDate") + " doesn't work and the result is " + identificationDateResult);
        }
        //Gender
        setText(waitingForApprovalPage.searchField,searchWords.get("Gender"));
        sleep(DURATION); // Wait 1s to let the result to be refreshed
        getScreenShot();
        String genderResult = lookForExpectedResult(expectedResult.get("Gender"), waitingForApprovalPage.identifierTableResults, 4);
        if(genderResult.contains(expectedResult.get("Gender"))){
            logger.log(LogStatus.INFO,"Gender search for " + searchWords.get("Gender") + " works and the result is " + genderResult);
        } else {
            logger.log(LogStatus.ERROR,"ERROR: Gender search for " + searchWords.get("Gender") + " doesn't work and the result is " + genderResult);
        }
        //Cat Breed
        setText(waitingForApprovalPage.searchField,searchWords.get("CatBreed"));
        sleep(DURATION); // Wait 1s to let the result to be refreshed
        getScreenShot();
        String breedResult = lookForExpectedResult(expectedResult.get("CatBreed"), waitingForApprovalPage.identifierTableResults, 5);
        if(breedResult.contains(expectedResult.get("CatBreed"))){
            logger.log(LogStatus.INFO,"Breed search for " + searchWords.get("CatBreed") + " works and the result is " + breedResult);
        } else {
            logger.log(LogStatus.ERROR,"ERROR: Breed search for " + searchWords.get("CatBreed") + " doesn't work and the result is " + breedResult);
        }
        //Sterilized
        setText(waitingForApprovalPage.searchField,searchWords.get("Sterelized"));
        sleep(DURATION); // Wait 1s to let the result to be refreshed
        getScreenShot();
        String sterilizedResult = lookForExpectedResult(expectedResult.get("Sterelized"), waitingForApprovalPage.identifierTableResults, 6);
        if(sterilizedResult.contains(expectedResult.get("Sterelized"))){
            logger.log(LogStatus.INFO,"Sterilzed search for " + searchWords.get("Sterelized") + " works and the result is " + sterilizedResult);
        } else {
            logger.log(LogStatus.ERROR,"ERROR: Sterilzed search for " + searchWords.get("Sterelized") + " doesn't work and the result is " + sterilizedResult);
        }
        //sterilized date
        setText(waitingForApprovalPage.searchField,searchWords.get("SterelizationDate"));
        sleep(DURATION); // Wait 1s to let the result to be refreshed
        getScreenShot();
        String sterilizedDateResult = lookForExpectedResult(expectedResult.get("SterelizationDate"), waitingForApprovalPage.identifierTableResults, 7);
        if(sterilizedDateResult.contains(expectedResult.get("SterelizationDate"))){
            logger.log(LogStatus.INFO,"Sterilzed date search for " + searchWords.get("SterelizationDate") + " works and the result is " + sterilizedDateResult);
        } else {
            logger.log(LogStatus.ERROR,"ERROR: Sterilzed date search for " + searchWords.get("SterelizationDate") + " doesn't work and the result is " + sterilizedDateResult);
        }
        //owner information
        setText(waitingForApprovalPage.searchField,searchWords.get("Info"));
        sleep(DURATION); // Wait 1s to let the result to be refreshed
        getScreenShot();
        String ownerInfoResult = lookForExpectedResult(expectedResult.get("Info"), waitingForApprovalPage.identifierTableResults, 8);
        if(ownerInfoResult.contains(searchWords.get("Info"))){
            logger.log(LogStatus.INFO,"Owner Information search for " + searchWords.get("Info") + " works and the result is " + ownerInfoResult);
        } else {
            logger.log(LogStatus.ERROR,"ERROR: Owner Information search for " + searchWords.get("Info") + " doesn't work and the result is " + ownerInfoResult);
        }

        //status
        setText(waitingForApprovalPage.searchField,searchWords.get("Status"));
        sleep(DURATION); // Wait 1s to let the result to be refreshed
        getScreenShot();
        String statusResult = lookForExpectedResult(expectedResult.get("Status"), waitingForApprovalPage.identifierTableResults, 9);
        if(statusResult.contains(searchWords.get("Status"))){
            logger.log(LogStatus.INFO,"Status search for " + searchWords.get("Status") + " works and the result is " + statusResult);
        } else {
            logger.log(LogStatus.ERROR,"ERROR: Status search for " + searchWords.get("Status") + " doesn't work and the result is " + statusResult);
        }
    }

        private void waitingForApprovalFilterCheck(WebWaitingForApprovalPage waitingForApprovalPage, WebStartPage startPage, WebEazyIdPage eazyIdPage, SqlOracleMiddleware metaformMiddleWare, WebRegisterPage registerPage, WebModifyPage modifyPage) throws Exception {
        click(startPage.homeButton);
        //by clicking on the menu filters - Waiting for Approval
        //The all cats page is displayed
        //all cats where vet are linked and approved by system are displayed
        //total number of cats is the sum of
        String numberCatWaitingForApprovalDisplayed = "";
        String numberCatWaitingForApprovalSql = "";
        if(registrator.contains("Veterinary")){
            WaitForElement(startPage.filterWaitingForApprovalVetActionButton,10);
            numberCatWaitingForApprovalDisplayed = startPage.filterWaitingForApprovalVetActionButton.getAttribute("textContent").replaceAll("\\s+", "");
        } else {
            WaitForElement(startPage.filterWaitingForApprovalBreederAndRefugeActionButton,5);
            numberCatWaitingForApprovalDisplayed = startPage.filterWaitingForApprovalBreederAndRefugeActionButton.getAttribute("textContent").replaceAll("\\s+", "");
        }
        ArrayList<HashMap> rs = new ArrayList<HashMap>();
        if(registrator.contains("Veterinary 2")){
            rs = metaformMiddleWare.executeSqlQuery("SELECT COUNT(*) FROM METAFORM_ABIEC_ANIMAL WHERE (MF_STATUS = 64 OR  MF_STATUS = 2 OR  MF_STATUS = 65536 OR  MF_STATUS = 16384) AND VET_REFNUM = 'F0024'");
        } else {
            String refnumId;
            if(registrator.equals("Breeder 4")){ //Breeder
                refnumId = "58219";
            } else { //Refuge
                refnumId = "58225";
            }
            rs = metaformMiddleWare.executeSqlQuery("SELECT COUNT(*) from METAFORM_ABIEC_ANIMAL WHERE MF_STATUS = 16384 AND OWNER_REFNUM_ID_2ND = '" + refnumId + "'");
        }

        if (rs.size() == 0) {
            logger.log(LogStatus.ERROR, "Something is wrong with SQL statement or DB connection");
        } else {
            numberCatWaitingForApprovalSql = (String)rs.get(0).get("COUNT(*)");
            if(numberCatWaitingForApprovalSql != null && numberCatWaitingForApprovalDisplayed != null && numberCatWaitingForApprovalSql.equals(numberCatWaitingForApprovalDisplayed)){
                logger.log(LogStatus.INFO,"All cats where vet is linked and in waiting for approval by system are displayed");
            } else {
                logger.log(LogStatus.ERROR,"ERROR: All cats where vet is not linked or waiting for approval by system are not displayed: " + numberCatWaitingForApprovalDisplayed + " SQL: " + numberCatWaitingForApprovalSql);
            }
        }
        //Page is showing 10 cats per page by default
        if(registrator.contains("Veterinary")){
            click(startPage.filterWaitingForApprovalVetActionButton);
        } else {
            click(startPage.filterWaitingForApprovalBreederAndRefugeActionButton);
        }
        List<WebElement> listResults = waitingForApprovalPage.identifierTableResults.findElements(By.tagName("tr"));
        //Wait a little bit the page is completely loaded to take a screenshot
        WaitForElement(waitingForApprovalPage.firstIdentifierTableResult,5);
        if(listResults.size() == 10){
            logger.log(LogStatus.INFO,"Page is showing 10 cats per page by default");
            if(isElementPresent(waitingForApprovalPage.nextButton)){
                //case of more than 10 cats
                //next button is displayed
                logger.log(LogStatus.INFO,"Next button is displayed");
                //case there are more than one page available
                //case there are more than 10 cats
                //pages number are displayed
                List<WebElement> pageNumberList = waitingForApprovalPage.paginationMenu.findElements(By.tagName("a"));
                if(pageNumberList.size() > 5){
                    //is it possible to click on the specific page
                    //selected page is displayed
                    //other available pages are still available for click
                    //page 1 display results from 1 to 10
                    //page 2 display result from 11 to 20
                    //page 3 display result from 21 to 30

                    logger.log(LogStatus.INFO,"Pages number are displayed");
                    if(waitingForApprovalPage.pageInfo.getAttribute("textContent").contains("1 to 10")){
                        logger.log(LogStatus.INFO,"Page 1 display results from 1 to 10");
                    } else {
                        logger.log(LogStatus.ERROR,"ERROR: Page 1 doesn't display results from 1 to 10: " + waitingForApprovalPage.pageInfo.getAttribute("textContent"));
                    }
                    click(pageNumberList.get(1)); //Click page 2
                    WaitForElement(waitingForApprovalPage.firstIdentifierTableResult,5);
                    logger.log(LogStatus.INFO,"Click page 2");
                    if(waitingForApprovalPage.pageInfo.getAttribute("textContent").contains("11 to 20")){
                        logger.log(LogStatus.INFO,"Page 2 display results from 11 to 20");
                    } else {
                        logger.log(LogStatus.ERROR,"ERROR: Page 2 doesn't display results from 11 to 20: " + waitingForApprovalPage.pageInfo.getAttribute("textContent"));
                    }
                    pageNumberList = waitingForApprovalPage.paginationMenu.findElements(By.tagName("a"));
                    //Indexes change between multiple clicks
                    click(pageNumberList.get(3)); //Click page 3
                    WaitForElement(waitingForApprovalPage.firstIdentifierTableResult,5);
                    logger.log(LogStatus.INFO,"Click page 3");
                    if(waitingForApprovalPage.pageInfo.getAttribute("textContent").contains("21")){
                        logger.log(LogStatus.INFO,"Page 3 display results from 21");
                    } else {
                        logger.log(LogStatus.ERROR,"ERROR: Page 3 doesn't display results from 21 to 30: " + waitingForApprovalPage.pageInfo.getAttribute("textContent"));
                    }
                    pageNumberList = waitingForApprovalPage.paginationMenu.findElements(By.tagName("a"));
                    click(pageNumberList.get(1)); //Click page 1
                    WaitForElement(waitingForApprovalPage.firstIdentifierTableResult,5);
                    logger.log(LogStatus.INFO,"Click page 1");
                } else {
                    logger.log(LogStatus.INFO,"There are not enough forms in the filter to test multiple pages");
                }

            } else {
                logger.log(LogStatus.INFO,"Next button is not displayed");
            }
            if(waitingForApprovalPage.numberResultByPageSelectionDroplist.getAttribute("textContent").contains("10")
                    && waitingForApprovalPage.numberResultByPageSelectionDroplist.getAttribute("textContent").contains("25")
                    && waitingForApprovalPage.numberResultByPageSelectionDroplist.getAttribute("textContent").contains("50")
                    && waitingForApprovalPage.numberResultByPageSelectionDroplist.getAttribute("textContent").contains("100")){

                logger.log(LogStatus.INFO,"Number of result by page can be modify between {10, 25, 50, 100} rows");
            } else {
                logger.log(LogStatus.ERROR,"ERROR: Number of result by page cannot be modify between {10, 25, 50, 100} rows");
            }
            if(isElementPresent(waitingForApprovalPage.previousButton)) {
                //case of more than 10 cats
                //previous button is displayed
                logger.log(LogStatus.INFO, "Previous button is displayed");
            } else {
                logger.log(LogStatus.ERROR, "ERROR: Previous button is not displayed");
            }


        } else {
            logger.log(LogStatus.INFO,"There are not enough forms in the filter to test more than 10 pages");
        }
        ascendingSortCheck(waitingForApprovalPage, startPage);
        descendingSortCheck(waitingForApprovalPage, startPage);
        checkSearchField(waitingForApprovalPage,startPage);
        checkSelectAndUnSelectLine(waitingForApprovalPage,startPage);
        checkButtonMenu(waitingForApprovalPage,startPage,registerPage, eazyIdPage, modifyPage);
    }
}
