package ElementaryScripts.Web;

import DataBaseFramework.SqlMySqlMiddleware;
import Screens.*;
import com.relevantcodes.extentreports.LogStatus;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class GenerateRegistration extends FilterPage {
    private static Pattern pattern;
    private static Matcher matcher;

    public void run() throws Exception {
        try {
//            WebHomepage homePage = new WebHomepage(driver);

//            for(int i = 0; i < 100; i++){
//                setText(homePage.SearchField,"123456789500169");
//                click(homePage.submitButton);
//                WaitForElement(homePage.homePage,10);
//                click(homePage.returnHomePageButton);
//                logger.log(LogStatus.INFO," itteration: " + i + "\u037E");
//            }


            WebStartPage startPage = new WebStartPage(driver);
            WebRegisterPage registerPage = new WebRegisterPage(driver);
            WebApprovedPage approvedPage = new WebApprovedPage(driver);
            WebModifyPage modifyPage = new WebModifyPage(driver);
            WebBiTriggerPage triggerPage = new WebBiTriggerPage(driver);
            SqlMySqlMiddleware mySql = new SqlMySqlMiddleware(logger, "bus-bi-test", "MweneZWLsmKDa55T", "jdbc:mysql://192.168.1.9:3306");

            List<String> listIdentifiers = new ArrayList<String>();
            for(int i = 0; i < 3; i++){
//                registerNewBreederOrRefugeCat(registerPage, startPage);
                registerNewVetCatToPro(registerPage, startPage, "HK43214321", false);
                listIdentifiers.add(getLastIdentifier());
            }
            sleep(5000);
            this.setBaseUrl("http://192.168.17.21:8080/bi-lz/ui/service/schedules");
            OpenBaseURL();
            driver.manage().window().maximize();
            WaitForElement(triggerPage.runButton,60);
            click(triggerPage.runButton);
            WaitForElement(triggerPage.endBiMessageTrigger,60);

//            for(int i = 0; i < 50; i++){
//                if(registrator.equals("Breeder 4")){
//                    transferCatWithSpecifiKeyWord(startPage,approvedPage,modifyPage,"HK852456",i,"TC00");
//                } else {
//                    transferCatWithSpecifiKeyWord(startPage,approvedPage,modifyPage,"HK159357",i,"TC00");
//                }
//
//            }

//            ArrayList<HashMap> rs = mySql.executeSqlQuery("SELECT * FROM `bus-bi-preprod`.BiEvent WHERE applicationCode = 'CatID' ORDER BY ID DESC LIMIT 100");

//            ArrayList<HashMap> rs = mySql.executeSqlQuery("SELECT * FROM `bus-bi-preprod`.BiEvent ORDER BY ID DESC LIMIT 100");
//            ArrayList<HashMap> rs = mySql.executeSqlQuery("SELECT * FROM `bus-bi-test`.BiEvent WHERE applicationCode = 'CatID' ORDER BY ID DESC LIMIT 100");
//            if (rs.size() == 0) {
//                logger.log(LogStatus.ERROR, "Something is wrong with SQL statement or DB connection");
//            } else {
//                int i = 0;
//                for(HashMap map : rs){
//                    Set set = map.entrySet();
//                    Iterator iterator = set.iterator();
//                    System.out.println("##################################################################################");
//                    while(iterator.hasNext()){
//                        Map.Entry mentry = (Map.Entry)iterator.next();
//                        System.out.println("key is: "+ mentry.getKey() + " & Value is: " + mentry.getValue());
//                    }
//                    System.out.println("##################################################################################");
//                }
//            }
            ArrayList<HashMap> rs;
            pattern = Pattern.compile("<requestId>(.*)</requestId>");
            String entityId;

            //BI Registration
            for(String identifier : listIdentifiers){
                //Get the create action where the cat identifier occurs
                rs = mySql.executeSqlQuery("SELECT * FROM `bus-bi-preprod`.BiEvent WHERE applicationCode = 'CatID' AND DATA LIKE '%" + identifier + "%' AND LABEL = 'CREATE_ACTION' ORDER BY ID DESC LIMIT 100");
                if (rs.size() == 2) { // Get Actions NEW_ANIMAL and IDENTIFIER_STATUS
                    matcher = pattern.matcher((String)rs.get(0).get("data"));
                    logger.log(LogStatus.INFO,(String)rs.get(0).get("data"));
                    if(matcher.find()){
                        entityId = matcher.group(1);
                        logger.log(LogStatus.INFO,"The CREATE_ACTION: NEW_ANIMAL and IDENTIFIER_STATUS linked to " + entityId + " are created");
                        //Get the other action linked to the cat identifier and the BI entityID linked to it
                        rs = mySql.executeSqlQuery("SELECT * FROM `bus-bi-preprod`.BiEvent WHERE applicationCode = 'CatID' AND DATA LIKE '%" + entityId + "%' AND DATA LIKE '%ANIMAL_STATUS%' ORDER BY ID DESC LIMIT 100");
                        if (rs.size() == 1) { // Get Actions NEW_ANIMAL and IDENTIFIER_STATUS
                            logger.log(LogStatus.INFO,"The CREATE_ACTION: ANIMAL_STATUS linked to " + entityId + " is created");
                        } else {
                            logger.log(LogStatus.ERROR,"ERROR: DB statement for ANIMAL_STATUS failed");
                        }
                        //Get the Original request linked to the identifier
                        rs = mySql.executeSqlQuery("SELECT * FROM `bus-bi-preprod`.BiEvent WHERE applicationCode = 'CatID' AND ENTITYID = '" + entityId + "' AND LABEL = 'CREATE_REQUEST'  ORDER BY ID DESC LIMIT 100");
                        if (rs.size() == 1) { // Get Actions NEW_ANIMAL and IDENTIFIER_STATUS
                            logger.log(LogStatus.INFO,"The CREATE_REQUEST with ID: " + entityId + " is created");
                        } else {
                            logger.log(LogStatus.ERROR,"ERROR: DB statement for CREATE_REQUEST failed");
                        }
                    } else {
                        logger.log(LogStatus.ERROR,"ERROR: No entityId found");
                    }

                } else {
                    logger.log(LogStatus.ERROR,"ERROR: DB statement for  NEW_ANIMAL and IDENTIFIER_STATUS failed");
                }
            }

            //logout for next page
//            click(startPage.logoutButton);
        }catch (Exception e) {
            logger.logScreenshot(LogStatus.ERROR, "ERROR stopping the test", getScreenShot());
            throw e;
        }
    }
}
