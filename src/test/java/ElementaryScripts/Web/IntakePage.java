package ElementaryScripts.Web;

import ElementaryScripts.General.AbstractCatIdElementaryScript;
import Screens.*;
import com.relevantcodes.extentreports.LogStatus;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by thierry.foulon on 12/5/2017.
 */
public class IntakePage extends AbstractCatIdElementaryScript {
    public void run() throws Exception {
        try {
            WebIntakePage intakePage = new WebIntakePage(driver);
            WebEazyIdPage eazyIdPage = new WebEazyIdPage(driver);
            WebStartPage startPage = new WebStartPage(driver);
            WebRegisterPage registerPage = new WebRegisterPage(driver);

            //Modification check for Veterinary
            WaitForElement(startPage.registerTabButton, 60);

//            //Operation can be reached from:
//            //Menu – Modify (Tab)
//            click(startPage.intakeRefugeActionButton);
//            WaitForElement(intakePage.searchField, 60);
//            click(startPage.homeButton);

            //Operation can be reached from:
            //Button – Modify (Action)
            click(startPage.intakeRefugeTabButton);
            WaitForElement(intakePage.searchField, 60);

            //Register a new cat for modify purpose
            registerNewCat(registerPage, startPage, eazyIdPage);

            //Identify a Cat
            checkIdentifyCat(intakePage,startPage);

            //Check adopter
            checkChangeResponsibleInformation(intakePage, startPage,eazyIdPage);
            //logout for next page
            click(startPage.logoutButton);
        }catch (Exception e) {
            logger.logScreenshot(LogStatus.ERROR, "ERROR stopping the test", getScreenShot());
            throw e;
        }
    }

    private void checkFieldsSecondResponsibleFromPrivatePerson(WebIntakePage intakePage, String privatePerson){
        //All mandatory fields are filled (meaning the RRN is fetch)
        WaitForElement(intakePage.adopterFirstName, 60);
        if(isFilled(intakePage.adopterFirstName) &&
                isFilled(intakePage.adopterLastName) &&
                isFilled(intakePage.adopterNationalNumberField)){
            logger.log(LogStatus.INFO, "Fields are filled for " + privatePerson);
        } else {
            logger.log(LogStatus.ERROR, "Fields are not filled for " + privatePerson);
        }

        //Address fields are not editable
        //Firt Name
        //Last Name
        //RRN
        if(!isEditable(intakePage.adopterNationalNumberField)){
            logger.log(LogStatus.INFO, "Fields are  non-editable for  " + privatePerson);
        } else {
            logger.log(LogStatus.ERROR, "Fields are editable for " + privatePerson);
        }
        //Address fields are editable
        //Postal code
        //Street
        //Number
        //Phone
        //Mobile
        //Fax
        //Email
        if(isEditable(intakePage.adopterCity) &&
                isEditable(intakePage.adopterStreet) &&
                isEditable(intakePage.adopterStreetNumber) &&
                isEditable(intakePage.adopterPhone) &&
                isEditable(intakePage.adopterMobile) &&
                isEditable(intakePage.adopterFax) &&
                isEditable(intakePage.adopterMail)){
            logger.log(LogStatus.INFO, "Fields are editable for  " + privatePerson);
        } else {
            logger.log(LogStatus.ERROR, "Fields are non-editable for " + privatePerson);
        }
    }

    private void checkChangeResponsibleInformation(WebIntakePage intakePage, WebStartPage startPage, WebEazyIdPage eazyIdPage){
        reloadIntakePageWithCatIdentifier(intakePage,startPage);
        WaitForElement(intakePage.adopterDropDown,5);
        clickHiddenElement(intakePage.adopterDropDown);

        //From Regular
        //Fetch information details is displayed
        //User can fetch by RRN
        setText(intakePage.adopterPrivaPersonSearchField, "71715100070");
        clickHiddenElement(intakePage.adopterSearchButton);

        checkFieldsSecondResponsibleFromPrivatePerson(intakePage, " RRN 71715100070");
        //Clear all fields to make sure the fields are filled
        if(registrator.equals("Veterinary")){
            click(intakePage.adopterClearVetButton);
        } else {
            click(intakePage.adopterClearButton);
        }
        //User can fetch from EID card
        click(intakePage.adopterReadDataDromEidCardButton);
        click(eazyIdPage.consentCheckbox);
        click(eazyIdPage.agreeButton);
        checkFieldsSecondResponsibleFromPrivatePerson(intakePage, "eID card");

        //Check confidential flag for private person
        click(intakePage.adopterConfidentialFlag);
        //Check the confidential flag is enable
        String isCheckedString = intakePage.adopterConfidentialFlag.getAttribute("class");
        if(isCheckedString.equals("ui toggle checkbox checked")){
            logger.logScreenshot(LogStatus.INFO, "Confidential flag is enable", getScreenShot());
        } else {
            logger.logScreenshot(LogStatus.ERROR, "Confidential flag is disable: " + isCheckedString + " ...", getScreenShot());
        }

        click(intakePage.adopterConfidentialFlag);
        //Check the confidential flag is disable
        isCheckedString = intakePage.adopterConfidentialFlag.getAttribute("class");
        if(isCheckedString.equals("ui toggle checkbox")){
            logger.logScreenshot(LogStatus.INFO, "Confidential flag is disable", getScreenShot());
        } else {
            logger.logScreenshot(LogStatus.ERROR, "Confidential flag is enable: " + isCheckedString + " ...", getScreenShot());
        }

        //transfer date must be defined
        isFilled(intakePage.adopterTransfertDate);
    }

    private void checkIdentifyCat(WebIntakePage intake, WebStartPage startPage){
        WebWaitingForApprovalPage waiting = new WebWaitingForApprovalPage(driver);
        WebApprovedPage approved = new WebApprovedPage(driver);
        String errorMessage;

        //Wait for the conclusion of CATREG-673
        //As conclusion from CATREG-673, the cat in Waiting for approval  is now visible from Intake

//        //user encode a full valid "waiting for approval"and click search
//        //no animals is fetch
//        //message animal not found is displayed
//        click(startPage.filterButton);
//        if(registrator.equals("Veterinary")){
//            click(startPage.filterWaitingForApprovalVetTabButton);
//        } else {
//            click(startPage.filterWaitingForApprovalBreederAndRefugeTabButton);
//        }
//
//        String waitingForApprovalIdentifier = waiting.firstIdentifierTableResult.getAttribute("textContent");
//        click(startPage.intakeRefugeTabButton);
//        WaitForElement(intake.searchField,60);
//        setText(intake.searchField,waitingForApprovalIdentifier);
//        click(intake.buttonSearch);
//        try{
//
//            WaitForElement(intake.errorLabelNoSearchFound,5);
//            errorMessage = intake.errorLabelNoSearchFound.getAttribute("textContent").replaceAll("\\s+","");
//            if(errorMessage.equals("Animalnotfound")){//There were a lot a useless space, so they were removed
//                logger.log(LogStatus.INFO, "\"Animal not found\" is displayed for waiting for an approval identifier");
//            } else {
//                logger.log(LogStatus.ERROR, "ERROR: \"Animal not found\" is not displayed for waiting for an approval identifier");
//            }
//        } catch (Exception e){
//            logger.log(LogStatus.ERROR, "ERROR: Animal in waiting for Approval is detected, please check if the animal is visible in Approved list");
//        }


        //user encode a not full valid identifier number and click search
        //no data must be fetched
        setText(intake.searchField,"123");
        click(intake.buttonSearch);
        if(WaitForElement(intake.errorLabelNoSearchFound,5)) {
            errorMessage = intake.errorLabelNoSearchFound.getAttribute("textContent").replaceAll("\\s+", "");
            if (errorMessage.equals("Animalnotfound")) {//There were a lot a useles space, so they were removed
                logger.log(LogStatus.INFO, "\"Animal not found\" is displayed for an incorrect identifier");
            } else {
                logger.log(LogStatus.ERROR, "ERROR: \"Animal not found\" is not displayed for an incorrect identifier");
            }
        }else{
            logger.log(LogStatus.ERROR, "ERROR: A result is displayed with incomplete identifier");
        }

        //user encode a full valid approved identifier number and click search
        //the registration page is displayed
        click(startPage.filterButton);
        if(registrator.equals("Veterinary")){
            click(startPage.filterApprovedVetTabButton);
        } else {
            click(startPage.filterApprovedBreederAndRefugeTabButton);
        }

        String approvedlIdentifier = approved.firstIdentifierTableResult.getAttribute("textContent");
        click(startPage.intakeRefugeTabButton);
        WaitForElement(intake.searchField,60);
        setText(intake.searchField,approvedlIdentifier);
        click(intake.buttonSearch);

        try {
            WaitForElement(intake.adopterDropDown,5);
            logger.log(LogStatus.INFO, "Approved cat identifier is found");

        }catch (Exception e){
            logger.log(LogStatus.ERROR, "ERROR: Approved cat identifier is not found");
        }
    }

    private void reloadIntakePageWithCatIdentifier(WebIntakePage intakePage, WebStartPage startPage){
        click(startPage.intakeRefugeTabButton);
        WaitForElement(intakePage.searchField,60);
        setText(intakePage.searchField,catIdentifier); //use one unique identifier
        click(intakePage.buttonSearch);
    }

    private void registerNewCat(WebRegisterPage registerPage, WebStartPage startPage, WebEazyIdPage eazyIdPage) throws Exception {
        click(startPage.registerTabButton);
        WaitForElement(registerPage.transfertOfResponsibility,60);
        //Fill First Responsible Info
        if(registrator.equals("Veterinary") ){
            setText(registerPage.refNumberTextFieldFirstOwner, "HK43214321");
            click(registerPage.selectFisrtResponsibleToolTip);
        }

        ///!\/!\/!\/!\/!\/!\ Make sure the first cat identifier is available in the DB   /!\/!\/!\/!\/!\/!\/!\/!\
        // The cat identifier available has to match with the last line of src/test/java/Resources/IdentifierList
        //Fill Cat identification

        fillCatIdentifier(registerPage.firstIdentifier);
        catIdentifier = registerPage.firstIdentifier.getAttribute("value").replace("-", "");
        String passportNumberDate = new SimpleDateFormat("dd-MM-yyyy_HH-mm-ss").format(new Date());
        String passportText = tcFeatureListNumber  + "_" + passportNumberDate;
        logger.log(LogStatus.INFO,passportText);
        setText(registerPage.passportNumber, passportText);

        //Fill cat identification
        String timeStamp = new SimpleDateFormat("dd.MM.yyyy").format(new Date());
        setText(registerPage.birthDate,timeStamp);
        setText(registerPage.identificationDate,timeStamp);
        setText(registerPage.sterilizationDate, timeStamp);
        setText(registerPage.colorCatTextField, "black");
        setText(registerPage.nameCatTextField,"Minuit");
        setText(registerPage.breedCatTextField, "Ab");
        click(registerPage.selectBreedTooltip);

        //Submit the form
        if(registrator.equals("Veterinary")){
            click(registerPage.submitButtonVet);
        } else {
            click(registerPage.submitButtonOther);
        }

        WaitForElement(registerPage.signVetNowOrAccrSendVetButton, 60);
        click(registerPage.signVetNowOrAccrSendVetButton);

        if(registrator.equals("Veterinary")){
            eazyId(eazyIdPage);
        }

        click(eazyIdPage.closeValidSubmitMessage);

        if(!registrator.equals("Veterinary")){
            click(startPage.logoutButton);
            //A breeder or refuge needs to make their registration be validated by a Vet
            OpenHomepage openHomepage = new OpenHomepage();
            openHomepage.setDriver(driver);
            openHomepage.setLogger(logger);
            openHomepage.run();
            Login loginpopup = new Login();
            loginpopup.setUserRole("Veterinary");
            loginpopup.setDriver(driver);
            loginpopup.setLogger(logger);
            loginpopup.run();
            WaitingForApprovalCatRegistration waiting = new WaitingForApprovalCatRegistration();
            waiting.setCatIdentifier(catIdentifier);
            waiting.setDriver(driver);
            waiting.setLogger(logger);
            waiting.run();
            //Re-log as non vet
            openHomepage.run();
            loginpopup.setUserRole(registrator);
            loginpopup.run();

        }
        click(startPage.intakeRefugeTabButton);
    }
}
