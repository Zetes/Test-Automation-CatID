package ElementaryScripts.Web;

import ElementaryScripts.General.AbstractCatIdElementaryScript;
import Screens.WebEazyIdPage;
import Screens.WebHomepage;
import Screens.WebLoginpage;
import Screens.WebStartPage;
import com.relevantcodes.extentreports.LogStatus;

public class Login extends AbstractCatIdElementaryScript{

	public String userRole = "Veterinary"; //Veterinary, Breeder, Refuge

	public boolean checkValidPin = false;

	public String getUserRole(){
		return userRole;
	}

	public void setUserRole(String userRole){
		this.userRole = userRole;
	}
	public void setCheckValidPin(boolean checkValidPin) { this.checkValidPin = checkValidPin; }

	public void run() throws Exception {
		try {
			WebHomepage homepage = new WebHomepage(driver);
			WebLoginpage loginpage = new WebLoginpage(driver);
			WebEazyIdPage eazyIdPage = new WebEazyIdPage(driver);
			WebStartPage startPage = new WebStartPage(driver);

			//Select default language if necessary
            try{
                WaitForElement(homepage.englishDefaultSelectionButton,2);
				logger.log(LogStatus.INFO, "Select English as default language");
				click(homepage.englishDefaultSelectionButton);
			} catch (Exception e) {
                logger.log(LogStatus.INFO, "No selection default language is required");
            }

			//Sometimes for unknown reason the click on logout is not recognize even if the button is clickable...
			try{
				WaitForElement(homepage.loginButton, 60);
			} catch (Exception e) {
				//If the login button is not visible after the WaitFor it is mostly because the previous logout didn't work
				click(startPage.logoutButton);
				WaitForElement(homepage.loginButton, 60);
				logger.log(LogStatus.ERROR,e.getMessage());
			}

			logger.log(LogStatus.INFO, "Click on login button");
			click(homepage.loginButton);

			//Check agreements + invalid pin code
			//EXPECTED : user is prompted to retry until 3 times
			//Check agreements + valid pin code

			eazyId(eazyIdPage,checkValidPin);

			WaitForElement(loginpage.vetDropdown, 60);
			//User has multiple user references
			click(loginpage.vetDropdown);
			if (getUserRole().equals("Veterinary")) {
				logger.log(LogStatus.INFO, "Select Veterinary F0032");
				click(loginpage.vet1Selection);
				logger.log(LogStatus.INFO, "Confirm Veterinary F0032");
			} else if (getUserRole().equals("Veterinary 2")) {
				logger.log(LogStatus.INFO, "Select Veterinary F0024");
				click(loginpage.vet2Selection);
				logger.log(LogStatus.INFO, "Confirm Veterinary F0024");
			} else if (getUserRole().equals("Veterinary 3")) {
				logger.log(LogStatus.INFO, "Select Veterinary F1560");
				click(loginpage.vet3Selection);
				logger.log(LogStatus.INFO, "Confirm Veterinary F1560");
			}else if (getUserRole().equals("Breeder 1")) {
				logger.log(LogStatus.INFO, "Select Breeder HK43214321");
				click(loginpage.breeder1Selection);
				logger.log(LogStatus.INFO, "Confirm Select Breeder HK43214321");

			} else if (getUserRole().equals("Breeder 2")) {
				logger.log(LogStatus.INFO, "Select Breeder BR88888888");
				click(loginpage.breeder2Selection);
				logger.log(LogStatus.INFO, "Confirm Select Breeder BR88888888");

			} else if (getUserRole().equals("Breeder 3")) {
				logger.log(LogStatus.INFO, "Select Breeder HK10101235");
				click(loginpage.breeder3Selection);
				logger.log(LogStatus.INFO, "Confirm Select Breeder HK10101235");

			} else if (getUserRole().equals("Breeder 4")) {
				logger.log(LogStatus.INFO, "Select Breeder HK159357");
				click(loginpage.breeder4Selection);
				logger.log(LogStatus.INFO, "Confirm Select Breeder HK159357");

			} else if (getUserRole().equals("Refuge 1")) {
				logger.log(LogStatus.INFO, "Select Refuge HK12341234");
				click(loginpage.refuge1Selection);
				logger.log(LogStatus.INFO, "Confirm Refuge HK12341234");
			} else if (getUserRole().equals("Refuge 2")) {
				logger.log(LogStatus.INFO, "Select Refuge RE88888888");
				click(loginpage.refuge2Selection);
				logger.log(LogStatus.INFO, "Confirm Refuge RE88888888");
			}else if (getUserRole().equals("Refuge 3")) {
				logger.log(LogStatus.INFO, "Select Refuge HK852456");
				click(loginpage.refuge3Selection);
				logger.log(LogStatus.INFO, "Confirm Refuge HK852456");
			}

	//		WaitForElement(loginpage.vetDropdownContainer, 60);
	//		logger.log(LogStatus.INFO, "VetDropdownContainer visible");
	//		click(loginpage.vetDropdownContainer);
	//
	//		selectDropDown(loginpage.vetDropdown, "F0032");

			click(loginpage.selectVetButton);

			if(checkValidPin){
				//User is prompted to choose between his references
				//References are only the ones of the RRN used
				//User has  only one reference
				//User come to the home page
				//EXPECTED : The selected user reference is displayed
				//EXPECTED : the available quota number is displayed
				String user = "";
				if (getUserRole().equals("Veterinary")) {
					user = "F0032";
				} else if (getUserRole().equals("Breeder 1")) {
					user = "HK43214321";
				} else if (getUserRole().equals("Refuge 1")) {
					user = "HK12341234";
				}
				if(startPage.quota.getAttribute("textContent").contains("quota") && startPage.username.getAttribute("textContent").contains(user)){
					logger.log(LogStatus.INFO, "The selected user reference is displayed and the available quota number is displayed ");
				} else{
					logger.log(LogStatus.ERROR, "The selected user reference is not displayed or the available quota number is not displayed ");
				}
			}

			logger.log(LogStatus.INFO, "LOGIN done");
		}catch (Exception e) {
			logger.logScreenshot(LogStatus.ERROR, "ERROR stopping the test", getScreenShot());
			throw e;
		}
	}

}
