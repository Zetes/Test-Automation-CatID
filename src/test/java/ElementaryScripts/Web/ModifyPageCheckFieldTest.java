package ElementaryScripts.Web;

import Screens.*;
import com.relevantcodes.extentreports.LogStatus;
import org.openqa.selenium.support.ui.Select;
import DataBaseFramework.SqlOracleMiddleware;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

public class ModifyPageCheckFieldTest extends ModifyPage{
    public void run() throws Exception {
        try {
            WebModifyPage modifyPage = new WebModifyPage(driver);
            WebEazyIdPage eazyIdPage = new WebEazyIdPage(driver);
            WebStartPage startPage = new WebStartPage(driver);
            WebRegisterPage registerPage = new WebRegisterPage(driver);
//            WebAccountSettingsPage accountSettingsPage = new WebAccountSettingsPage(driver);

//            SqlOracleMiddleware dogIdMiddleWare = new SqlOracleMiddleware(logger, "borabiec", "borabiec", "jdbc:oracle:thin:@192.168.17.6:1521:PREPROD");
//            SqlOracleMiddleware catIdMiddleWare = new SqlOracleMiddleware(logger, "CATID", "CATID", "jdbc:oracle:thin:@192.168.17.6:1521:PREPROD");

            //Modification check for Veterinary
            WaitForElement(startPage.registerTabButton, 60);

            //Operation can be reached from:
            //Menu – Modify (Tab)
            click(startPage.modifyTabButton);
            WaitForElement(modifyPage.searchField, 60);
//            click(startPage.homeButton);

//            //Operation can be reached from:
//            //Button – Modify (Action)
//            click(startPage.modifyActionButton);
//            WaitForElement(modifyPage.searchField, 60);

            //Register a new cat for modify purpose
            registerNewCat(registerPage, startPage, eazyIdPage);

            //Identify a Cat
            checkIdentifyCat(modifyPage,startPage);

            //Check label
            //All data must match the DB model
            checkAllDataMathDbModel(modifyPage, startPage);


            //Check modify page fields
            checkModifyPageFields(modifyPage, startPage, eazyIdPage);

            //Check change responsible
            checkChangeResponsibleInformation(modifyPage, startPage,eazyIdPage);

            //Check same user test
            userFirstAndSecondResponsibleAreDifferent(modifyPage,startPage,eazyIdPage);

            //logout for next page
            click(startPage.logoutButton);
        }catch (Exception e) {
            logger.logScreenshot(LogStatus.ERROR, "ERROR stopping the test", getScreenShot());
            throw e;
        }
    }

    private void checkModifyPageFields(WebModifyPage modifyPage, WebStartPage startPage, WebEazyIdPage eazyIdPage){
        click(startPage.modifyTabButton);
        WaitForElement(modifyPage.searchField,60);
        if(catIdentifier.equals("")){
            catIdentifier = "123456789416029";
        }
        setText(modifyPage.searchField,catIdentifier); //use one unique identifier
        click(modifyPage.buttonSearch);
        WaitForElement(modifyPage.changeResponsibleDropDown,5);
        String currentDate = new SimpleDateFormat("dd-MM-yyyy").format(new Date());
        Calendar calendar = Calendar.getInstance();
        Date tomorrowDate = new Date();
        calendar.setTime(tomorrowDate);
        calendar.add(Calendar.DATE, 1);
        tomorrowDate = calendar.getTime();
        String tomorrowDateTimeStamp = new SimpleDateFormat("dd.MM.yyyy").format(tomorrowDate);
        if(registrator.equals("Veterinary")) {
            //User can select a status
            //by selecting value in listbox Identifier status
            click(modifyPage.firstCatIdentifierStatusDropdownContainerVet);
            //values are
            //Valid Identifier
            //Unreadable
            String statusIdentifier = modifyPage.firstCatIdentifierStatusVet.getAttribute("textContent");

            if (statusIdentifier.contains("Valid") && statusIdentifier.contains("Unreadable")) {
                logger.log(LogStatus.INFO, "User can select a status between values Valid and Unreadable");
                sleep(1000);
                getScreenShot();
            } else {
                logger.log(LogStatus.ERROR, "ERROR: User cannot select a status between values Valid and Unreadable");
            }
            //User has only one identifier defined for cat
            //user select valid identifier
            //form can be submitted
            click(modifyPage.firstCatIdentifierStatusDropdownContainerVet);
            clickHiddenElement(modifyPage.firstCatIdentifierStatusValidVet);
            checkDetectionValidSubmit(modifyPage, eazyIdPage, "Now");
            //user select unreadable
            //form cannot be submitted
            reloadModifyPageWithCatIdentifier(modifyPage, startPage);
            click(modifyPage.firstCatIdentifierStatusDropdownContainerVet);
            clickHiddenElement(modifyPage.firstCatIdentifierStatusUnreadableVet);
            if (!checkDetectionErrorMessage(modifyPage, eazyIdPage, "At least one of the identifiers must be valid.")) {
                reloadModifyPageWithCatIdentifier(modifyPage, startPage);
            }

            setText(modifyPage.searchField, catIdentifier); //use one unique identifier
            click(modifyPage.buttonSearch);
            //user can add second identifier
            //by clicking on the add identifier link
            try {
                click(modifyPage.addSecondCatIdentifierDropDownVet);
                WaitForElement(modifyPage.addCatIdentifierIdentificationDateVet, 5);
                logger.log(LogStatus.INFO, "User can add a second identifier");
            } catch (Exception e) {
                logger.log(LogStatus.ERROR, "ERROR: User cannot add a second identifier");
            }
            //user select id location
            click(modifyPage.newIdentifierLocationContainerVet);
            click(modifyPage.newIdentifierLocationRightEarVet);
            //user select identification date
            setText(modifyPage.addCatIdentifierIdentificationDateVet, tomorrowDateTimeStamp);
            fillCatIdentifier(modifyPage.addCatIdentifierFieldVet);
            if (!checkDetectionErrorMessage(modifyPage, eazyIdPage, "Date cannot be in future")) {
                reloadModifyPageWithCatIdentifier(modifyPage, startPage);
            }
            setText(modifyPage.addCatIdentifierIdentificationDateVet, currentDate);
            //user select valid identifier for first identifier
            click(modifyPage.firstCatIdentifierStatusDropdownContainerVet);
            clickHiddenElement(modifyPage.firstCatIdentifierStatusValidVet);
            getScreenShot();
            //user select unreadable for first identifier
            click(modifyPage.firstCatIdentifierStatusDropdownContainerVet);
            clickHiddenElement(modifyPage.firstCatIdentifierStatusUnreadableVet);
            getScreenShot();
            fillCatIdentifier(modifyPage.newIdentifierVet);
            checkDetectionValidSubmit(modifyPage, eazyIdPage, "Now");

            //User has two identifier defined for cat
            reloadModifyPageWithCatIdentifier(modifyPage, startPage);
            //user can set identifier 1 valid
            click(modifyPage.firstCatIdentifierStatusDropdownContainerVet);
            clickHiddenElement(modifyPage.firstCatIdentifierStatusValidVet);
            //user can set identifier 2 unreadable
            click(modifyPage.secondCatIdentifierStatusDropdownContainerVet);
            clickHiddenElement(modifyPage.secondCatIdentifierStatusUnreadableVet);
            //form can be submitted
            checkDetectionValidSubmit(modifyPage, eazyIdPage, "Now");
            reloadModifyPageWithCatIdentifier(modifyPage, startPage);
            //user can set identifier 2 valid
            click(modifyPage.secondCatIdentifierStatusDropdownContainerVet);
            clickHiddenElement(modifyPage.secondCatIdentifierStatusValidVet);
            //form can be submitted
            checkDetectionValidSubmit(modifyPage, eazyIdPage, "Now");
            reloadModifyPageWithCatIdentifier(modifyPage, startPage);
            //user can modify the date with value less or equal to current date
            setText(modifyPage.firstCatIdentificationDateVet, tomorrowDateTimeStamp);
            click(modifyPage.catPassportNumberVet);
            if (!checkDetectionErrorMessage(modifyPage, eazyIdPage, "Date cannot be in future")) {
                reloadModifyPageWithCatIdentifier(modifyPage, startPage);
            }
            setText(modifyPage.firstCatIdentificationDateVet, currentDate);
            //user can set identifier 1 unreadable
            click(modifyPage.firstCatIdentifierStatusDropdownContainerVet);
            clickHiddenElement(modifyPage.firstCatIdentifierStatusUnreadableVet);
            //user can set identifier 2 unreadable
            click(modifyPage.secondCatIdentifierStatusDropdownContainerVet);
            clickHiddenElement(modifyPage.secondCatIdentifierStatusUnreadableVet);
            //form cannot be submitted
            if (!checkDetectionErrorMessage(modifyPage, eazyIdPage, "At least one of the identifiers must be valid.")) {
                reloadModifyPageWithCatIdentifier(modifyPage, startPage);
            }
            //user can set identifier 2 valid
            click(modifyPage.secondCatIdentifierStatusDropdownContainerVet);
            clickHiddenElement(modifyPage.secondCatIdentifierStatusValidVet);
            //form can be submitted
            checkDetectionValidSubmit(modifyPage, eazyIdPage, "Now");
            reloadModifyPageWithCatIdentifier(modifyPage, startPage);
            //user can modify the date with value less or equal to current date
            setText(modifyPage.secondCatIdentifierDateVet, tomorrowDateTimeStamp);
            if (!checkDetectionErrorMessage(modifyPage, eazyIdPage, "Date cannot be in future")) {
                reloadModifyPageWithCatIdentifier(modifyPage, startPage);
            }
            setText(modifyPage.secondCatIdentifierDateVet, currentDate);
            //user can add a third identifier
            //by clicking on the add identifier link
            try {
                click(modifyPage.addThirdCatIdentifierDropDownVet);
                WaitForElement(modifyPage.addTextLabelAdvertissementForThirdValidIdVet, 5);
                logger.log(LogStatus.INFO, "User can add a third identifier");
            } catch (Exception e) {
                logger.log(LogStatus.ERROR, "ERROR: User cannot add a third identifier");
            }
            String message = modifyPage.addTextLabelAdvertissementForThirdValidIdVet.getAttribute("textContent").replaceAll("\\s+", "");
            if (message.equals("OnlyonevalidchipIDisallowed.")) {
                logger.log(LogStatus.INFO, "\"Only one valid chipID is allowed.\" message is displayed");
            } else {
                logger.log(LogStatus.ERROR, "ERROR: \"Only one valid chipID is allowed.\" message is not displayed");
            }

            //Identifier has one passport defined
            //user is able to change passport by clicking the "add passport" link
            click(modifyPage.addCatPassportDropDownVet);

            String originalRegisteredPassportNumber = modifyPage.catPassportNumberVet.getAttribute("value");
            String passportNumber = "";
            try {
                SqlOracleMiddleware catIdMiddleWare = new SqlOracleMiddleware(logger, "CATID", "CATID", "jdbc:oracle:thin:@192.168.17.6:1521:PREPROD");
                ArrayList<HashMap> rs = catIdMiddleWare.executeSqlQuery("SELECT PASSPORTNUMBER FROM ABIEC_PASSPORT WHERE ANIMALID IS NULL AND ANIMALID_PROJECT IS NULL AND GISTATUS = 'associated' OFFSET 20 ROWS FETCH NEXT 1 ROWS ONLY");
                if (rs.size() == 0) {
                    logger.log(LogStatus.ERROR, "No more passport available in GI. A Passport order needs to be bought (a bundle containing 10 passports, cf: https://wiki.zetes.com/confluence/pages/viewpage.action?pageId=92564433 subsection CATID – PREPROD – PAYMENT)");
                } else {
                    //new passport is a valid Belgian
                    passportNumber = (String) rs.get(0).get("PASSPORTNUMBER");
                    setText(modifyPage.addCatPassportNumberFieldVet, passportNumber);
                    checkDetectionValidSubmit(modifyPage, eazyIdPage, "Now");
                    logger.log(LogStatus.INFO, "New passport " + passportNumber + " is linked to identifier " + catIdentifier);
                    //passport is not linked to a dog


                    rs = catIdMiddleWare.executeSqlQuery("SELECT ANIMALID FROM ABIEC_PASSPORT WHERE PASSPORTNUMBER = '" + passportNumber + "'");
                    String catIdDbRsAnimalIDLinkedToPassport = null;
                    String dogIdDbRsAnimalIDLinkedToPassport = null;
                    String catIdDbRsAnimalIDLinkedToIdentifier = null;
                    String catIdDbRsAnimalIDLOriginalpassport = null;
                    if (rs.size() == 0) {
                        logger.log(LogStatus.ERROR, "CATID SQL error for " + passportNumber);
                    } else {
                        catIdDbRsAnimalIDLinkedToPassport = (String) rs.get(0).get("ANIMALID");
                    }
                    SqlOracleMiddleware dogIdMiddleWare = new SqlOracleMiddleware(logger, "borabiec", "borabiec", "jdbc:oracle:thin:@192.168.17.6:1521:PREPROD");
                    rs = dogIdMiddleWare.executeSqlQuery("SELECT ANIMALID FROM ABIEC_PASSPORT WHERE PASSPORTNUMBER = '" + passportNumber + "'");
                    if (rs.size() == 0) {
                        logger.log(LogStatus.ERROR, "DOGID SQL error for " + passportNumber);
                    } else {
                        dogIdDbRsAnimalIDLinkedToPassport = (String) rs.get(0).get("ANIMALID");
                    }

                    rs = dogIdMiddleWare.executeSqlQuery("SELECT ID FROM ABIEC_ANIMAL WHERE ID = '" + catIdDbRsAnimalIDLinkedToPassport + "'");
                    if (rs.size() == 0) { //Meaning no row found. If You look for a cat ID in Dog DB, the result set is empty
                        logger.log(LogStatus.INFO, "New passport " + passportNumber + " is not linked to a dog and it is linked to a cat");
                    } else {
                        logger.log(LogStatus.ERROR, "ERROR: New passport " + passportNumber + " is linked to a dog ");
                    }

                    rs = dogIdMiddleWare.executeSqlQuery("SELECT ANIMALID FROM ABIEC_PASSPORT WHERE PASSPORTNUMBER = '" + originalRegisteredPassportNumber + "'");
                    if (rs.size() == 0) {
                        logger.log(LogStatus.ERROR, "DOGID SQL error for " + passportNumber);
                    } else {
                        catIdDbRsAnimalIDLOriginalpassport = (String) rs.get(0).get("ANIMALID");
                    }
                    rs = dogIdMiddleWare.executeSqlQuery("SELECT * FROM ABIEC_ANIMAL WHERE ID = '" + catIdDbRsAnimalIDLOriginalpassport + "'");
                    if (rs.size() == 0) { //Meaning no row found. If You look for a cat ID in Dog DB, the result set is empty
                        logger.log(LogStatus.INFO, "Old passport " + originalRegisteredPassportNumber + " is not linked to the cat identifier " + catIdentifier + " anymore");
                    } else {
                        logger.log(LogStatus.ERROR, "ERROR: Old passport " + originalRegisteredPassportNumber + " is linked to the cat identifier " + catIdentifier + " yet");
                    }
                }
            } catch (SQLException e) {
                logger.log(LogStatus.ERROR, e.getMessage());
            }
            //form can be submitted

            reloadModifyPageWithCatIdentifier(modifyPage, startPage);
            //passport is linked to an existing dog
            //form cannot be submitted
            click(modifyPage.addCatPassportDropDownVet);
            setText(modifyPage.addCatPassportNumberFieldVet, "BE0234790082");
            if (!checkDetectionErrorMessage(modifyPage, eazyIdPage, " already used passport")) {
                clearWaitingOrSentForApprovalForm(startPage, "BE0234790082");
                sleep(1000);
            }
            reloadModifyPageWithCatIdentifier(modifyPage, startPage);
            //new passport is an invalid Belgian
            //form cannot be submitted
            click(modifyPage.addCatPassportDropDownVet);
            setText(modifyPage.addCatPassportNumberFieldVet, "BE0000000000");
            if (!checkDetectionErrorMessage(modifyPage, eazyIdPage, "Passport is no issue by Zetes")) {
                clearWaitingOrSentForApprovalForm(startPage, "BE0000000000");
                sleep(1000);
            }
            reloadModifyPageWithCatIdentifier(modifyPage, startPage);
            //new passport is foreigner
            //form can be submitted
            click(modifyPage.addCatPassportDropDownVet);
            setText(modifyPage.addCatPassportNumberFieldVet, "CH03001457882");
            if (checkDetectionValidSubmit(modifyPage, eazyIdPage, "Later")) {
                //new passport is linked to identifier
                try {
                    clearWaitingOrSentForApprovalForm(startPage, "CH03001457882");
                    logger.log(LogStatus.INFO, "New passport CH03001457882 is linked to identifier " + catIdentifier);
                } catch (Exception e) {
                    logger.log(LogStatus.ERROR, "New passport CH03001457882 is not linked to identifier " + catIdentifier);
                }
                //If the test is enable to delete this form, that mean the passport is correctly assigned to the cat
                sleep(1000);
            }
            reloadModifyPageWithCatIdentifier(modifyPage, startPage);

            //old passport is unlinked with identifier
            click(modifyPage.addCatPassportDropDownVet);
            setText(modifyPage.addCatPassportNumberFieldVet, "CH03001457883");
            if (checkDetectionValidSubmit(modifyPage, eazyIdPage, "Later")) {
                //new passport is linked to identifier
                try {
                    clearWaitingOrSentForApprovalForm(startPage, "CH03001457883");
                    logger.log(LogStatus.INFO, "New passport CH03001457883 is linked to identifier " + catIdentifier);
                } catch (Exception e) {
                    logger.log(LogStatus.ERROR, "New passport CH03001457883 is not linked to identifier " + catIdentifier);
                }
                //If the test is enable to delete this form, that mean the passport is correctly assigned to the cat
                sleep(1000);
            }

            reloadModifyPageWithCatIdentifier(modifyPage, startPage);
            click(modifyPage.addCatPassportDropDownVet);
            setText(modifyPage.addCatPassportNumberFieldVet, "CH03001457882");
            if (checkDetectionValidSubmit(modifyPage, eazyIdPage, "Later")) {
                //new passport is linked to identifier
                try {
                    clearWaitingOrSentForApprovalForm(startPage, "CH03001457882");
                    logger.log(LogStatus.INFO, "Old passport CH03001457883 is unlinked with identifier " + catIdentifier + " and foreign passport CH03001457882 is reusable");
                } catch (Exception e) {
                    logger.log(LogStatus.ERROR, "Old passport CH03001457883 is not unlinked with identifier " + catIdentifier);
                }
                //If the test is enable to delete this form, that mean the passport is correctly assigned to the cat
                sleep(1000);
            }
            reloadModifyPageWithCatIdentifier(modifyPage, startPage);
        }
        //User can set a status
        //user can select a status from the dropdown list
        //list of status contains
        //none
        //lost
        //stolen
        //exported
        //dead
        String status = modifyPage.catStatus.getAttribute("textContent");
        if(registrator.equals("Veterinary")) {
            click(modifyPage.catVetStatusContainer);
        } else {
            click(modifyPage.catBreederOrRefugeStatusContainer);
        }
        if(status.contains("None") && status.contains("Lost") && status.contains("Stolen") && status.contains("Exported") && status.contains("Dead")){
            logger.log(LogStatus.INFO, "User can select a status between None, Lost, Stolen, Exported and Dead ");
            sleep(1000);
            getScreenShot();
            if(registrator.equals("Veterinary")) {
                clickHiddenElement(modifyPage.catStatusLostVet);
            } else {
                clickHiddenElement(modifyPage.catStatusLostBreederAndrefuge);
            }
        } else {
            logger.log(LogStatus.ERROR, "ERROR: User cannot select a status between None, Lost, Stolen, Exported and Dead ");
        }
        //user can select a status date
        //default value is current date
        String displayedDate = modifyPage.catStatusDate.getAttribute("value").replace("/","-");
        if(displayedDate.equals(currentDate)){
            logger.log(LogStatus.INFO, "Default value is current date");
        } else {
            logger.log(LogStatus.ERROR, "ERROR: Default value is not current date: " + displayedDate);
        }
        //by clicking on the field status date, a calendar popup is displayed
        //User can select up to current date
        checkDateUiCalendar(modifyPage.catStatusDate, modifyPage.statusDateBodyCalendar, modifyPage.statusDateHeadCalendar, "Status Date");

        if(registrator.equals("Veterinary")) {
            //form can be submitted
            checkDetectionValidSubmit(modifyPage,eazyIdPage,"Now");
            //user can update a status case a status exists
            reloadModifyPageWithCatIdentifier(modifyPage,startPage);
            click(modifyPage.catVetStatusContainer);
            // User select a new status from the dropdown list
            //Switch to None
            clickHiddenElement(modifyPage.catStatusNoneVet);
        } else {
            //form can be submitted
            checkDetectionValidSubmit(modifyPage,eazyIdPage,"Later");
            //user can update a status case a status exists
            reloadModifyPageWithCatIdentifier(modifyPage,startPage);
            click(modifyPage.catBreederOrRefugeStatusContainer);
            // User select a new status from the dropdown list
            //Switch to None
            clickHiddenElement(modifyPage.catStatusNoneBreederAndrefuge);
        }

        //date cannot be in the past
        //Select the date to yesterday
        calendar.add(Calendar.DATE, -2);
        Date yesterdayDate = calendar.getTime();
        String yesterdayDateTimeStamp = new SimpleDateFormat("dd.MM.yyyy").format(yesterdayDate);
        setText(modifyPage.catStatusDate,yesterdayDateTimeStamp);
        //form cannot be submitted

        if(checkDetectionErrorMessage(modifyPage,eazyIdPage,"New status date must be after the last status date")){
            logger.log(LogStatus.INFO, "Date cannot be in the past");
        } else {
            logger.log(LogStatus.ERROR, "ERROR: Date can be in the past: " + displayedDate);
            clearWaitingOrSentForApprovalForm(startPage,catIdentifier);
            sleep(1000);
        }
        reloadModifyPageWithCatIdentifier(modifyPage,startPage);
        //user can update the date od status
        setText(modifyPage.catStatusDate,currentDate);
        displayedDate = modifyPage.catStatusDate.getAttribute("value").replace("/","-");
        if(displayedDate.equals(currentDate)){
            logger.log(LogStatus.INFO, "User can update the date od status");
        } else {
            logger.log(LogStatus.ERROR, "ERROR: User cannot update the date od status: " + displayedDate);
        }
        reloadModifyPageWithCatIdentifier(modifyPage,startPage);
        //date cannot be in the future
        setText(modifyPage.catStatusDate,tomorrowDateTimeStamp);
        //form cannot be submitted
        if(!checkDetectionErrorMessage(modifyPage,eazyIdPage,"Date cannot be in future")){
            clearWaitingOrSentForApprovalForm(startPage,catIdentifier);
            sleep(1000);
        }
        reloadModifyPageWithCatIdentifier(modifyPage,startPage);
        if(registrator.equals("Veterinary")) {
            //identifier is a crossing
            //user can set the crossing to false
            //by putting the slider crossing on true
            //form can be submitted
            //signature is required
            click(modifyPage.crossingBreedCheckbox);
            checkDetectionValidSubmit(modifyPage, eazyIdPage, "Now", true);
            reloadModifyPageWithCatIdentifier(modifyPage, startPage);
            //identifier is a crossing
            //user can set the crossing to false
            //by putting the slider crossing on false
            //form can be submitted
            //signature is required
            click(modifyPage.crossingBreedCheckbox);
            checkDetectionValidSubmit(modifyPage, eazyIdPage, "Now", true);
        }
    }

    private void clearWaitingOrSentForApprovalForm(WebStartPage startPage, String keyWord){
        if(registrator.equals("Veterinary")){
            //Search for the form to delete
            click(startPage.filterButton);
            click(startPage.filterWaitingForApprovalVetTabButton);
            WebWaitingForApprovalPage wait = new WebWaitingForApprovalPage(driver);
            setText(wait.searchField, keyWord);
            //Wait for the update of the first line
            sleep(5000);
            click(wait.firstIdentifierTableResult);
            click(wait.deleteButton);
            click(wait.acceptDeletionButton);

        } else {
            //Search for the form to delete
            click(startPage.filterButton);
            click(startPage.filterSentForApprovalBreederAndRefugeTabButton);
            WebSentForApprovalPage sent = new WebSentForApprovalPage(driver);
            setText(sent.searchField, keyWord);
            //Wait for the update of the first line
            sleep(5000);
            click(sent.firstIdentifierTableResult);
            click(sent.deleteButton);
            click(sent.acceptDeletionButton);
        }
    }

    private void reloadModifyPageWithCatIdentifier(WebModifyPage modifyPage, WebStartPage startPage){
        click(startPage.modifyTabButton);
        WaitForElement(modifyPage.searchField,60);
        setText(modifyPage.searchField,catIdentifier); //use one unique identifier
        click(modifyPage.buttonSearch);
    }

    private void checkAllDataMathDbModel(WebModifyPage modifyPage, WebStartPage startPage){
        click(startPage.modifyTabButton);
        WaitForElement(modifyPage.searchField,60);

        if(registrator.equals("Veterinary")) {
            setText(modifyPage.searchField,"123456789416617"); //use one unique identifier
            click(modifyPage.buttonSearch);
            WaitForElement(modifyPage.changeResponsibleDropDown,5);
            Select catStatusDropdown = new Select(modifyPage.catStatus);
            //Cat Identification check
            Select firstIdentifierlocationDropdown = new Select(modifyPage.firstCatIdentifierLocationVet);
            Select firstStatusDropdown = new Select(modifyPage.firstCatIdentifierStatusVet);
            Select secondIdentifierlocationDropdown = new Select(modifyPage.secondCatIdentifierLocationVet);
            Select secondStatusDropdown = new Select(modifyPage.secondCatIdentifierStatusVet);

            if (modifyPage.firstCatIdentifierVet.getAttribute("value").equals("123-456-789-416-616")
                    && firstIdentifierlocationDropdown.getFirstSelectedOption().getAttribute("textContent").equals("Right ear")
                    && firstStatusDropdown.getFirstSelectedOption().getAttribute("textContent").equals("Unreadable")
                    && modifyPage.firstCatIdentificationDateVet.getAttribute("value").equals("14/11/2017")
                    && modifyPage.secondCatIdentifierVet.getAttribute("value").equals("123-456-789-416-617")
                    && secondIdentifierlocationDropdown.getFirstSelectedOption().getAttribute("textContent").equals("Left neck")
                    && secondStatusDropdown.getFirstSelectedOption().getAttribute("textContent").equals("Valid")
                    && modifyPage.secondCatIdentifierDateVet.getAttribute("value").equals("16/11/2017")
                    && modifyPage.catPassportNumberVet.getAttribute("value").equals("TC5_16-11-2017_09-32-59")) {
                logger.log(LogStatus.INFO, "Cat Identification data match with the DB model");
            } else {
                logger.log(LogStatus.ERROR, "ERROR: Cat Identification data don't match with the DB model");
            }

            //Cat information
            Select genderDropdown = new Select(modifyPage.catGender);

            //Double click on the 2 checkboxes because the initial value allowing to know if they are checked or not is always set at false when the page is loaded...
            //The checkbox cannot be check after loading the page, the value is always at "non-checked" but the graphic element is displayed as checked
            //Workarround to make sure it is correctly according to the DB, double click on it !
            String currentSterilizeDate = modifyPage.stereilizedDate.getAttribute("value").replaceAll("/", "-");
            click(modifyPage.sterilizedCheckbox);
            setText(modifyPage.stereilizedDate, currentSterilizeDate);//Reset the date re-enable the checkbox fot sterelization
            click(modifyPage.crossingBreedCheckbox);
            click(modifyPage.crossingBreedCheckbox);

            if (genderDropdown.getFirstSelectedOption().getAttribute("textContent").equals("Male")
                    && modifyPage.catBirthDate.getAttribute("value").equals("08/11/2017")
                    && modifyPage.stereilizedDate.getAttribute("value").equals("16/11/2017")
                    && modifyPage.sterilizedCheckbox.getAttribute("class").equals("ui toggle checkbox sterilized checked") //graphical bug after loading (17/11/2017)
                    && modifyPage.crossingBreedCheckbox.getAttribute("class").equals("ui toggle checkbox")
                    && modifyPage.catBreed.getAttribute("value").equals("Abyssinian")
                    && modifyPage.catName.getAttribute("value").equals("Minuit")
                    && modifyPage.catColor.getAttribute("value").equals("black")
                    && catStatusDropdown.getFirstSelectedOption().getAttribute("textContent").equals("None")
                    && modifyPage.catStatusDate.getAttribute("value").equals("16/11/2017")) {
                logger.log(LogStatus.INFO, "Cat information data match with the DB model");
            } else {
                logger.log(LogStatus.ERROR, "ERROR: Cat information data don't match with the DB model");
            }

            //Responsible information
            String refNum = "HK43214321";

            if (modifyPage.responsibleRefNumber.getAttribute("value").equals(refNum)
                    && modifyPage.responsibleFirstName.getAttribute("value").equals("Alice Geldigekaart")
                    && modifyPage.responsibleLastName.getAttribute("value").equals("SPECIMEN") //graphical bug after loading (17/11/2017)
                    && modifyPage.responsibleNationalNumber.getAttribute("value").equals("71.71.51-000.70")
                    && modifyPage.responsibleCity.getAttribute("value").equals("1930 Zaventem")
                    && modifyPage.responsibleStreet.getAttribute("value").equals("Excelsiorlaan")
                    && modifyPage.responsibleStreetNumber.getAttribute("value").equals("43")
                    && modifyPage.responsiblePhone.getAttribute("value").equals("+/797979")
                    && modifyPage.responsibleMobile.getAttribute("value").equals("+/797979")
                    && modifyPage.responsibleFax.getAttribute("value").equals("+/797979")
                    && modifyPage.responsibleMail.getAttribute("value").equals("thierry.foulon@zetes.com")) {
                logger.log(LogStatus.INFO, "Responsible information data match with the DB model");
            } else {
                logger.log(LogStatus.ERROR, "ERROR: Responsible information data don't match with the DB model");
            }
        } else{
            String identifier;
            String responsibleInfo;
            String passportNumber;
            if(registrator.equals("Breeder 1")){
                identifier = "123456789416617";
                responsibleInfo = "HK43214321 Alice Geldigekaart SPECIMEN";
                passportNumber = "TC5_16-11-2017_09-32-59";
            } else {
                identifier = "123456789416618";
                responsibleInfo = "HK12341234 Alice Geldigekaart SPECIMEN";
                passportNumber = "TC6_16-11-2017_09-33-38";
            }
            setText(modifyPage.searchField,identifier); //use one unique identifier
            click(modifyPage.buttonSearch);
            WaitForElement(modifyPage.changeResponsibleDropDown,5);
            Select catStatusDropdown = new Select(modifyPage.catStatus);
            //Responsible information
            if (modifyPage.responsibleInformationBreederOrRefuge.getAttribute("textContent").equals(responsibleInfo)
                    && modifyPage.responsibleAddressBreederOrRefuge.getAttribute("textContent").equals("Excelsiorlaan 43 Zaventem 1930")){
                logger.log(LogStatus.INFO, "Responsible information data match with the DB model");
            } else{
                logger.log(LogStatus.ERROR, "ERROR: Responsible information data don't match with the DB model");
            }

            //Cat identification
            String crossing = modifyPage.catCrossingBreederOrRefuge.getAttribute("textContent").replaceAll("\\s+", ""); //Clear all hidden chars and spaces
            String sterelized = modifyPage.catSterelizedFlagBreederOrRefuge.getAttribute("textContent").replaceAll("\\s+", "");
            if (modifyPage.catIdentifierBreederOrRefuge.getAttribute("textContent").equals(identifier)
                    && modifyPage.catPassportNumberBreederOrRefuge.getAttribute("textContent").equals(passportNumber)
                    && modifyPage.catIdentificationDateBreederOrRefuge.getAttribute("textContent").equals("16/11/2017")
                    && modifyPage.catBirthDateBreederOrRefuge.getAttribute("textContent").equals("08/11/2017")
                    && modifyPage.catGenderBreederOrRefuge.getAttribute("textContent").equals("Male")
                    && crossing.equals("No")
                    && modifyPage.catSterelizedDateBreederOrRefuge.getAttribute("textContent").equals("16/11/2017")
                    && sterelized.equals("Yes")) {
                logger.log(LogStatus.INFO, "Cat Identification data match with the DB model");
            } else {
                logger.log(LogStatus.ERROR, "ERROR: Cat Identification data don't match with the DB model");
            }

            //Cat information
            if (modifyPage.catName.getAttribute("value").equals("Minuit")
                    && modifyPage.catColor.getAttribute("value").equals("black")
                    && catStatusDropdown.getFirstSelectedOption().getAttribute("textContent").equals("None")
                    && modifyPage.catStatusDate.getAttribute("value").equals("16/11/2017")) {
                logger.log(LogStatus.INFO, "Cat information data match with the DB model");
            } else {
                logger.log(LogStatus.ERROR, "ERROR: Cat information data don't match with the DB model");
            }
        }
    }

    private void checkIdentifyCat(WebModifyPage modifyPage, WebStartPage startPage){
        WebWaitingForApprovalPage waiting = new WebWaitingForApprovalPage(driver);
        WebApprovedPage approved = new WebApprovedPage(driver);
        //user encode a full valid "waiting for approval"and click search
        //no animals is fetch
        //message animal not found is displayed
        click(startPage.filterButton);
        if(registrator.equals("Veterinary")){
            click(startPage.filterWaitingForApprovalVetTabButton);
        } else {
            click(startPage.filterWaitingForApprovalBreederAndRefugeTabButton);
        }

        String waitingForApprovalIdentifier = waiting.firstIdentifierTableResult.getAttribute("textContent");
        click(startPage.modifyTabButton);
        WaitForElement(modifyPage.searchField,60);
        setText(modifyPage.searchField,waitingForApprovalIdentifier);
        click(modifyPage.buttonSearch);
        String errorMessage;
        try{

            WaitForElement(modifyPage.errorLabelNoSearchFound,5);
            errorMessage = modifyPage.errorLabelNoSearchFound.getAttribute("textContent").replaceAll("\\s+","");
            if(errorMessage.equals("Animalnotfound")){//There were a lot a useless space, so they were removed
                logger.log(LogStatus.INFO, "\"Animal not found\" is displayed for waiting for an approval identifier");
            } else {
                logger.log(LogStatus.ERROR, "ERROR: \"Animal not found\" is not displayed for waiting for an approval identifier");
            }
        } catch (Exception e){
            logger.log(LogStatus.ERROR, "ERROR: Animal in waiting for Approval is detected, please check if the animal is visible in Approved list");
        }


        //user encode a not full valid identifier number and click search
        //no data must be fetched
        setText(modifyPage.searchField,"123");
        click(modifyPage.buttonSearch);
        if(WaitForElement(modifyPage.errorLabelNoSearchFound,5)) {
            errorMessage = modifyPage.errorLabelNoSearchFound.getAttribute("textContent").replaceAll("\\s+", "");
            if (errorMessage.equals("Animalnotfound")) {//There were a lot a useles space, so they were removed
                logger.log(LogStatus.INFO, "\"Animal not found\" is displayed for an incorrect identifier");
            } else {
                logger.log(LogStatus.ERROR, "ERROR: \"Animal not found\" is not displayed for an incorrect identifier");
            }
        }else{
            logger.log(LogStatus.ERROR, "ERROR: A result is displayed with incomplete identifier");
        }

        //user encode a full valid approved identifier number and click search
        //the registration page is displayed
        click(startPage.filterButton);
        if(registrator.equals("Veterinary")){
            click(startPage.filterApprovedVetTabButton);
        } else {
            click(startPage.filterApprovedBreederAndRefugeTabButton);
        }

        String approvedlIdentifier = approved.firstIdentifierTableResult.getAttribute("textContent");
        click(startPage.modifyTabButton);
        WaitForElement(modifyPage.searchField,60);
        setText(modifyPage.searchField,approvedlIdentifier);
        click(modifyPage.buttonSearch);

        try {
            WaitForElement(modifyPage.changeResponsibleDropDown,5);
            logger.log(LogStatus.INFO, "Approved cat identifier is found");

        }catch (Exception e){
            logger.log(LogStatus.ERROR, "ERROR: Approved cat identifier is not found");
        }
    }

    private void registerNewCat(WebRegisterPage registerPage, WebStartPage startPage, WebEazyIdPage eazyIdPage) throws Exception {
        click(startPage.registerTabButton);
        WaitForElement(registerPage.transfertOfResponsibility,60);
        //Fill First Responsible Info
        if(registrator.equals("Veterinary") ){
            setText(registerPage.refNumberTextFieldFirstOwner, "HK43214321");
            click(registerPage.selectFisrtResponsibleToolTip);
        }

        ///!\/!\/!\/!\/!\/!\ Make sure the first cat identifier is available in the DB   /!\/!\/!\/!\/!\/!\/!\/!\
        // The cat identifier available has to match with the last line of src/test/java/Resources/IdentifierList
        //Fill Cat identification

        fillCatIdentifier(registerPage.firstIdentifier);
        catIdentifier = registerPage.firstIdentifier.getAttribute("value").replace("-", "");
        String passportNumberDate = new SimpleDateFormat("dd-MM-yyyy_HH-mm-ss").format(new Date());
        String passportText = tcFeatureListNumber  + "_" + passportNumberDate;
        logger.log(LogStatus.INFO,passportText);
        setText(registerPage.passportNumber, passportText);

        //Fill cat identification
        String timeStamp = new SimpleDateFormat("dd.MM.yyyy").format(new Date());
        setText(registerPage.birthDate,timeStamp);
        setText(registerPage.identificationDate,timeStamp);
        setText(registerPage.sterilizationDate, timeStamp);
        setText(registerPage.colorCatTextField, "black");
        setText(registerPage.nameCatTextField,"Minuit");
        setText(registerPage.breedCatTextField, "Ab");
        click(registerPage.selectBreedTooltip);

        //Submit the form
        if(registrator.equals("Veterinary")){
            click(registerPage.submitButtonVet);
        } else {
            click(registerPage.submitButtonOther);
        }

        WaitForElement(registerPage.signVetNowOrAccrSendVetButton, 60);
        click(registerPage.signVetNowOrAccrSendVetButton);

        if(registrator.equals("Veterinary")){
            eazyId(eazyIdPage);
        }

        click(eazyIdPage.closeValidSubmitMessage);

        if(!registrator.equals("Veterinary")){
            click(startPage.logoutButton);
            //A breeder or refuge needs to make their registration be validated by a Vet
            OpenHomepage openHomepage = new OpenHomepage();
            openHomepage.setDriver(driver);
            openHomepage.setLogger(logger);
            openHomepage.run();
            Login loginpopup = new Login();
            loginpopup.setUserRole("Veterinary");
            loginpopup.setDriver(driver);
            loginpopup.setLogger(logger);
            loginpopup.run();
            WaitingForApprovalCatRegistration waiting = new WaitingForApprovalCatRegistration();
            waiting.setCatIdentifier(catIdentifier);
            waiting.setDriver(driver);
            waiting.setLogger(logger);
            waiting.run();
            //Re-log as non vet
            openHomepage.run();
            loginpopup.setUserRole(registrator);
            loginpopup.run();

        }
        click(startPage.modifyTabButton);
    }

    private boolean checkDetectionErrorMessage(WebModifyPage modifyPage, WebEazyIdPage eazyIdPage, String errorMessage){
        if(registrator.equals("Veterinary")){
            return checkDetectionErrorMessage(modifyPage, eazyIdPage, errorMessage, "Later");
        } else {
            return checkDetectionErrorMessage(modifyPage, eazyIdPage, errorMessage, "Vet");
        }
    }

    private boolean checkDetectionErrorMessage(WebModifyPage modifyPage, WebEazyIdPage eazyIdPage, String errorMessage, String kindOfSign){
        boolean isDetected = false;
        if(registrator.equals("Veterinary")){
            click(modifyPage.submitVetButton);
        } else {
            click(modifyPage.submitBreederAndRefugeButton);
        }
        //For breeder "Sign Later" becomes "Send to Pending" and "Sign Now" becomes "Send to Vet"
        if(kindOfSign.equals("Later") || kindOfSign.equals("Pending") || kindOfSign.equals("Vet")){
            click(modifyPage.submitYesOrSignLaterButton);
        } else if (kindOfSign.equals("Now")){
            click(modifyPage.submitSignNowButton);
        }

        try {
            WaitForElement(modifyPage.errorMessagePage,5);
            logger.log(LogStatus.INFO,"Found: " + modifyPage.errorMessagePage.getAttribute("textContent"));
            logger.log(LogStatus.INFO,"Expected: " +errorMessage);
            if(modifyPage.errorMessagePage.getAttribute("textContent").equals(errorMessage)){
                logger.logScreenshot(LogStatus.INFO, "Error case correctly detected for " + registrator, getScreenShot());
                isDetected = true;
            }
            else {
                logger.logScreenshot(LogStatus.ERROR, "ERROR: Error case message is wrong... for " + registrator, getScreenShot());
                WaitForElement(eazyIdPage.closeValidSubmitMessage, 5);
                click(eazyIdPage.closeValidSubmitMessage);
            }
        } catch (Exception e){
//            WaitForElement(eazyIdPage.closeValidSubmitMessage, 5);
//            click(eazyIdPage.closeValidSubmitMessage);
            sleep(5000);// The notification disappear after 5 second (cannot wait anymore on it) since CatID 5.0
            logger.logScreenshot(LogStatus.ERROR, "ERROR: Error case message is wrong for " + registrator + " Expected: " + errorMessage, getScreenShot());
        }

        return isDetected;
    }

    private boolean checkDetectionValidSubmit(WebModifyPage modifyPage, WebEazyIdPage eazyIdPage){
        return checkDetectionValidSubmit(modifyPage, eazyIdPage, "Later");
    }
    private boolean checkDetectionValidSubmit(WebModifyPage modifyPage, WebEazyIdPage eazyIdPage, String kindOfSign){
        return checkDetectionValidSubmit(modifyPage, eazyIdPage, kindOfSign, false);
    }

    private boolean checkDetectionValidSubmit(WebModifyPage modifyPage, WebEazyIdPage eazyIdPage, String kindOfSign, boolean isSignatureRequire){
        boolean isDetected = false;
        if(registrator.equals("Veterinary")){
            click(modifyPage.submitVetButton);
        } else {
            click(modifyPage.submitBreederAndRefugeButton);
        }
        //For breeder "Sign Later" becomes "Send to Pending" and "Sign Now" becomes "Send to Vet"
        if(kindOfSign.equals("Later") || kindOfSign.equals("Pending") || kindOfSign.equals("Vet")){
            click(modifyPage.submitYesOrSignLaterButton);
        } else if (kindOfSign.equals("Now")){
            click(modifyPage.submitSignNowButton);
            try{
                logger.log(LogStatus.INFO,"Try to sign");
                logger.log(LogStatus.INFO,"Signing");
                eazyId(eazyIdPage);
                logger.log(LogStatus.INFO,"Signed");
            } catch (Exception e){
                if(isSignatureRequire){
                    logger.logScreenshot(LogStatus.ERROR, "Signature needed for " + registrator, getScreenShot());
                } else{
                    logger.logScreenshot(LogStatus.INFO, "No signature needed for " + registrator, getScreenShot());
                }

            }

        }
        sleep(5000); // The notification disappear after 5 seconds (cannot wait for it anymore)
        if(isElementPresent(modifyPage.errorMessagePage)){
            logger.logScreenshot(LogStatus.ERROR, "ERROR: Valid submit is not correctly detected for " + registrator, getScreenShot());
        } else{
            logger.logScreenshot(LogStatus.INFO, "Valid submit is correctly detected for " + registrator, getScreenShot());
            isDetected = true;
        }

        return isDetected;
    }

    private void checkFieldsSecondResponsibleFromHk(WebModifyPage modifyPage, String hkNumber){
        //Fields are non-editable
        if(isEditable(modifyPage.changeResponsibleFirstName) ||
                isEditable(modifyPage.changeResponsibleLastName) ||
                isEditable(modifyPage.changeResponsibleNationalNumberField) ||
                isEditable(modifyPage.changeResponsibleCity) ||
                isEditable(modifyPage.changeResponsibleStreet) ||
                isEditable(modifyPage.changeResponsibleStreetNumber) ||
                isEditable(modifyPage.changeResponsiblePhone) ||
                isEditable(modifyPage.changeResponsibleMobile) ||
                isEditable(modifyPage.changeResponsibleFax) ||
                isEditable(modifyPage.changeResponsibleMail)){
            logger.log(LogStatus.ERROR, "Fields are editable for user " + hkNumber);
        } else {
            logger.log(LogStatus.INFO, "Fields are non-editable for user " + hkNumber );
        }
    }

    private void checkFieldsSecondResponsibleFromPrivatePerson(WebModifyPage modifyPage, String privatePerson){
        //All mandatory fields are filled (meaning the RRN is fetch)
        WaitForElement(modifyPage.changeResponsibleFirstName, 60);
        if(isFilled(modifyPage.changeResponsibleFirstName) &&
                isFilled(modifyPage.changeResponsibleLastName) &&
                isFilled(modifyPage.changeResponsibleNationalNumberField)){
            logger.log(LogStatus.INFO, "Fields are filled for " + privatePerson);
        } else {
            logger.log(LogStatus.ERROR, "Fields are not filled for " + privatePerson);
        }

        //Address fields are not editable
        //Firt Name
        //Last Name
        //RRN
        if(!isEditable(modifyPage.changeResponsibleFirstName) &&
                !isEditable(modifyPage.changeResponsibleLastName) &&
                !isEditable(modifyPage.changeResponsibleNationalNumberField)){
            logger.log(LogStatus.INFO, "Fields are  non-editable for  " + privatePerson);
        } else {
            logger.log(LogStatus.ERROR, "Fields are editable for " + privatePerson);
        }
        //Address fields are editable
        //Postal code
        //Street
        //Number
        //Phone
        //Mobile
        //Fax
        //Email
        if(isEditable(modifyPage.changeResponsibleCity) &&
                isEditable(modifyPage.changeResponsibleStreet) &&
                isEditable(modifyPage.changeResponsibleStreetNumber) &&
                isEditable(modifyPage.changeResponsiblePhone) &&
                isEditable(modifyPage.changeResponsibleMobile) &&
                isEditable(modifyPage.changeResponsibleFax) &&
                isEditable(modifyPage.changeResponsibleMail)){
            logger.log(LogStatus.INFO, "Fields are editable for  " + privatePerson);
        } else {
            logger.log(LogStatus.ERROR, "Fields are non-editable for " + privatePerson);
        }
    }

    private void checkChangeResponsibleInformation(WebModifyPage modifyPage, WebStartPage startPage, WebEazyIdPage eazyIdPage){
        reloadModifyPageWithCatIdentifier(modifyPage,startPage);
        WaitForElement(modifyPage.changeResponsibleDropDown,5);
        clickHiddenElement(modifyPage.changeResponsibleDropDown);

        //From HK
        //User can encode invalid HK
        scrollOnElement(modifyPage.changeResponsiblePhone);
        setText(modifyPage.changeResponsibleHkRefNumSecondSearchField, "aaaa");
        sleep(2000);//Wait for the field is red
        //Fetch result give red characters
        if (isRed(getRgbString(modifyPage.changeResponsibleHkRefNumSecondSearchField))) {
            logger.log(LogStatus.INFO, "refNumberTextFieldSecondOwner is red for incorrect input");
        } else {
            logger.log(LogStatus.ERROR, "refNumberTextFieldSecondOwner is not red for incorrect input");
        }
        //No data reached and filled
        if (modifyPage.changeResponsibleHkRefNumSecondSearchField.getText().equals("")) {
            logger.log(LogStatus.INFO, "no data reached and filled for incorrect input");
        } else {
            logger.log(LogStatus.ERROR, "data reached and filled for incorrect input");
        }

        //Refuge cannot serach for another refuge
        if(!registrator.equals("Refuge 1")){
            //User can encode valid HK
            setText(modifyPage.changeResponsibleHkRefNumSecondSearchField, "HK12341234");
            //Valid HK format is X2 + N8
            //Fetch result is a popup list of users
            click(modifyPage.changeResponsibleHkRefNumSecondSearchTooltip);//if clickable so it is valid
        }

        //All fields are automatically filled
        //Fields are non-editable
        checkFieldsSecondResponsibleFromHk(modifyPage, "HK12341234");

        //Clear all fields to make sure the fields are filled
        click(modifyPage.changeResponsibleClearVetButton);

        //User can select another responsible from list resulting of the fetch
        setText(modifyPage.changeResponsibleHkRefNumSecondSearchField, "HK43214321");
        //Valid HK format is X2 + N8
        //Fetch result is a popup list of users
        click(modifyPage.changeResponsibleClearVetButton);//if clickable so it is valid

        //All fields are automatically filled
        //Fields are non-editable
        checkFieldsSecondResponsibleFromHk(modifyPage, "HK43214321");

        //User can click on Regular to change data from HK to Regular
        clickHiddenElement(modifyPage.changeResponsiblePrivatePersonCheckBox);

        //From Regular
        //Fetch information details is displayed
        //User can fetch by RRN
        setText(modifyPage.changeResponsiblePrivaPersonSearchField, "71715100070");
        clickHiddenElement(modifyPage.changeResponsibleSearchButton);

        checkFieldsSecondResponsibleFromPrivatePerson(modifyPage, " RRN 71715100070");
        //Clear all fields to make sure the fields are filled
        if(registrator.equals("Veterinary")){
            click(modifyPage.changeResponsibleClearVetButton);
        } else {
            click(modifyPage.changeResponsibleClearBreederOrRefugeButton);
        }
        //User can fetch from EID card
        click(modifyPage.changeResponsibleReadDataDromEidCardButton);
        click(eazyIdPage.consentCheckbox);
        click(eazyIdPage.agreeButton);
        checkFieldsSecondResponsibleFromPrivatePerson(modifyPage, "eID card");

        //Check confidential flag for private person
        click(modifyPage.changeResponsibleConfidentialFlag);
        //Check the confidential flag is enable
        String isCheckedString = modifyPage.changeResponsibleConfidentialFlag.getAttribute("class");
        if(isCheckedString.equals("ui toggle checkbox checked")){
            logger.logScreenshot(LogStatus.INFO, "Confidential flag is enable", getScreenShot());
        } else {
            logger.logScreenshot(LogStatus.ERROR, "Confidential flag is disable: " + isCheckedString + " ...", getScreenShot());
        }

        click(modifyPage.changeResponsibleConfidentialFlag);
        //Check the confidential flag is disable
        isCheckedString = modifyPage.changeResponsibleConfidentialFlag.getAttribute("class");
        if(isCheckedString.equals("ui toggle checkbox")){
            logger.logScreenshot(LogStatus.INFO, "Confidential flag is disable", getScreenShot());
        } else {
            logger.logScreenshot(LogStatus.ERROR, "Confidential flag is enable: " + isCheckedString + " ...", getScreenShot());
        }

        //transfer date must be defined
        isFilled(modifyPage.changeResponsibleTransfertDate);
    }

    private void userFirstAndSecondResponsibleAreDifferent(WebModifyPage modifyPage, WebStartPage startPage, WebEazyIdPage eazyIdPage){
        if(!registrator.equals("Refuge 1")) {//A refuge cannot search for another refuge or itself...
            reloadModifyPageWithCatIdentifier(modifyPage, startPage);
            WaitForElement(modifyPage.changeResponsibleDropDown, 5);
            clickHiddenElement(modifyPage.changeResponsibleDropDown);

            //Select Second Breeder user
            scrollOnElement(modifyPage.changeResponsiblePhone);

            setText(modifyPage.changeResponsibleHkRefNumSecondSearchField, "HK43214321");
            click(modifyPage.changeResponsibleHkRefNumSecondSearchTooltip);//if clickable so it is valid
            scrollOnElement(modifyPage.changeResponsibleTransfertDate);
            //Check error message
            if (registrator.equals("Breeder 1")) {
                clickHiddenElement((modifyPage.changeResponsibleAgreeBreederRefugeCheckBox));
            }
            checkDetectionErrorMessage(modifyPage, eazyIdPage, "The same owner was selected. Please choose different owners");
        }
    }
}
