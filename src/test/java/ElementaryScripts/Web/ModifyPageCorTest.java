package ElementaryScripts.Web;

import Screens.WebEazyIdPage;
import Screens.WebModifyPage;
import Screens.WebStartPage;
import com.relevantcodes.extentreports.LogStatus;

import java.text.SimpleDateFormat;
import java.util.Date;


public class ModifyPageCorTest extends ModifyPage {
    public void run() throws Exception {
        try {
            WebEazyIdPage eazyIdPage = new WebEazyIdPage(driver);
            WebStartPage startPage = new WebStartPage(driver);
            WebModifyPage modifyPage = new WebModifyPage(driver);
            logger.log(LogStatus.INFO, "Modify as " + registrator);
            WaitForElement(startPage.modifyTabButton, 60);
            click(startPage.modifyTabButton);
            setText(modifyPage.searchField, catIdentifier);
            click(modifyPage.buttonSearch);
            scrollOnElement(modifyPage.changeResponsibleDropDown);
            WaitForElement(modifyPage.changeResponsibleDropDown, 60);
            clickHiddenElement(modifyPage.changeResponsibleDropDown);
            scrollOnElement(modifyPage.changeResponsibleTransfertDate);
            logger.log(LogStatus.INFO, "New responsible is " + newResponsible);
            if (newResponsible.equals("Private Person")) {
                scrollOnElement(modifyPage.changeResponsibleHkRefNumSecondSearchField);
                clickHiddenElement(modifyPage.changeResponsiblePrivatePersonCheckBox);
                click(modifyPage.changeResponsibleReadDataDromEidCardButton);
                click(eazyIdPage.consentCheckbox);
                click(eazyIdPage.agreeButton);
            } else if (newResponsible.equals("Breeder 1")) {
                logger.log(LogStatus.INFO, "M-COR: " + newResponsible);
                setText(modifyPage.changeResponsibleHkRefNumSecondSearchField, "HK43214321");
                click(modifyPage.responsibleHkSelectResponsibleToolTip);
            } else if (newResponsible.equals("Breeder 4")) {
                logger.log(LogStatus.INFO, "M-COR: " + newResponsible);
                setText(modifyPage.changeResponsibleHkRefNumSecondSearchField, "HK159357");
                click(modifyPage.responsibleHkSelectResponsibleToolTip);
            } else if (newResponsible.equals("Refuge 1")) {
                logger.log(LogStatus.INFO, "M-COR: " + newResponsible);
                setText(modifyPage.changeResponsibleHkRefNumSecondSearchField, "HK12341234");
                click(modifyPage.responsibleHkSelectResponsibleToolTip);
            } else if (newResponsible.equals("Breeder 2")) {
                logger.log(LogStatus.INFO, "M-COR: " + newResponsible);
                setText(modifyPage.changeResponsibleHkRefNumSecondSearchField, "BR88888888");
                click(modifyPage.responsibleHkSelectResponsibleToolTip);
            } else if (newResponsible.equals("Breeder 3")) {
                logger.log(LogStatus.INFO, "M-COR: " + newResponsible);
                setText(modifyPage.changeResponsibleHkRefNumSecondSearchField, "HK10101235");
                click(modifyPage.responsibleHkSelectResponsibleToolTip);
            } else if (newResponsible.equals("Refuge 2")) {
                logger.log(LogStatus.INFO, "M-COR: " + newResponsible);
                setText(modifyPage.changeResponsibleHkRefNumSecondSearchField, "RE88888888");
                click(modifyPage.responsibleHkSelectResponsibleToolTip);
            } else if (newResponsible.equals("Refuge 3")) {
                logger.log(LogStatus.INFO, "M-COR: " + newResponsible);
                setText(modifyPage.changeResponsibleHkRefNumSecondSearchField, "HK852456");
                click(modifyPage.responsibleHkSelectResponsibleToolTip);
            }
            logger.log(LogStatus.INFO, "Submit ");
            if (registrator.contains("Veterinary")) {
                scrollOnElement(modifyPage.submitVetButton);
                String testCaseDateHour = new SimpleDateFormat("dd.MM.yyyy").format(new Date());
                setText(modifyPage.changeResponsibleTransfertDate, testCaseDateHour);
                click(modifyPage.submitVetButton);
                click(modifyPage.submitSignNowButton);
                eazyId(eazyIdPage);
                click(eazyIdPage.closeValidSubmitMessage);

            } else {
                scrollOnElement(modifyPage.submitBreederAndRefugeButton);
                clickHiddenElement((modifyPage.changeResponsibleAgreeBreederRefugeCheckBox));
                scrollOnElement(modifyPage.submitBreederAndRefugeButton);
                click(modifyPage.submitBreederAndRefugeButton);
                click(modifyPage.submitYesOrSignLaterButton);

                if (newResponsible.equals("Private Person")) {
                    eazyId(eazyIdPage);
                    click(eazyIdPage.downloadCertificateAfterValidSubmitButton);
                }

            }

            //logout
            sleep(2000);
            click(startPage.logoutButton);
        } catch (Exception e) {
            logger.logScreenshot(LogStatus.ERROR, "ERROR stopping the test", getScreenShot());
            throw e;
        }
    }
}
