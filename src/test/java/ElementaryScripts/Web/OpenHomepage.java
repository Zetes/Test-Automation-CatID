package ElementaryScripts.Web;

import ElementaryScripts.General.AbstractCatIdElementaryScript;
import com.relevantcodes.extentreports.LogStatus;

public class OpenHomepage extends AbstractCatIdElementaryScript {

    public void run() {
        String desiredUrl = "https://pp-online.catid.be/"; //External address in Preprod env
//        String desiredUrl = "http://192.168.17.20/"; //Internal IP address in Preprod env
//        String desiredUrl = "http://192.168.16.123/"; //Internal IP address in Test env

        logger.log(LogStatus.INFO, "Opened " + desiredUrl);

        this.setBaseUrl(desiredUrl);

        OpenBaseURL();
        driver.manage().window().maximize();

        logger.log(LogStatus.INFO, "Open homepage");

    }

}
