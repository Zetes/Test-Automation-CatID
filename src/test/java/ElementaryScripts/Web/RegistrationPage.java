package ElementaryScripts.Web;

import ElementaryScripts.General.AbstractCatIdElementaryScript;

import java.text.SimpleDateFormat;
import java.util.Date;

public abstract class RegistrationPage extends AbstractCatIdElementaryScript {

    protected void takeScreenshot(String folderResultName){
        String testCaseDateHour = new SimpleDateFormat("dd.MM.yyyy-HH.mm.ss").format(new Date());
        String screenshotPath = "src/test/java/Results/" + tcFeatureListNumber  +"_CatRegistrationFeatureList_" + folderResultName + "/";
        String screenshotName = screenshotPath + testCaseDateHour;
        getScreenShot(screenshotName);
    }

    protected void displayCurrentDirectory(){
        System.out.println("Working Directory = " +
                System.getProperty("user.dir"));
    }

}
