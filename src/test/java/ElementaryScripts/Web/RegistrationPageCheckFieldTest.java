package ElementaryScripts.Web;

import Screens.*;
import com.relevantcodes.extentreports.LogStatus;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import java.text.SimpleDateFormat;
import java.util.Map;
import java.util.HashMap;
import java.util.Date;
import java.util.Calendar;

public class RegistrationPageCheckFieldTest extends RegistrationPage {
    public void run() throws Exception{
        try {
            WebRegisterPage registerPage = new WebRegisterPage(driver);
            WebEazyIdPage eazyIdPage = new WebEazyIdPage(driver);
            WebStartPage startPage = new WebStartPage(driver);
            WebAccountSettingsPage accountSettingsPage = new WebAccountSettingsPage(driver);

            //Registration check for Veterinary
            WaitForElement(startPage.registerTabButton, 60);

            //Operation can be reached from:
            //Menu – Register Cat (Tab)
            click(startPage.registerTabButton);
            WaitForElement(registerPage.transfertOfResponsibility, 60);
//            click(startPage.homeButton);

//            //Operation can be reached from:
//            //Button – Register Cat (Action)
//            click(startPage.registerActionButton);
//            WaitForElement(registerPage.transfertOfResponsibility, 60);

            //First Responsible Information
            checkFirstResponsibleInformation(registerPage, eazyIdPage);

            //Add Responsible (direct transfer from resp1 to resp2)
            click(registerPage.transfertOfResponsibility);
            if (registrator.equals("Veterinary")) {
                scrollOnElement(registerPage.phoneTextFieldFirstOwner);
            }
            //New Responsible Information
            checkNewResponsibleInformation(registerPage, eazyIdPage);
            //second responsible must be different of the first responsible
            userFirstAndSecondResponsibleAreDifferent(registerPage, startPage, eazyIdPage);

            //Check Vet info for breeder and refuge
            checkVetInformation(registerPage, accountSettingsPage, startPage, eazyIdPage);

            //Cat Identification
            checkCatIdentification(registerPage, eazyIdPage, startPage);

            //Cat information
            checkCatInformation(registerPage, eazyIdPage, startPage);

            //TODO add field title in the report (for readability)
            //logout for next page
            click(startPage.logoutButton);
        } catch (Exception e) {
            logger.logScreenshot(LogStatus.ERROR, "ERROR stopping the test", getScreenShot());
            throw e;
        }
    }

    private void checkVetInformation(WebRegisterPage registerPage, WebAccountSettingsPage accountSettingsPage, WebStartPage startPage, WebEazyIdPage eazyIdPage) {
        //Only breeder or refuge, so no vet
        if(!registrator.equals("Veterinary")){
            //Vet information
            //Approved vet
            //If Approved Vet is not set yet at 'your account', then a message is displayed:


            //Clean known vet for this test
            click(startPage.accountSettings);
            WaitForElement(accountSettingsPage.addVetTextFieldForBreederAndRefuge,30);
            //Remove all white space characters
            String vetValue = accountSettingsPage.vetTextFieldForBreederAndRefuge.getAttribute("textContent");
            if (vetValue != null){
                String vetFieldTextContent = vetValue.replaceAll("\\s+","");
                while(vetFieldTextContent != null && !vetFieldTextContent.equals("")){
                    click(accountSettingsPage.deleteVetButtonForBreederAndRefuge);
                    click(eazyIdPage.closeValidSubmitMessage);
                    vetValue = accountSettingsPage.vetTextFieldForBreederAndRefuge.getAttribute("textContent");
                    if (vetValue != null) {
                        vetFieldTextContent = vetValue.replaceAll("\\s+", "");
                    } else {
                        vetFieldTextContent = null;
                    }
                }
            }

            //There is no approving vet. Please select Other or add from account settings
            click(startPage.registerTabButton);
            WaitForElement(registerPage.transfertOfResponsibility,30);
            String label = registerPage.informationTextLabel.getAttribute("textContent");
            if(label != null && label.equals("There is no approving vet. Please select Other or add from account settings")){
                logger.log(LogStatus.INFO, "No default vet message is displayed");
            } else {
                logger.log(LogStatus.ERROR, "ERROR: No default vet message is not displayed");
            }

            //Reload default vet F0032
            click(startPage.accountSettings);
            WaitForElement(accountSettingsPage.addVetTextFieldForBreederAndRefuge,30);
            setText(accountSettingsPage.addVetTextFieldForBreederAndRefuge,"F0032");
            click(accountSettingsPage.addVetTooltipForBreederAndRefuge);
            click(accountSettingsPage.yesAddVetButtonForBreederAndRefuge);
            click(eazyIdPage.closeValidSubmitMessage);
            sleep(1000);
            WaitForElement(startPage.registerTabButton,30);
            click(startPage.registerTabButton);
            WaitForElement(registerPage.transfertOfResponsibility,30);

            //Other
            //enter a valid user ref of a vet
            clickHiddenElement(registerPage.otherVetCheckbox);
            setText(registerPage.otherVeterinaryTextField,"F0032");
            click(registerPage.otherVetTooltip);
            //The known vet is displayed in the dropdown list
            if(registerPage.otherVeterinaryTextField.getAttribute("value").equals("F0032,jira282 jira2820")){
                logger.log(LogStatus.INFO, "The known vet is displayed in the dropdown list");
            } else {
                logger.log(LogStatus.ERROR, "ERROR: The known vet is not displayed in the dropdown list");
            }
            //enter an invalid user
            //An error message is displayed (is red?)
            setText(registerPage.otherVeterinaryTextField,"UNKNOWN_VET");
            if(isRed(getRgbString(registerPage.otherVeterinaryTextField))){
                logger.log(LogStatus.INFO, "Other vet field is red for unknown vet");
            } else {
                logger.log(LogStatus.ERROR, "ERROR: Other vet field is not red for unknown vet");
            }
            //Restore correct vet
            clickHiddenElement(registerPage.myContactVetCheckbox);
        }
    }

    private void checkCatInformation(WebRegisterPage registerPage, WebEazyIdPage eazyIdPage, WebStartPage startPage) {
        //Re filled private person info
        click(startPage.registerTabButton);
        if (registrator.equals("Veterinary")) {
            resetPrivatePersonInfoFromeID(registerPage, eazyIdPage);
        }
        //User must select a gender value from dorpdown list
        //male
        //female

        Select dropdown = new Select(registerPage.gender);

        //Check default value is : Male
        if (dropdown.getFirstSelectedOption().getAttribute("textContent").equals("Male")) {
            logger.log(LogStatus.INFO, "Default gender is male");
        } else {
            logger.log(LogStatus.ERROR, "ERROR: Default gender is not male");
        }

        Map visibleDropListElement = new HashMap();
        visibleDropListElement.put("Male", 0);
        visibleDropListElement.put("Female", 0);

        int numberIdentifierLocation = 0;
        for (WebElement we : dropdown.getOptions()) {
            if (visibleDropListElement.containsKey(we.getAttribute("textContent"))) {
                numberIdentifierLocation++;
            }
        }

        if (numberIdentifierLocation == 2) {
            logger.log(LogStatus.INFO, "Genders are correct");
        } else {
            logger.log(LogStatus.ERROR, "ERROR: Genders are not correct");
        }

        //field cannot be empty
        if (!isEditable(registerPage.gender)) {
            logger.log(LogStatus.INFO, "Gender cannot be edit");
        } else {
            logger.log(LogStatus.ERROR, "ERROR: Gender can be edit");
        }

        //Fill previous mandatory fields
        setText(registerPage.breedCatTextField, "Abyssinian");
        click(registerPage.selectBreedTooltip);

        //User must encode birthdate
        //on submit, birthdate cannot be empty for vet
        if (registrator.equals("Veterinary")) {
            checkDetectionErrorMessage(registerPage, eazyIdPage, "Missing mandatory fields");
        } else {
            fillCatIdentifier(registerPage.firstIdentifier);
            String currentPendingIdentifier = registerPage.firstIdentifier.getAttribute("value").replace("-", "");
            //on submit, birthdate can be empty (pending)
            checkDetectionValidSubmit(registerPage, eazyIdPage);
            //Check the form is sent to pending if mandatory fields are missing
            //Search for the identifier to approve
            click(startPage.filterButton);
            click(startPage.filterPendingBreederAndRefugeTabButton);
            WebPendingPage pending = new WebPendingPage(driver);
            setText(pending.searchField, currentPendingIdentifier);
            //Wait for the update of the first line
            sleep(5000);
            checkIdentifierIsPresentInFilter(pending.firstLineTableResult, pending.firstIdentifierTableResult, currentPendingIdentifier, "the pending");
            click(startPage.registerTabButton);
            WaitForElement(registerPage.transfertOfResponsibility, 10);
        }
        scrollOnElement(registerPage.birthDate);
        Date tomorrowDate = new Date();
        Date yesterdayDate = new Date();
        Date beforeYesterdayDate = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(tomorrowDate);
        calendar.add(Calendar.DATE, 1);
        tomorrowDate = calendar.getTime();
        calendar.add(Calendar.DATE, -2);
        yesterdayDate = calendar.getTime();
        calendar.add(Calendar.DATE, -3);
        beforeYesterdayDate = calendar.getTime();
        String tomorrowDateTimeStamp = new SimpleDateFormat("dd.MM.yyyy").format(tomorrowDate);
        String yesterdayDateTimeStamp = new SimpleDateFormat("dd.MM.yyyy").format(yesterdayDate);
        String currentDateTimeStamp = new SimpleDateFormat("dd.MM.yyyy").format(new Date());
        String beforeYesterDayDateTimeStamp = new SimpleDateFormat("dd.MM.yyyy").format(beforeYesterdayDate);
        setText(registerPage.birthDate, tomorrowDateTimeStamp);
        //User select date greater that current date
        if (!checkDetectionErrorMessage(registerPage, eazyIdPage, "Date cannot be in future")) {
            if (registrator.equals("Veterinary")) {
                resetPrivatePersonInfoFromeID(registerPage, eazyIdPage);
            }
            setText(registerPage.breedCatTextField, "Abyssinian");
            click(registerPage.selectBreedTooltip);
        }
        scrollOnElement(registerPage.birthDate);
        //Birthday cannot be after identification date
        setText(registerPage.birthDate, currentDateTimeStamp);
        setText(registerPage.identificationDate, yesterdayDateTimeStamp);
        if (!checkDetectionErrorMessage(registerPage, eazyIdPage, "ID date must be after date of birth")) {
            if (registrator.equals("Veterinary")) {
                resetPrivatePersonInfoFromeID(registerPage, eazyIdPage);
            }
            setText(registerPage.breedCatTextField, "Abyssinian");
            click(registerPage.selectBreedTooltip);
        }
        scrollOnElement(registerPage.birthDate);
        //Birthday cannot be after sterilization date
        setText(registerPage.birthDate, currentDateTimeStamp);
        setText(registerPage.sterilizationDate, yesterdayDateTimeStamp);
        if (!checkDetectionErrorMessage(registerPage, eazyIdPage, "Sterilized date must be after date of birth")) {
            if (registrator.equals("Veterinary")) {
                resetPrivatePersonInfoFromeID(registerPage, eazyIdPage);
            }
        }
        scrollOnElement(registerPage.birthDate);

        checkDateUiCalendar(registerPage.birthDate, registerPage.birthDateBodyCalendar, registerPage.birthDateHeadCalendar, "Birthdate");
        clearCatInformationField(registerPage);

        //User must encode Identification date
        //Fill previous mandatory fields
        //Re filled private person info
        click(startPage.registerTabButton);
        if (registrator.equals("Veterinary")) {
            clickHiddenElement(registerPage.checkBoxPrivatePersonFirstOwner);
            click(registerPage.readDataFromFirstOwnerEiDCardButton);
            click(eazyIdPage.consentCheckbox);
            click(eazyIdPage.agreeButton);
            click(registerPage.pinFirstOwner);
        }
        scrollOnElement(registerPage.birthDate);
        fillCatIdentifier(registerPage.firstIdentifier);
        setText(registerPage.breedCatTextField, "Abyssinian");
        click(registerPage.selectBreedTooltip);
        clickHiddenElement(registerPage.pinCatBreed);
        setText(registerPage.birthDate, currentDateTimeStamp);
        //on submit, birthdate cannot be empty for vet
        if (registrator.equals("Veterinary")) {
            checkDetectionErrorMessage(registerPage, eazyIdPage, "Missing mandatory fields");
        } else {
            fillCatIdentifier(registerPage.firstIdentifier);
            String currentPendingIdentifier = registerPage.firstIdentifier.getAttribute("value").replace("-", "");
            //on submit, birthdate can be empty (pending)
            checkDetectionValidSubmit(registerPage, eazyIdPage);
            //Check the form is sent to pending if mandatory fields are missing
            //Search for the identifier to approve
            click(startPage.filterButton);
            click(startPage.filterPendingBreederAndRefugeTabButton);
            WebPendingPage pending = new WebPendingPage(driver);
            setText(pending.searchField, currentPendingIdentifier);
            //Wait for the update of the first line
            sleep(5000);
            checkIdentifierIsPresentInFilter(pending.firstLineTableResult, pending.firstIdentifierTableResult, currentPendingIdentifier, "the pending");
            click(startPage.registerTabButton);
            WaitForElement(registerPage.transfertOfResponsibility, 10);
        }

        scrollOnElement(registerPage.identificationDate);
        setText(registerPage.identificationDate, tomorrowDateTimeStamp);
        //User select date greater that current date
        checkDetectionErrorMessage(registerPage, eazyIdPage, "Date cannot be in future");
        scrollOnElement(registerPage.identificationDate);
        checkDateUiCalendar(registerPage.identificationDate, registerPage.identificationDateBodyCalendar, registerPage.identificationDateHeadCalendar, "Identification date");
        //User select a identification date greater than Date of birth
        //form can be submitted
        scrollOnElement(registerPage.identificationDate);
        setText(registerPage.identificationDate, currentDateTimeStamp);
        setText(registerPage.birthDate, yesterdayDateTimeStamp);
        logger.log(LogStatus.INFO, "User select a identification date greater than Date of birth");
        checkDetectionValidSubmit(registerPage, eazyIdPage);
        //Re fill reponsible info and identifier
        //User select a identification date equal to Date of birth
        //form can be submitted
        scrollOnElement(registerPage.identificationDate);
        setText(registerPage.identificationDate, currentDateTimeStamp);
        setText(registerPage.birthDate, currentDateTimeStamp);
        logger.log(LogStatus.INFO, "User select a identification date equal to Date of birth");
        checkDetectionValidSubmit(registerPage, eazyIdPage);
        //User select a identification date less than Date of birth
        //form cannot be submitted
        //Already tested above for birthday
        //User select a identification date greater than Date of birth and greater than sterilized
        //form can be submitted
        scrollOnElement(registerPage.identificationDate);
        setText(registerPage.identificationDate, currentDateTimeStamp);
        setText(registerPage.birthDate, yesterdayDateTimeStamp);
        setText(registerPage.sterilizationDate, yesterdayDateTimeStamp);
        logger.log(LogStatus.INFO, "User select a identification date greater than Date of birth and greater than sterilized");
        checkDetectionValidSubmit(registerPage, eazyIdPage);
        //User select a identification date equal to Date of birth and equal to sterilized
        //form can be submitted
        scrollOnElement(registerPage.identificationDate);
        setText(registerPage.identificationDate, currentDateTimeStamp);
        setText(registerPage.birthDate, currentDateTimeStamp);
        setText(registerPage.sterilizationDate, currentDateTimeStamp);
        logger.log(LogStatus.INFO, "User select a identification date equal to Date of birth and equal to sterilized");
        checkDetectionValidSubmit(registerPage, eazyIdPage);
        //User select a identification date less than Date of birth and less than sterilized
        //form cannot be submitted
        //Already tested above with birthday
        //User select a identification date less than Date of birth and greater than sterilized
        //form cannot be submitted
        scrollOnElement(registerPage.identificationDate);
        setText(registerPage.identificationDate, yesterdayDateTimeStamp);
        setText(registerPage.birthDate, currentDateTimeStamp);
        setText(registerPage.sterilizationDate, beforeYesterDayDateTimeStamp);
        checkDetectionErrorMessage(registerPage, eazyIdPage, "Sterilized date must be after date of birth");
        //User select a identification date less than Date of birth and greated than sterilized
        //form cannot be submitted
        scrollOnElement(registerPage.identificationDate);
        setText(registerPage.identificationDate, beforeYesterDayDateTimeStamp);
        setText(registerPage.birthDate, yesterdayDateTimeStamp);
        setText(registerPage.sterilizationDate, currentDateTimeStamp);
        checkDetectionErrorMessage(registerPage, eazyIdPage, "ID date must be after date of birth");
        //date of birth is less or equal to identification and sterilized dates
        //form can be submitted
        //Already tested above with (all date equal)
        //date of birth is greater to identification and less to sterilized dates
        //form cannot be submitted
        //Already tested above with birth date
        //date of birth is greater to identification and less to sterilized dates
        //form cannot be submitted
        //date of birth is greater to identification and less to sterilized dates
        // form cannot be submitted
        //Already tested above with birth date
        //date of birth is less to identification and greater to sterilized dates
        //form cannot be submitted
        scrollOnElement(registerPage.identificationDate);
        setText(registerPage.identificationDate, currentDateTimeStamp);
        setText(registerPage.birthDate, yesterdayDateTimeStamp);
        setText(registerPage.sterilizationDate, beforeYesterDayDateTimeStamp);
        checkDetectionErrorMessage(registerPage, eazyIdPage, "Sterilized date must be after date of birth");
        //date of identification is greater to birthdate and equal or less of sterilized
        //form can be submitted
        scrollOnElement(registerPage.identificationDate);
        setText(registerPage.identificationDate, currentDateTimeStamp);
        setText(registerPage.birthDate, yesterdayDateTimeStamp);
        setText(registerPage.sterilizationDate, currentDateTimeStamp);
        logger.log(LogStatus.INFO, "date of identification is greater to birthdate and equal or less of sterilized");
        checkDetectionValidSubmit(registerPage, eazyIdPage);
        //date of identification is less to birthdate and equal or less of sterilized
        //form cannot be submitted
        //Already tested above
        //date of identification is greater to birthdate and equal to sterilized
        //form can be submitted
        //Already tested above
        //date of identification is greater to birthdate and less to sterilized
        //form can be submitted
        scrollOnElement(registerPage.identificationDate);
        setText(registerPage.identificationDate, yesterdayDateTimeStamp);
        setText(registerPage.birthDate, beforeYesterDayDateTimeStamp);
        setText(registerPage.sterilizationDate, currentDateTimeStamp);
        logger.log(LogStatus.INFO, "date of identification is greater to birthdate and less to sterilized");
        checkDetectionValidSubmit(registerPage, eazyIdPage);
        //date of sterilized is greater to birthdate and greater or less to identification
        //form can be submitted
        //Already tested above
        //date of sterilized is less to birthdate and greater or less to identification
        //form cannot be submitted
        scrollOnElement(registerPage.identificationDate);
        setText(registerPage.identificationDate, beforeYesterDayDateTimeStamp);
        setText(registerPage.birthDate, currentDateTimeStamp);
        setText(registerPage.sterilizationDate, yesterdayDateTimeStamp);
        checkDetectionErrorMessage(registerPage, eazyIdPage, "Sterilized date must be after date of birth");

        //by default the slider sterilized is set to false
        click(startPage.registerTabButton);
        scrollOnElement(registerPage.birthDate);
        WebElement slidButton = null;
        if (registrator.equals("Veterinary")) {
            slidButton = registerPage.sterilizationVetSlideButton;
        } else {
            slidButton = registerPage.sterilizationOtherSlideButton;
        }
        if (slidButton.getAttribute("class").equals("ui toggle checkbox sterilized")) {
            logger.log(LogStatus.INFO, "By default the slider sterilized is set to false");
        } else {
            logger.log(LogStatus.ERROR, "ERROR: By default the slider sterilized is set to true");
        }
        //user set sterilized slider to true
        //fill mandatory field
        fillCatIdentifier(registerPage.firstIdentifier);
        setText(registerPage.birthDate, currentDateTimeStamp);
        setText(registerPage.identificationDate, currentDateTimeStamp);
        click(slidButton);
        //no sterilized date is encoded
        //form cannot be submitted
        checkDetectionErrorMessage(registerPage, eazyIdPage, "Sterilized date is mandatory");
        //sterilized date is encoded and valid
        //can be submitted
        scrollOnElement(registerPage.birthDate);
        setText(registerPage.sterilizationDate, currentDateTimeStamp);
        logger.log(LogStatus.INFO, "sterilized date is encoded and valid");
        checkDetectionValidSubmit(registerPage, eazyIdPage);
        //sterilized date is encoded and less than birthdate
        //form cannot be submitted
        //Already tested above
        //sterilized date is encoded and invalid
        //form cannot be submitted
        //Already tested above
        scrollOnElement(registerPage.birthDate);
        //fill mandatory field
        fillCatIdentifier(registerPage.firstIdentifier);
        setText(registerPage.birthDate, currentDateTimeStamp);
        setText(registerPage.identificationDate, currentDateTimeStamp);
        setText(registerPage.sterilizationDate, currentDateTimeStamp);
        //user set sterilized slider to false
        click(slidButton);//This click should empty the sterilized date

        //no sterilized date is encoded
        //form can be submitted
        if (registerPage.sterilizationDate.getAttribute("value").equals("")) {
            logger.log(LogStatus.INFO, "No sterilized date is encoded");
        } else {
            logger.log(LogStatus.ERROR, "ERROR: A sterilized date is encoded");
        }
        checkDetectionValidSubmit(registerPage, eazyIdPage);
        //fill mandatory field
        scrollOnElement(registerPage.birthDate);
        setText(registerPage.birthDate, currentDateTimeStamp);
        setText(registerPage.identificationDate, currentDateTimeStamp);
        //sterilized date is encoded and valid
        //form cannot be submitted
        //Already tested above
        //sterilized date is encoded and invalid
        //form cannot be submitted
        //Already tested above
        //user can encode fur type and color
        //field is free text
        if (isEditable(registerPage.colorCatTextField)) {
            logger.log(LogStatus.INFO, "Color cat field is editable");
        } else {
            logger.log(LogStatus.ERROR, "ERROR: Color cat field is not editable");
        }
        setText(registerPage.colorCatTextField, "");
        //field is not mandatory
        //fill mandatory field
        setText(registerPage.birthDate, currentDateTimeStamp);
        setText(registerPage.identificationDate, currentDateTimeStamp);
        checkDetectionValidSubmit(registerPage, eazyIdPage);
        //user can encode a cat Name
        //field is free text
        if (isEditable(registerPage.nameCatTextField)) {
            logger.log(LogStatus.INFO, "Name of cat field is editable");
        } else {
            logger.log(LogStatus.ERROR, "ERROR: Name of cat field is not editable");
        }
        setText(registerPage.nameCatTextField, "");
        //field is not mandatory
        //fill mandatory field
        fillCatIdentifier(registerPage.firstIdentifier);
        setText(registerPage.birthDate, currentDateTimeStamp);
        setText(registerPage.identificationDate, currentDateTimeStamp);
        checkDetectionValidSubmit(registerPage, eazyIdPage);

        //user must encode and select a cat breed
        //User can encode text to fetch breed

        //Field is editable
        if (isEditable(registerPage.breedCatTextField)) {
            logger.log(LogStatus.INFO, "Breed field is editable");
        } else {
            logger.log(LogStatus.ERROR, "ERROR: Breed field is not editable");
        }

        //Clear breed field before test it
        setText(registerPage.breedCatTextField, "");
        click(registerPage.nameCatTextField);//In order to lose focus on breed field
        //At least two charaters will give a fetch result
        setText(registerPage.breedCatTextField, "b");
        try {
            WaitForElement(registerPage.selectBreedTooltip, 5);
            logger.log(LogStatus.ERROR, "ERROR: Breed list is incorrectly displayed with 1 character");
        } catch (Exception e) {
            logger.log(LogStatus.INFO, "Breed list is not incorrectly displayed with 1 character");
        }
        click(registerPage.nameCatTextField);//In order to lose focus on breed field
        setText(registerPage.breedCatTextField, "ab");
        //a fetch result dropdown list is displayed
        try {
            WaitForElement(registerPage.selectBreedTooltip, 5);
            logger.log(LogStatus.INFO, "Breed list is correctly displayed with at least 2 character");
        } catch (Exception e) {
            logger.log(LogStatus.ERROR, "ERROR: Breed list is not correctly displayed with at least 2 character");
        }
        //user can select on the result
        click(registerPage.selectBreedTooltip);
        //result is encoded in the field
        if (isFilled(registerPage.breedCatTextField)) {
            logger.log(LogStatus.INFO, "Breed field is encoded");
        } else {
            logger.log(LogStatus.ERROR, "ERROR: Breed field is not encoded");
        }
        //user can select text
        //Already tested above
        //result text displayed is a valid breed
        if (!isRed(getRgbString(registerPage.breedCatTextField))) {
            logger.log(LogStatus.INFO, "Breed field is not red");
        } else {
            logger.log(LogStatus.ERROR, "ERROR: Breed field is red");
        }
        //form can be submitted
        fillCatIdentifier(registerPage.firstIdentifier);
        setText(registerPage.birthDate, currentDateTimeStamp);
        setText(registerPage.identificationDate, currentDateTimeStamp);
        checkDetectionValidSubmit(registerPage, eazyIdPage);
        //user can deleted selected text
        setText(registerPage.breedCatTextField, "");
        //result text displayed is no more a valid breed
        if (!isFilled(registerPage.breedCatTextField) && !isRed(getRgbString(registerPage.breedCatTextField))) {
            logger.log(LogStatus.INFO, "Breed field is correctly cleared");
        } else {
            logger.log(LogStatus.ERROR, "ERROR: Breed field is not correctly cleared");
        }
        if (registrator.equals("Veterinary")) {
            //form cannot be submitted
            checkDetectionErrorMessage(registerPage, eazyIdPage, "Breed must be selected from list");
            //on click in the field, the drop down list is displayed
            //???? How???? The field is cleared????
        }

        //Clear breed after date tests
        scrollOnElement(registerPage.birthDate);
        setText(registerPage.breedCatTextField, "");

        //User can submit form
        //by clicking on the submit button
        if (registrator.equals("Veterinary")) {
            click(registerPage.submitButtonVet);
        } else {
            click(registerPage.submitButtonOther);
        }
        //A popup messge is displayed allowing user to choose
        //User click on NO button
        click(registerPage.signNoButton);
        //User comes back to the registration page filled with cat information
        try {
            WaitForElement(registerPage.breedCatTextField, 5);
            logger.log(LogStatus.INFO, "User comes back to the registration page filled with cat information");
        } catch (Exception e) {
            logger.log(LogStatus.ERROR, "ERROR: User doesn't come back to the registration page filled with cat information");
        }

        if (registrator.equals("Veterinary")) {
            //sign later
            //Make sur breed is empty
            scrollOnElement(registerPage.birthDate);
            setText(registerPage.breedCatTextField, "");
            //the system will validate encoded data
            //at least one data is detected invalid
            //form is not submitted
            checkDetectionErrorMessage(registerPage, eazyIdPage, "Breed must be selected from list", "Now");
            //the registration page si displayed
            //error message is displayed regarding the data in error


            scrollOnElement(registerPage.birthDate);
            if (isRed(getRgbString(registerPage.breedCatTextField))) {
                logger.log(LogStatus.INFO, "Breed field display font is red");
            } else {
                logger.log(LogStatus.ERROR, "Breed field display font is not red");
            }
        }
        //all data are valid
        setText(registerPage.birthDate,currentDateTimeStamp);
        setText(registerPage.identificationDate,currentDateTimeStamp);
        fillCatIdentifier(registerPage.firstIdentifier);
        //Get current identifier
        String currentIdentifier = registerPage.firstIdentifier.getAttribute("value").replace("-","");
        setText(registerPage.breedCatTextField,"Abyssinian");
        click(registerPage.selectBreedTooltip);
        checkDetectionValidSubmit(registerPage, eazyIdPage);
        //form is submitted and available in the waiting for approval
        //Open Waiting for Approval page
        WebWaitingForApprovalPage waiting = new WebWaitingForApprovalPage(driver);
        WebPendingPage pending = new WebPendingPage(driver);
        click(startPage.filterButton);
        if(registrator.equals("Veterinary")) {
            click(startPage.filterWaitingForApprovalVetTabButton);
            //Search for the identifier to approve

            setText(waiting.searchField,currentIdentifier);
            //Wait for the update of the first line
            sleep(5000);
            checkIdentifierIsPresentInFilter(waiting.firstLineTableResult,waiting.uniqueIdentifierTableResult,currentIdentifier,"the waiting for approval");
            click(startPage.registerTabButton);
        } else {
            click(startPage.filterPendingBreederAndRefugeTabButton);
            //Search for the identifier to approve
            setText(pending.searchField,currentIdentifier);
            //Wait for the update of the first line
            sleep(5000);
            checkIdentifierIsPresentInFilter(pending.firstLineTableResult,pending.firstIdentifierTableResult,currentIdentifier,"the pending");
            click(startPage.registerTabButton);
        }


        if (registrator.equals("Veterinary")) {
            //user click on sign now
            setText(registerPage.birthDate, currentDateTimeStamp);
            setText(registerPage.identificationDate, currentDateTimeStamp);
            fillCatIdentifier(registerPage.firstIdentifier);
            setText(registerPage.breedCatTextField, "");
            //the system will validate encoded data
            //at least one data is detected invalid
            //form is not submitted
            checkDetectionErrorMessage(registerPage, eazyIdPage, "Breed must be selected from list", "Now");
            //the registration page si displayed
            //error message is displayed regarding the data in error
            scrollOnElement(registerPage.breedCatTextField);
            if (isRed(getRgbString(registerPage.breedCatTextField))) {
                logger.log(LogStatus.INFO, "Breed field display font is red");
            } else {
                logger.log(LogStatus.ERROR, "Breed field display font is not red");
            }
        }

        //all data are valid
        scrollOnElement(registerPage.birthDate);
        setText(registerPage.breedCatTextField,"Abyssinian");
        click(registerPage.selectBreedTooltip);
        currentIdentifier = registerPage.firstIdentifier.getAttribute("value").replace("-","");
        if(registrator.equals("Veterinary")){
            click(registerPage.submitButtonVet);
            //user has right to sign registration for veterinary only
            click(registerPage.signVetNowOrAccrSendVetButton);
            //User autenticate and process signature
            eazyId(eazyIdPage);
            click(eazyIdPage.closeValidSubmitMessage);
            //Signature process is successful
            //form is signed
            //form is available in approved
            //Search for the identifier approved
            WaitForElement(startPage.filterButton,60);
            click(startPage.filterButton);
            click(startPage.filterApprovedVetTabButton);
            WebApprovedPage approvedPage = new WebApprovedPage(driver);
            setText(approvedPage.searchField,currentIdentifier);
            //Wait for the update of the first line
            sleep(5000);
            checkIdentifierIsPresentInFilter(approvedPage.firstLineTableResult,approvedPage.uniqueIdentifierTableResult,currentIdentifier,"approved");
            click(startPage.registerTabButton);
            WaitForElement(registerPage.firstNameTextFieldFirstOwner, 60);
            scrollOnElement(registerPage.birthDate);
            fillCatIdentifier(registerPage.firstIdentifier);
            currentIdentifier = registerPage.firstIdentifier.getAttribute("value").replace("-","");
            scrollOnElement(registerPage.birthDate);
            setText(registerPage.birthDate,currentDateTimeStamp);
            setText(registerPage.identificationDate,currentDateTimeStamp);
            setText(registerPage.breedCatTextField,"Ab");
            click(registerPage.selectBreedTooltip);
            //easylink signature process is displayed
            click(registerPage.submitButtonVet);
            click(registerPage.signVetNowOrAccrSendVetButton);
            click(eazyIdPage.denyButton);
//            sleep(5000); //Cannot wait for hidden element ...
            //Exit "Yes" button is not reachable, check if the button is an alert or whatever
            clickHiddenElement(eazyIdPage.diablogSubmitMessage);
            WaitForElement(eazyIdPage.closeValidSubmitMessage,10);
            click(eazyIdPage.closeValidSubmitMessage);
            logger.log(LogStatus.INFO, "Signature process is unsuccessful, form is not signed, form is available in waiting for approvals");
            //Signature process is unsuccessful
            //form is not signed
            //form is available in waiting for approvals
            //Search for the identifier to approve
            click(startPage.filterButton);
            click(startPage.filterWaitingForApprovalVetTabButton);
            setText(waiting.searchField,currentIdentifier);
            //Wait for the update of the first line
            sleep(5000);
            checkIdentifierIsPresentInFilter(waiting.firstLineTableResult,waiting.uniqueIdentifierTableResult,currentIdentifier,"the waiting for approval");
            //Unpin first owner pin
            click(startPage.registerTabButton);
            click(registerPage.pinFirstOwner);
        }


        //Unpin breed pin
        scrollOnElement(registerPage.pinCatBreed);
        clickHiddenElement(registerPage.pinCatBreed);

        //Pin test
        if(checkPinnedValues(registerPage,eazyIdPage, startPage)){
            logger.log(LogStatus.INFO, "All pin buttons are OK");
        } else {
            logger.log(LogStatus.ERROR, "ERROR: All pin buttons are not OK");
        }
    }

    private void checkIdentifierIsPresentInFilter(WebElement firstLine, WebElement identifierCell,String currentIdentifier, String filterType){
        try {
            WaitForElement(firstLine,5);
            if(checkFirstFilteredLineIdentifier(identifierCell,currentIdentifier)){
                logger.log(LogStatus.INFO, "Form submitted is available in " + filterType);
            } else {
                logger.log(LogStatus.ERROR, "ERROR: Form submitted is not available in " + filterType);
            }

        } catch (Exception e){
            logger.log(LogStatus.ERROR, "ERROR: Form submitted is not available in " + filterType + "...");
        }
    }

    private boolean checkFirstFilteredLineIdentifier (WebElement we, String identifier){
        boolean isDetected = false;

        if(we.getAttribute("textContent").equals(identifier)){
            isDetected = true;
        } else { //Maybe the filterred line is not updated yet, so wait again once and recheck a last time
            sleep(5000);
            if(we.getAttribute("textContent").equals(identifier)){
                click(we);
                isDetected = true;
            }
        }
        return isDetected;
    }

    private void resetPrivatePersonInfoFromeID(WebRegisterPage registerPage, WebEazyIdPage eazyIdPage){
        if(registrator.equals("Veterinary")) {
            clickHiddenElement(registerPage.checkBoxPrivatePersonFirstOwner);
            click(registerPage.readDataFromFirstOwnerEiDCardButton);
            click(eazyIdPage.consentCheckbox);
            click(eazyIdPage.agreeButton);
        }
        WaitForElement(registerPage.birthDate,60);
        scrollOnElement(registerPage.birthDate);
        fillCatIdentifier(registerPage.firstIdentifier);
    }

    private boolean checkPinnedValues(WebRegisterPage registerPage, WebEazyIdPage eazyIdPage, WebStartPage startPage){
        boolean areAllPinnedValuesCorrect = false;

        //Fill all pinnable values
        //Re filled private person info
        if(registrator.equals("Veterinary")) {
            click(startPage.registerTabButton);
            clickHiddenElement(registerPage.checkBoxPrivatePersonFirstOwner);
            click(registerPage.readDataFromFirstOwnerEiDCardButton);
            click(eazyIdPage.consentCheckbox);
            click(eazyIdPage.agreeButton);
            click(registerPage.pinFirstOwner);
        }

        //fill identifier just in case
        fillCatIdentifier(registerPage.firstIdentifier);
        scrollOnElement(registerPage.birthDate);

        //fill cat information fields which have a pin
        String currentTimeStamp = new SimpleDateFormat("dd.MM.yyyy").format(new Date());
        setText(registerPage.identificationDate,currentTimeStamp);
        setText(registerPage.birthDate,currentTimeStamp);
        setText(registerPage.sterilizationDate,currentTimeStamp);
        setText(registerPage.colorCatTextField,"Red");
        setText(registerPage.breedCatTextField,"Abyssinian");

        click(registerPage.pinBirthDate);
        click(registerPage.pinCatBreed);
        click(registerPage.pinCatColor);
        click(registerPage.pinIdentificationDate);
        click(registerPage.pinSterilizationDateDate);


        click(startPage.registerTabButton);
        //Pre-requisite
        if(isFilled(registerPage.firstNameTextFieldFirstOwner) &&
           isFilled(registerPage.identificationDate) &&
           isFilled(registerPage.birthDate) &&
           isFilled(registerPage.sterilizationDate) &&
           isFilled(registerPage.colorCatTextField) &&
           isFilled(registerPage.breedCatTextField)) {
            areAllPinnedValuesCorrect = true;
        }

        return areAllPinnedValuesCorrect;
    }

    private void checkCatIdentification(WebRegisterPage registerPage, WebEazyIdPage eazyIdPage, WebStartPage startPage){
        //Re filled private person info
        if(registrator.equals("Veterinary")) {
            clickHiddenElement(registerPage.checkBoxPrivatePersonFirstOwner);
            click(registerPage.readDataFromFirstOwnerEiDCardButton);
            click(eazyIdPage.consentCheckbox);
            click(eazyIdPage.agreeButton);
        }
        WaitForElement(registerPage.transfertOfResponsibility,60);
        scrollOnElement(registerPage.transfertOfResponsibility);
        //user can encode an invalid format of identifier
        setText(registerPage.firstIdentifier,"ABCD");
        if(registerPage.firstIdentifier.getAttribute("value").equals("")){
            logger.log(LogStatus.INFO, "Identifier is only numeric");
        } else {
            logger.log(LogStatus.ERROR, "ERROR: Identifier is alphanumeric");
        }

        setText(registerPage.firstIdentifier, "123");
        scrollOnElement(registerPage.birthDate);
        if(!checkDetectionErrorMessage(registerPage,eazyIdPage,"Identifier must be 15 digits")){
            resetPrivatePersonInfoFromeID(registerPage,eazyIdPage);
        }

        //user will be able to submit form
        //This submit test is tested on Class RegistrationPageCorTest (17 times :) )

        //user encode an already used cat identifier
        setText(registerPage.firstIdentifier,"123456789409022");
        if(isRed(getRgbString(registerPage.firstIdentifier))){
            logger.log(LogStatus.INFO, "Identifier display font is red");
        } else {
            logger.log(LogStatus.ERROR, "Identifier display font is not red");
        }

        //user encode an already used dog identifier
        setText(registerPage.firstIdentifier,"444201603021405");
        if(isRed(getRgbString(registerPage.firstIdentifier))){
            logger.log(LogStatus.INFO, "Identifier display font is red");
        } else {
            logger.log(LogStatus.ERROR, "Identifier display font is not red");
        }

        //user encode a non-used cat identifier
        //identifier display font green
        fillCatIdentifier(registerPage.firstIdentifier);
        if(isGreen(getRgbString(registerPage.firstIdentifier))){
            logger.log(LogStatus.INFO, "Identifier display font is green");
        } else {
            logger.log(LogStatus.ERROR, "Identifier display font is not green");
        }

        //user must select an id location  from dropdown list
        //left ear
        //right ear
        //left thigh
        //right thigh
        //left neck
        //right neck

        Select dropdown = new Select(registerPage.firstIdentifierLocation);

        //Check default value is left neck
        if(dropdown.getFirstSelectedOption().getAttribute("textContent").equals("Left neck")){
            logger.log(LogStatus.INFO, "Default identifier Location is Left neck");
        } else {
            logger.log(LogStatus.ERROR, "ERROR: Default identifier Location is not Left neck");
        }

        Map  visibleDropListElement = new HashMap();
        visibleDropListElement.put("Left neck", 0);
        visibleDropListElement.put("Right neck", 0);
        visibleDropListElement.put("Left ear", 0);
        visibleDropListElement.put("Right ear", 0);
        visibleDropListElement.put("Left thigh", 0);
        visibleDropListElement.put("Right thigh", 0);

        int numberIdentifierLocation = 0;
        for(WebElement we : dropdown.getOptions()){
            //System.out.println(we.getAttribute("textContent"));
            if(visibleDropListElement.containsKey(we.getAttribute("textContent"))){
                numberIdentifierLocation++;
            }
        }

        if(numberIdentifierLocation == 6){
            logger.log(LogStatus.INFO, "Identifier Locations are correct");
        } else {
            logger.log(LogStatus.ERROR, "ERROR: Identifier Locations are not correct");
        }

        //User cannot edit field
        // Identifier location cannot be empty
        if(!isEditable(registerPage.firstIdentifierLocation)){
            logger.log(LogStatus.INFO, "Identifier location cannot be empty");
        } else {
            logger.log(LogStatus.ERROR, "ERROR: Identifier location can be empty");
        }

        //Check Passport
        //Belgium passport already used by a another Cat
        setText(registerPage.passportNumber,"BE0286930057");
        //Check error message
        checkDetectionErrorMessage(registerPage,eazyIdPage," already used passport");
        scrollOnElement(registerPage.passportNumber);
        //Belgium passport already used by a another Dog
        setText(registerPage.passportNumber,"BE0234790082");
        //Check error message
        checkDetectionErrorMessage(registerPage,eazyIdPage," already used passport");
        scrollOnElement(registerPage.passportNumber);
        //Belgium passport is not correctly formated (not modulo 97)
        setText(registerPage.passportNumber,"BE0000000000");
        //Check error message
        checkDetectionErrorMessage(registerPage,eazyIdPage,"Passport is no issue by Zetes");
        scrollOnElement(registerPage.passportNumber);
        //Belgium passport is is correct
        setText(registerPage.passportNumber,"BE03003457799"); //passport number extract from real one. Never submit it to complete a registration or you need to change this value
        //Check error message for breed, because if passport number is correct, the GUI will complain on the breed (which is not filled yet)
        if(registrator.equals("Veterinary")) {
            checkDetectionErrorMessage(registerPage, eazyIdPage, "Breed must be selected from list");
        } else  {
            checkDetectionValidSubmit(registerPage, eazyIdPage, "Vet");
            //Check the form is sent to pending if mandatory fields are missing
            //Search for the form to delete
            click(startPage.filterButton);
            click(startPage.filterSentForApprovalBreederAndRefugeTabButton);
            WebSentForApprovalPage sent = new WebSentForApprovalPage(driver);
            //Delete the form in order to be able to reuse this passport
            setText(sent.searchField,"BE03003457799");
            //Wait for the update of the first line
            sleep(5000);
            click(sent.firstIdentifierTableResult);
            click(sent.deleteButton);
            click(sent.acceptDeletionButton);
            sleep(1000);
            WaitForElement(startPage.registerTabButton,60);
            click(startPage.registerTabButton);
            sleep(5000);
            fillCatIdentifier(registerPage.firstIdentifier);
        }

        scrollOnElement(registerPage.passportNumber);
        //Foreign passport already used by a another Cat
        setText(registerPage.passportNumber,"CH201706010931");
        //Check error message
        checkDetectionErrorMessage(registerPage,eazyIdPage," already used passport");
        scrollOnElement(registerPage.passportNumber);
        //Foreign passport already used by a another Dog
        setText(registerPage.passportNumber,"CH201706011218");
        //Check error message
        checkDetectionErrorMessage(registerPage,eazyIdPage," already used passport");
        scrollOnElement(registerPage.passportNumber);
        //Belgium passport is is correct
        setText(registerPage.passportNumber,"CH201706011219"); //passport number extract from real one. Never submit it to complete a registration or you need to change this value
        //Check error message for breed, because if passport number is correct, the GUI will complain on the breed (which is not filled yet)
        if(registrator.equals("Veterinary")) {
            checkDetectionErrorMessage(registerPage, eazyIdPage, "Breed must be selected from list");
        } else  {
            //on submit, birthdate can be empty (pending)
            checkDetectionValidSubmit(registerPage, eazyIdPage, "Vet");
            //Check the form is sent to pending if mandatory fields are missing
            //Search for the identifier to approve
            click(startPage.filterButton);
            click(startPage.filterSentForApprovalBreederAndRefugeTabButton);
            WebSentForApprovalPage sent = new WebSentForApprovalPage(driver);
            setText(sent.searchField,"CH201706011219");
            //Wait for the update of the first line
            sleep(5000);
            click(sent.firstIdentifierTableResult);
            click(sent.deleteButton);
            click(sent.acceptDeletionButton);
            WaitForElement(startPage.registerTabButton,60);
            sleep(1000);
            click(startPage.registerTabButton);
            sleep(5000);
            fillCatIdentifier(registerPage.firstIdentifier);
        }
        scrollOnElement(registerPage.passportNumber);
        //Clear passport to avoid error for future test
        setText(registerPage.passportNumber,"");

        //User can add an second identifier
        //second identifier must be displayed
        if(registrator.equals("Veterinary")){
            click(registerPage.addIdentifierVet);
        } else {
            click(registerPage.addIdentifierOther);
        }

        try {
            WaitForElement(registerPage.secondIdentifier,5);
            logger.log(LogStatus.INFO, "Second identifier can be added");
        } catch (Exception e){
            logger.log(LogStatus.ERROR, "ERROR: Second identifier cannot be added");
        }

        //User can remove an second identifier
        //second identifier is deleted
        if(registrator.equals("Veterinary")){
            click(registerPage.addIdentifierVet);
        } else {
            click(registerPage.addIdentifierOther);
        }

        try {
            WaitForElement(registerPage.secondIdentifier,5);
            logger.log(LogStatus.ERROR, "ERROR: Second identifier cannot be removed");
        } catch (Exception e){
            logger.log(LogStatus.INFO, "Second identifier can be removed");
        }

    }

    private void checkFirstResponsibleInformation(WebRegisterPage registerPage, WebEazyIdPage eazyIdPage){
        if(registrator.equals("Veterinary")) {
            //From HK
            //User can encode invalid HK
            setText(registerPage.refNumberTextFieldFirstOwner, "aaaa");
            sleep(2000);//Wait for the field is red
            //Fetch result give red characters
            if(isRed(getRgbString(registerPage.refNumberTextFieldFirstOwner))){
                logger.log(LogStatus.INFO, "refNumberTextFieldFirstOwner is red for incorrect input");
            } else {
                logger.log(LogStatus.ERROR, "refNumberTextFieldFirstOwner is not red for incorrect input");
            }
            //No data reached and filled
            if(registerPage.refNumberTextFieldFirstOwner.getText().equals("")){
                logger.log(LogStatus.INFO, "no data reached and filled for incorrect input");
            } else {
                logger.log(LogStatus.ERROR, "data reached and filled for incorrect input");
            }

            //User can encode valid HK
            setText(registerPage.refNumberTextFieldFirstOwner, "HK12341234");
            //Valid HK format is X2 + N8
            //Fetch result is a popup list of users
            click(registerPage.selectFisrtResponsibleToolTip);//if clickable so it is valid

            //All fields are automatically filled
            //Fields are non-editable
            checkFieldsFirstResponsibleFromHk(registerPage, "HK12341234");

            //Clear all fields to make sure the fields are filled
            click(registerPage.clearFirstOwner);

            //User can select another responsible from list resulting of the fetch
            setText(registerPage.refNumberTextFieldFirstOwner, "HK43214321");
            //Valid HK format is X2 + N8
            //Fetch result is a popup list of users
            click(registerPage.selectFisrtResponsibleToolTip);//if clickable so it is valid

            //All fields are automatically filled
            //Fields are non-editable
            checkFieldsFirstResponsibleFromHk(registerPage, "HK43214321");

            //User can click on Regular to change data from HK to Regular
            clickHiddenElement(registerPage.checkBoxPrivatePersonFirstOwner);
            //From Regular
            //Fetch information details is displayed
            //User can fetch by RRN
            setText(registerPage.searchFirstOwnerPrivatePersonField, "71715100070");
            clickHiddenElement(registerPage.searchFirstOwnerPrivatePersonButton);

            checkFieldsFirstResponsibleFromPrivatePerson(registerPage, " RRN 71715100070");

            //Clear all fields to make sure the fields are filled
            click(registerPage.clearFirstOwner);

            //User can fetch from EID card
            click(registerPage.readDataFromFirstOwnerEiDCardButton);
            click(eazyIdPage.consentCheckbox);
            click(eazyIdPage.agreeButton);

            checkFieldsFirstResponsibleFromPrivatePerson(registerPage, "eID card");

            //Check confidential flag for private person
            click(registerPage.firstConfidentialCheckBox);
            //Check the confidential flag is enable
            if(registerPage.firstConfidentialCheckBox.getAttribute("class").equals("ui toggle checkbox checked")){
                logger.logScreenshot(LogStatus.INFO, "Confidential flag is enable", getScreenShot());
            } else {
                logger.logScreenshot(LogStatus.ERROR, "Confidential flag is disable", getScreenShot());
            }

            click(registerPage.firstConfidentialCheckBox);
            //Check the confidential flag is disable
            if(registerPage.firstConfidentialCheckBox.getAttribute("class").equals("ui toggle checkbox")){
                logger.logScreenshot(LogStatus.INFO, "Confidential flag is disable", getScreenShot());
            } else {
                logger.logScreenshot(LogStatus.ERROR, "Confidential flag is enable", getScreenShot());
            }
        } else {
            //The information of the First owner is displayed with Reference user, First name and Last name
            if(!isEditable(registerPage.firstResponsibleBreederOrRefugeInformation)){
                logger.logScreenshot(LogStatus.INFO, "Responsible information for non-Vet are not editable", getScreenShot());
                String responsibleInformation = registerPage.firstResponsibleBreederOrRefugeInformation.getAttribute("textContent");
                //System.out.println(responsibleInformation);
                if(registrator.equals("Breeder 1") && responsibleInformation != null){
                    if(responsibleInformation.contains("HK43214321") && responsibleInformation.contains("Alice Geldigekaart") && responsibleInformation.contains("SPECIMEN")){
                        logger.logScreenshot(LogStatus.INFO, "Breeder information is displayed with reference, first and last name", getScreenShot());
                    } else {
                        logger.logScreenshot(LogStatus.ERROR, "ERROR: Breeder information is not displayed with reference, first and last name", getScreenShot());
                    }
                } else if(registrator.equals("Refuge 1") && responsibleInformation != null){
                    if(responsibleInformation.contains("HK12341234") && responsibleInformation.contains("Alice Geldigekaart") && responsibleInformation.contains("SPECIMEN")){
                        logger.logScreenshot(LogStatus.INFO, "Refuge information is displayed with reference, first and last name", getScreenShot());
                    } else {
                        logger.logScreenshot(LogStatus.ERROR, "ERROR: Refuge information is not displayed with reference, first and last name", getScreenShot());
                    }
                }


            } else {
                logger.logScreenshot(LogStatus.ERROR, "ERROR: Responsible information for non-Vet are editable", getScreenShot());
            }
        }

    }

    private void checkNewResponsibleInformation(WebRegisterPage registerPage, WebEazyIdPage eazyIdPage){
        if(!registrator.equals("Refuge 1")) {//No Registration for HK or Vet allowed for refuge
            //From HK
            //User can encode invalid HK
            setText(registerPage.refNumberTextFieldSecondOwner, "aaaa");
            sleep(2000);//Wait for the field is red
            //Fetch result give red characters
            if (isRed(getRgbString(registerPage.refNumberTextFieldSecondOwner))) {
                logger.log(LogStatus.INFO, "refNumberTextFieldSecondOwner is red for incorrect input");
            } else {
                logger.log(LogStatus.ERROR, "refNumberTextFieldSecondOwner is not red for incorrect input");
            }
            //No data reached and filled
            if (registerPage.refNumberTextFieldSecondOwner.getText().equals("")) {
                logger.log(LogStatus.INFO, "no data reached and filled for incorrect input");
            } else {
                logger.log(LogStatus.ERROR, "data reached and filled for incorrect input");
            }

            //User can encode valid HK
            setText(registerPage.refNumberTextFieldSecondOwner, "HK12341234");
            //Valid HK format is X2 + N8
            //Fetch result is a popup list of users
            click(registerPage.selectSecondResponsibleToolTip);//if clickable so it is valid

            //All fields are automatically filled
            //Fields are non-editable
            checkFieldsSecondResponsibleFromHk(registerPage, "HK12341234");

            //Clear all fields to make sure the fields are filled
            click(registerPage.clearSecondOwnerForVetOrBreeder);

            //User can select another responsible from list resulting of the fetch
            setText(registerPage.refNumberTextFieldSecondOwner, "HK43214321");
            //Valid HK format is X2 + N8
            //Fetch result is a popup list of users
            click(registerPage.selectSecondResponsibleToolTip);//if clickable so it is valid

            //All fields are automatically filled
            //Fields are non-editable
            checkFieldsSecondResponsibleFromHk(registerPage, "HK43214321");

            //User can click on Regular to change data from HK to Regular
            clickHiddenElement(registerPage.checkBoxPrivatePersonSecondOwner);
        }
        //From Regular
        //Fetch information details is displayed
        //User can fetch by RRN
        setText(registerPage.searchSecondOwnerPrivatePersonField, "71715100070");
        clickHiddenElement(registerPage.searchSecondOwnerPrivatePersonButton);

        checkFieldsSecondResponsibleFromPrivatePerson(registerPage, " RRN 71715100070");
        //Clear all fields to make sure the fields are filled
        if(registrator.equals("Refuge 1")){
            click(registerPage.clearSecondOwnerForRefuge);
        } else {
            click(registerPage.clearSecondOwnerForVetOrBreeder);
        }
        //User can fetch from EID card
        click(registerPage.readDataFromSecondOwnerEiDCardButton);
        click(eazyIdPage.consentCheckbox);
        click(eazyIdPage.agreeButton);
        checkFieldsSecondResponsibleFromPrivatePerson(registerPage, "eID card");

        //Check confidential flag for private person
        click(registerPage.secondConfidentialCheckBox);
        //Check the confidential flag is enable
        if(registerPage.secondConfidentialCheckBox.getAttribute("class").equals("ui toggle checkbox checked")){
            logger.logScreenshot(LogStatus.INFO, "Confidential flag is enable", getScreenShot());
        } else {
            logger.logScreenshot(LogStatus.ERROR, "Confidential flag is disable", getScreenShot());
        }

        click(registerPage.secondConfidentialCheckBox);
        //Check the confidential flag is disable
        if(registerPage.secondConfidentialCheckBox.getAttribute("class").equals("ui toggle checkbox")){
            logger.logScreenshot(LogStatus.INFO, "Confidential flag is disable", getScreenShot());
        } else {
            logger.logScreenshot(LogStatus.ERROR, "Confidential flag is enable", getScreenShot());
        }

        //transfer date must be defined
        isFilled(registerPage.transfertDateField);
    }

    private void userFirstAndSecondResponsibleAreDifferent(WebRegisterPage registerPage, WebStartPage startPage, WebEazyIdPage eazyIdPage){
        if(registrator.equals("Veterinary")) {
            //Check same Private person
            click(startPage.registerTabButton);
            WaitForElement(registerPage.transfertOfResponsibility, 60);
            //Select First Private person user
            clickHiddenElement(registerPage.checkBoxPrivatePersonFirstOwner);
            setText(registerPage.searchFirstOwnerPrivatePersonField, "71715100070");
            clickHiddenElement(registerPage.searchFirstOwnerPrivatePersonButton);
            //Select Second Private person user
            click(registerPage.transfertOfResponsibility);
            scrollOnElement(registerPage.phoneTextFieldFirstOwner);
            clickHiddenElement(registerPage.checkBoxPrivatePersonSecondOwner);
            setText(registerPage.searchSecondOwnerPrivatePersonField, "71715100070");
            clickHiddenElement(registerPage.searchSecondOwnerPrivatePersonButton);

            //FillCat identification
            fillCatIdentifier(registerPage.firstIdentifier);

            //Fill cat information fields
            scrollOnElement(registerPage.identificationDate);
            String timeStamp = new SimpleDateFormat("dd.MM.yyyy").format(new Date());
            setText(registerPage.birthDate, timeStamp);
            registerPage.birthDate.sendKeys(Keys.RETURN);
            setText(registerPage.identificationDate, timeStamp);
            registerPage.identificationDate.sendKeys(Keys.RETURN); //For unknown reason the date is not correctly filled
            setText(registerPage.colorCatTextField, "black");
            setText(registerPage.breedCatTextField, "Ab");
            click(registerPage.selectBreedTooltip);

            //Click on all pin button for cat identification to check behavior for next test case
            click(registerPage.pinBirthDate);
            click(registerPage.pinCatBreed);
            click(registerPage.pinCatColor);
            click(registerPage.pinIdentificationDate);

            //Check error message
            checkDetectionErrorMessage(registerPage, eazyIdPage, "The same owner was selected. Please choose different owners");

            //Check same Breeder
            click(startPage.registerTabButton);
            WaitForElement(registerPage.transfertOfResponsibility, 60);
            //Select First Breeder user
            setText(registerPage.refNumberTextFieldFirstOwner, "HK43214321");
            click(registerPage.selectFisrtResponsibleToolTip);//if clickable so it is valid

            //Select Second Breeder user
            click(registerPage.transfertOfResponsibility);
            scrollOnElement(registerPage.phoneTextFieldFirstOwner);
            setText(registerPage.refNumberTextFieldSecondOwner, "HK43214321");
            click(registerPage.selectSecondResponsibleToolTip);//if clickable so it is valid
            scrollOnElement(registerPage.identificationDate);
            fillCatIdentifier(registerPage.firstIdentifier);
            //Check error message
            checkDetectionErrorMessage(registerPage, eazyIdPage, "The same owner was selected. Please choose different owners");

            //Check same Refuge
            click(startPage.registerTabButton);
            WaitForElement(registerPage.transfertOfResponsibility, 60);
            //Select First Refuge user
            setText(registerPage.refNumberTextFieldFirstOwner, "HK12341234");
            click(registerPage.selectFisrtResponsibleToolTip);//if clickable so it is valid

            //Select Second Refuge user
            click(registerPage.transfertOfResponsibility);
            scrollOnElement(registerPage.phoneTextFieldFirstOwner);
            setText(registerPage.refNumberTextFieldSecondOwner, "HK12341234");
            click(registerPage.selectSecondResponsibleToolTip);//if clickable so it is valid
            scrollOnElement(registerPage.identificationDate);
            fillCatIdentifier(registerPage.firstIdentifier);
            //Check error message
            checkDetectionErrorMessage(registerPage, eazyIdPage, "The same owner was selected. Please choose different owners");

            scrollOnElement(registerPage.identificationDate);
            //Unpin buttons
            click(registerPage.pinBirthDate);
            click(registerPage.pinCatBreed);
            click(registerPage.pinCatColor);
            click(registerPage.pinIdentificationDate);
            click(startPage.registerTabButton);
        } else if(registrator.equals("Breeder 1")){//Impossible to assign same user with Refuge (can select only private person for New responsible and current responsible cannot be a private person

            //Select same Second Breeder user
            click(startPage.registerTabButton);
            WaitForElement(registerPage.transfertOfResponsibility, 60);
            click(registerPage.transfertOfResponsibility);
            setText(registerPage.refNumberTextFieldSecondOwner, "HK43214321");
            click(registerPage.selectSecondResponsibleToolTip);//if clickable so it is valid
            scrollOnElement(registerPage.identificationDate);
            fillCatIdentifier(registerPage.firstIdentifier);
            //Fill cat information fields
            scrollOnElement(registerPage.identificationDate);
            String timeStamp = new SimpleDateFormat("dd.MM.yyyy").format(new Date());
            setText(registerPage.birthDate, timeStamp);
            registerPage.birthDate.sendKeys(Keys.RETURN);
            setText(registerPage.identificationDate, timeStamp);
            registerPage.identificationDate.sendKeys(Keys.RETURN); //For unknown reason the date is not correctly filled
            setText(registerPage.colorCatTextField, "black");
            setText(registerPage.breedCatTextField, "Ab");
            click(registerPage.selectBreedTooltip);
            //Check error message
            checkDetectionErrorMessage(registerPage, eazyIdPage, "The same owner was selected. Please choose different owners");

            //reset new responsible information
            click(startPage.registerTabButton);
            WaitForElement(registerPage.transfertOfResponsibility, 60);
        }
    }

    private boolean checkDetectionErrorMessage(WebRegisterPage registerPage, WebEazyIdPage eazyIdPage, String errorMessage){
        if(registrator.equals("Veterinary")){
            return checkDetectionErrorMessage(registerPage, eazyIdPage, errorMessage, "Later");
        } else {
            return checkDetectionErrorMessage(registerPage, eazyIdPage, errorMessage, "Vet");
        }
    }

    private boolean checkDetectionErrorMessage(WebRegisterPage registerPage, WebEazyIdPage eazyIdPage, String errorMessage, String kindOfSign){
        boolean isDetected = false;
        if(registrator.equals("Veterinary")){
            click(registerPage.submitButtonVet);
        } else {
            click(registerPage.submitButtonOther);
        }
        //For breeder "Sign Later" becomes "Send to Pending" and "Sign Now" becomes "Send to Vet"
        if(kindOfSign.equals("Later") || kindOfSign.equals("Pending")){
            click(registerPage.signVetLaterOrAccrPendingButton);
        } else if (kindOfSign.equals("Now") || kindOfSign.equals("Vet")){
            click(registerPage.signVetNowOrAccrSendVetButton);
        }

        try {
            WaitForElement(registerPage.errorMessagePage,5);
            logger.log(LogStatus.INFO,"Found: " + registerPage.errorMessagePage.getAttribute("textContent"));
            logger.log(LogStatus.INFO,"Expected: " +errorMessage);
            if(registerPage.errorMessagePage.getAttribute("textContent").equals(errorMessage)){
                logger.logScreenshot(LogStatus.INFO, "Error case correctly detected for " + registrator, getScreenShot());
                isDetected = true;
            }
            else {
                logger.logScreenshot(LogStatus.ERROR, "ERROR: Error case message is wrong... for " + registrator, getScreenShot());
                WaitForElement(eazyIdPage.closeValidSubmitMessage, 5);
                click(eazyIdPage.closeValidSubmitMessage);
            }
        } catch (Exception e){
//            WaitForElement(eazyIdPage.closeValidSubmitMessage, 5);
//            click(eazyIdPage.closeValidSubmitMessage);
            sleep(5000); // The notification disappear after 5 seconds (cannot wait for it anymore)
            fillCatIdentifier(registerPage.firstIdentifier);
            logger.logScreenshot(LogStatus.ERROR, "ERROR: Error case message is wrong for " + registrator + " Expected: " + errorMessage, getScreenShot());
        }

        return isDetected;
    }

    private boolean checkDetectionValidSubmit(WebRegisterPage registerPage, WebEazyIdPage eazyIdPage){
        return checkDetectionValidSubmit(registerPage, eazyIdPage, "Later");
    }

    private boolean checkDetectionValidSubmit(WebRegisterPage registerPage, WebEazyIdPage eazyIdPage, String kindOfSign){
        boolean isDetected = false;
        if(registrator.equals("Veterinary")){
            click(registerPage.submitButtonVet);
        } else {
            click(registerPage.submitButtonOther);
        }
        //For breeder "Sign Later" becomes "Send to Pending" and "Sign Now" becomes "Send to Vet"
        if(kindOfSign.equals("Later") || kindOfSign.equals("Pending")){
            click(registerPage.signVetLaterOrAccrPendingButton);
        } else if (kindOfSign.equals("Now") || kindOfSign.equals("Vet")){
            click(registerPage.signVetNowOrAccrSendVetButton);
            if(registrator.equals("Veterinary")) {
                eazyId(eazyIdPage);
            }
        }
        try {
            WaitForElement(eazyIdPage.closeValidSubmitMessage, 5);
            click(eazyIdPage.closeValidSubmitMessage);
            fillCatIdentifier(registerPage.firstIdentifier);
            logger.logScreenshot(LogStatus.INFO, "Valid submit is correctly detected for " + registrator, getScreenShot());
            isDetected = true;
        } catch (Exception e){
            WaitForElement(registerPage.errorMessagePage,5);
            logger.logScreenshot(LogStatus.ERROR, "Valid submit is not correctly detected... for " + registrator, getScreenShot());
        }

        return isDetected;
    }

    private void checkFieldsFirstResponsibleFromHk(WebRegisterPage registerPage, String hkNumber){
        //Fields are non-editable
        if(isEditable(registerPage.firstNameTextFieldFirstOwner) ||
                isEditable(registerPage.lastNameTextFieldFirstOwner) ||
                isEditable(registerPage.nationalNumberTextFieldFirstOwner) ||
                isReadOnly(registerPage.postalCodeAndMunicipalityTextFieldFirstOwner) ||
                isEditable(registerPage.streetNumberTextFieldFirstOwner) ||
                isEditable(registerPage.streetNumberTextFieldFirstOwner) ||
                isEditable(registerPage.phoneTextFieldFirstOwner) ||
                isEditable(registerPage.mobileTextFieldFirstOwner) ||
                isEditable(registerPage.faxTextFieldFirstOwner) ||
                isEditable(registerPage.emailTextFieldFirstOwner)){
            logger.log(LogStatus.ERROR, "Fields are editable for user " + hkNumber);
        } else {
            logger.log(LogStatus.INFO, "Fields are non-editable for user " + hkNumber );
        }
    }

    //Second responsible is the new one
    private void checkFieldsSecondResponsibleFromHk(WebRegisterPage registerPage, String hkNumber){
        //Fields are non-editable
        if(isEditable(registerPage.firstNameTextFieldSecondOwner) ||
                isEditable(registerPage.lastNameTextFieldSecondOwner) ||
                isEditable(registerPage.nationalNumberTextFieldSecondOwner) ||
                isEditable(registerPage.postalCodeAndMunicipalityTextFieldSecondOwner) ||
                isEditable(registerPage.streetTextFieldSecondOwner) ||
                isEditable(registerPage.streetNumberTextFieldSecondOwner) ||
                isEditable(registerPage.phoneTextFieldSecondOwner) ||
                isEditable(registerPage.mobileTextFieldSecondOwner) ||
                isEditable(registerPage.faxTextFieldSecondOwner) ||
                isEditable(registerPage.emailTextFieldSecondOwner)){
            logger.log(LogStatus.ERROR, "Fields are editable for user " + hkNumber);
        } else {
            logger.log(LogStatus.INFO, "Fields are non-editable for user " + hkNumber );
        }
    }

    private void checkFieldsFirstResponsibleFromPrivatePerson(WebRegisterPage registerPage, String privatePerson){
        //All mandatory fields are filled (meaning the RRN is fetch)
        WaitForElement(registerPage.firstNameTextFieldFirstOwner, 60);
        if(isFilled(registerPage.firstNameTextFieldFirstOwner) &&
                isFilled(registerPage.lastNameTextFieldFirstOwner)){
            logger.log(LogStatus.INFO, "Fields are filled for " + privatePerson);
        } else {
            logger.log(LogStatus.ERROR, "Fields are not filled for " + privatePerson);
        }

        //Address fields are not editable
        //Firt Name
        //Last Name
        //RRN
        if(!isEditable(registerPage.firstNameTextFieldFirstOwner) &&
                !isEditable(registerPage.lastNameTextFieldFirstOwner) &&
                !isEditable(registerPage.nationalNumberTextFieldFirstOwner)){
            logger.log(LogStatus.INFO, "Fields are  non-editable for  " + privatePerson);
        } else {
            logger.log(LogStatus.ERROR, "Fields are editable for " + privatePerson);
        }
        //Address fields are editable
        //Postal code
        //Street
        //Number
        //Phone
        //Mobile
        //Fax
        //Email
        if(isReadOnly(registerPage.postalCodeAndMunicipalityTextFieldFirstOwner) &&
                isEditable(registerPage.streetTextFieldFirstOwner) &&
                isEditable(registerPage.streetNumberTextFieldFirstOwner) &&
                isEditable(registerPage.phoneTextFieldFirstOwner) &&
                isEditable(registerPage.mobileTextFieldFirstOwner) &&
                isEditable(registerPage.faxTextFieldFirstOwner) &&
                isEditable(registerPage.emailTextFieldFirstOwner)){
            logger.log(LogStatus.INFO, "Fields are editable for " + privatePerson);
        } else {
            logger.log(LogStatus.ERROR, "Fields are non-editable for " + privatePerson);
        }
    }

    private void checkFieldsSecondResponsibleFromPrivatePerson(WebRegisterPage registerPage, String privatePerson){
        //All mandatory fields are filled (meaning the RRN is fetch)
        WaitForElement(registerPage.firstNameTextFieldSecondOwner, 60);
        if(isFilled(registerPage.firstNameTextFieldSecondOwner) &&
                isFilled(registerPage.lastNameTextFieldSecondOwner)){
            logger.log(LogStatus.INFO, "Fields are filled for " + privatePerson);
        } else {
            logger.log(LogStatus.ERROR, "Fields are not filled for " + privatePerson);
        }

        //Address fields are not editable
        //Firt Name
        //Last Name
        //RRN
        if(!isEditable(registerPage.firstNameTextFieldSecondOwner) &&
                !isEditable(registerPage.lastNameTextFieldSecondOwner) &&
                !isEditable(registerPage.nationalNumberTextFieldSecondOwner)){
            logger.log(LogStatus.INFO, "Fields are  non-editable for " + privatePerson);
        } else {
            logger.log(LogStatus.ERROR, "Fields are editable for " + privatePerson);
        }
        //Address fields are editable
        //Postal code
        //Street
        //Number
        //Phone
        //Mobile
        //Fax
        //Email
        if(isEditable(registerPage.postalCodeAndMunicipalityTextFieldSecondOwner) &&
                isEditable(registerPage.streetNumberTextFieldSecondOwner) &&
                isEditable(registerPage.streetNumberTextFieldSecondOwner) &&
                isEditable(registerPage.phoneTextFieldSecondOwner) &&
                isEditable(registerPage.mobileTextFieldSecondOwner) &&
                isEditable(registerPage.faxTextFieldSecondOwner) &&
                isEditable(registerPage.emailTextFieldSecondOwner)){
            logger.log(LogStatus.INFO, "Fields are editable for " + privatePerson);
        } else {
            logger.log(LogStatus.ERROR, "Fields are non-editable for " + privatePerson);
        }
    }
}
