package ElementaryScripts.Web;

import Screens.WebRegisterPage;
import Screens.WebEazyIdPage;
import Screens.WebStartPage;
import com.relevantcodes.extentreports.LogStatus;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by thierry.foulon on 10/27/2017.
 */
public class RegistrationPageCorTest extends RegistrationPage{

    public void run() throws Exception {
        try {
            WebRegisterPage registerPage = new WebRegisterPage(driver);
            WebEazyIdPage eazyIdPage = new WebEazyIdPage(driver);
            WebStartPage startPage = new WebStartPage(driver);
            WaitForElement(startPage.registerTabButton, 60);
            click(startPage.registerTabButton);
            WaitForElement(registerPage.transfertOfResponsibility, 60);
            //Fill First Responsible Info
            if (firstResponsible.equals("Private Person") && registrator.contains("Veterinary")) {
                clickHiddenElement(registerPage.checkBoxPrivatePersonFirstOwner);
                click(registerPage.readDataFromFirstOwnerEiDCardButton);
                click(registerPage.checkBoxEidConsent);
                click(registerPage.agreeEidButton);
                WaitForElement(registerPage.searchFirstOwnerPrivatePersonButton, 60);
            } else if (firstResponsible.equals("Breeder 1") && registrator.contains("Veterinary")) {
                setText(registerPage.refNumberTextFieldFirstOwner, "HK43214321");
                click(registerPage.selectFisrtResponsibleToolTip);
            } else if (firstResponsible.equals("Breeder 4") && registrator.contains("Veterinary")) {
                setText(registerPage.refNumberTextFieldFirstOwner, "HK159357");
                click(registerPage.selectFisrtResponsibleToolTip);
            } else if (firstResponsible.equals("Refuge 1") && registrator.contains("Veterinary")) {
                setText(registerPage.refNumberTextFieldFirstOwner, "HK12341234");
                click(registerPage.selectFisrtResponsibleToolTip);
            } else if (firstResponsible.equals("Refuge 3") && registrator.contains("Veterinary")) {
                setText(registerPage.refNumberTextFieldFirstOwner, "HK852456");
                click(registerPage.selectFisrtResponsibleToolTip);
            }

            //Fill New Responsible Info
            if (!newResponsible.equals("")) {
                click(registerPage.transfertOfResponsibility);

                if (newResponsible.equals("Private Person")) {
                    if (!registrator.contains("Refuge")) {
                        clickHiddenElement(registerPage.checkBoxPrivatePersonSecondOwner);
                    }
                    setText(registerPage.searchSecondOwnerPrivatePersonField, "89041952724");
                    click(registerPage.searchSecondOwnerPrivatePersonButton);
                } else if (newResponsible.equals("Breeder 1")) {
                    setText(registerPage.refNumberTextFieldSecondOwner, "HK43214321");
                    click(registerPage.selectSecondResponsibleToolTip);
                } else if (newResponsible.equals("Refuge 1")) {
                    setText(registerPage.refNumberTextFieldSecondOwner, "HK12341234");
                    click(registerPage.selectSecondResponsibleToolTip);
                } else if (newResponsible.equals("Breeder 2")) {
                    setText(registerPage.refNumberTextFieldSecondOwner, "BR88888888");
                    click(registerPage.selectSecondResponsibleToolTip);
                } else if (newResponsible.equals("Breeder 3")) {
                    setText(registerPage.refNumberTextFieldSecondOwner, "HK10101235");
                    click(registerPage.selectSecondResponsibleToolTip);
                } else if (newResponsible.equals("Breeder 4")) {
                    setText(registerPage.refNumberTextFieldSecondOwner, "HK159357");
                    click(registerPage.selectSecondResponsibleToolTip);
                } else if (newResponsible.equals("Refuge 2")) {
                    setText(registerPage.refNumberTextFieldSecondOwner, "RE88888888");
                    click(registerPage.selectSecondResponsibleToolTip);
                } else if (newResponsible.equals("Refuge 3")) {
                    setText(registerPage.refNumberTextFieldSecondOwner, "HK852456");
                    click(registerPage.selectSecondResponsibleToolTip);
                }
            }

            ///!\/!\/!\/!\/!\/!\ Make sure the first cat identifier is available in the DB   /!\/!\/!\/!\/!\/!\/!\/!\
            // The cat identifier available has to match with the last line of src/test/java/Resources/IdentifierList
            //Fill Cat identification

//        //Read the last identifier from text file
//        int lastIdentifierInteger = Integer.parseInt(this.getLastIdentifier());
//        //Compute the new first identifier
//        int firstNewIdentifier = lastIdentifierInteger + 1;
//        System.out.println(firstNewIdentifier);
//        String newFirstIdentifierString = "123456789" + String.valueOf(firstNewIdentifier);
//        //Compute the new second identifier
//        int secondNewIdentifier = firstNewIdentifier + 1;
//        System.out.println(secondNewIdentifier);
//        String newSecondIdentifierString = "123456789" + String.valueOf(secondNewIdentifier);
//
//        //Extend the Add Identifier
//        if(registrator.equals("Veterinary")){
//            scrollOnElement(registerPage.addIdentifierVet);
//            click(registerPage.addIdentifierVet);
//        } else {
//            scrollOnElement(registerPage.addIdentifierOther);
//            click(registerPage.addIdentifierOther);
//        }
//
//        //Fill the 2 identifiers
//        setText(registerPage.firstIdentifier,newFirstIdentifierString);
//        addNewIdentifier(String.valueOf(firstNewIdentifier));
//        setText(registerPage.secondIdentifier,newSecondIdentifierString);
//        addNewIdentifier(String.valueOf(secondNewIdentifier));

            fillTwoCatIdentifiers(registerPage);
            String passportNumberDate = new SimpleDateFormat("ddMMyyyyHHmmss").format(new Date());
            String passportText = tcFeatureListNumber + passportNumberDate;
            logger.log(LogStatus.INFO,"Passport: " + passportText);
            setText(registerPage.passportNumber, passportText);

            //Fill cat identification
            String timeStamp = new SimpleDateFormat("dd.MM.yyyy").format(new Date());
            setText(registerPage.birthDate, timeStamp);
            setText(registerPage.identificationDate, timeStamp);
            setText(registerPage.sterilizationDate, timeStamp);
            setText(registerPage.colorCatTextField, "black");
            setText(registerPage.nameCatTextField, "Minuit");
            setText(registerPage.breedCatTextField, "Ab");
            click(registerPage.selectBreedTooltip);

            //Submit the form
            if (registrator.contains("Veterinary")) {
                click(registerPage.submitButtonVet);
            } else {
                click(registerPage.submitButtonOther);
            }

            WaitForElement(registerPage.signVetNowOrAccrSendVetButton, 60);
            click(registerPage.signVetNowOrAccrSendVetButton);

            if (registrator.contains("Veterinary")) {
                eazyId(eazyIdPage);
            }

            click(eazyIdPage.closeValidSubmitMessage);

            //logout
            sleep(2000);
            click(startPage.logoutButton);
        } catch (Exception e) {
            logger.logScreenshot(LogStatus.ERROR, "ERROR stopping the test", getScreenShot());
            throw e;
        }
    }
}
