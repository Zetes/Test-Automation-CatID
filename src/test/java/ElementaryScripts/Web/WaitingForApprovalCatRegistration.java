package ElementaryScripts.Web;

import ElementaryScripts.General.AbstractCatIdElementaryScript;
import Screens.*;

public class WaitingForApprovalCatRegistration extends AbstractCatIdElementaryScript{
    public String catIdentifier = "";

    public String getCatIdentifier(){
        return catIdentifier;
    }

    public void setCatIdentifier(String identifier){
        catIdentifier = identifier;
    }

    public void run() throws Exception{
        WebStartPage startPage = new WebStartPage(driver);
        WebEazyIdPage eazyIdPage = new WebEazyIdPage(driver);
        WebWaitingForApprovalPage waiting = new WebWaitingForApprovalPage(driver);

        //Open Waiting for Approval page
        click(startPage.filterButton);
        click(startPage.filterWaitingForApprovalVetTabButton);

        //Search for the identifier to approve
        setText(waiting.searchField,catIdentifier);
        //Wait for the update of the first line
        sleep(2000);
        click(waiting.firstLineTableResult);
        click(waiting.massApproveVetButton);

        //Sign the approval
        eazyId(eazyIdPage);
        click(eazyIdPage.closeValidSubmitMessage);

        //logout
        sleep(2000);
        click(startPage.logoutButton);

    }
}
