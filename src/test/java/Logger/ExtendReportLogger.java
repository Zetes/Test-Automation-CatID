package Logger;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Stack;

/**
 * Created by rstevens on 26/09/2016.
 */
public class ExtendReportLogger implements  LoggerInterface{

    private ExtentReports extent;
    private ExtentTest scenario;
	private Stack<ExtentTest> stackTest;

    public ExtendReportLogger(){
        init();
		stackTest = new Stack<ExtentTest>();
    }

    public void init(){
        this.init(System.getProperty("user.dir")+"/Tempreport.html", "CatId Test", "Basic CatId test");
    }


    public void init(String filepath, String testName, String description){
        extent = new ExtentReports(filepath,false);

    }
	public void StartNewScenario(String name, String description){
        if (scenario != null){
            EndScenario();
        }
        scenario = extent.startTest(name, description);
        scenario.setStartedTime(getTime(System.currentTimeMillis()));
    }

    public void StartNewTest(String name, String description){
        /*if (test != null){
            EndTest();
        }*/
		ExtentTest test = extent.startTest(name, description);
		test.setStartedTime(getTime(System.currentTimeMillis()));
        stackTest.push(test);
    }

    public void EndTest(){
		ExtentTest test = stackTest.pop();
        test.setEndedTime(getTime(System.currentTimeMillis()));
        extent.endTest(test);
		scenario.appendChild(test);
    }
	
	public void EndScenario(){
        scenario.setEndedTime(getTime(System.currentTimeMillis()));
        extent.endTest(scenario);
        scenario = null;
    }


    public void log(LogStatus status, String message){
        stackTest.peek().log(status, message);
        displayOnTerminal(status, message);
    }

    public void log(LogStatus status, String message, String screenshotPath) {
        logScreenshot(status, message, screenshotPath);
    }
    public void logScreenshot(LogStatus status, String screenshotPath){
        stackTest.peek().log(status, "Snapshot below: " + stackTest.peek().addScreenCapture(screenshotPath));
    }
    public void logScreenshot(LogStatus status, String message, String screenshotPath){
        stackTest.peek().log(status, message + stackTest.peek().addScreenCapture(screenshotPath));
        displayOnTerminal(status, message);
    }


    private void displayOnTerminal(LogStatus status, String message) {
        String currentDateTimeStamp = new SimpleDateFormat("HH:mm:ss").format(new Date());
        if (status.toString().equals("error")) {
            System.out.println("\033[31;1m" + currentDateTimeStamp + "[" + status.toString() + "]" + ": " + message + "\033[0m");
        } else if (status.toString().equals("warning")){
            System.out.println("\033[33m" + currentDateTimeStamp + "[" + status.toString() + "]" + ": " + message + "\033[0m");
        } else {
            System.out.println(currentDateTimeStamp + "[" + status.toString() + "]" + ": " +message);
        }

    }
    private Date getTime(long millis) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(millis);
        return calendar.getTime();
    }

    public void Finalize(){
        if (scenario != null){
            EndScenario();
        }
        extent.flush();
        extent.close();

    }
}
