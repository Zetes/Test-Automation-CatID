package Logger;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.LogStatus;

/**
 * Created by rstevens on 26/09/2016.
 */
public interface LoggerInterface {

    void init();
    void init(String filepath, String testName, String description);
    void log(LogStatus status, String message);

    void log(LogStatus status, String message, String screenshotPath);
    void logScreenshot(LogStatus status, String screenshotPath);
    void logScreenshot(LogStatus status, String message, String screenshotPath);

	void StartNewScenario(String name, String description);
    void StartNewTest(String name, String description);
    void EndTest();
	void EndScenario();
    void Finalize();

}
