#include <FileConstants.au3>
#include <MsgBoxConstants.au3>
#include <File.au3>
#include <Date.au3>


Func _Au3RecordSetup()
Opt('WinWaitDelay',100)
Opt('WinDetectHiddenText',1)
Opt('MouseCoordMode',0)
Local $aResult = DllCall('User32.dll', 'int', 'GetKeyboardLayoutNameW', 'wstr', '')
If $aResult[1] <> '00000813' Then
  MsgBox(64, 'Warning', 'Recording has been done under a different Keyboard layout' & @CRLF & '(00000813->' & $aResult[1] & ')')
EndIf

EndFunc

Func _WinWaitActivate($title,$text,$timeout=0)
	WinWait($title,$text,$timeout)
	If Not WinActive($title,$text) Then WinActivate($title,$text)
	WinWaitActive($title,$text,$timeout)
EndFunc

_AU3RecordSetup()

Dim $LatestPassportNo = ""
$sFilePath = "C:\Users\akw\Documents\01. CatID\AutoIT scripts\PassportNo.txt"

Func _GetLatestNumber()
	; Open the file for reading and store the handle to a variable.
    Local $hFileOpen = FileOpen($sFilePath, $FO_READ)
    If $hFileOpen = -1 Then
        MsgBox($MB_SYSTEMMODAL, "", "An error occurred when reading the file.")
        Return False
    EndIf
	$BeforelastLine = _FileCountLines ( $sFilePath ) -1
	$LatestPassportNo = FileReadLine($hFileOpen, $BeforelastLine)

	FileClose($hFileOpen)
EndFunc

_GetLatestNumber()

_WinWaitActivate("Preshipping abiec","")

	; Open the file for writing (append to the end of a file) and store the handle to a variable.
    Local $hFileOpen = FileOpen($sFilePath, $FO_APPEND)
    If $hFileOpen = -1 Then
        MsgBox($MB_SYSTEMMODAL, "", "An error occurred whilst writing the temporary file.")
    EndIf

$totalPassports = InputBox("Generate Passports in GI", "How many passports to generate?")

If @error = 1 Then Exit

FileWriteLine($hFileOpen, "< Created on: " & _Now() & " >")

For $i = 1 to $totalPassports
	$currentPassportNo = $LatestPassportNo + $i
	;Msgbox(0,"Test", "BE03" & $currentPassportNo)
	Send("{CTRLDOWN}k{CTRLUP}")
	 _WinWaitActivate("Entrez la valeur","")
	Send("BE03" & $currentPassportNo & "{ENTER}")
	FileWriteLine($hFileOpen, $currentPassportNo)
	Sleep(1000)
Next

FileWriteLine($hFileOpen, "==================================")
FileClose($hFileOpen)