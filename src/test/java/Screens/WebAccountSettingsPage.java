package Screens;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class WebAccountSettingsPage {
    @FindBy(xpath="//*[@id=\"MainContainer\"]/form/div/div[1]/div/div[7]/div/div/div/div")
    public WebElement vetTextFieldForBreederAndRefuge;

    @FindBy(xpath="//*[@id=\"MainContainer\"]/form/div/div[1]/div/div[7]/div/div/div/button[1]")
    public WebElement setDefaultVetButtonForBreederAndRefuge;

    @FindBy(xpath="//*[@id=\"MainContainer\"]/form/div/div[1]/div/div[7]/div/div/div/button[2]")
    public WebElement deleteVetButtonForBreederAndRefuge;

    @FindBy(id="addVetRefnum")
    public WebElement addVetTextFieldForBreederAndRefuge;

    @FindBy(xpath="//*[@id=\"searchVet\"]/div[2]/a/div/div")
    public WebElement addVetTooltipForBreederAndRefuge;

    @FindBy(xpath="//*[@id=\"dialogAddVet\"]/div[3]/div[1]")
    public WebElement noAddVetButtonForBreederAndRefuge;

    @FindBy(xpath="//*[@id=\"dialogAddVet\"]/div[3]/div[2]")
    public WebElement yesAddVetButtonForBreederAndRefuge;

    @FindBy(id="FirstPostalCodeAndMunicipality")
    public WebElement editTextFieldForPostalCodeAndMunicipality;

    @FindBy(id="LoggedInUserInformation_AddressData_Street")
    public WebElement editTextFieldForStreet;

    @FindBy(id="LoggedInUserInformation_AddressData_StreetNr")
    public WebElement editTextFieldForStreetNumber;

    @FindBy(id="LoggedInUserInformation_PersonData_ContactData_PhoneNumber")
    public WebElement editTextFieldForPhoneNumber;

    @FindBy(xpath="//*[@id=\"LoggedInUserInformation_PersonData_ContactData_MobileNumber\"]")
    public WebElement editTextFieldForMobileNumber;

    @FindBy(xpath="//*[@id=\"LoggedInUserInformation_PersonData_ContactData_FaxNumber\"]")
    public WebElement editTextFieldForFaxNumber;

    @FindBy(xpath="//*[@id=\"LoggedInUserInformation_PersonData_ContactData_EmailAddress\"]")
    public WebElement editTextFieldForEmailAddress;

    @FindBy(xpath="//*[@id=\"QuotaWarning\"]")
    public WebElement editTextFieldForQuotaWarning;

    @FindBy(xpath="//*[text()='Save']")
    public WebElement saveButtonAccountDetails;

    @FindBy(xpath="//*[@id=\"messages\"]/div/div")
    public WebElement messagePopup;

    @FindBy(xpath="//*[@id=\"ErrorMsgContent\"]")
    public WebElement messageError;

    @FindBy(xpath="//*[@id=\"MainContainer\"]/form/div/div[1]/div/div[7]/div/div/div/div/div[2]/div[2]")
    public WebElement newSecondVetAdded;

    @FindBy(xpath="//*[@id=\"MainContainer\"]/form/div/div[1]/div/div[7]/div/div/div/div/div[2]")
    public WebElement listContainerVeterinary;

    @FindBy(xpath="//*[@id=\"searchVet\"]/div[2]/a/div/div")
    public WebElement newVetSearchFirstResult;

    @FindBy(xpath="//*[@id=\"MainContainer\"]/form/div/div[1]/div/div[1]/ul/li[1]")
    public WebElement labelAccountName;

    @FindBy(xpath="//*[@id=\"MainContainer\"]/form/div/div[1]/div/div[1]/ul/li[2]")
    public WebElement labelAccreditationNumber;

    @FindBy(xpath="//*[@id=\"MainContainer\"]/form/div/div[1]/div/div[1]/ul/li[3]")
    public WebElement labelAddress;

    //constructor expecting a driver as parameter
    public WebAccountSettingsPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }
}
