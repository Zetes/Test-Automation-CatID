package Screens;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


public class WebApprovedPage extends WebFilterPage{
    @FindBy(xpath="//*[@id=\"searchTable_wrapper\"]/div/div[1]/div[1]/div/button[1]")
    public WebElement selectALLButton;

    @FindBy(xpath="//*[@id=\"searchTable_wrapper\"]/div/div[1]/div[1]/div/button[2]")
    public WebElement unselectAllButton;

    @FindBy(xpath="//*[@id=\"searchTable_wrapper\"]/div/div[1]/div[1]/div/button[3]")
    public WebElement editButton;

    @FindBy(xpath="//*[@id=\"searchTable_wrapper\"]/div/div[1]/div[1]/div/button[4]")
    public WebElement createCertificateButton;

    @FindBy(xpath="//*[@id=\"searchTable_wrapper\"]/div/div[1]/div[1]/div/button[5]")
    public WebElement createCsvButton;

    //constructor expecting a driver as parameter
    public WebApprovedPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }
}
