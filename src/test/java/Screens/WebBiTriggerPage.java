package Screens;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class WebBiTriggerPage {
    @FindBy(xpath="//*[@id=\"container\"]/div[2]/div[2]/form/input")
    public WebElement runButton;

    @FindBy(id="humane")
    public WebElement endBiMessageTrigger;

    //constructor expecting a driver as parameter
    public WebBiTriggerPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }
}
