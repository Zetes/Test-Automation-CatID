package Screens;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class WebBulkUploadPage {

    @FindBy(xpath="//*[@id=\"BulkUploadForm\"]/div[1]/div/div/div/input[1]")
    public WebElement bulkUploadFileTextField;

    @FindBy(id="File")
    public WebElement bulkUploadFileInputFileField;

    @FindBy(xpath="//*[@id=\"BulkUploadForm\"]/div[1]/div/div/div/button")
    public WebElement uploadButton;

    @FindBy(xpath="//*[@id=\"BulkUploadForm\"]/div[3]/div[1]/div")
    public WebElement validationOrWarningMessage;

    @FindBy(xpath="//*[@id=\"BulkUploadForm\"]/div[3]/div")
    public WebElement errorMessage;

    @FindBy(xpath="//*[@id=\"BulkUploadForm\"]/table/tbody")
    public WebElement errorTabList;

    @FindBy(xpath="//*[@id=\"BulkUploadForm\"]/div[3]/div[2]/table/tbody")
    public WebElement warningTabList;

    @FindBy(xpath="//*[@id=\"dialogConfirm\"]/div[3]/div[1]")
    public WebElement signNoButton;

    @FindBy(xpath="//*[@id=\"dialogConfirm\"]/div[3]/div[2]")
    public WebElement signVetLaterOrAccrPendingButton;

    @FindBy(xpath="//*[@id=\"dialogConfirm\"]/div[3]/div[3]")
    public WebElement signVetNowOrSendToEndButton;

    @FindBy(xpath="//*[@id=\"BulkUploadForm\"]/div[4]/button")
    public WebElement submitVetFileButton;

    @FindBy(xpath="//*[@id=\"BulkUploadForm\"]/div[5]/button")
    public WebElement submitRefugeAndBreederFileButton;


    //constructor expecting a driver as parameter
    public WebBulkUploadPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }
}
