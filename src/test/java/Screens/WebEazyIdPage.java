package Screens;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


public class WebEazyIdPage {

    @FindBy(id="user_consent_CB")
    public WebElement consentCheckbox;

    @FindBy(id="user_consent_agree_button")
    public WebElement agreeButton;

    @FindBy(id="user_consent_deny_button")
    public WebElement denyButton;

    @FindBy(id="pin")
    public WebElement signCode;

    @FindBy(id="pinOK")
    public WebElement signOK;

    //EazySign (TODO will be remove in next version)
    @FindBy(id="loginFeedbackMsg")
    public WebElement errorMessageMessage;

    @FindBy(id="overviewButton")
    public WebElement signaturesButton;

    @FindBy(id="exitButton")
    public WebElement exitButton;

    @FindBy(xpath="/html/body/div/div[1]/div[2]/div/div[2]/button[1]")
    public WebElement exitYesButton;

    @FindBy(xpath="/html/body/div/div[1]/div[2]/div/div[2]/button[2]")
    public WebElement exitNoButton;

    @FindBy(xpath="//*[@id=\"signatureOverviewContainer\"]/table/tbody/tr[2]/td[3]/a/span")
    public WebElement signPencilVetButton;

    @FindBy(xpath="//*[@id=\"overviewModal\"]/div[2]/div/div[2]/button")
    public WebElement closeVetButton;

    @FindBy(xpath="//*[@id=\"signModal\"]/div[2]/div/div[2]/div[4]/button[1]")
    public WebElement validateSignVetButton;

    @FindBy(id="cancelSignButton")
    public WebElement cancelSignVetButton;

    @FindBy(id="pin")
    public WebElement pinVetField;

    @FindBy(id="pinCancel")
    public WebElement pinCancelVetField;

    @FindBy(id="pinOK")
    public WebElement pinOkVetField;

    @FindBy(id="doneSignButton")
    public WebElement pinDoneVetField;

    @FindBy(xpath="//*[@id=\"messages\"]/div/div")
    public WebElement diablogSubmitMessage;

    @FindBy(xpath="//*[@id=\"messages\"]/div/div[2]")
    public WebElement downloadCertificateAfterValidSubmitButton;

    @FindBy(id="messageclose")
    public WebElement closeValidSubmitMessage;


    //constructor expecting a driver as parameter
    public WebEazyIdPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }
}
