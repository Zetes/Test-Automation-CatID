package Screens;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Created by thierry.foulon on 12/13/2017.
 */
public class WebFilterPage {
    @FindBy(xpath="//*[@id=\"searchTable_filter\"]/label/input")
    public WebElement searchField;

    @FindBy(xpath="//*[@id=\"searchTable\"]/tbody")
    public WebElement tableResult;

    @FindBy(xpath="//*[@id=\"searchTable\"]/tbody/tr[1]")
    public WebElement firstLineTableResult;

    @FindBy(xpath="//*[@id=\"searchTable\"]/tbody/tr/td[1]")
    public WebElement uniqueIdentifierTableResult;

    @FindBy(xpath="//*[@id=\"searchTable\"]/tbody/tr[1]/td[1]")
    public WebElement firstIdentifierTableResult;

    @FindBy(xpath="//*[@id=\"searchTable\"]/tbody")
    public WebElement identifierTableResults;

    @FindBy(id="searchTable_previous")
    public WebElement previousButton;

    @FindBy(id="searchTable_next")
    public WebElement nextButton;

    @FindBy(xpath="//*[@id=\"searchTable_paginate\"]/div")
    public WebElement paginationMenu;

    @FindBy(id="searchTable_info")
    public WebElement pageInfo;

    @FindBy(xpath="//*[@id=\"searchTable_length\"]/label/div")
    public WebElement pageSelectionDropList;

    @FindBy(xpath="//*[@id=\"searchTable_length\"]/label/div/div[2]")
    public WebElement numberResultByPageSelectionDroplist;

    @FindBy(xpath="//*[@id=\"searchTable_length\"]/label/div/div[1]")
    public WebElement defautResultByPageSelection;

    @FindBy(xpath="//*[@id=\"searchTable_length\"]/label/div/div[2]/div[1]")
    public WebElement tenRowsByPageSelection;

    @FindBy(xpath="//*[@id=\"searchTable_length\"]/label/div/div[2]/div[2]")
    public WebElement twentyFiveRowsByPageSelection;

    @FindBy(xpath="//*[@id=\"searchTable_length\"]/label/div/div[2]/div[3]")
    public WebElement fiftyRowsByPageSelection;

    @FindBy(xpath="//*[@id=\"searchTable_length\"]/label/div/div[2]/div[4]")
    public WebElement hundredRowsByPageSelection;

    @FindBy(xpath="//*[@id=\"searchTable\"]/thead/tr")
    public WebElement headerRow;

    @FindBy(id="messages")
    public WebElement messageDialog;
}
