package Screens;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


public class WebHomepage {

    @FindBy(id="nav-logo")
    public WebElement homeButton;
    //xpath="//*[@id=\'sc-buy-box-ptc-button\']/span/input")

    //@FindBy(xpath="(//a[contains(text(),'Login')])[2]")
    @FindBy(xpath="(//a[@href='/signin'])[2]")
    public WebElement loginButton;

    @FindBy(xpath="//*[@id=\'nav-link-accountList\']/span[1]")
    public WebElement hoverLogin;

    @FindBy(id="CatId")
    public WebElement SearchField;

    @FindBy(id="submitButton")
    public WebElement submitButton;

    @FindBy(xpath="/html/body/div[3]/div/div[1]/div[1]/a/img")
    public WebElement returnHomePageButton;

    @FindBy(xpath="/html/body/div[2]/div/div/div/div/div[1]/a")
    public WebElement dutchDefaultSelectionButton;

    @FindBy(xpath="/html/body/div[2]/div/div/div/div/div[2]/a")
    public WebElement frenchDefaultSelectionButton;

    @FindBy(xpath="/html/body/div[2]/div/div/div/div/div[3]/a")
    public WebElement englishDefaultSelectionButton;

    //constructor expecting a driver as parameter
	public WebHomepage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
}
