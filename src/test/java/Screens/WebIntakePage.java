package Screens;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


public class WebIntakePage {

    @FindBy(id="searchField")
    public WebElement searchField;

    @FindBy(id="buttonSearch")
    public WebElement buttonSearch;

    //Cat identification
    @FindBy(xpath="//*[@id=\"AllInformationDiv\"]/div[3]/div/div[1]/div/div[1]/div/h3")
    public WebElement catIdentifierBreederOrRefuge;

    @FindBy(xpath="//*[@id=\"AllInformationDiv\"]/div[3]/div/div[1]/div/div[2]/div/h3")
    public WebElement catPassportNumberBreederOrRefuge;

    @FindBy(xpath="//*[@id=\"AllInformationDiv\"]/div[3]/div/div[1]/div/div[3]/div/h3")
    public WebElement catIdentificationDateBreederOrRefuge;

    @FindBy(xpath="//*[@id=\"AllInformationDiv\"]/div[3]/div/div[1]/div/div[4]/div/h3")
    public WebElement catBirthDateBreederOrRefuge;

    @FindBy(xpath="//*[@id=\"AllInformationDiv\"]/div[3]/div/div[2]/div/div[1]/div/h3")
    public WebElement catBreedBreederOrRefuge;

    @FindBy(xpath="//*[@id=\"AllInformationDiv\"]/div[3]/div/div[2]/div/div[2]/div/h3")
    public WebElement catCrossingBreederOrRefuge;

    @FindBy(xpath="//*[@id=\"AllInformationDiv\"]/div[3]/div/div[2]/div/div[3]/div/h3")
    public WebElement catSterelizedDateBreederOrRefuge;

    @FindBy(xpath="//*[@id=\"AllInformationDiv\"]/div[3]/div/div[2]/div/div[4]/div/h3")
    public WebElement catSterelizedFlagBreederOrRefuge;

    @FindBy(xpath="//*[@id=\"AllInformationDiv\"]/div[3]/div/div[3]/div/div[1]/div/h3")
    public WebElement catGenderBreederOrRefuge;

    @FindBy(xpath="//*[@id=\"AllInformationDiv\"]/div[3]/div/div[3]/div/div[2]/div/h3")
    public WebElement catDateStatusBreederOrRefuge;

    @FindBy(xpath="//*[@id=\"AllInformationDiv\"]/div[3]/div/div[3]/div/div[3]/div/h3")
    public WebElement catStatusBreederOrRefuge;

    @FindBy(xpath="//*[@id=\"AllInformationDiv\"]/div[3]/div/div[3]/div/div[4]/div/h3")
    public WebElement catNameBreederOrRefuge;

    @FindBy(xpath="//*[@id=\"AllInformationDiv\"]/div[3]/div/div[3]/div/div[5]/div/h3")
    public WebElement catFurColorBreederOrRefuge;
    //Cat information
    @FindBy(xpath="//*[@id=\"AllInformationDiv\"]/div[4]/div/div/div/div[3]/div")
    public WebElement catBreederOrRefugeStatusContainer;

    @FindBy(id="CatData_CatGender")
    public WebElement catGender;

    @FindBy(id="CatData_CatDateOfBirth")
    public WebElement catBirthDate;

    @FindBy(id="CatData_SterilizedDate")
    public WebElement stereilizedDate;

    @FindBy(xpath="//*[@id=\"AllInformationDiv\"]/div[2]/div/div[1]/div/div[4]/div")
    public WebElement sterilizedCheckbox;

    @FindBy(xpath="//*[@id=\"AllInformationDiv\"]/div[2]/div/div[1]/div/div[5]/div")
    public WebElement crossingBreedCheckbox;

    @FindBy(id="CatData_CatBreed")
    public WebElement catBreed;

    @FindBy(id="CatData_CatName")
    public WebElement catName;

    @FindBy(id="CatData_FurTypeAndColor")
    public WebElement catColor;

    @FindBy(id="CatData_CatStatus")
    public WebElement catStatus;

    @FindBy(id="CatData_StatusDate")
    public WebElement catStatusDate;

    @FindBy(xpath="//*[@id=\"CatDateOfBirth\"]/div/div/table/tbody")
    public WebElement birthDateBodyCalendar;

    @FindBy(xpath="//*[@id=\"StatusDate\"]/div/div/table/tbody")
    public WebElement statusDateBodyCalendar;

    @FindBy(xpath="//*[@id=\"IdentificationDate\"]/div/div/table/tbody")
    public WebElement identificationDateBodyCalendar;

    @FindBy(xpath="//*[@id=\"SterilizedDate\"]/div/div/table/tbody")
    public WebElement sterilizationDateBodyCalendar;

    @FindBy(xpath="//*[@id=\"CatDateOfBirth\"]/div/div/table/thead")
    public WebElement birthDateHeadCalendar;

    @FindBy(xpath="//*[@id=\"StatusDate\"]/div/div/table/thead")
    public WebElement statusDateHeadCalendar;

    @FindBy(xpath="//*[@id=\"IdentificationDate\"]/div/div/table/thead")
    public WebElement identificationDateHeadCalendar;

    @FindBy(xpath="//*[@id=\"SterilizedDate\"]/div/div/table/thead")
    public WebElement sterilizationDateHeadCalendar;

    //Responsible information
    @FindBy(xpath="//*[@id=\"AllInformationDiv\"]/div[1]/div/div/p[1]")
    public WebElement responsibleInformationBreederOrRefuge;

    @FindBy(xpath="//*[@id=\"AllInformationDiv\"]/div[1]/div/div/p[2]")
    public WebElement responsibleAddressBreederOrRefuge;

    //Adopter Information
    @FindBy(xpath="//*[@id=\"NewRespDiv\"]/div[1]")
    public WebElement adopterDropDown;

    @FindBy(xpath="//*[@id=\"NewRespDiv\"]/div[2]/div/div[2]/div/div[1]/div/input")
    public WebElement adopterHkCheckBox;

    @FindBy(xpath="//*[@id=\"NewRespDiv\"]/div[2]/div/div[2]/div/div[2]/div/input")
    public WebElement adopterPrivatePersonCheckBox;

    @FindBy(id="Agree")
    public WebElement adopterAgreeBreederRefugeCheckBox;

    @FindBy(id="SecondOwnerInformation_PersonData_NationalNumberNumber")
    public WebElement adopterNationalNumberField;

    @FindBy(id="RefNumSecond")
    public WebElement adopterHkRefNumSecondSearchField;

    @FindBy(xpath="//*[@id=\"SecondHKAutoCom\"]/div[2]/a/div/div")
    public WebElement adopterHkRefNumSecondSearchTooltip;

    @FindBy(xpath="//*[@id=\"NewRespDiv\"]/div[2]/div[1]/div[3]/h6")
    public WebElement adopterClearButton;

    @FindBy(xpath="//*[@id=\"SecondConfidential\"]/div")
    public WebElement adopterConfidentialFlag;

    @FindBy(xpath="//*[@id=\"NewRespDiv\"]/div[2]/div/div[2]/h6")
    public WebElement adopterClearVetButton;

    @FindBy(id="2ndFetchedOwnerSearchRrnOrId")
    public WebElement adopterPrivaPersonSearchField;

    @FindBy(xpath="//*[@id=\"SecondRegularFetchDiv\"]/div/div[2]/div/button[1]")
    public WebElement adopterSearchButton;

    @FindBy(id="SecondFetchInformation")
    public WebElement adopterReadDataDromEidCardButton;

    @FindBy(id="SecondOwnerInformation_TransferDate")
    public WebElement adopterTransfertDate;

    @FindBy(id="SecondOwnerInformation_PersonData_FirstName")
    public WebElement adopterFirstName;

    @FindBy(id="SecondOwnerInformation_PersonData_LastName")
    public WebElement adopterLastName;

    @FindBy(id="SecondOwnerInformation_AddressData_PostalCodeAndMunicipality")
    public WebElement adopterCity;

    @FindBy(id="SecondOwnerInformation_AddressData_Street")
    public WebElement adopterStreet;

    @FindBy(id="SecondOwnerInformation_AddressData_StreetNr")
    public WebElement adopterStreetNumber;

    @FindBy(id="SecondOwnerInformation_PersonData_ContactData_PhoneNumber")
    public WebElement adopterPhone;

    @FindBy(id="SecondOwnerInformation_PersonData_ContactData_MobileNumber")
    public WebElement adopterMobile;

    @FindBy(id="SecondOwnerInformation_PersonData_ContactData_FaxNumber")
    public WebElement adopterFax;

    @FindBy(id="SecondOwnerInformation_PersonData_ContactData_EmailAddress")
    public WebElement adopterMail;

    //Submit
    @FindBy(xpath="//*[@id=\"AllInformationDiv\"]/div[5]/button[1]")
    public WebElement createCertificateVetButton;

    @FindBy(xpath="//*[@id=\"AllInformationDiv\"]/div[5]/button[2]")
    public WebElement submitVetButton;

    @FindBy(xpath="//*[@id=\"AllInformationDiv\"]/div[5]/button")
    public WebElement submitBreederAndRefugeButton;

    @FindBy(xpath="//*[@id=\"dialogConfirm\"]/div[3]/div[1]")
    public WebElement submitNoButton;

    @FindBy(xpath="//*[@id=\"dialogConfirm\"]/div[3]/div[2]")
    public WebElement submitYesOrSignLaterButton;

    @FindBy(xpath="//*[@id=\"dialogConfirm\"]/div[3]/div[3]")
    public WebElement submitSignNowButton;

    @FindBy(xpath="//*[@id=\"errorModal\"]/div[2]/div/i")
    public WebElement submitFinalValidationButton;

    @FindBy(xpath="//*[@id=\"NoSearchResultFound\"]/div/div/div")
    public WebElement errorLabelNoSearchFound;

    //Error Message
    @FindBy(xpath="//*[@id=\"ModifyForm\"]/div[1]/ul/li")
    public WebElement errorMessageRegisterPage;

    //constructor expecting a driver as parameter
    public WebIntakePage(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }
}
