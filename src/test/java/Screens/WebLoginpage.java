package Screens;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
//import org.openqa.selenium.support.ui.Select;

public class WebLoginpage {

	@FindBy(xpath="//*[@id=\"UserSelectionForm\"]/div[1]/div")
	public WebElement vetDropdown;

	@FindBy(xpath="//div[@data-value='F0032,True']")
	public WebElement vet1Selection;

	@FindBy(xpath="//div[@data-value='F0024,True']")
	public WebElement vet2Selection;

	@FindBy(xpath="//div[@data-value='F1560,True']")
	public WebElement vet3Selection;

	@FindBy(xpath="//div[@data-value='HK43214321,True']")
	public WebElement breeder1Selection;

	@FindBy(xpath="//div[@data-value='BR88888888,True']")
	public WebElement breeder2Selection;

	@FindBy(xpath="//div[@data-value='HK10101235,True']")
	public WebElement breeder3Selection;

	@FindBy(xpath="//div[@data-value='HK159357,True']")
	public WebElement breeder4Selection;

	@FindBy(xpath="//div[@data-value='HK12341234,True']")
	public WebElement refuge1Selection;

	@FindBy(xpath="//div[@data-value='RE88888888,True']")
	public WebElement refuge2Selection;

	@FindBy(xpath="//div[@data-value='HK852456,True']")
	public WebElement refuge3Selection;


//	@FindBy(xpath="//*[@id=\"UserSelectionForm\"]/div[2]/div")
//	public WebElement vetDropdownContainer;
//
//	@FindBy(id="Refnum")
//	public WebElement vetDropdown;

	@FindBy(xpath="//input[@value='Select']")
	public WebElement selectVetButton;


	//constructor expecting a driver as parameter
	public WebLoginpage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
}
