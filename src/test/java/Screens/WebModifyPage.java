package Screens;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


public class WebModifyPage {

    @FindBy(id="searchField")
    public WebElement searchField;

    @FindBy(id="buttonSearch")
    public WebElement buttonSearch;

    //Cat identification
    //For Vet
    @FindBy(id="IdentifierData_0__Identifier")
    public WebElement firstCatIdentifierVet;

    @FindBy(id="IdentifierData_0__IdentifierStatus")
    public WebElement firstCatIdentifierStatusVet;

    @FindBy(xpath="//*[@id=\"AllInformationDiv\"]/div[1]/div[1]/div[1]/div/div[2]/div")
    public WebElement firstCatIdentifierStatusDropdownContainerVet;

    @FindBy(xpath="//*[@id=\"AllInformationDiv\"]/div[1]/div[1]/div[1]/div/div[2]/div/div[2]/div[1]")
    public WebElement firstCatIdentifierStatusValidVet;

    @FindBy(xpath="//*[@id=\"AllInformationDiv\"]/div[1]/div[1]/div[1]/div/div[2]/div/div[2]/div[2]")
    public WebElement firstCatIdentifierStatusUnreadableVet;

    @FindBy(xpath="//*[@id=\"AllInformationDiv\"]/div[1]/div[1]/div[2]/div/div[2]/div")
    public WebElement secondCatIdentifierStatusDropdownContainerVet;
    //*[@id="AllInformationDiv"]/div[1]/div[1]/div[3]/div[1]
    @FindBy(xpath="//*[@id=\"AllInformationDiv\"]/div[1]/div[1]/div[2]/div/div[2]/div/div[2]/div[1]")
    public WebElement secondCatIdentifierStatusValidVet;

    @FindBy(xpath="//*[@id=\"AllInformationDiv\"]/div[1]/div[1]/div[2]/div/div[2]/div/div[2]/div[2]")
    public WebElement secondCatIdentifierStatusUnreadableVet;

    @FindBy(id="IdentifierData_0__IdentifierDate")
    public WebElement firstCatIdentificationDateVet;

    @FindBy(id="IdentifierData_0__IdentifierLocation")
    public WebElement firstCatIdentifierLocationVet;

    @FindBy(id="IdentifierData_1__Identifier")
    public WebElement secondCatIdentifierVet;

    @FindBy(id="IdentifierData_1__IdentifierStatus")
    public WebElement secondCatIdentifierStatusVet;

    @FindBy(id="IdentifierData_1__IdentifierLocation")
    public WebElement secondCatIdentifierLocationVet;

    @FindBy(id="IdentifierData_1__IdentifierDate")
    public WebElement secondCatIdentifierDateVet;

    @FindBy(id="PassportData_PassportNumber")
    public WebElement catPassportNumberVet;

    @FindBy(xpath="//*[@id=\"AllInformationDiv\"]/div[1]/div[1]/div[2]/div[1]")
    public WebElement addSecondCatIdentifierDropDownVet;

    @FindBy(xpath="//*[@id=\"AllInformationDiv\"]/div[1]/div[1]/div[3]/div[1]")
    public WebElement addThirdCatIdentifierDropDownVet;

    @FindBy(xpath="//*[@id=\"AllInformationDiv\"]/div[1]/div[1]/div[3]/div[2]/div/div[1]")
    public WebElement addTextLabelAdvertissementForThirdValidIdVet;

    @FindBy(xpath="//*[@id=\"AllInformationDiv\"]/div[1]/div[1]/div[2]/div[1]")
    public WebElement addTextLabelAdvertissementForSecondValidIdVet;

    @FindBy(id="NewIdentifier_IdentifierLocation")
    public WebElement addCatIdentifierLocationVet;

    @FindBy(id="NewIdentifier_IdentifierDate")
    public WebElement addCatIdentifierIdentificationDateVet;

    @FindBy(id="NewIdentifier_Identifier")
    public WebElement addCatIdentifierFieldVet;

    @FindBy(xpath="//*[@id=\"AllInformationDiv\"]/div[1]/div[2]/div[2]/div[1]/i")
    public WebElement addCatPassportDropDownVet;

    @FindBy(id="NewPassportData_PassportNumber")
    public WebElement addCatPassportNumberFieldVet;

    @FindBy(xpath="//*[@id=\"AllInformationDiv\"]/div[1]/div[1]/div[2]/div[2]/div/div[2]/div[2]/div")
    public WebElement newIdentifierLocationContainerVet;

    @FindBy(xpath="//*[@id=\"AllInformationDiv\"]/div[1]/div[1]/div[2]/div[2]/div/div[2]/div[2]/div/div[2]/div[2]")
    public WebElement newIdentifierLocationLeftEarVet;

    @FindBy(xpath="//*[@id=\"AllInformationDiv\"]/div[1]/div[1]/div[2]/div[2]/div/div[2]/div[2]/div/div[2]/div[3]")
    public WebElement newIdentifierLocationRightEarVet;

    @FindBy(xpath="//*[@id=\"AllInformationDiv\"]/div[1]/div[1]/div[2]/div[2]/div/div[2]/div[2]/div/div[2]/div[4]")
    public WebElement newIdentifierLocationLeftThighVet;

    @FindBy(xpath="//*[@id=\"AllInformationDiv\"]/div[1]/div[1]/div[2]/div[2]/div/div[2]/div[2]/div/div[2]/div[5]")
    public WebElement newIdentifierLocationRightThighVet;

    @FindBy(xpath="//*[@id=\"AllInformationDiv\"]/div[1]/div[1]/div[2]/div[2]/div/div[2]/div[2]/div/div[2]/div[1]")
    public WebElement newIdentifierLocationLeftNeckVet;

    @FindBy(xpath="//*[@id=\"AllInformationDiv\"]/div[1]/div[1]/div[2]/div[2]/div/div[2]/div[2]/div/div[2]/div[6]")
    public WebElement newIdentifierLocationRightNeckVet;

    @FindBy(id="NewIdentifier_Identifier")
    public WebElement newIdentifierVet;

    @FindBy(id="NewIdentifier_IdentifierLocation")
    public WebElement newIdentifierLocationVet;

    @FindBy(id="NewIdentifier_IdentifierDate")
    public WebElement newIdentifierDateVet;

    //For breeder and refuge
    @FindBy(xpath="//*[@id=\"AllInformationDiv\"]/div[3]/div/div[1]/div/div[1]/div/h3")
    public WebElement catIdentifierBreederOrRefuge;

    @FindBy(xpath="//*[@id=\"AllInformationDiv\"]/div[3]/div/div[1]/div/div[2]/div/h3")
    public WebElement catPassportNumberBreederOrRefuge;

    @FindBy(xpath="//*[@id=\"AllInformationDiv\"]/div[3]/div/div[1]/div/div[3]/div/h3")
    public WebElement catIdentificationDateBreederOrRefuge;

    @FindBy(xpath="//*[@id=\"AllInformationDiv\"]/div[3]/div/div[1]/div/div[4]/div/h3")
    public WebElement catBirthDateBreederOrRefuge;

    @FindBy(xpath="//*[@id=\"AllInformationDiv\"]/div[3]/div/div[2]/div/div[1]/div/h3")
    public WebElement catGenderBreederOrRefuge;

    @FindBy(xpath="//*[@id=\"AllInformationDiv\"]/div[3]/div/div[2]/div/div[2]/div/h3")
    public WebElement catBreedBreederOrRefuge;

    @FindBy(xpath="//*[@id=\"AllInformationDiv\"]/div[3]/div/div[2]/div/div[3]/div/h3")
    public WebElement catCrossingBreederOrRefuge;

    @FindBy(xpath="//*[@id=\"AllInformationDiv\"]/div[3]/div/div[2]/div/div[4]/div/h3")
    public WebElement catSterelizedDateBreederOrRefuge;

    @FindBy(xpath="//*[@id=\"AllInformationDiv\"]/div[3]/div/div[2]/div/div[5]/div/h3")
    public WebElement catSterelizedFlagBreederOrRefuge;

    //Cat information
    @FindBy(xpath="//*[@id=\"AllInformationDiv\"]/div[4]/div/div/div/div[3]/div")
    public WebElement catBreederOrRefugeStatusContainer;

    @FindBy(xpath="//*[@id=\"AllInformationDiv\"]/div[2]/div/div[2]/div/div[3]/div")
    public WebElement catVetStatusContainer;

    @FindBy(xpath="//*[@id=\"AllInformationDiv\"]/div[4]/div/div/div/div[3]/div/div[2]/div[1]")
    public WebElement catStatusNoneBreederAndrefuge;

    @FindBy(xpath="//*[@id=\"AllInformationDiv\"]/div[4]/div/div/div/div[3]/div/div[2]/div[1]")
    public WebElement catStatusLostBreederAndrefuge;

    @FindBy(xpath="//*[@id=\"AllInformationDiv\"]/div[4]/div/div/div/div[3]/div/div[2]/div[1]")
    public WebElement catStatusStolenBreederAndrefuge;

    @FindBy(xpath="//*[@id=\"AllInformationDiv\"]/div[4]/div/div/div/div[3]/div/div[2]/div[1]")
    public WebElement catStatusExportedBreederAndrefuge;

    @FindBy(xpath="//*[@id=\"AllInformationDiv\"]/div[4]/div/div/div/div[3]/div/div[2]/div[1]")
    public WebElement catStatusDeadBreederAndrefuge;

    @FindBy(xpath="//*[@id=\"AllInformationDiv\"]/div[2]/div/div[2]/div/div[3]/div/div[2]/div[1]")
    public WebElement catStatusNoneVet;

    @FindBy(xpath="//*[@id=\"AllInformationDiv\"]/div[2]/div/div[2]/div/div[3]/div/div[2]/div[2]")
    public WebElement catStatusLostVet;

    @FindBy(xpath="//*[@id=\"AllInformationDiv\"]/div[2]/div/div[2]/div/div[3]/div/div[2]/div[3]")
    public WebElement catStatusStolenVet;

    @FindBy(xpath="//*[@id=\"AllInformationDiv\"]/div[2]/div/div[2]/div/div[3]/div/div[2]/div[4]")
    public WebElement catStatusExportedVet;

    @FindBy(xpath="//*[@id=\"AllInformationDiv\"]/div[2]/div/div[2]/div/div[3]/div/div[2]/div[5]")
    public WebElement catStatusDeadVet;

    @FindBy(id="CatData_CatDateOfBirth")
    public WebElement catBirthDate;

    @FindBy(id="CatData_CatGender")
    public WebElement catGender;


    @FindBy(xpath="//*[@id=\"AllInformationDiv\"]/div[2]/div/div[1]/div/div[1]/div/div[2]")
    public WebElement catVetGenderHiddenElement;

    @FindBy(id="//*[@id=\"AllInformationDiv\"]/div[2]/div/div[1]/div/div[1]/div")
    public WebElement catGenderContainer;

    @FindBy(xpath="//*[@id=\"AllInformationDiv\"]/div[2]/div/div[1]/div/div[1]/div/div[2]/div[1]")
    public WebElement catVetGenderMale;

    @FindBy(xpath="//*[@id=\"AllInformationDiv\"]/div[2]/div/div[1]/div/div[1]/div/div[2]/div[2]")
    public WebElement catVetGenderFemale;

    @FindBy(id="CatData_SterilizedDate")
    public WebElement stereilizedDate;

    @FindBy(xpath="//*[@id=\"AllInformationDiv\"]/div[2]/div/div[1]/div/div[4]/div")
    public WebElement sterilizedCheckbox;

    @FindBy(xpath="//*[@id=\"AllInformationDiv\"]/div[2]/div/div[1]/div/div[5]/div")
    public WebElement crossingBreedCheckbox;

    @FindBy(id="CatData_CatBreed")
    public WebElement catBreed;

    @FindBy(xpath="//*[@id=\"BreedSearch\"]/div[2]/a/div/div")
    public WebElement catBreedToolTip;

    @FindBy(id="CatData_CatName")
    public WebElement catName;

    @FindBy(id="CatData_FurTypeAndColor")
    public WebElement catColor;

    @FindBy(id="CatData_CatStatus")
    public WebElement catStatus;

    @FindBy(id="CatData_StatusDate")
    public WebElement catStatusDate;

    @FindBy(xpath="//*[@id=\"CatDateOfBirth\"]/div/div/table/tbody")
    public WebElement birthDateBodyCalendar;

    @FindBy(xpath="//*[@id=\"StatusDate\"]/div/div/table/tbody")
    public WebElement statusDateBodyCalendar;

    @FindBy(xpath="//*[@id=\"IdentificationDate\"]/div/div/table/tbody")
    public WebElement identificationDateBodyCalendar;

    @FindBy(xpath="//*[@id=\"SterilizedDate\"]/div/div/table/tbody")
    public WebElement sterilizationDateBodyCalendar;

    @FindBy(xpath="//*[@id=\"CatDateOfBirth\"]/div/div/table/thead")
    public WebElement birthDateHeadCalendar;

    @FindBy(xpath="//*[@id=\"StatusDate\"]/div/div/table/thead")
    public WebElement statusDateHeadCalendar;

    @FindBy(xpath="//*[@id=\"IdentificationDate\"]/div/div/table/thead")
    public WebElement identificationDateHeadCalendar;

    @FindBy(xpath="//*[@id=\"SterilizedDate\"]/div/div/table/thead")
    public WebElement sterilizationDateHeadCalendar;

    //Responsible information
    @FindBy(xpath="//*[@id=\"AllInformationDiv\"]/div[1]/div/div/p[1]")
    public WebElement responsibleInformationBreederOrRefuge;

    @FindBy(xpath="//*[@id=\"AllInformationDiv\"]/div[1]/div/div/p[2]")
    public WebElement responsibleAddressBreederOrRefuge;

    @FindBy(id="FirstOwnerInformation_PersonData_FirstName")
    public WebElement responsibleFirstName;

    @FindBy(id="FirstOwnerInformation_PersonData_LastName")
    public WebElement responsibleLastName;

    @FindBy(id="FirstOwnerInformation_RefNum")
    public WebElement responsibleRefNumber;

    @FindBy(id="FirstOwnerInformation_PersonData_NationalNumberNumber")
    public WebElement responsibleNationalNumber;

    @FindBy(id="FirstOwnerInformation_AddressData_PostalCodeAndMunicipality")
    public WebElement responsibleCity;

    @FindBy(id="FirstOwnerInformation_AddressData_Street")
    public WebElement responsibleStreet;

    @FindBy(id="FirstOwnerInformation_AddressData_StreetNr")
    public WebElement responsibleStreetNumber;

    @FindBy(id="FirstOwnerInformation_PersonData_ContactData_PhoneNumber")
    public WebElement responsiblePhone;

    @FindBy(id="FirstOwnerInformation_PersonData_ContactData_MobileNumber")
    public WebElement responsibleMobile;

    @FindBy(id="FirstOwnerInformation_PersonData_ContactData_FaxNumber")
    public WebElement responsibleFax;

    @FindBy(id="FirstOwnerInformation_PersonData_ContactData_EmailAddress")
    public WebElement responsibleMail;

    @FindBy(xpath="//*[@id=\"SecondHKAutoCom\"]/div[2]/a/div/div")
    public WebElement responsibleHkSelectResponsibleToolTip;

    //New responsible information
    @FindBy(xpath="//*[@id=\"NewRespDiv\"]/div[1]")
    public WebElement changeResponsibleDropDown;

    @FindBy(xpath="//*[@id=\"NewRespDiv\"]/div[2]/div/div[2]/div/div[1]/div/input")
    public WebElement changeResponsibleHkCheckBox;

    @FindBy(xpath="//*[@id=\"NewRespDiv\"]/div[2]/div/div[2]/div/div[2]/div/input")
    public WebElement changeResponsiblePrivatePersonCheckBox;

    @FindBy(id="Agree")
    public WebElement changeResponsibleAgreeBreederRefugeCheckBox;

    @FindBy(id="SecondOwnerInformation_PersonData_NationalNumberNumber")
    public WebElement changeResponsibleNationalNumberField;

    @FindBy(id="RefNumSecond")
    public WebElement changeResponsibleHkRefNumSecondSearchField;

    @FindBy(xpath="//*[@id=\"SecondHKAutoCom\"]/div[2]/a/div/div")
    public WebElement changeResponsibleHkRefNumSecondSearchTooltip;

    @FindBy(xpath="//*[@id=\"NewRespDiv\"]/div[2]/div[1]/div[2]/h6")
    public WebElement changeResponsibleClearBreederOrRefugeButton;

    @FindBy(xpath="//*[@id=\"SecondConfidential\"]/div")
    public WebElement changeResponsibleConfidentialFlag;

    @FindBy(xpath="//*[@id=\"NewRespDiv\"]/div[2]/div/div[2]/h6")
    public WebElement changeResponsibleClearVetButton;

    @FindBy(id="2ndFetchedOwnerSearchRrnOrId")
    public WebElement changeResponsiblePrivaPersonSearchField;

    @FindBy(xpath="//*[@id=\"SecondRegularFetchDiv\"]/div/div[2]/div/button[1]")
    public WebElement changeResponsibleSearchButton;

    @FindBy(id="SecondFetchInformation")
    public WebElement changeResponsibleReadDataDromEidCardButton;

    @FindBy(id="SecondOwnerInformation_TransferDate")
    public WebElement changeResponsibleTransfertDate;

    @FindBy(id="SecondOwnerInformation_PersonData_FirstName")
    public WebElement changeResponsibleFirstName;

    @FindBy(id="SecondOwnerInformation_PersonData_LastName")
    public WebElement changeResponsibleLastName;

    @FindBy(id="SecondOwnerInformation_AddressData_PostalCodeAndMunicipality")
    public WebElement changeResponsibleCity;

    @FindBy(id="SecondOwnerInformation_AddressData_Street")
    public WebElement changeResponsibleStreet;

    @FindBy(id="SecondOwnerInformation_AddressData_StreetNr")
    public WebElement changeResponsibleStreetNumber;

    @FindBy(id="SecondOwnerInformation_PersonData_ContactData_PhoneNumber")
    public WebElement changeResponsiblePhone;

    @FindBy(id="SecondOwnerInformation_PersonData_ContactData_MobileNumber")
    public WebElement changeResponsibleMobile;

    @FindBy(id="SecondOwnerInformation_PersonData_ContactData_FaxNumber")
    public WebElement changeResponsibleFax;

    @FindBy(id="SecondOwnerInformation_PersonData_ContactData_EmailAddress")
    public WebElement changeResponsibleMail;

    //Submit
    @FindBy(xpath="//*[@id=\"AllInformationDiv\"]/div[5]/button[1]")
    public WebElement createCertificateVetButton;

    @FindBy(xpath="//*[@id=\"AllInformationDiv\"]/div[5]/button[2]")
    public WebElement submitVetButton;

    @FindBy(xpath="//*[@id=\"AllInformationDiv\"]/div[5]/button")
    public WebElement submitBreederAndRefugeButton;

    @FindBy(xpath="//*[@id=\"dialogConfirm\"]/div[3]/div[1]")
    public WebElement submitNoButton;

    @FindBy(xpath="//*[@id=\"dialogConfirm\"]/div[3]/div[2]")
    public WebElement submitYesOrSignLaterButton;

    @FindBy(xpath="//*[@id=\"dialogConfirm\"]/div[3]/div[3]")
    public WebElement submitSignNowButton;

    @FindBy(xpath="//*[@id=\"NoSearchResultFound\"]/div/div/div")
    public WebElement errorLabelNoSearchFound;

    //Error Message
    @FindBy(id="ErrorMsgContent")
    public WebElement errorMessagePage;

    //constructor expecting a driver as parameter
    public WebModifyPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }
}
