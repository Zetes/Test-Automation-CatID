package Screens;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


public class WebRegisterPage {
	//Error Message
	@FindBy(id="ErrorMsgContent")
	public WebElement errorMessagePage;

	//Responsible information
	@FindBy(xpath="//*[@id=\"OwnerDiv1\"]/div[2]/div/p")
	public WebElement firstResponsibleBreederOrRefugeInformation;

	@FindBy(id="pinOwner-1")
	public WebElement pinFirstOwner;

	@FindBy(id="pinOwner-2")
	public WebElement pinFSecondOwner;

	@FindBy(xpath="//*[@id=\"FirstConfidential\"]/div")
	public WebElement firstConfidentialCheckBox;

	@FindBy(xpath="//*[@id=\"SecondConfidential\"]/div")
	public WebElement secondConfidentialCheckBox;

	@FindBy(xpath="//*[@id=\"OwnerDiv1\"]/div[2]/h6[2]")
	public WebElement clearFirstOwner;

	@FindBy(xpath="//*[@id=\"OwnerDiv2\"]/div[2]/h6[2]")
	public WebElement clearSecondOwnerForVetOrBreeder;

	@FindBy(xpath="//*[@id=\"OwnerDiv2\"]/div[3]/h6[2]")
	public WebElement clearSecondOwnerForRefuge;

	@FindBy(xpath="//*[@id=\"FirstOwnerDetailsDiv\"]/div[1]/div/div[1]/label")
	public WebElement firstOwnerDetailsDivFirstName;

	@FindBy(xpath="//*[@id=\"FirstOwnerDetailsDiv\"]/div[1]/div/div[2]/label")
	public WebElement firstOwnerDetailsDivLastName;

	@FindBy(xpath="//*[@id=\"FirstOwnerDetailsDiv\"]/div[1]/div/div[3]/label")
	public WebElement firstOwnerDetailsDivRRN;

	@FindBy(xpath="//*[@id=\"searchAddressFirst\"]/label")
	public WebElement firstOwnerDetailsDivPostalAndMunicipality;

	@FindBy(xpath="//*[@id=\"searchStreetFirst\"]/label")
	public WebElement firstOwnerDetailsDivStreet;

	@FindBy(xpath="//*[@id=\"FirstOwnerDetailsDiv\"]/div[2]/div/div[3]/label")
	public WebElement firstOwnerDetailsDivStreetNumber;

	@FindBy(xpath="//*[@id=\"FirstOwnerDetailsDiv\"]/div[3]/div/div[1]/label")
	public WebElement firstOwnerDetailsDivPhone;

	@FindBy(xpath="//*[@id=\"FirstOwnerDetailsDiv\"]/div[3]/div/div[2]/label")
	public WebElement firstOwnerDetailsDivMobile;

	@FindBy(xpath="//*[@id=\"FirstOwnerDetailsDiv\"]/div[3]/div/div[3]/label")
	public WebElement firstOwnerDetailsDivFax;

	@FindBy(xpath="//*[@id=\"FirstHkAutoCom\"]/div[2]/a/div/div")
	public WebElement firstOwnerDetailsDivEmail;

	@FindBy(xpath="//*[@id=\"FirstHkAutoCom\"]/div[2]/a/div/div")
	public WebElement selectFisrtResponsibleToolTip;

	@FindBy(xpath="//*[@id=\"SecondHKAutoCom\"]/div[2]/a/div/div")
	public WebElement selectSecondResponsibleToolTip;

	@FindBy(id="SelectedOwner-1-1")
	public WebElement checkBoxHkFirstOwner;

	@FindBy(id="SelectedOwner-1-2")
	public WebElement checkBoxPrivatePersonFirstOwner;

	@FindBy(id="FirstFetchInformation")
	public WebElement readDataFromFirstOwnerEiDCardButton;

	@FindBy(id="SecondFetchInformation")
	public WebElement readDataFromSecondOwnerEiDCardButton;

	@FindBy(id="RefNumFirst")
	public WebElement refNumberTextFieldFirstOwner;

	@FindBy(id="FirstOwnerInformation_PersonData_FirstName")
	public WebElement firstNameTextFieldFirstOwner;

	@FindBy(id="FirstOwnerInformation_PersonData_LastName")
	public WebElement lastNameTextFieldFirstOwner;

	@FindBy(id="FirstOwnerInformation_PersonData_NationalNumberNumber")
	public WebElement nationalNumberTextFieldFirstOwner;

	@FindBy(id="FirstOwnerInformation_AddressData_PostalCodeAndMunicipality")
	public WebElement postalCodeAndMunicipalityTextFieldFirstOwner;

	@FindBy(id="FirstOwnerInformation_AddressData_Street")
	public WebElement streetTextFieldFirstOwner;

	@FindBy(id="FirstOwnerInformation_AddressData_StreetNr")
	public WebElement streetNumberTextFieldFirstOwner;

	@FindBy(id="FirstOwnerInformation_PersonData_ContactData_PhoneNumber")
	public WebElement phoneTextFieldFirstOwner;

	@FindBy(id="FirstOwnerInformation_PersonData_ContactData_MobileNumber")
	public WebElement mobileTextFieldFirstOwner;

	@FindBy(id="FirstOwnerInformation_PersonData_ContactData_FaxNumber")
	public WebElement faxTextFieldFirstOwner;

	@FindBy(id="FirstOwnerInformation_PersonData_ContactData_EmailAddress")
	public WebElement emailTextFieldFirstOwner;

	@FindBy(id="accordionTitle")
	public WebElement transfertOfResponsibility;

	@FindBy(id="SelectedOwner-2-1")
	public WebElement checkBoxHkSecondOwner;

	@FindBy(id="SelectedOwner-2-2")
	public WebElement checkBoxPrivatePersonSecondOwner;

	@FindBy(id="RefNumSecond")
	public WebElement refNumberTextFieldSecondOwner;

	@FindBy(id="SecondOwnerInformation_PersonData_FirstName")
	public WebElement firstNameTextFieldSecondOwner;

	@FindBy(id="SecondOwnerInformation_PersonData_LastName")
	public WebElement lastNameTextFieldSecondOwner;

	@FindBy(id="SecondOwnerInformation_PersonData_NationalNumberNumber")
	public WebElement nationalNumberTextFieldSecondOwner;

	@FindBy(id="SecondOwnerInformation_AddressData_PostalCodeAndMunicipality")
	public WebElement postalCodeAndMunicipalityTextFieldSecondOwner;

	@FindBy(id="SecondOwnerInformation_AddressData_Street")
	public WebElement streetTextFieldSecondOwner;

	@FindBy(id="SecondOwnerInformation_AddressData_StreetNr")
	public WebElement streetNumberTextFieldSecondOwner;

	@FindBy(id="SecondOwnerInformation_PersonData_ContactData_PhoneNumber")
	public WebElement phoneTextFieldSecondOwner;

	@FindBy(id="SecondOwnerInformation_PersonData_ContactData_MobileNumber")
	public WebElement mobileTextFieldSecondOwner;

	@FindBy(id="SecondOwnerInformation_PersonData_ContactData_FaxNumber")
	public WebElement faxTextFieldSecondOwner;

	@FindBy(id="SecondOwnerInformation_TransferDate")
	public WebElement transfertdateSecondOwner;

	@FindBy(id="SecondOwnerInformation_PersonData_ContactData_EmailAddress")
	public WebElement emailTextFieldSecondOwner;

	@FindBy(xpath="//*[@id=\"FirstRegularFetchDiv\"]/div[1]/div[2]/div/button[1]")
	public WebElement searchFirstOwnerPrivatePersonButton;

	@FindBy(id="FetchedOwnerSearchRrnOrId")
	public WebElement searchFirstOwnerPrivatePersonField;

	@FindBy(xpath="//*[@id=\"SecondRegularFetchDiv\"]/div[1]/div[2]/div/button[1]")
	public WebElement searchSecondOwnerPrivatePersonButton;

	@FindBy(id="2ndFetchedOwnerSearchRrnOrId")
	public WebElement searchSecondOwnerPrivatePersonField;

	@FindBy(id="SecondOwnerInformation_TransferDate")
	public WebElement transfertDateField;

	//eID registration for Private person
	@FindBy(id="user_consent_CB")
	public WebElement checkBoxEidConsent;

	@FindBy(id="user_consent_agree_button")
	public WebElement agreeEidButton;

	@FindBy(id="user_consent_deny_button")
	public WebElement denyEidButton;

	//Cat Identification
	@FindBy(id="FirstIdentifier_Identifier")
	public WebElement firstIdentifier;

	@FindBy(id="SecondIdentifier_Identifier")
	public WebElement secondIdentifier;

	@FindBy(id="FirstIdentifier_IdentifierLocation")
	public WebElement firstIdentifierLocation;

	@FindBy(id="SecondIdentifier_IdentifierLocation")
	public WebElement secondIdentifierLocation;

	@FindBy(xpath="//*[@id=\"RegisterForm\"]/div[4]/div[1]/div[2]/div[1]")
	public WebElement addIdentifierVet;

	@FindBy(xpath="//*[@id=\"RegisterForm\"]/div[5]/div[1]/div[2]/div[1]")
	public WebElement addIdentifierOther;

	@FindBy(id="PassportData_PassportNumber")
	public WebElement passportNumber;
	//Veterinary Information

	@FindBy(id="VetInformation_OtherVet")
	public WebElement otherVeterinaryTextField;

	@FindBy(xpath="//*[@id=\"RegisterForm\"]/div[4]/div[1]/div/div[2]/div/label")
	public WebElement otherVetCheckbox;

	@FindBy(xpath="//*[@id=\"searchVet\"]/div[2]/a/div/div")
	public WebElement otherVetTooltip;

	@FindBy(xpath="//*[@id=\"VetApproved\"]/div")
	public WebElement myContactVeterinaryDropList;

	@FindBy(id="VetInformation_SelectedVetType")
	public WebElement myContactVetCheckbox;

	@FindBy(xpath="//*[@id=\"VetApproved\"]/div/p")
	public WebElement informationTextLabel;

	//Cat Information
	@FindBy(id="CatData_CatGender")
	public WebElement gender;

	@FindBy(id="CatData_CatDateOfBirth")
	public WebElement birthDate;

	@FindBy(id="CatData_IdentificationDate")
	public WebElement identificationDate;

	@FindBy(id="CatData_SterilizedDate")
	public WebElement sterilizationDate;

	@FindBy(xpath="//*[@id=\"RegisterForm\"]/div[5]/div/div[1]/div/div[5]/div")
	public WebElement sterilizationVetSlideButton;

	@FindBy(xpath="//*[@id=\"RegisterForm\"]/div[6]/div/div[1]/div/div[5]/div")
	public WebElement sterilizationOtherSlideButton;

	@FindBy(id="PinDob")
	public WebElement pinBirthDate;

	@FindBy(id="PinDID")
	public WebElement pinIdentificationDate;

	@FindBy(id="PinSterDate")
	public WebElement pinSterilizationDateDate;

	@FindBy(id="CatData_FurTypeAndColor")
	public WebElement colorCatTextField;

	@FindBy(id="CatData_CatName")
	public WebElement nameCatTextField;

	@FindBy(id="CatData_CatBreed")
	public WebElement breedCatTextField;

	@FindBy(id="PinFur")
	public WebElement pinCatColor;

	@FindBy(id="PinBreed")
	public WebElement pinCatBreed;

	@FindBy(xpath="//*[@id=\"BreedSearch\"]/div[2]/a/div/div")
	public WebElement selectBreedTooltip;

	@FindBy(xpath="//*[@id=\"CatDateOfBirth\"]/div/div/table/tbody")
	public WebElement birthDateBodyCalendar;

	@FindBy(xpath="//*[@id=\"IdentificationDate\"]/div/div/table/tbody")
	public WebElement identificationDateBodyCalendar;

	@FindBy(xpath="//*[@id=\"SterilizedDate\"]/div/div/table/tbody")
	public WebElement sterilizationDateBodyCalendar;

	@FindBy(xpath="//*[@id=\"CatDateOfBirth\"]/div/div/table/thead")
	public WebElement birthDateHeadCalendar;

	@FindBy(xpath="//*[@id=\"IdentificationDate\"]/div/div/table/thead")
	public WebElement identificationDateHeadCalendar;

	@FindBy(xpath="//*[@id=\"SterilizedDate\"]/div/div/table/thead")
	public WebElement sterilizationDateHeadCalendar;
	//Submit menu
	@FindBy(xpath="//*[@id=\"RegisterForm\"]/div[6]/button")
	public WebElement submitButtonVet;

	@FindBy(xpath="//*[@id=\"RegisterForm\"]/div[7]/button")
	public WebElement submitButtonOther;

	@FindBy(xpath="//*[@id=\"dialogConfirm\"]/div[3]/div[1]")
	public WebElement signNoButton;

	@FindBy(xpath="//*[@id=\"dialogConfirm\"]/div[3]/div[2]")
	public WebElement signVetLaterOrAccrPendingButton;

	@FindBy(xpath="//*[@id=\"dialogConfirm\"]/div[3]/div[3]")
	public WebElement signVetNowOrAccrSendVetButton;

	//constructor expecting a driver as parameter
	public WebRegisterPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
}
