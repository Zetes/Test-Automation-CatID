package Screens;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class WebSentForApprovalPage extends WebFilterPage{

    @FindBy(xpath="//*[@id=\"searchTable_wrapper\"]/div/div[1]/div[1]/div/button[1]")
    public WebElement selectAllButton;

    @FindBy(xpath="//*[@id=\"searchTable_wrapper\"]/div/div[1]/div[1]/div/button[2]")
    public WebElement unselectAllButton;

    @FindBy(xpath="//*[@id=\"searchTable_wrapper\"]/div/div[1]/div[1]/div/button[3]")
    public WebElement editButton;

    @FindBy(xpath="//*[@id=\"searchTable_wrapper\"]/div/div[1]/div[1]/div/button[4]")
    public WebElement deleteButton;

    @FindBy(xpath="//*[@id=\"searchTable_wrapper\"]/div/div[1]/div[1]/div/button[5]")
    public WebElement createCsvButton;

    @FindBy(xpath="//*[@id=\"searchTable\"]/tbody")
    public WebElement tableResult;

    @FindBy(xpath="//*[@id=\"searchTable\"]/tbody/tr/td[1]")
    public WebElement firstLineTableResult;

    @FindBy(xpath="//*[@id=\"searchTable\"]/tbody/tr/td[1]")
    public WebElement firstIdentifierTableResult;

    @FindBy(xpath="//*[@id=\"dialogConfirm\"]/div[2]/div[2]")
    public WebElement acceptDeletionButton;

    @FindBy(xpath="//*[@id=\"dialogConfirm\"]/div[2]/div[1]")
    public WebElement refuseDeletionButton;

    //constructor expecting a driver as parameter
    public WebSentForApprovalPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }
}
