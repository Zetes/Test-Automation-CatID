package Screens;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


public class WebStartPage {
    @FindBy(xpath="/html/body/div[3]/navbar/div[1]/div[1]/a/img")
    public WebElement homeButton;

    @FindBy(xpath="/html/body/div[3]/navbar/div[2]/div[1]/div/a[1]")
    public WebElement registerTabButton;

    @FindBy(xpath="/html/body/div[3]/navbar/div[2]/div[1]/div/a[2]")
    public WebElement modifyTabButton;

    @FindBy(xpath="/html/body/div[3]/navbar/div[2]/div[1]/div/a[3]")
    public WebElement intakeRefugeTabButton;

    @FindBy(xpath="/html/body/div[3]/navbar/div[2]/div[1]/div/a[4]")
    public WebElement tokenreportVetAndBreederTabButton;

    @FindBy(xpath="/html/body/div[3]/navbar/div[2]/div[1]/div/a[5]")
    public WebElement tokenreportRefugeTabButton;

    @FindBy(xpath="/html/body/div[3]/navbar/div[2]/div[1]/div/a[3]")
    public WebElement bulkTabVetAndBreederButton;

    @FindBy(xpath="/html/body/div[3]/navbar/div[2]/div[1]/div/a[4]")
    public WebElement bulkTabRefugeButton;

    @FindBy(xpath="/html/body/div[3]/navbar/div[2]/div[1]/div/a[5]")
    public WebElement webshopVetAndBreederButton;

    @FindBy(xpath="/html/body/div[3]/navbar/div[2]/div[1]/div/a[6]")
    public WebElement webshopRefugeButton;

    @FindBy(xpath="/html/body/div[3]/navbar/div[2]/div[2]/div/a[1]")
    public WebElement accountSettings;

    @FindBy(xpath="/html/body/div[2]/div/div/span")
    public WebElement quota;

    @FindBy(xpath="//*[@id=\"MainContainer\"]/div[3]/h1")
    public WebElement username;

    //FilterPage For Vet

    @FindBy(xpath="/html/body/div[3]/navbar/div[2]/div[1]/div/div/div/a[1]")
    public WebElement filterIncompleteVetTabButton;

    @FindBy(xpath="//*[@id=\"MainContainer\"]/div[3]/div/div/div[1]/div[1]")
    public WebElement filterIncompleteVetActionButton;

    @FindBy(xpath="/html/body/div[3]/navbar/div[2]/div[1]/div/div/div/a[2]")
    public WebElement filterWaitingForApprovalVetTabButton;

    @FindBy(xpath="//*[@id=\"MainContainer\"]/div[3]/div/div/div[2]/div[1]")
    public WebElement filterWaitingForApprovalVetActionButton;

    @FindBy(xpath="/html/body/div[3]/navbar/div[2]/div[1]/div/div/div/a[3]")
    public WebElement filterRejectedVetTabButton;

    @FindBy(xpath="//*[@id=\"MainContainer\"]/div[3]/div/div/div[3]/div[1]")
    public WebElement filterRejectedVetActionButton;

    @FindBy(xpath="/html/body/div[3]/navbar/div[2]/div[1]/div/div/div/a[4]")
    public WebElement filterApprovedVetTabButton;

    @FindBy(xpath="//*[@id=\"MainContainer\"]/div[3]/div/div/div[4]/div[1]")
    public WebElement filterApprovedVetActionButton;

    //FilterPage For Breeder and Refuge
    @FindBy(xpath="/html/body/div[3]/navbar/div[2]/div[1]/div/div/div/a[1]")
    public WebElement filterPendingBreederAndRefugeTabButton;

    @FindBy(xpath="//*[@id=\"MainContainer\"]/div[3]/div/div/div[1]/div[1]")
    public WebElement filterPendingBreederAndRefugeActionButton;

    @FindBy(xpath="/html/body/div[3]/navbar/div[2]/div[1]/div/div/div/a[2]")
    public WebElement filterWaitingForApprovalBreederAndRefugeTabButton;

    @FindBy(xpath="//*[@id=\"MainContainer\"]/div[3]/div/div/div[3]/div[1]")
    public WebElement filterWaitingForApprovalBreederAndRefugeActionButton;

    @FindBy(xpath="/html/body/div[3]/navbar/div[2]/div[1]/div/div/div/a[3]")
    public WebElement filterSentForApprovalBreederAndRefugeTabButton;

    @FindBy(xpath="//*[@id=\"MainContainer\"]/div[3]/div/div/div[2]/div[1]")
    public WebElement filterSentForApprovalBreederAndRefugeActionButton;

    @FindBy(xpath="/html/body/div[3]/navbar/div[2]/div[1]/div/div/div/a[4]")
    public WebElement filterRejectsBreederAndRefugeTabButton;

    @FindBy(xpath="//*[@id=\"MainContainer\"]/div[3]/div/div/div[4]/div[1]")
    public WebElement filterRejectsBreederAndRefugeActionButton;

    @FindBy(xpath="/html/body/div[3]/navbar/div[2]/div[1]/div/div/div/a[5]")
    public WebElement filterApprovedBreederAndRefugeTabButton;

    @FindBy(xpath="//*[@id=\"MainContainer\"]/div[3]/div/div/div[5]/div[1]")
    public WebElement filterApprovedBreederAndRefugeActionButton;

    @FindBy(xpath="/html/body/div[3]/navbar/div[2]/div[1]/div/div")
    public WebElement filterButton;

    @FindBy(xpath="/html/body/div[3]/navbar/div[2]/div[2]/div/a[2]")
    public WebElement logoutButton;


    //constructor expecting a driver as parameter
	public WebStartPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
}
