package Screens;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


public class WebWaitingForApprovalPage extends WebFilterPage{

    @FindBy(xpath="//*[@id=\"searchTable_wrapper\"]/div/div[1]/div[1]/div/button[1]")
    public WebElement selectAllButton;

    @FindBy(xpath="//*[@id=\"searchTable_wrapper\"]/div/div[1]/div[1]/div/button[2]")
    public WebElement unselectAllButton;

    @FindBy(xpath="//*[@id=\"searchTable_wrapper\"]/div/div[1]/div[1]/div/button[3]")
    public WebElement reviewButton;

    @FindBy(xpath="//*[@id=\"searchTable_wrapper\"]/div/div[1]/div[1]/div/button[4]")
    public WebElement deleteButton;

    @FindBy(xpath="//*[@id=\"dialogConfirm\"]/div[2]/div[2]")
    public WebElement acceptDeletionButton;

    @FindBy(xpath="//*[@id=\"dialogConfirm\"]/div[2]/div[1]")
    public WebElement refuseDeletionButton;

    @FindBy(xpath="//*[@id=\"searchTable_wrapper\"]/div/div[1]/div[1]/div/button[5]")
    public WebElement massApproveVetButton;

    @FindBy(xpath="//*[@id=\"searchTable_wrapper\"]/div/div[1]/div[1]/div/button[4]")
    public WebElement massApproveBreederOrRefugeButton;

    @FindBy(xpath="//*[@id=\"searchTable_wrapper\"]/div/div[1]/div[1]/div/button[6]")
    public WebElement massRejectVetButton;

    @FindBy(xpath="//*[@id=\"searchTable_wrapper\"]/div/div[1]/div[1]/div/button[5]")
    public WebElement massRejectBreederOrRefugeButton;

    @FindBy(xpath="//*[@id=\"searchTable_wrapper\"]/div/div[1]/div[1]/div/button[7]")
    public WebElement createCsvVetButton;

    @FindBy(xpath="//*[@id=\"searchTable_wrapper\"]/div/div[1]/div[1]/div/button[6]")
    public WebElement createCsvBreederOrRefugeButton;

    @FindBy(id="rejectReasonTextArea")
    public WebElement rejectTextField;

    @FindBy(xpath="//*[@id=\"rejectModal\"]/div[3]/div[1]")
    public WebElement cancelRejectButton;

    @FindBy(xpath="//*[@id=\"rejectModal\"]/div[3]/div[2]")
    public WebElement acceptRejectButton;

    //constructor expecting a driver as parameter
    public WebWaitingForApprovalPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }
}
