import Csv.CsvHandler;
import Driver.Edge.EdgeDriver;
import ElementaryScripts.Web.*;
import Logger.ExtendReportLogger;
import Logger.LoggerInterface;
import com.relevantcodes.extentreports.LogStatus;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.edge.EdgeOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class TestScenarioSandBoxEdge {

    private static WebDriver driver;
    private static LoggerInterface logger;
    private static String CSV_REGISTRATION_COR_PATH_FILE = "src/test/java/Resources/ResponsibleList.csv";
    private static String CSV_REGISTRATION_COR_RESULT_PATH_FILE = "src/test/java/Results/CatResgistration";
    private static String CSV_REGISTRATION_BULK_PATH_FILE = "src/test/java/Resources/BulkResponsibleList.csv";
    private static String CSV_REGISTRATION_BULK_RESULT_PATH_FILE = "src/test/java/Results/BulkResgistration";

    @BeforeClass(alwaysRun = true)
    public void setUp() throws Exception {
        if (logger == null){
            logger = new ExtendReportLogger();
            String path = System.getProperty("user.dir")+"/src/test/java/Results/CatIdReport.html";
            logger.init(path,"CatId Test", "Basic CatId test");
        }

        //Unable to specify download location for Edge or IE ...
//        String downloadFilepath = System.getProperty("user.dir")+"/src/test/java/Results";

        EdgeOptions options = new EdgeOptions();
        DesiredCapabilities cap = DesiredCapabilities.edge();
        cap.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
        cap.setCapability(EdgeOptions.CAPABILITY, options);
        //Remote tests
//        driver = (new EdgeDriver(cap,"192.168.15.2")).getDriver();
        //Local tests
        driver = (new EdgeDriver(cap)).getDriver();
        driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
    }

    private void homepageScript() throws Exception {
        logger.StartNewTest("OpenHomepage", "OpenHomepage");
        OpenHomepage openHomepage = new OpenHomepage();

        openHomepage.setDriver(driver);
        openHomepage.setLogger(logger);
        openHomepage.run();
        logger.EndTest();
    }

    private void loginScript(String tcNumber, String role, String screenshotPath) throws Exception {
        loginScript(tcNumber, role, screenshotPath,false);
    }

    private void loginScript(String tcNumber, String role, String screenshotPath, boolean checkValidPin) throws Exception {
        logger.StartNewTest("Login " + tcNumber, "Login");
        Login loginPopup = new Login();
        loginPopup.setScreenshotFolderLocation(screenshotPath);
        loginPopup.setUserRole(role);
        loginPopup.setDriver(driver);
        loginPopup.setLogger(logger);
        loginPopup.setCheckValidPin(checkValidPin);
        loginPopup.run();
        loginPopup.restoreScreenshotFolderLocation();
        logger.EndTest();
    }

    private void catRegistrationFeatureListScript(String tcNumber, RegistrationPageCorTest script, String registrator, String firstResponsible, String newResponsible) throws Exception {


        logger.StartNewTest("RegistrationPage " + tcNumber, "RegistrationPage");
        String message = "Test " + tcNumber + " Registrator:" + registrator + "- First responsible:" + firstResponsible + "- New responsible:" + newResponsible + " COR rules: https://wiki.zetes.com/confluence/pages/viewpage.action?pageId=92549832";
        logger.log(LogStatus.INFO, "Test case: " + tcNumber );
        logger.log(LogStatus.INFO, message);
        script.setTcFeatureListNumber(tcNumber);
        script.setFirstRegistrator(registrator);
        script.setFirstResponsible(firstResponsible);
        script.setNewResponsible(newResponsible);
        script.setDriver(driver);
        script.setLogger(logger);
        script.run();

        logger.EndTest();
    }

    private void bulkCatRegistrationFeatureListScript(String tcNumber, BulkRegistrationTest script, String registrator, String firstResponsible, String newResponsible) throws Exception {

        logger.StartNewTest("BulkRegistrationPage " + tcNumber, "BulkRegistrationPage");
        String message = "Test " + tcNumber + " Registrator:" + registrator + "- First responsible:" + firstResponsible + "- New responsible:" + newResponsible + " COR rules: https://wiki.zetes.com/confluence/pages/viewpage.action?pageId=92549832";
        logger.log(LogStatus.INFO, "Test case: " + tcNumber );
        logger.log(LogStatus.INFO, message);
        script.setTcFeatureListNumber(tcNumber);
        script.setFirstRegistrator(registrator);
        script.setFirstResponsible(firstResponsible);
        script.setNewResponsible(newResponsible);
        script.setDriver(driver);
        script.setLogger(logger);
        script.run();

        logger.EndTest();
    }

    private void waitingForApprovalScript(String tcNumber, String identifier, String screenshotPath) throws Exception {
        logger.StartNewTest("WaitingForApproval " + tcNumber, "WaitingForApproval");
        WaitingForApprovalCatRegistration waiting = new WaitingForApprovalCatRegistration();
        waiting.setScreenshotFolderLocation(screenshotPath);
        waiting.setCatIdentifier(identifier);
        waiting.setDriver(driver);
        waiting.setLogger(logger);
        waiting.run();
        waiting.restoreScreenshotFolderLocation();
        logger.EndTest();
    }

    private void changeOfReponsibilityUsingModify(String tcNumber, String identifier, String registrator, String newResponsible, String screenshotPath) throws Exception {
        logger.StartNewTest("ChangeOfReponsibilityUsingModify " + tcNumber, "ChangeOfReponsibilityUsingModify");
        logger.log(LogStatus.INFO, "M-COR, using modify feature");
        ModifyPageCorTest modifyPage = new ModifyPageCorTest();
        modifyPage.setScreenshotFolderLocation(screenshotPath);
        modifyPage.setCatIdentifier(identifier);
        modifyPage.setFirstRegistrator(registrator);
        modifyPage.setNewResponsible(newResponsible);
        modifyPage.setDriver(driver);
        modifyPage.setLogger(logger);
        modifyPage.run();
        modifyPage.restoreScreenshotFolderLocation();
        logger.EndTest();
    }

    private void checkRegistrationPage( String registrator, String screenshotPath){
        logger.StartNewTest("checkRegistrationPage ", "checkRegistrationPage");
        RegistrationPageCheckFieldTest registrationPage = new RegistrationPageCheckFieldTest();
        registrationPage.setScreenshotFolderLocation(screenshotPath);
        registrationPage.setFirstRegistrator(registrator);
        try {
            registrationPage.run();
        } catch (Exception e) {
            logger.log(LogStatus.ERROR,e.getMessage());
        } finally {
            logger.EndTest();
            registrationPage.restoreScreenshotFolderLocation();
        }
    }

    private void checkModifyPage(String tcNumber, String registrator, String screenshotPath){
        logger.StartNewTest("checkModifyPage ", "checkModifyPage");
        ModifyPageCheckFieldTest modifyPage = new ModifyPageCheckFieldTest();
        modifyPage.setScreenshotFolderLocation(screenshotPath);
        modifyPage.setFirstRegistrator(registrator);
        modifyPage.setTcFeatureListNumber(tcNumber);
        try {
            modifyPage.run();
        } catch (Exception e) {
            logger.log(LogStatus.ERROR,e.getMessage());
        } finally {
            logger.EndTest();
            modifyPage.restoreScreenshotFolderLocation();
        }
    }

    private void checkIntakePage(String tcNumber, String registrator, String screenshotPath){
        logger.StartNewTest("checkIntakePage ", "checkIntakePage");
        IntakePage intakePage = new IntakePage();
        intakePage.setScreenshotFolderLocation(screenshotPath);
        intakePage.setFirstRegistrator(registrator);
        intakePage.setTcFeatureListNumber(tcNumber);
        try {
            intakePage.run();
        } catch (Exception e) {
            logger.log(LogStatus.ERROR,e.getMessage());
        } finally {
            logger.EndTest();
            intakePage.restoreScreenshotFolderLocation();
        }
    }

    private void checkApprovedFilterPage(String tcNumber, String registrator, String screenshotPath){
        logger.StartNewTest("checkApprovedFilterPage ", "checkApprovedFilterPage");
        FilterApprovedPage approvedFilter = new FilterApprovedPage();
        approvedFilter.setScreenshotFolderLocation(screenshotPath);
        approvedFilter.setFirstRegistrator(registrator);
        approvedFilter.setTcFeatureListNumber(tcNumber);
        try {
            approvedFilter.run();
        } catch (Exception e) {
            logger.log(LogStatus.ERROR,e.getMessage());
        } finally {
            logger.EndTest();
            approvedFilter.restoreScreenshotFolderLocation();
        }
    }

    private void checkRejectFilterPage(String tcNumber, String registrator, String screenshotPath){
        logger.StartNewTest("checkRejectFilterPage ", "checkRejectFilterPage");
        FilterRejectPage rejectFilter = new FilterRejectPage();
        rejectFilter.setScreenshotFolderLocation(screenshotPath);
        rejectFilter.setFirstRegistrator(registrator);
        rejectFilter.setTcFeatureListNumber(tcNumber);
        try {
            rejectFilter.run();
        } catch (Exception e) {
            logger.log(LogStatus.ERROR,e.getMessage());
        } finally {
            logger.EndTest();
            rejectFilter.restoreScreenshotFolderLocation();
        }
    }

    private void checkWaitingForApprovalFilterPage(String tcNumber, String registrator, String screenshotPath){
        logger.StartNewTest("checkWaitingForApprovalFilterPage ", "checkWaitingForApprovalFilterPage");
        FilterWaitingForApprovalPage waitingForApprovalFilter = new FilterWaitingForApprovalPage();
        waitingForApprovalFilter.setScreenshotFolderLocation(screenshotPath);
        waitingForApprovalFilter.setFirstRegistrator(registrator);
        waitingForApprovalFilter.setTcFeatureListNumber(tcNumber);
        try {
            waitingForApprovalFilter.run();
        } catch (Exception e) {
            logger.log(LogStatus.ERROR,e.getMessage());
        } finally {
            logger.EndTest();
            waitingForApprovalFilter.restoreScreenshotFolderLocation();
        }
    }

    private void generateRegistrations(String tcNumber, String registrator, String screenshotPath){
        logger.StartNewTest("generateRegistrations ", "generateRegistrations");
        GenerateRegistration generatePage = new GenerateRegistration();
        generatePage.setScreenshotFolderLocation(screenshotPath);
        generatePage.setFirstRegistrator(registrator);
        generatePage.setTcFeatureListNumber(tcNumber);
        try {
            generatePage.run();
        } catch (Exception e) {
            logger.log(LogStatus.ERROR,e.getMessage());
        } finally {
            logger.EndTest();
            generatePage.restoreScreenshotFolderLocation();
        }
    }

    private void biTest(String tcNumber, String registrator, String screenshotPath){
        logger.StartNewTest("biTest ", "biTest");
        BiEvent biTest = new BiEvent();
        biTest.setScreenshotFolderLocation(screenshotPath);
        biTest.setFirstRegistrator(registrator);
        biTest.setTcFeatureListNumber(tcNumber);
        try {
            biTest.run();
        } catch (Exception e) {
            logger.log(LogStatus.ERROR,e.getMessage());
        } finally {
            logger.EndTest();
            biTest.restoreScreenshotFolderLocation();
        }
    }

    private void checkIncompleteFilterPage(String tcNumber, String registrator, String screenshotPath){
        logger.StartNewTest("checkIncompleteFilterPage ", "checkIncompleteFilterPage");
        FilterIncompletePage incompleteFilter = new FilterIncompletePage();
        incompleteFilter.setScreenshotFolderLocation(screenshotPath);
        incompleteFilter.setFirstRegistrator(registrator);
        incompleteFilter.setTcFeatureListNumber(tcNumber);
        try {
            incompleteFilter.run();
        } catch (Exception e) {
            logger.log(LogStatus.ERROR,e.getMessage());
        } finally {
            logger.EndTest();
            incompleteFilter.restoreScreenshotFolderLocation();
        }
    }

    private void checkPendingFilterPage(String tcNumber, String registrator, String screenshotPath){
        logger.StartNewTest("checkPendingFilterPage ", "checkPendingFilterPage");
        FilterPendingPage pendingFilter = new FilterPendingPage();
        pendingFilter.setScreenshotFolderLocation(screenshotPath);
        pendingFilter.setFirstRegistrator(registrator);
        pendingFilter.setTcFeatureListNumber(tcNumber);
        try {
            pendingFilter.run();
        } catch (Exception e) {
            logger.log(LogStatus.ERROR,e.getMessage());
        } finally {
            logger.EndTest();
            pendingFilter.restoreScreenshotFolderLocation();
        }
    }

    private void checkSentForApprovalFilterPage(String tcNumber, String registrator, String screenshotPath){
        logger.StartNewTest("checkSentForApprovalFilterPage ", "checkSentForApprovalFilterPage");
        FilterSentForApprovalPage sentForApprovalFilter = new FilterSentForApprovalPage();
        sentForApprovalFilter.setScreenshotFolderLocation(screenshotPath);
        sentForApprovalFilter.setFirstRegistrator(registrator);
        sentForApprovalFilter.setTcFeatureListNumber(tcNumber);
        try {
            sentForApprovalFilter.run();
        } catch (Exception e) {
            logger.log(LogStatus.ERROR,e.getMessage());
        } finally {
            logger.EndTest();
            sentForApprovalFilter.restoreScreenshotFolderLocation();
        }
    }

//    @Test
//    public void GenerateRegisrationScript() throws Exception {
//        logger.StartNewScenario("GenerateRegisrationPage", "GenerateRegisrationPage");
//        String testCaseDateHour = new SimpleDateFormat("dd.MM.yyyy-HH.mm").format(new Date());
//        String screenshotPath = "src/test/java/Results/TC00_FilterApprovedVetCheck_"+testCaseDateHour;
//        //Open Home Page
//        homepageScript();
////        loginScript("TC00","Veterinary",screenshotPath);
//        generateRegistrations("TC00", "Veterinary", screenshotPath);
//        logger.EndScenario();
//    }

    private void bulkgeneration(String tcNumber, String registrator, String screenshotPath){
        logger.StartNewTest("bulkgeneration ", "bulkgeneration");
        BulkRegistrationTest bulkGeneration = new BulkRegistrationTest();
        bulkGeneration.setScreenshotFolderLocation(screenshotPath);
        bulkGeneration.setFirstRegistrator(registrator);
        bulkGeneration.setTcFeatureListNumber(tcNumber);
        try {
            bulkGeneration.generateCsvWithNRows(1000);
        } catch (Exception e) {
            logger.log(LogStatus.ERROR,e.getMessage());
        } finally {
            logger.EndTest();
            bulkGeneration.restoreScreenshotFolderLocation();
        }
    }

    @Test
    public void BulkGeneration() throws Exception {
        logger.StartNewScenario("BulkGeneration", "BulkGeneration");
        String testCaseDateHour = new SimpleDateFormat("dd.MM.yyyy-HH.mm").format(new Date());
        String screenshotPath = "src/test/java/Results/TCXXX_Bulk_generation_"+testCaseDateHour;
        //Open Home Page
        homepageScript();
        loginScript("TCXXX","Veterinary",screenshotPath);
        bulkgeneration("TCXXX","Veterinary",screenshotPath);
        logger.EndScenario();
    }

    @Test
    public void BiTestScript() throws Exception {
        logger.StartNewScenario("BiTestScript", "BiTestScript");
        String testCaseDateHour = new SimpleDateFormat("dd.MM.yyyy-HH.mm").format(new Date());
        String screenshotPath = "src/test/java/Results/TC162_BI_Test_"+testCaseDateHour;
        //Open Home Page
        homepageScript();
        loginScript("TC162","Veterinary",screenshotPath);
        biTest("TC162", "Veterinary", screenshotPath);
        logger.EndScenario();
    }
    @Test
    public void BulkTestScriptDataDriven() throws Exception {
        logger.StartNewScenario("BulkTestScriptDataDriven", "BulkTestScriptDataDriven");

        //Init parameters
        String mainVet = "Veterinary";
        String tcNumber;
        String registrator;
        String firstResponsible;
        String newResponsible;
        String mCor;
        String mCorRegistrator;
        String testCaseDateHour = new SimpleDateFormat("dd-MM-yyyy_HH-mm").format(new Date());
        String screenshotPath;
        //Open Home Page
        homepageScript();

        //Data driven
        BulkRegistrationTest script = new BulkRegistrationTest();
        CsvHandler csvWriteHandler = new CsvHandler(logger);
        csvWriteHandler.setCsvPath(CSV_REGISTRATION_BULK_RESULT_PATH_FILE + "_" + testCaseDateHour +".csv");
        CsvHandler csvReadHandler = new CsvHandler(logger);
        csvReadHandler.setCsvPath(CSV_REGISTRATION_BULK_PATH_FILE);
        List<String[]> entries = csvReadHandler.readAll();
        ArrayList<String> colunmList =  new ArrayList<String>();
        colunmList.add("Testcase");
        colunmList.add("Registration (Login as)");
        colunmList.add("First Responsible");
        colunmList.add("New Responsible");
        colunmList.add("Modification (Login as)");
        colunmList.add("M-COR (new responsible)");
        colunmList.add("Identifiers");
        colunmList.add("Registration test status");
        colunmList.add("Failure reason for registration");
        colunmList.add("Modify test status");
        colunmList.add("Failure reason for Modify");
        try {
            csvWriteHandler.writeNextRow(colunmList);
        } catch (Exception e){
            e.printStackTrace();
        }

        for (String[] entry : entries) {
            tcNumber = entry[0];
            registrator = entry[1];
            firstResponsible = entry[2];
            newResponsible = entry[3];
            mCorRegistrator = entry[4];
            mCor = entry[5];
            ArrayList<String> resultList =  new ArrayList<String>();
            resultList.add(tcNumber);
            resultList.add(registrator);
            resultList.add(firstResponsible);
            resultList.add(newResponsible);
            resultList.add(mCorRegistrator);
            resultList.add(mCor);
            String lastIdentifierRegistered;

            if(registrator.contains("Veterinary")){
                mainVet = registrator;
            }
            try {
                screenshotPath = "src/test/java/Results/" + tcNumber + "_BulkResgistration" + testCaseDateHour;
                script.setScreenshotFolderLocation(screenshotPath);
                //Login as "registrator"
                loginScript(tcNumber,registrator,screenshotPath);

                //Perform cat registration
                // /!\/!\/!\/!\/!\/!\ Make sure the first cat identifier is available in the DB   /!\/!\/!\/!\/!\/!\/!\/!\
                // The cat identifier available has to match with the last line of src/test/java/Resources/IdentifierList
                bulkCatRegistrationFeatureListScript(tcNumber, script, registrator,firstResponsible, newResponsible);
                lastIdentifierRegistered = "123456789" + script.getLastIdentifier();

//                //Approve a cat registration in case the registrator is not a veterinary
//                if(!registrator.contains("Veterinary")){
//                    loginScript(tcNumber,mainVet,screenshotPath);
//                    waitingForApprovalScript(tcNumber,lastIdentifierRegistered, screenshotPath);
//                }

                //Save registration result
                resultList.add(lastIdentifierRegistered);
                resultList.add("PASS");
                resultList.add("No failure");

                //Run m-COR if needed
//                if(!mCor.equals("N/A")){
//
//                    try{
//                        loginScript(tcNumber,mCorRegistrator,screenshotPath);
//                        changeOfReponsibilityUsingModify(tcNumber,lastIdentifierRegistered, mCorRegistrator, mCor, screenshotPath);
//                        resultList.add("PASS");
//                        resultList.add("No failure");
//                    } catch (Exception e){
//                        resultList.add("FAIL");
//                        resultList.add(e.getMessage().substring(0,300));
//                    }
//                }

                csvWriteHandler.writeNextRow(resultList);
            } catch (Exception e){
                lastIdentifierRegistered = "123456789" + script.getLastIdentifier();
                resultList.add(lastIdentifierRegistered);
                resultList.add("FAIL");
                resultList.add(e.getMessage().substring(0,300));
                csvWriteHandler.writeNextRow(resultList);
            }
        }
        script.restoreScreenshotFolderLocation();
        logger.EndScenario();
    }

    @Test
    public void CatRegistrationAndModifyDataDriven() throws Exception {
        logger.StartNewScenario("RegistrationPageCorDataDriven", "RegistrationPageCorDataDriven");

        //Init parameters
        String mainVet = "Veterinary";
        String tcNumber;
        String registrator;
        String firstResponsible;
        String newResponsible;
        String mCor;
        String mCorRegistrator;
        String ppCOr;
        String testCaseDateHour = new SimpleDateFormat("dd-MM-yyyy_HH-mm").format(new Date());
        String screenshotPath;
        //Open Home Page
        homepageScript();

        //Data driven

        RegistrationPageCorTest script = new RegistrationPageCorTest();
        CsvHandler csvWriteHandler = new CsvHandler(logger);
        csvWriteHandler.setCsvPath(CSV_REGISTRATION_COR_RESULT_PATH_FILE + "_" + testCaseDateHour +".csv");
        CsvHandler csvReadHandler = new CsvHandler(logger);
        csvReadHandler.setCsvPath(CSV_REGISTRATION_COR_PATH_FILE);
        List<String[]> entries = csvReadHandler.readAll();
        ArrayList<String> colunmList =  new ArrayList<String>();
        colunmList.add("Testcase");
        colunmList.add("Registrator for registration");
        colunmList.add("First Responsible");
        colunmList.add("New Responsible");
        colunmList.add("Registrator for Modify");
        colunmList.add("M-COR (Refuge and Breeder COR)");
        colunmList.add("PP-ACTIONS (Private Person COR, CoAC, Download Certificate)");
        colunmList.add("Identifiers");
        colunmList.add("Registration test status");
        colunmList.add("Failure reason for registration");
        colunmList.add("Modify test status");
        colunmList.add("Failure reason for Modify");
        try {
            csvWriteHandler.writeNextRow(colunmList);
        } catch (Exception e){
            e.printStackTrace();
        }
        for (String[] entry : entries) {
            tcNumber = entry[0];
            registrator = entry[1];
            firstResponsible = entry[2];
            newResponsible = entry[3];
            mCorRegistrator = entry[4];
            mCor = entry[5];
            ppCOr = entry[6];
            ArrayList<String> resultList =  new ArrayList<String>();
            resultList.add(tcNumber);
            resultList.add(registrator);
            resultList.add(firstResponsible);
            resultList.add(newResponsible);
            resultList.add(mCorRegistrator);
            resultList.add(mCor);
            resultList.add(ppCOr);
            String lastIdentifierRegistered;

            if(registrator.contains("Veterinary")){
                mainVet = registrator;
            }
            try {
                screenshotPath = "src/test/java/Results/" + tcNumber + "_CatResgistration" + testCaseDateHour;
                script.setScreenshotFolderLocation(screenshotPath);
                //Login as "registrator"
                loginScript(tcNumber,registrator,screenshotPath);

                //Perform cat registration
                // /!\/!\/!\/!\/!\/!\ Make sure the first cat identifier is available in the DB   /!\/!\/!\/!\/!\/!\/!\/!\
                // The cat identifier available has to match with the last line of src/test/java/Resources/IdentifierList
                catRegistrationFeatureListScript(tcNumber, script, registrator,firstResponsible, newResponsible);
                lastIdentifierRegistered = "123456789" + script.getLastIdentifier();

                //Approve a cat registration in case the registrator is not a veterinary
                if(!registrator.contains("Veterinary")){
                    loginScript(tcNumber,mainVet,screenshotPath);
                    waitingForApprovalScript(tcNumber,lastIdentifierRegistered, screenshotPath);
                }

                //Save registration result
                resultList.add(lastIdentifierRegistered);
                resultList.add("PASS");
                resultList.add("No failure");

                //Run m-COR if needed
                if(!mCor.equals("N/A")){

                    try{
                        loginScript(tcNumber,mCorRegistrator,screenshotPath);
                        changeOfReponsibilityUsingModify(tcNumber,lastIdentifierRegistered, mCorRegistrator, mCor, screenshotPath);
                        resultList.add("PASS");
                        resultList.add("No failure");
                    } catch (Exception e){
                        resultList.add("FAIL");
                        resultList.add(e.getMessage().substring(0,300));
                    }
                }

                csvWriteHandler.writeNextRow(resultList);
            } catch (Exception e){
                lastIdentifierRegistered = "123456789" + script.getLastIdentifier();
                resultList.add(lastIdentifierRegistered);
                resultList.add("FAIL");
                resultList.add(e.getMessage().substring(0,300));
                csvWriteHandler.writeNextRow(resultList);
            }
        }
        script.restoreScreenshotFolderLocation();
        logger.EndScenario();
    }

    @Test
    public void CatModifyVetCheckFields() throws Exception {
        logger.StartNewScenario("CatModifyVetCheckFields", "CatModifyVetCheckFields");
        String testCaseDateHour = new SimpleDateFormat("dd.MM.yyyy-HH.mm").format(new Date());
        String screenshotPath = "src/test/java/Results/TC21_CatModifyVetCheckFields_"+testCaseDateHour;

        //Open Home Page
        homepageScript();
        //Login as "registrator"
        loginScript("TC21","Veterinary",screenshotPath);
        checkModifyPage("TC21", "Veterinary", screenshotPath);

        logger.EndScenario();
    }

    @Test
    public void CatModifyBreederCheckFields() throws Exception {
        logger.StartNewScenario("CatModifyBreederCheckFields", "CatModifyBreederCheckFields");
        String testCaseDateHour = new SimpleDateFormat("dd.MM.yyyy-HH.mm").format(new Date());
        String screenshotPath = "src/test/java/Results/TC22_CatModifyBreederCheckFields_"+testCaseDateHour;

        //Open Home Page
        homepageScript();
        //Login as "registrator"
        loginScript("TC22","Breeder 1",screenshotPath);
        checkModifyPage("TC22", "Breeder 1", screenshotPath);

        logger.EndScenario();
    }

    @Test
    public void CatModifyRefugeCheckFields() throws Exception {
        logger.StartNewScenario("CatModifyRefugeCheckFields", "CatModifyRefugeCheckFields");
        String testCaseDateHour = new SimpleDateFormat("dd.MM.yyyy-HH.mm").format(new Date());
        String screenshotPath = "src/test/java/Results/TC23_CatModifyRefugeCheckFields_"+testCaseDateHour;

        //Open Home Page
        homepageScript();
        //Login as "registrator"
        loginScript("TC23","Refuge 1",screenshotPath);
        checkModifyPage("TC23", "Refuge 1", screenshotPath);

        logger.EndScenario();
    }

    @Test
    public void CatRegistrationVetCheckFields() throws Exception {
        logger.StartNewScenario("CatRegistrationVetCheckFields", "CatRegistrationVetCheckFields");
        String testCaseDateHour = new SimpleDateFormat("dd.MM.yyyy-HH.mm").format(new Date());
        String screenshotPath = "src/test/java/Results/TC18_CatRegistrationVetCheckFields_"+testCaseDateHour;

        //Open Home Page
        homepageScript();
        //Login as "registrator"
        loginScript("TC18","Veterinary",screenshotPath);
        checkRegistrationPage("Veterinary", screenshotPath);

        logger.EndScenario();
    }

    @Test
    public void CatRegistrationBreederCheckFields() throws Exception {
        logger.StartNewScenario("CatRegistrationBreederCheckFields", "CatRegistrationBreederCheckFields");
        String testCaseDateHour = new SimpleDateFormat("dd.MM.yyyy-HH.mm").format(new Date());
        String screenshotPath = "src/test/java/Results/TC19_CatRegistrationBreederCheckFields_"+testCaseDateHour;

        //Open Home Page
        homepageScript();

        loginScript("TC19","Breeder 1",screenshotPath);
        checkRegistrationPage("Breeder 1", screenshotPath);

        logger.EndScenario();
    }

    @Test
    public void CatRegistrationRefugeCheckFields() throws Exception {
        logger.StartNewScenario("CatRegistrationRefugeCheckFields", "CatRegistrationRefugeCheckFields");
        String testCaseDateHour = new SimpleDateFormat("dd.MM.yyyy-HH.mm").format(new Date());
        String screenshotPath = "src/test/java/Results/TC20_CatRegistrationRefugeCheckFields_"+testCaseDateHour;

        //Open Home Page
        homepageScript();

        loginScript("TC20","Refuge 1",screenshotPath);
        checkRegistrationPage("Refuge 1", screenshotPath);

        logger.EndScenario();
    }

    @Test
    public void CatIntakeCheckFields() throws Exception {
        logger.StartNewScenario("CatIntakeCheckFields", "CatIntakeCheckFields");
        String testCaseDateHour = new SimpleDateFormat("dd.MM.yyyy-HH.mm").format(new Date());
        String screenshotPath = "src/test/java/Results/TC24_CatIntakeCheckFields_"+testCaseDateHour;

        //Open Home Page
        homepageScript();
        loginScript("TC24","Refuge 1",screenshotPath);
        checkIntakePage("TC24", "Refuge 1", screenshotPath);

        logger.EndScenario();
    }

    @Test
    public void LoginVetCheck() throws Exception {
        logger.StartNewScenario("LoginVetCheck", "LoginVetCheck");
        String testCaseDateHour = new SimpleDateFormat("dd.MM.yyyy-HH.mm").format(new Date());
        String screenshotPath = "src/test/java/Results/TC25_LoginVetCheck_"+testCaseDateHour;
        //Open Home Page
        homepageScript();
        loginScript("TC25","Veterinary",screenshotPath, true);

        logger.EndScenario();
    }

    @Test
    public void LoginBreederCheck() throws Exception {
        logger.StartNewScenario("LoginBreederCheck", "LoginBreederCheck");
        String testCaseDateHour = new SimpleDateFormat("dd.MM.yyyy-HH.mm").format(new Date());
        String screenshotPath = "src/test/java/Results/TC26_LoginBreederCheck_"+testCaseDateHour;
        //Open Home Page
        homepageScript();
        loginScript("TC26","Breeder 1",screenshotPath, true);

        logger.EndScenario();
    }

    @Test
    public void LoginRefugeCheck() throws Exception {
        logger.StartNewScenario("LoginRefugeCheck", "LoginRefugeCheck");
        String testCaseDateHour = new SimpleDateFormat("dd.MM.yyyy-HH.mm").format(new Date());
        String screenshotPath = "src/test/java/Results/TC27_LoginRefugeCheck_"+testCaseDateHour;
        //Open Home Page
        homepageScript();
        loginScript("TC27","Refuge 1",screenshotPath, true);

        logger.EndScenario();
    }

    @Test
    public void FilterApprovedVetCheck() throws Exception {
        logger.StartNewScenario("FilterApprovedVetCheck", "FilterApprovedVetCheck");
        String testCaseDateHour = new SimpleDateFormat("dd.MM.yyyy-HH.mm").format(new Date());
        String screenshotPath = "src/test/java/Results/TC28_FilterApprovedVetCheck_"+testCaseDateHour;
        //Open Home Page
        homepageScript();
        loginScript("TC28","Veterinary 2",screenshotPath);
        checkApprovedFilterPage("TC28", "Veterinary 2", screenshotPath);
        logger.EndScenario();
    }

    @Test
    public void FilterApprovedRefugeCheck() throws Exception {
        logger.StartNewScenario("FilterApprovedRefugeCheck", "FilterApprovedRefugeCheck");
        String testCaseDateHour = new SimpleDateFormat("dd.MM.yyyy-HH.mm").format(new Date());
        String screenshotPath = "src/test/java/Results/TC29_FilterApprovedRefugeCheck_"+testCaseDateHour;
        //Open Home Page
        homepageScript();
        loginScript("TC29","Refuge 3",screenshotPath);
        checkApprovedFilterPage("TC29", "Refuge 3", screenshotPath);
        logger.EndScenario();
    }

    @Test
    public void FilterApprovedBreederCheck() throws Exception {
        logger.StartNewScenario("FilterApprovedBreederCheck", "FilterApprovedBreederCheck");
        String testCaseDateHour = new SimpleDateFormat("dd.MM.yyyy-HH.mm").format(new Date());
        String screenshotPath = "src/test/java/Results/TC30_FilterApprovedBreederCheck_"+testCaseDateHour;
        //Open Home Page
        homepageScript();
        loginScript("TC30","Breeder 4",screenshotPath);
        checkApprovedFilterPage("TC30", "Breeder 4", screenshotPath);
        logger.EndScenario();
    }

    @Test
    public void FilterRejectVetCheck() throws Exception {
        logger.StartNewScenario("FilterRejectVetCheck", "FilterRejectVetCheck");
        String testCaseDateHour = new SimpleDateFormat("dd.MM.yyyy-HH.mm").format(new Date());
        String screenshotPath = "src/test/java/Results/TC31_FilterRejectVetCheck_"+testCaseDateHour;
        //Open Home Page
        homepageScript();
        loginScript("TC31","Veterinary 2",screenshotPath);
        checkRejectFilterPage("TC31", "Veterinary 2", screenshotPath);
        logger.EndScenario();
    }

    @Test
    public void FilterRejectRefugeCheck() throws Exception {
        logger.StartNewScenario("FilterRejectRefugeCheck", "FilterRejectRefugeCheck");
        String testCaseDateHour = new SimpleDateFormat("dd.MM.yyyy-HH.mm").format(new Date());
        String screenshotPath = "src/test/java/Results/TC32_FilterRejectRefugeCheck_"+testCaseDateHour;
        //Open Home Page
        homepageScript();
        loginScript("TC32","Refuge 3",screenshotPath);
        checkRejectFilterPage("TC32", "Refuge 3", screenshotPath);
        logger.EndScenario();
    }

    @Test
    public void FilterRejectBreederCheck() throws Exception {
        logger.StartNewScenario("FilterRejectBreederCheck", "FilterRejectBreederCheck");
        String testCaseDateHour = new SimpleDateFormat("dd.MM.yyyy-HH.mm").format(new Date());
        String screenshotPath = "src/test/java/Results/TC33_FilterRejectBreederCheck_"+testCaseDateHour;
        //Open Home Page
        homepageScript();
        loginScript("TC33","Breeder 4",screenshotPath);
        checkRejectFilterPage("TC33", "Breeder 4", screenshotPath);
        logger.EndScenario();
    }

    @Test
    public void FilterWaitingForApprovalVetCheck() throws Exception {
        logger.StartNewScenario("FilterWaitingForApprovalVetCheck", "FilterWaitingForApprovalVetCheck");
        String testCaseDateHour = new SimpleDateFormat("dd.MM.yyyy-HH.mm").format(new Date());
        String screenshotPath = "src/test/java/Results/TC34_FilterWaitingForApprovalVetCheck_"+testCaseDateHour;
        //Open Home Page
        homepageScript();
        loginScript("TC34","Veterinary 2",screenshotPath);
        checkWaitingForApprovalFilterPage("TC34", "Veterinary 2", screenshotPath);
        logger.EndScenario();
    }

    @Test
    public void FilterWaitingForApprovalBreederCheck() throws Exception {
        logger.StartNewScenario("FilterWaitingForApprovalBreederCheck", "FilterWaitingForApprovalBreederCheck");
        String testCaseDateHour = new SimpleDateFormat("dd.MM.yyyy-HH.mm").format(new Date());
        String screenshotPath = "src/test/java/Results/TC35_FilterWaitingForApprovalBreederCheck_"+testCaseDateHour;
        //Open Home Page
        homepageScript();
        loginScript("TC35","Breeder 4",screenshotPath);
        checkWaitingForApprovalFilterPage("TC35", "Breeder 4", screenshotPath);
        logger.EndScenario();
    }

    @Test
    public void FilterWaitingForApprovalRefugeCheck() throws Exception {
        logger.StartNewScenario("FilterWaitingForApprovalRefugeCheck", "FilterWaitingForApprovalRefugeCheck");
        String testCaseDateHour = new SimpleDateFormat("dd.MM.yyyy-HH.mm").format(new Date());
        String screenshotPath = "src/test/java/Results/TC36_FilterWaitingForApprovalRefugeCheck_"+testCaseDateHour;
        //Open Home Page
        homepageScript();
        loginScript("TC36","Refuge 3",screenshotPath);
        checkWaitingForApprovalFilterPage("TC36", "Refuge 3", screenshotPath);
        logger.EndScenario();
    }

    @Test
    public void FilterIncompleteVetCheck() throws Exception {
        logger.StartNewScenario("FilterIncompleteVetCheck", "FilterIncompleteVetCheck");
        String testCaseDateHour = new SimpleDateFormat("dd.MM.yyyy-HH.mm").format(new Date());
        String screenshotPath = "src/test/java/Results/TC37_FilterIncompleteVetCheck_"+testCaseDateHour;
        //Open Home Page
        homepageScript();
        loginScript("TC37","Veterinary 2",screenshotPath);
        checkIncompleteFilterPage("TC37", "Veterinary 2", screenshotPath);
        logger.EndScenario();
    }

    @Test
    public void FilterPendingBreederCheck() throws Exception {
        logger.StartNewScenario("FilterPendingBreederCheck", "FilterPendingBreederCheck");
        String testCaseDateHour = new SimpleDateFormat("dd.MM.yyyy-HH.mm").format(new Date());
        String screenshotPath = "src/test/java/Results/TC38_FilterPendingBreederCheck_"+testCaseDateHour;
        //Open Home Page
        homepageScript();
        loginScript("TC38","Breeder 4",screenshotPath);
        checkPendingFilterPage("TC38", "Breeder 4", screenshotPath);
        logger.EndScenario();
    }

    @Test
    public void FilterPendingRefugeCheck() throws Exception {
        logger.StartNewScenario("FilterPendingRefugeCheck", "FilterPendingRefugeCheck");
        String testCaseDateHour = new SimpleDateFormat("dd.MM.yyyy-HH.mm").format(new Date());
        String screenshotPath = "src/test/java/Results/TC39_FilterPendingRefugeCheck_"+testCaseDateHour;
        //Open Home Page
        homepageScript();
        loginScript("TC39","Refuge 3",screenshotPath);
        checkPendingFilterPage("TC39", "Refuge 3", screenshotPath);
        logger.EndScenario();
    }

    @Test
    public void FilterSentForApprovalBreederCheck() throws Exception {
        logger.StartNewScenario("FilterSentForApprovalBreederCheck", "FilterSentForApprovalBreederCheck");
        String testCaseDateHour = new SimpleDateFormat("dd.MM.yyyy-HH.mm").format(new Date());
        String screenshotPath = "src/test/java/Results/TC40_FilterSentForApprovalBreederCheck_"+testCaseDateHour;
        //Open Home Page
        homepageScript();
        loginScript("TC40","Breeder 4",screenshotPath);
        checkSentForApprovalFilterPage("TC40", "Breeder 4", screenshotPath);
        logger.EndScenario();
    }

    @Test
    public void FilterSentForApprovalRefugeCheck() throws Exception {
        logger.StartNewScenario("FilterSentForApprovalRefugeCheck", "FilterSentForApprovalRefugeCheck");
        String testCaseDateHour = new SimpleDateFormat("dd.MM.yyyy-HH.mm").format(new Date());
        String screenshotPath = "src/test/java/Results/TC41_FilterSentForApprovalRefugeCheck_"+testCaseDateHour;
        //Open Home Page
        homepageScript();
        loginScript("TC41","Refuge 3",screenshotPath);
        checkSentForApprovalFilterPage("TC41", "Refuge 3", screenshotPath);
        logger.EndScenario();
    }

    @AfterClass(alwaysRun = true)
    public void tearDown() throws Exception {
        logger.Finalize();
        driver.quit();
    }
}
